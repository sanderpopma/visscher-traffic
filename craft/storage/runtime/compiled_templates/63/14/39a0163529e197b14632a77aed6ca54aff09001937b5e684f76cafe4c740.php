<?php

/* _elements/element */
class __TwigTemplate_631439a0163529e197b14632a77aed6ca54aff09001937b5e684f76cafe4c740 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo \Craft\craft()->templates->invokeHook("cp.elements.element", $context);

    }

    public function getTemplateName()
    {
        return "_elements/element";
    }

    public function getDebugInfo()
    {
        return array (  109 => 12,  95 => 11,  83 => 9,  79 => 7,  77 => 6,  71 => 5,  67 => 4,  49 => 3,  37 => 2,  61 => 11,  59 => 10,  55 => 8,  34 => 6,  29 => 5,  24 => 4,  19 => 1,);
    }
}

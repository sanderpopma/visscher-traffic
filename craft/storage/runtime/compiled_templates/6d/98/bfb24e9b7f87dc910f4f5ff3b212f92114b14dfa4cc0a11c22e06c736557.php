<?php

/* contact */
class __TwigTemplate_6d98bfb24e9b7f87dc910f4f5ff3b212f92114b14dfa4cc0a11c22e06c736557 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_layout");

        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_layout";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        $context["title"] = $this->getAttribute($_entry_, "title");
        // line 3
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        $context["metadescription"] = $this->getAttribute($_entry_, "metadescription");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_javascript($context, array $blocks = array())
    {
        // line 6
        echo "<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false\"></script>
<script src=\"";
        // line 7
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/maps.js\"></script>
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "\t 
\t<div id=\"slider-small\">
\t\t<div class=\"item\" style=\"background-image: url(";
        // line 13
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/images/slides/vervolgpagina.jpg); display: block; transition: opacity 1000ms ease-in-out; -webkit-transition: opacity 1000ms ease-in-out; \">
\t\t</div>
\t</div>
\t\t<div class=\"breadcrumb\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-6 col-xs-12 \">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"";
        // line 21
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "\"><i class=\"fa fa-home\"></i></a></li>
\t\t\t\t\t\t<li><i class=\"fa fa-angle-right\"></i></li>
\t\t\t\t\t\t<li>";
        // line 23
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "title"), "html", null, true);
        echo "</li>
\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"content\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 \">
\t\t\t\t\t<h1 class=\"gray\">Neem contact met ons op</h1>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 \">
\t\t\t\t\t<div class=\"naw\">
\t\t\t\t\t\t<h5>";
        // line 40
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "naam"), "html", null, true);
        echo "</h5>
                        <p>
                        <strong>Bezoekadres</strong>
                        <br />
                        ";
        // line 44
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "adres"), "html", null, true);
        echo "<br />
                        ";
        // line 45
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "postcode"), "html", null, true);
        echo " ";
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "woonplaats"), "html", null, true);
        echo "<br />
                        <br /> 
                        <strong>Postadres</strong>
                        <br />
                        ";
        // line 49
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "postadres"), "html", null, true);
        echo "<br />
                        ";
        // line 50
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "postadresPostcode"), "html", null, true);
        echo " ";
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "postadresWoonplaats"), "html", null, true);
        echo "<br/>
                        <br />
                        ";
        // line 52
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "telefoonnummer"), "html", null, true);
        echo "<br />
                        E-mail: ";
        // line 53
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "email"), "html", null, true);
        echo "</p>
\t\t\t\t\t</div>
\t\t\t\t\t<p>Volg ons op: <a href=\"http://www.facebook.com/visschertraffic\"><i class=\"fa fa-facebook-square fa-2x gray\"></i></a>
\t\t\t\t\t<a href=\"http://www.linkedin.com/visschertraffic\"><i class=\"fa fa-linkedin-square fa-2x gray\"></i></a>
\t\t\t\t\t</p> 
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 \">
\t\t\t\t\t<div id=\"map-canvas\"></div> 
\t\t\t\t</div>
 \t\t\t</div>
 \t\t\t
\t\t</div>
\t</div>
 \t";
        // line 73
        echo "    
    ";
        // line 74
        $context["__internal_2bca64d51521c67a4f73251b295c0e678dd160e8818b22482d7f6ffc69b5b410"] = $this;
        // line 75
        echo "    <div class=\"bottom-contact\">
        <div class=\"container\">
            <div class=\"row\">
                <form method=\"post\" action=\"\" accept-charset=\"UTF-8\">\t\t\t\t\t
                        <input type=\"hidden\" name=\"action\" value=\"contactForm/sendMessage\" />
                                <input type=\"hidden\" name=\"fromName\" value=\"Visschertraffic\" />
                                <input type=\"hidden\" name=\"fromEmail\" value=\"info@spm-design.nl\" />
                                <input type=\"hidden\" name=\"redirect\" value=\"bedankt\">
                                
                                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">
                          <div class=\"form-group\">
                            <label for=\"naam\">Uw naam*:</label>
                            <input type=\"text\" class=\"form-control\" name=\"message[Naam]\"  required />
                          </div>
                          <div class=\"form-group\">
                            <label for=\"bedrijfsnaam\">Bedrijfsnaam (optioneel):</label>
                            <input type=\"text\" class=\"form-control\" name=\"message[Bedrijfsnaam]\"   />
                            </div>
                         <div class=\"form-group\">
                            <label for=\"emailadres\">Emailadres*:</label>
                            <input type=\"email\" class=\"form-control\" name=\"message[Email]\"  required />
                          </div>
                         </div>
                            <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\">
                                 <div class=\"form-group\">
                                     <label for=\"bericht\">Bericht*:</label>
                                 <textarea class=\"form-control\" rows=\"5\" name=\"message[body]\" required></textarea>
                                 
                             </div>
                             <p>(*) Verplichte velden </p>
                             <div class=\"form-group\">
                                <button type=\"submit\" class=\"btn btn-primary btn-sm\" value=\"Send\">Verzend</button>
                             </div>
                        </div>
                        
                        </form>
            </div>
        </div>
    </div>
";
    }

    // line 66
    public function geterrorList($_errors = null)
    {
        $context = $this->env->mergeGlobals(array(
            "errors" => $_errors,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 67
            echo "        ";
            if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
            if ($_errors_) {
                // line 68
                echo "                 ";
                if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($_errors_);
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 69
                    echo "                  <div class=\"alert alert-danger\">";
                    if (isset($context["error"])) { $_error_ = $context["error"]; } else { $_error_ = null; }
                    echo twig_escape_filter($this->env, $_error_, "html", null, true);
                    echo "</div>  
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 71
                echo "         ";
            }
            // line 72
            echo "    ";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "contact";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 72,  233 => 71,  223 => 69,  217 => 68,  213 => 67,  202 => 66,  159 => 75,  157 => 74,  154 => 73,  137 => 53,  132 => 52,  123 => 50,  118 => 49,  107 => 45,  102 => 44,  94 => 40,  73 => 23,  67 => 21,  55 => 13,  51 => 11,  48 => 10,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  26 => 2,);
    }
}

<?php

/* assets/_missing_items */
class __TwigTemplate_61262ce3898eadae40539e234287da5b9bdb4624aaf4fbeb469abf53a09d025f extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["missingFolders"])) { $_missingFolders_ = $context["missingFolders"]; } else { $_missingFolders_ = null; }
        if (isset($context["missingFiles"])) { $_missingFiles_ = $context["missingFiles"]; } else { $_missingFiles_ = null; }
        if (($_missingFolders_ && $_missingFiles_)) {
            // line 2
            echo "\t";
            $context["items"] = \Craft\Craft::t("folders and files");
        } elseif ($_missingFolders_) {
            // line 4
            echo "\t";
            $context["items"] = \Craft\Craft::t("folders");
        } else {
            // line 6
            echo "\t";
            $context["items"] = \Craft\Craft::t("files");
        }
        // line 8
        echo "
<h2>";
        // line 9
        if (isset($context["items"])) { $_items_ = $context["items"]; } else { $_items_ = null; }
        echo twig_escape_filter($this->env, \Craft\Craft::t("Missing {items}", array("items" => $_items_)), "html", null, true);
        echo "</h2>
<p>";
        // line 10
        if (isset($context["items"])) { $_items_ = $context["items"]; } else { $_items_ = null; }
        echo twig_escape_filter($this->env, \Craft\Craft::t("The following {items} could not be found. Should they be deleted from the index?", array("items" => $_items_)), "html", null, true);
        echo "</p>

<ul>
    ";
        // line 13
        if (isset($context["missingFolders"])) { $_missingFolders_ = $context["missingFolders"]; } else { $_missingFolders_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_missingFolders_);
        foreach ($context['_seq'] as $context["folderId"] => $context["folderName"]) {
            // line 14
            echo "        <li><label><input type=\"checkbox\" checked=\"checked\" name=\"deleteFolder[]\" value=\"";
            if (isset($context["folderId"])) { $_folderId_ = $context["folderId"]; } else { $_folderId_ = null; }
            echo twig_escape_filter($this->env, $_folderId_, "html", null, true);
            echo "\"> ";
            if (isset($context["folderName"])) { $_folderName_ = $context["folderName"]; } else { $_folderName_ = null; }
            echo twig_escape_filter($this->env, $_folderName_, "html", null, true);
            echo "</label></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['folderId'], $context['folderName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
    ";
        // line 17
        if (isset($context["missingFiles"])) { $_missingFiles_ = $context["missingFiles"]; } else { $_missingFiles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_missingFiles_);
        foreach ($context['_seq'] as $context["fileId"] => $context["fileName"]) {
            // line 18
            echo "        <li><label><input type=\"checkbox\" checked=\"checked\" name=\"deleteFile[]\" value=\"";
            if (isset($context["fileId"])) { $_fileId_ = $context["fileId"]; } else { $_fileId_ = null; }
            echo twig_escape_filter($this->env, $_fileId_, "html", null, true);
            echo "\"> ";
            if (isset($context["fileName"])) { $_fileName_ = $context["fileName"]; } else { $_fileName_ = null; }
            echo twig_escape_filter($this->env, $_fileName_, "html", null, true);
            echo "</label></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['fileId'], $context['fileName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "assets/_missing_items";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 20,  76 => 18,  71 => 17,  68 => 16,  55 => 14,  50 => 13,  43 => 10,  38 => 9,  35 => 8,  31 => 6,  27 => 4,  23 => 2,  19 => 1,);
    }
}

<?php

/* entries/_fields */
class __TwigTemplate_2a19130ddc6741da46d9cd5c0821d13a8630659a0678b57bbc0038b001e73b9a extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
        if ($this->getAttribute($_entryType_, "hasTitleField")) {
            // line 2
            echo "\t";
            $this->env->loadTemplate("entries/_titlefield")->display($context);
        }
        // line 4
        echo "
<div>
\t";
        // line 6
        if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($_entryType_, "getFieldLayout", array(), "method"), "getTabs", array(), "method"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 7
            echo "\t\t<div id=\"tab";
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_loop_, "index"), "html", null, true);
            echo "\"";
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if ((!$this->getAttribute($_loop_, "first"))) {
                echo " class=\"hidden\"";
            }
            echo ">
\t\t\t";
            // line 8
            if (isset($context["tab"])) { $_tab_ = $context["tab"]; } else { $_tab_ = null; }
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            if (isset($context["static"])) { $_static_ = $context["static"]; } else { $_static_ = null; }
            $this->env->loadTemplate("_includes/fields")->display(array("fields" => $this->getAttribute($_tab_, "getFields", array(), "method"), "element" => $_entry_, "static" => ((array_key_exists("static", $context)) ? ($_static_) : (false))));
            // line 13
            echo "\t\t</div>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "entries/_fields";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 15,  30 => 6,  22 => 2,  105 => 16,  91 => 15,  64 => 13,  59 => 8,  44 => 6,  26 => 4,  21 => 2,  188 => 45,  181 => 43,  176 => 42,  169 => 39,  166 => 38,  162 => 36,  152 => 33,  147 => 32,  133 => 31,  128 => 30,  122 => 28,  118 => 27,  113 => 26,  103 => 22,  93 => 19,  88 => 18,  74 => 17,  69 => 16,  63 => 14,  60 => 13,  46 => 10,  32 => 4,  28 => 3,  19 => 1,  719 => 302,  715 => 300,  712 => 299,  706 => 296,  699 => 293,  693 => 289,  688 => 288,  683 => 285,  677 => 282,  674 => 281,  669 => 280,  663 => 276,  654 => 274,  650 => 273,  647 => 272,  631 => 270,  628 => 269,  625 => 268,  622 => 267,  618 => 266,  613 => 265,  610 => 264,  606 => 263,  602 => 262,  600 => 261,  592 => 259,  584 => 254,  574 => 248,  569 => 245,  561 => 240,  558 => 238,  554 => 236,  548 => 233,  541 => 230,  536 => 229,  530 => 225,  522 => 221,  519 => 219,  514 => 216,  508 => 214,  504 => 213,  499 => 212,  493 => 211,  490 => 209,  481 => 207,  473 => 202,  468 => 199,  461 => 196,  453 => 195,  447 => 191,  443 => 190,  440 => 189,  437 => 187,  433 => 185,  426 => 182,  422 => 181,  419 => 180,  415 => 179,  410 => 176,  406 => 174,  401 => 171,  398 => 170,  395 => 169,  388 => 165,  384 => 164,  381 => 163,  376 => 162,  373 => 161,  368 => 158,  363 => 153,  360 => 152,  356 => 151,  354 => 150,  351 => 149,  346 => 148,  343 => 147,  339 => 145,  334 => 138,  330 => 136,  325 => 129,  322 => 128,  318 => 126,  311 => 119,  307 => 118,  304 => 117,  300 => 115,  293 => 108,  289 => 107,  285 => 105,  280 => 97,  277 => 96,  273 => 94,  267 => 88,  263 => 87,  260 => 86,  256 => 84,  252 => 83,  249 => 82,  245 => 80,  238 => 78,  232 => 76,  223 => 75,  217 => 71,  211 => 64,  206 => 63,  202 => 62,  194 => 61,  189 => 60,  183 => 44,  180 => 58,  177 => 57,  173 => 41,  170 => 55,  167 => 54,  157 => 51,  155 => 50,  149 => 47,  146 => 46,  142 => 44,  139 => 43,  136 => 42,  129 => 37,  125 => 34,  123 => 33,  117 => 29,  110 => 25,  107 => 24,  98 => 25,  90 => 24,  84 => 23,  81 => 22,  76 => 20,  70 => 19,  67 => 13,  61 => 310,  58 => 309,  54 => 9,  51 => 13,  48 => 7,  45 => 11,  42 => 10,  38 => 6,  35 => 5,  29 => 5,  27 => 4,  25 => 2,);
    }
}

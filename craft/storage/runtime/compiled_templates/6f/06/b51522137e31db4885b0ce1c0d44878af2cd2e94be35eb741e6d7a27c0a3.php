<?php

/* over-ons */
class __TwigTemplate_6f06b51522137e31db4885b0ce1c0d44878af2cd2e94be35eb741e6d7a27c0a3 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_layout");

        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_layout";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        $context["title"] = $this->getAttribute($_entry_, "title");
        // line 3
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        $context["metadescription"] = $this->getAttribute($_entry_, "metadescription");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_javascript($context, array $blocks = array())
    {
        // line 6
        echo "<script type=\"text/javascript\" src=\"";
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/main.js\"></script>
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "\t 
\t<div id=\"slider-small\">
\t\t<div class=\"item\" style=\"background-image: url(";
        // line 12
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/images/slides/vervolgpagina.jpg); display: block;   \">
\t\t</div>
\t</div>
\t<div class=\"breadcrumb\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-6 col-xs-12 \">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"";
        // line 20
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "\"><i class=\"fa fa-home\"></i></a></li>
\t\t\t\t\t\t<li><i class=\"fa fa-angle-right\"></i></li>
\t\t\t\t\t\t<li>Over ons</li>
  \t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"content\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 \">
\t\t\t\t\t<h1 class=\"gray\">";
        // line 32
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "header"), "html", null, true);
        echo "</h1>
\t\t\t\t</div>
 \t\t\t</div>
 \t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 \">
 \t\t\t\t\t";
        // line 37
        if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "body"), "html", null, true);
        echo "
 \t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-3 col-md-3 col-sm-3 col-md-offset-1\">
\t\t\t\t\t<img src=\"";
        // line 40
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/images/logo.png\" alt=\"Logo Visscher traffic\" class=\"img-responsive\"> 
 \t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"divider\"></div>
            
\t\t\t<div class=\"row\">
            \t";
        // line 46
        if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($_craft_, "entries"), "section", array(0 => "overOns"), "method"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 47
            echo "                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 \">
\t\t\t\t\t<a href=\"";
            // line 48
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "uri"), "html", null, true);
            echo "\">
\t\t\t\t\t\t<div class=\"big-thumb animated fadeInDown\">
                        \t";
            // line 50
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_entry_, "logo"));
            foreach ($context['_seq'] as $context["_key"] => $context["asset"]) {
                // line 51
                echo "                                ";
                if (isset($context["asset"])) { $_asset_ = $context["asset"]; } else { $_asset_ = null; }
                if (($this->getAttribute($_asset_, "extension") == "svg")) {
                    // line 52
                    echo "                               \t\t<object type=\"image/svg+xml\" data=\"";
                    if (isset($context["asset"])) { $_asset_ = $context["asset"]; } else { $_asset_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_asset_, "url"), "html", null, true);
                    echo "\" class=\"thumb\"></object>
                                ";
                } else {
                    // line 54
                    echo "                                \t<img src=\"";
                    if (isset($context["asset"])) { $_asset_ = $context["asset"]; } else { $_asset_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_asset_, "url"), "html", null, true);
                    echo "\" class=\"thumb\" />
                                ";
                }
                // line 56
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['asset'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "\t\t\t\t\t\t\t<h4>";
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "title"), "html", null, true);
            echo "</h4>
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
 \t\t\t\t</div> 
                ";
            // line 61
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if ((((($this->getAttribute($_loop_, "index") == 3) || ($this->getAttribute($_loop_, "index") == 6)) || ($this->getAttribute($_loop_, "index") == 9)) || ($this->getAttribute($_loop_, "index") == 12))) {
                echo "   
                </div> 
                <div class=\"row\">  
                ";
            }
            // line 65
            echo "                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo " \t\t\t</div>
\t\t</div>
\t</div>
    
    ";
        // line 70
        $this->env->loadTemplate("formulier")->display($context);
    }

    public function getTemplateName()
    {
        return "over-ons";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 70,  194 => 66,  180 => 65,  172 => 61,  163 => 57,  157 => 56,  150 => 54,  143 => 52,  139 => 51,  134 => 50,  128 => 48,  125 => 47,  107 => 46,  97 => 40,  90 => 37,  81 => 32,  65 => 20,  53 => 12,  49 => 10,  46 => 9,  38 => 6,  35 => 5,  29 => 3,  26 => 2,);
    }
}

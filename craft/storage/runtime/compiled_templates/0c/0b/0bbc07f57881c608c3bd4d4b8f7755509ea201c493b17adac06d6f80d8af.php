<?php

/* _components/fieldtypes/PlainText/input */
class __TwigTemplate_0c0b0bbc07f57881c608c3bd4d4b8f7755509ea201c493b17adac06d6f80d8af extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["forms"] = $this->env->loadTemplate("_includes/forms");
        // line 2
        echo "
";
        // line 3
        if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
        if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        $context["config"] = array("id" => $_name_, "name" => $_name_, "value" => $_value_, "class" => "nicetext", "maxlength" => $this->getAttribute($_settings_, "maxLength"), "showCharsLeft" => true, "placeholder" => $this->getAttribute($_settings_, "placeholder"), "rows" => $this->getAttribute($_settings_, "initialRows"));
        // line 13
        echo "
";
        // line 14
        if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
        if ($this->getAttribute($_settings_, "multiline")) {
            // line 15
            echo "\t";
            if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
            if (isset($context["config"])) { $_config_ = $context["config"]; } else { $_config_ = null; }
            echo $_forms_->gettextarea($_config_);
            echo "
";
        } else {
            // line 17
            echo "\t";
            if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
            if (isset($context["config"])) { $_config_ = $context["config"]; } else { $_config_ = null; }
            echo $_forms_->gettext($_config_);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "_components/fieldtypes/PlainText/input";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 17,  145 => 31,  140 => 30,  135 => 29,  130 => 28,  120 => 26,  115 => 25,  99 => 22,  97 => 21,  86 => 16,  72 => 15,  41 => 9,  50 => 12,  23 => 2,  31 => 16,  24 => 3,  79 => 15,  30 => 4,  22 => 2,  105 => 23,  91 => 15,  64 => 5,  59 => 8,  44 => 11,  26 => 4,  21 => 2,  188 => 45,  181 => 43,  176 => 42,  169 => 39,  166 => 38,  162 => 36,  152 => 34,  147 => 32,  133 => 31,  128 => 30,  122 => 28,  118 => 27,  113 => 26,  103 => 22,  93 => 19,  88 => 18,  74 => 17,  69 => 14,  63 => 14,  60 => 13,  46 => 10,  32 => 14,  28 => 3,  19 => 1,  719 => 302,  715 => 300,  712 => 299,  706 => 296,  699 => 293,  693 => 289,  688 => 288,  683 => 285,  677 => 282,  674 => 281,  669 => 280,  663 => 276,  654 => 274,  650 => 273,  647 => 272,  631 => 270,  628 => 269,  625 => 268,  622 => 267,  618 => 266,  613 => 265,  610 => 264,  606 => 263,  602 => 262,  600 => 261,  592 => 259,  584 => 254,  574 => 248,  569 => 245,  561 => 240,  558 => 238,  554 => 236,  548 => 233,  541 => 230,  536 => 229,  530 => 225,  522 => 221,  519 => 219,  514 => 216,  508 => 214,  504 => 213,  499 => 212,  493 => 211,  490 => 209,  481 => 207,  473 => 202,  468 => 199,  461 => 196,  453 => 195,  447 => 191,  443 => 190,  440 => 189,  437 => 187,  433 => 185,  426 => 182,  422 => 181,  419 => 180,  415 => 179,  410 => 176,  406 => 174,  401 => 171,  398 => 170,  395 => 169,  388 => 165,  384 => 164,  381 => 163,  376 => 162,  373 => 161,  368 => 158,  363 => 153,  360 => 152,  356 => 151,  354 => 150,  351 => 149,  346 => 148,  343 => 147,  339 => 145,  334 => 138,  330 => 136,  325 => 129,  322 => 128,  318 => 126,  311 => 119,  307 => 118,  304 => 117,  300 => 115,  293 => 108,  289 => 107,  285 => 105,  280 => 97,  277 => 96,  273 => 94,  267 => 88,  263 => 87,  260 => 86,  256 => 84,  252 => 83,  249 => 82,  245 => 80,  238 => 78,  232 => 76,  223 => 75,  217 => 71,  211 => 64,  206 => 63,  202 => 62,  194 => 61,  189 => 60,  183 => 44,  180 => 58,  177 => 57,  173 => 41,  170 => 55,  167 => 54,  157 => 51,  155 => 35,  149 => 47,  146 => 46,  142 => 44,  139 => 43,  136 => 42,  129 => 37,  125 => 27,  123 => 33,  117 => 29,  110 => 24,  107 => 24,  98 => 25,  90 => 18,  84 => 23,  81 => 22,  76 => 12,  70 => 6,  67 => 13,  61 => 310,  58 => 309,  54 => 9,  51 => 13,  48 => 7,  45 => 11,  42 => 10,  38 => 8,  35 => 15,  29 => 13,  27 => 3,  25 => 2,);
    }
}

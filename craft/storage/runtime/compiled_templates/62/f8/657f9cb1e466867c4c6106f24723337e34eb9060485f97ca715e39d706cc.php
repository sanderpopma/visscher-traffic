<?php

/* _layout */
class __TwigTemplate_62f8657f9cb1e466867c4c6106f24723337e34eb9060485f97ca715e39d706cc extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"nl\">
  <head>
  \t ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 39
        echo "  <body>
\t <header class=\"header\">
\t\t  <div class=\"container\">
\t\t\t  <div class=\"row\">
\t\t\t  \t <div class=\"col-lg-3 col-md-2 col-sm-4 col-xs-4 logo\">
\t\t\t  \t \t<a href=\"";
        // line 44
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "\">
\t\t\t\t  \t \t<img src=\"";
        // line 45
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/images/logo.png\" alt=\"Terug naar home\" />
\t\t\t  \t \t</a>
\t\t\t  \t </div>
\t\t\t  \t 
\t\t\t  \t <div class=\"col-lg-5 col-md-6 col-sm-8 col-md-offset-4 col-lg-offset-3 col-sm-offset-0 \">
\t\t\t\t   <div class=\"navigation\">
\t\t\t\t   <nav class=\"nav\">
\t\t\t\t\t  \t<button type=\"button\" data-toggle=\"button\" id=\"menu\" class=\"btn visible-xs\">
\t\t\t\t\t  \t\t<span class=\"lines\"></span>
\t\t\t\t\t  \t</button>
\t\t\t\t\t  \t <ul>
\t\t\t\t\t\t  \t <li>
\t\t\t\t\t\t\t  \t <a href=\"";
        // line 57
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "\">Home</a>
\t\t\t\t\t\t  \t </li>
                              <li>
\t\t\t\t\t\t\t  \t <a href=\"";
        // line 60
        echo twig_escape_filter($this->env, \Craft\UrlHelper::getUrl("over-ons"), "html", null, true);
        echo "\">Over ons</a>
                                 <ul>
                                    ";
        // line 62
        if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($_craft_, "entries"), "section", array(0 => "overOns"), "method"), "order", array(0 => "title asc"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 63
            echo "                                    <li><a href=\"";
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "url"), "html", null, true);
            echo " \">";
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "title"), "html", null, true);
            echo "</a></li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "                                </ul>
\t\t\t\t\t\t  \t </li>
\t\t\t\t\t\t  \t 
\t\t\t\t\t\t  \t <li>
\t\t\t\t\t\t\t  \t <a href=\"";
        // line 69
        echo twig_escape_filter($this->env, \Craft\UrlHelper::getUrl("diensten"), "html", null, true);
        echo "\">Diensten</a>
                                 <ul>
                                    ";
        // line 71
        if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($_craft_, "entries"), "section", array(0 => "diensten"), "method"), "order", array(0 => "title asc"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 72
            echo "                                    <li><a href=\"";
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "url"), "html", null, true);
            echo " \">";
            if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "title"), "html", null, true);
            echo "</a></li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                                </ul>
\t\t\t\t\t\t  \t </li>
\t\t\t\t\t\t  \t <li>
\t\t\t\t\t\t\t  \t <a href=\"";
        // line 77
        echo twig_escape_filter($this->env, \Craft\UrlHelper::getUrl("contact"), "html", null, true);
        echo "\">Contact</a>
\t\t\t\t\t\t  \t </li>
 \t\t\t\t\t  \t </ul>
\t\t\t\t  \t</nav>
\t\t\t\t  \t</div>
\t\t\t\t  \t<!--<div class=\"social\">
\t\t\t\t  \t\t<ul>
\t\t\t\t  \t\t\t<li><a href=\"http://www.facebook.com/visschertraffic\"><i class=\"fa fa-facebook-square fa-lg gray\"></i></a></li>
\t\t\t\t  \t\t\t<li><a href=\"http://www.linkedin.com/visschertraffic\"><i class=\"fa fa-linkedin-square fa-lg gray\"></i></a></li>
\t\t\t\t  \t\t</ul>-->
\t\t\t\t  \t</div>
\t\t\t\t  </div>
\t \t\t  </div>
\t\t  </div>
\t </header>
\t\t";
        // line 92
        $this->displayBlock('content', $context, $blocks);
        // line 94
        echo " \t<div class=\"footer\">
\t\t  <div class=\"container\">
\t\t      <div class=\"row\">
\t\t\t      <footer>
\t\t\t      \t<div class=\"col-lg-3 col-md-3 col-sm-2 col-xs-3 \">
\t\t\t\t      \t<p>";
        // line 99
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "naam"), "html", null, true);
        echo "</p>
\t\t\t      \t</div>
\t\t\t      \t<div class=\"col-lg-4 col-md-4 col-sm-5 col-xs-5 col-lg-offset-3\">
\t\t\t\t      \t<p><i class=\"fa fa-home\"> ";
        // line 102
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "adres"), "html", null, true);
        echo " ";
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "postcode"), "html", null, true);
        echo "  ";
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "woonplaats"), "html", null, true);
        echo "</i></p>
\t\t\t      \t</div>
\t\t\t      \t<div class=\"col-md-2 col-lg-2 col-sm-3 col-xs-4  \">
\t\t\t\t\t  \t <i class=\"fa fa-phone\"> ";
        // line 105
        if (isset($context["algemeen"])) { $_algemeen_ = $context["algemeen"]; } else { $_algemeen_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_algemeen_, "telefoonnummer"), "html", null, true);
        echo "</i>
\t \t\t      \t</div>
\t\t\t      </footer>
\t\t      </div>
\t\t  </div>
\t\t</div>
\t    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
\t
\t    <!-- Include all compiled plugins (below), or include individual files as needed -->
\t    
\t    <script  type=\"text/javascript\" src=\"";
        // line 115
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/bootstrap.min.js\"></script>
\t<!--\t <script type=\"text/javascript\" src=\"js/retina-1.1.0.js\"></script>-->
<!--\t\t <script type=\"text/javascript\" src=\"";
        // line 117
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/instafeed.min.js\"></script>-->
\t\t\t<script>
\t\t\t  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
\t\t\t\t  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
\t\t\t  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
\t\t\t  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
\t\t\t
\t\t\t  ga('create', 'UA-56094206-1', 'auto');
\t\t\t  ga('send', 'pageview');
\t\t\t
\t\t\t</script>
\t\t  </body>
\t</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "  \t
 \t<title>";
        // line 6
        if (array_key_exists("title", $context)) {
            if (isset($context["title"])) { $_title_ = $context["title"]; } else { $_title_ = null; }
            echo twig_escape_filter($this->env, $_title_, "html", null, true);
            echo " - ";
        }
        if (isset($context["siteName"])) { $_siteName_ = $context["siteName"]; } else { $_siteName_ = null; }
        echo twig_escape_filter($this->env, $_siteName_, "html", null, true);
        echo "</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"author\" content=\"SPM design & more\">
    
    <!-- Bootstrap -->
    <link href=\"";
        // line 11
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">
    <link href=\"";
        // line 12
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/css/default.css\" rel=\"stylesheet\" media=\"screen\">
    <link href=\"";
        // line 13
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/css/menu.css\" rel=\"stylesheet\" media=\"screen\">
\t<link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\" rel=\"stylesheet\">
\t";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo " \t<meta name=\"description\" content=\"";
        if (array_key_exists("metadescription", $context)) {
            if (isset($context["metadescription"])) { $_metadescription_ = $context["metadescription"]; } else { $_metadescription_ = null; }
            echo twig_escape_filter($this->env, $_metadescription_, "html", null, true);
        }
        echo "\" />
\t<meta name=\"keywords\" content=\"";
        // line 18
        if (array_key_exists("keywords", $context)) {
            if (isset($context["keywords"])) { $_keywords_ = $context["keywords"]; } else { $_keywords_ = null; }
            echo twig_escape_filter($this->env, $_keywords_, "html", null, true);
        }
        echo "\" /> 
 \t<meta name=\"robots\" content=\"index, follow\" /> 
\t<meta name=\"googlebot\" content=\"index, follow\" /> 
\t<link rel=\"shortcut icon\" id=\"favicon\" href=\"images/favicon.ico\"> 
\t<script  type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 23
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/scroll.js\"></script>
\t<link rel=\"shortcut icon\" href=\"/favicon.ico\" type=\"image/x-icon\" />
\t
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300,600' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
         <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"";
        // line 30
        if (isset($context["siteUrl"])) { $_siteUrl_ = $context["siteUrl"]; } else { $_siteUrl_ = null; }
        echo twig_escape_filter($this->env, $_siteUrl_, "html", null, true);
        echo "static/js/contact.js\"></script>

         <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
       <![endif]-->
       ";
        // line 34
        $this->displayBlock('javascript', $context, $blocks);
        // line 35
        echo "\t\t
\t 
   </head>
   ";
    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 16
        echo "\t";
    }

    // line 34
    public function block_javascript($context, array $blocks = array())
    {
        // line 35
        echo "       ";
    }

    // line 92
    public function block_content($context, array $blocks = array())
    {
        // line 93
        echo "\t\t";
    }

    public function getTemplateName()
    {
        return "_layout";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 93,  306 => 92,  302 => 35,  299 => 34,  295 => 16,  292 => 15,  285 => 35,  283 => 34,  275 => 30,  264 => 23,  253 => 18,  245 => 17,  243 => 15,  237 => 13,  232 => 12,  227 => 11,  213 => 6,  210 => 5,  207 => 4,  189 => 117,  183 => 115,  169 => 105,  156 => 102,  149 => 99,  142 => 94,  140 => 92,  122 => 77,  117 => 74,  94 => 69,  88 => 65,  70 => 62,  65 => 60,  58 => 57,  42 => 45,  37 => 44,  30 => 39,  28 => 4,  23 => 1,  134 => 50,  129 => 49,  119 => 41,  109 => 38,  104 => 72,  99 => 71,  96 => 35,  91 => 34,  75 => 63,  68 => 19,  63 => 18,  54 => 13,  49 => 10,  46 => 9,  38 => 6,  35 => 5,  29 => 3,  26 => 2,);
    }
}

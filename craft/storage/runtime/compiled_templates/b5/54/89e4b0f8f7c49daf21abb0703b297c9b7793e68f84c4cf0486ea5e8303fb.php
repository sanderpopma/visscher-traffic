<?php

/* _includes/forms/textarea */
class __TwigTemplate_b55489e4b0f8f7c49daf21abb0703b297c9b7793e68f84c4cf0486ea5e8303fb extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["class"])) { $_class_ = $context["class"]; } else { $_class_ = null; }
        if (isset($context["placeholder"])) { $_placeholder_ = $context["placeholder"]; } else { $_placeholder_ = null; }
        if (isset($context["size"])) { $_size_ = $context["size"]; } else { $_size_ = null; }
        $context["class"] = twig_join_filter(array_filter(array(0 => "text", 1 => (((array_key_exists("class", $context) && $_class_)) ? ($_class_) : (null)), 2 => (((array_key_exists("placeholder", $context) && $_placeholder_)) ? ("nicetext") : (null)), 3 => (((array_key_exists("size", $context) && $_size_)) ? (null) : ("fullwidth")))), " ");
        // line 8
        if (isset($context["rows"])) { $_rows_ = $context["rows"]; } else { $_rows_ = null; }
        $context["rows"] = ((array_key_exists("rows", $context)) ? ($_rows_) : (2));
        // line 9
        if (isset($context["cols"])) { $_cols_ = $context["cols"]; } else { $_cols_ = null; }
        $context["cols"] = ((array_key_exists("cols", $context)) ? ($_cols_) : (50));
        // line 11
        echo "<textarea class=\"";
        if (isset($context["class"])) { $_class_ = $context["class"]; } else { $_class_ = null; }
        echo twig_escape_filter($this->env, $_class_, "html", null, true);
        echo "\" rows=\"";
        if (isset($context["rows"])) { $_rows_ = $context["rows"]; } else { $_rows_ = null; }
        echo twig_escape_filter($this->env, $_rows_, "html", null, true);
        echo "\" cols=\"";
        if (isset($context["cols"])) { $_cols_ = $context["cols"]; } else { $_cols_ = null; }
        echo twig_escape_filter($this->env, $_cols_, "html", null, true);
        echo "\"";
        // line 12
        if (array_key_exists("id", $context)) {
            echo " id=\"";
            if (isset($context["id"])) { $_id_ = $context["id"]; } else { $_id_ = null; }
            echo twig_escape_filter($this->env, $_id_, "html", null, true);
            echo "\"";
        }
        // line 13
        if (array_key_exists("name", $context)) {
            echo " name=\"";
            if (isset($context["name"])) { $_name_ = $context["name"]; } else { $_name_ = null; }
            echo twig_escape_filter($this->env, $_name_, "html", null, true);
            echo "\"";
        }
        // line 14
        if (isset($context["maxlength"])) { $_maxlength_ = $context["maxlength"]; } else { $_maxlength_ = null; }
        if ((array_key_exists("maxlength", $context) && $_maxlength_)) {
            echo " maxlength=\"";
            if (isset($context["maxlength"])) { $_maxlength_ = $context["maxlength"]; } else { $_maxlength_ = null; }
            echo twig_escape_filter($this->env, $_maxlength_, "html", null, true);
            echo "\"";
        }
        // line 15
        if (isset($context["showCharsLeft"])) { $_showCharsLeft_ = $context["showCharsLeft"]; } else { $_showCharsLeft_ = null; }
        if ((array_key_exists("showCharsLeft", $context) && $_showCharsLeft_)) {
            echo " data-show-chars-left";
        }
        // line 16
        if (isset($context["autofocus"])) { $_autofocus_ = $context["autofocus"]; } else { $_autofocus_ = null; }
        if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
        if (((array_key_exists("autofocus", $context) && $_autofocus_) && (!$this->getAttribute($this->getAttribute($_craft_, "request"), "isMobileBrowser", array(0 => true), "method")))) {
            echo " autofocus";
        }
        // line 17
        if (isset($context["disabled"])) { $_disabled_ = $context["disabled"]; } else { $_disabled_ = null; }
        if ((array_key_exists("disabled", $context) && $_disabled_)) {
            echo " disabled";
        }
        // line 18
        if (array_key_exists("placeholder", $context)) {
            echo " placeholder=\"";
            if (isset($context["placeholder"])) { $_placeholder_ = $context["placeholder"]; } else { $_placeholder_ = null; }
            echo twig_escape_filter($this->env, $_placeholder_, "html", null, true);
            echo "\"";
        }
        echo ">";
        if (isset($context["value"])) { $_value_ = $context["value"]; } else { $_value_ = null; }
        echo twig_escape_filter($this->env, ((array_key_exists("value", $context)) ? ($_value_) : (null)), "html", null, true);
        echo "</textarea>
";
    }

    public function getTemplateName()
    {
        return "_includes/forms/textarea";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 14,  41 => 12,  24 => 8,  93 => 52,  89 => 50,  78 => 38,  58 => 20,  43 => 10,  40 => 9,  34 => 7,  87 => 23,  81 => 21,  61 => 26,  82 => 43,  79 => 18,  74 => 17,  70 => 14,  65 => 28,  49 => 12,  46 => 11,  37 => 8,  32 => 5,  22 => 2,  124 => 30,  120 => 29,  114 => 28,  107 => 22,  102 => 60,  96 => 56,  86 => 45,  77 => 19,  73 => 22,  68 => 16,  62 => 12,  54 => 18,  51 => 18,  47 => 16,  38 => 9,  33 => 5,  23 => 3,  21 => 2,  19 => 1,  757 => 244,  754 => 243,  746 => 251,  737 => 249,  734 => 248,  730 => 246,  728 => 243,  724 => 241,  717 => 238,  714 => 237,  711 => 236,  700 => 235,  697 => 234,  694 => 233,  691 => 232,  688 => 231,  682 => 227,  678 => 226,  674 => 225,  670 => 224,  667 => 223,  661 => 217,  654 => 214,  651 => 213,  649 => 212,  646 => 211,  639 => 209,  636 => 208,  633 => 207,  628 => 204,  615 => 202,  610 => 201,  606 => 199,  602 => 198,  599 => 197,  590 => 285,  584 => 283,  581 => 282,  576 => 281,  570 => 278,  565 => 277,  561 => 275,  558 => 274,  553 => 271,  545 => 269,  540 => 268,  528 => 265,  517 => 263,  514 => 262,  507 => 261,  496 => 260,  490 => 256,  488 => 223,  483 => 220,  481 => 197,  475 => 193,  469 => 192,  459 => 190,  455 => 189,  450 => 188,  446 => 187,  436 => 179,  428 => 176,  422 => 174,  419 => 173,  416 => 172,  407 => 171,  401 => 170,  396 => 169,  383 => 165,  370 => 159,  362 => 157,  357 => 156,  351 => 155,  345 => 152,  341 => 150,  336 => 147,  329 => 145,  313 => 143,  308 => 142,  301 => 140,  296 => 139,  291 => 137,  283 => 134,  280 => 133,  277 => 132,  274 => 131,  271 => 130,  268 => 129,  265 => 128,  262 => 127,  254 => 123,  247 => 122,  239 => 121,  236 => 120,  232 => 119,  229 => 118,  226 => 117,  222 => 116,  218 => 115,  214 => 114,  210 => 113,  207 => 112,  204 => 111,  201 => 110,  198 => 109,  195 => 108,  192 => 107,  189 => 106,  186 => 105,  182 => 104,  177 => 103,  174 => 102,  168 => 98,  165 => 97,  162 => 96,  159 => 95,  155 => 93,  145 => 91,  140 => 90,  137 => 89,  133 => 88,  129 => 87,  126 => 86,  123 => 85,  117 => 83,  112 => 82,  109 => 64,  30 => 11,  28 => 4,  128 => 32,  122 => 35,  119 => 34,  111 => 65,  106 => 29,  94 => 27,  88 => 26,  84 => 25,  80 => 28,  76 => 23,  71 => 31,  66 => 21,  63 => 15,  56 => 12,  52 => 10,  48 => 13,  45 => 12,  42 => 7,  31 => 7,  29 => 6,  27 => 9,  25 => 4,);
    }
}

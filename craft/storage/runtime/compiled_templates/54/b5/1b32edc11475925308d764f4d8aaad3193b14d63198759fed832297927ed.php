<?php

/* _components/widgets/GetHelp/body */
class __TwigTemplate_54b51b32edc11475925308d764f4d8aaad3193b14d63198759fed832297927ed extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["forms"] = $this->env->loadTemplate("_includes/forms");
        // line 2
        echo "
<form method=\"post\" accept-charset=\"UTF-8\" enctype=\"multipart/form-data\">
\t";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('craft')->getCsrfInputFunction(), "html", null, true);
        echo "

\t";
        // line 6
        if (isset($context["getHelp"])) { $_getHelp_ = $context["getHelp"]; } else { $_getHelp_ = null; }
        if (isset($context["currentUser"])) { $_currentUser_ = $context["currentUser"]; } else { $_currentUser_ = null; }
        $context["fromEmail"] = ((array_key_exists("getHelp", $context)) ? ($this->getAttribute($_getHelp_, "fromEmail")) : ($this->getAttribute($_currentUser_, "email")));
        // line 7
        echo "
\t";
        // line 8
        if (isset($context["fromEmail"])) { $_fromEmail_ = $context["fromEmail"]; } else { $_fromEmail_ = null; }
        if ((($_fromEmail_ == "support@pixelandtonic.com") || ($_fromEmail_ == "support@buildwithcraft.com"))) {
            // line 9
            echo "\t\t";
            $context["fromEmail"] = "";
            // line 10
            echo "\t";
        }
        // line 11
        echo "
\t";
        // line 12
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        if (isset($context["fromEmail"])) { $_fromEmail_ = $context["fromEmail"]; } else { $_fromEmail_ = null; }
        if (isset($context["getHelp"])) { $_getHelp_ = $context["getHelp"]; } else { $_getHelp_ = null; }
        echo $_forms_->gettextField(array("label" => \Craft\Craft::t("Your Email"), "class" => "fromEmail", "name" => "fromEmail", "value" => $_fromEmail_, "errors" => ((array_key_exists("getHelp", $context)) ? ($this->getAttribute($_getHelp_, "getErrors", array(0 => "fromEmail"), "method")) : (""))));
        // line 18
        echo "

\t";
        // line 20
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        echo $_forms_->gettextareaField(array("label" => \Craft\Craft::t("Message"), "class" => "message", "name" => "message", "placeholder" => \Craft\Craft::t("Tell us about your problems."), "rows" => 4));
        // line 26
        echo "

\t<a class=\"fieldtoggle\" data-target=\"gethelp-more\">";
        // line 28
        echo twig_escape_filter($this->env, \Craft\Craft::t("More"), "html", null, true);
        echo "</a>

\t<div id=\"gethelp-more\" class=\"hidden\">
\t\t";
        // line 31
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        echo $_forms_->getcheckboxField(array("label" => \Craft\Craft::t("Attach error logs?"), "class" => "attachLogs", "name" => "attachLogs", "checked" => true));
        // line 36
        echo "

\t\t";
        // line 38
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
        echo $_forms_->getcheckboxField(array("label" => \Craft\Craft::t("Attach a database backup?"), "class" => "attachDbBackup", "name" => "attachDbBackup", "checked" => $this->getAttribute($this->getAttribute($_craft_, "config"), "backupDbOnUpdate")));
        // line 43
        echo "

\t\t";
        // line 45
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        echo $_forms_->getcheckboxField(array("name" => "attachTemplates", "checked" => true, "class" => "attachTemplates", "label" => \Craft\Craft::t("Include your template files?")));
        // line 50
        echo "

\t\t";
        // line 52
        if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
        echo $_forms_->getfileField(array("label" => \Craft\Craft::t("Attach an additional file?"), "class" => "attachAdditionalFile", "name" => "attachAdditionalFile"));
        // line 56
        echo "
\t</div>

\t<div class=\"buttons last\">
\t\t<input type=\"submit\" class=\"btn submit\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, \Craft\Craft::t("Send"), "html", null, true);
        echo "\">
\t\t<div class=\"spinner hidden\"></div>
\t</div>

\t";
        // line 64
        $context["email"] = ('' === $tmp = "<a href=\"mailto:support@buildwithcraft.com\">support@buildwithcraft.com</a>") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 65
        echo "\t<p class=\"error hidden\">";
        if (isset($context["email"])) { $_email_ = $context["email"]; } else { $_email_ = null; }
        echo \Craft\Craft::t("Couldn’t send your message. Please email it to {email} instead.", array("email" => $_email_));
        echo "</p>
</form>
";
    }

    public function getTemplateName()
    {
        return "_components/widgets/GetHelp/body";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 52,  89 => 50,  78 => 38,  58 => 20,  43 => 10,  40 => 9,  34 => 7,  87 => 23,  81 => 21,  61 => 26,  82 => 43,  79 => 18,  74 => 36,  70 => 14,  65 => 28,  49 => 12,  46 => 11,  37 => 8,  32 => 5,  22 => 2,  124 => 30,  120 => 29,  114 => 28,  107 => 22,  102 => 60,  96 => 56,  86 => 45,  77 => 19,  73 => 22,  68 => 15,  62 => 12,  54 => 18,  51 => 18,  47 => 16,  38 => 9,  33 => 5,  23 => 3,  21 => 2,  19 => 1,  757 => 244,  754 => 243,  746 => 251,  737 => 249,  734 => 248,  730 => 246,  728 => 243,  724 => 241,  717 => 238,  714 => 237,  711 => 236,  700 => 235,  697 => 234,  694 => 233,  691 => 232,  688 => 231,  682 => 227,  678 => 226,  674 => 225,  670 => 224,  667 => 223,  661 => 217,  654 => 214,  651 => 213,  649 => 212,  646 => 211,  639 => 209,  636 => 208,  633 => 207,  628 => 204,  615 => 202,  610 => 201,  606 => 199,  602 => 198,  599 => 197,  590 => 285,  584 => 283,  581 => 282,  576 => 281,  570 => 278,  565 => 277,  561 => 275,  558 => 274,  553 => 271,  545 => 269,  540 => 268,  528 => 265,  517 => 263,  514 => 262,  507 => 261,  496 => 260,  490 => 256,  488 => 223,  483 => 220,  481 => 197,  475 => 193,  469 => 192,  459 => 190,  455 => 189,  450 => 188,  446 => 187,  436 => 179,  428 => 176,  422 => 174,  419 => 173,  416 => 172,  407 => 171,  401 => 170,  396 => 169,  383 => 165,  370 => 159,  362 => 157,  357 => 156,  351 => 155,  345 => 152,  341 => 150,  336 => 147,  329 => 145,  313 => 143,  308 => 142,  301 => 140,  296 => 139,  291 => 137,  283 => 134,  280 => 133,  277 => 132,  274 => 131,  271 => 130,  268 => 129,  265 => 128,  262 => 127,  254 => 123,  247 => 122,  239 => 121,  236 => 120,  232 => 119,  229 => 118,  226 => 117,  222 => 116,  218 => 115,  214 => 114,  210 => 113,  207 => 112,  204 => 111,  201 => 110,  198 => 109,  195 => 108,  192 => 107,  189 => 106,  186 => 105,  182 => 104,  177 => 103,  174 => 102,  168 => 98,  165 => 97,  162 => 96,  159 => 95,  155 => 93,  145 => 91,  140 => 90,  137 => 89,  133 => 88,  129 => 87,  126 => 86,  123 => 85,  117 => 83,  112 => 82,  109 => 64,  30 => 6,  28 => 4,  128 => 32,  122 => 35,  119 => 34,  111 => 65,  106 => 29,  94 => 27,  88 => 26,  84 => 25,  80 => 28,  76 => 23,  71 => 31,  66 => 21,  63 => 20,  56 => 12,  52 => 10,  48 => 13,  45 => 12,  42 => 7,  31 => 7,  29 => 6,  27 => 3,  25 => 4,);
    }
}

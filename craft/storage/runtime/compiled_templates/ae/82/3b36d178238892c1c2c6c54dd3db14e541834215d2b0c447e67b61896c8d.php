<?php

/* _components/widgets/QuickPost/body */
class __TwigTemplate_ae823b36d178238892c1c2c6c54dd3db14e541834215d2b0c447e67b61896c8d extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["forms"] = $this->env->loadTemplate("_includes/forms");
        // line 2
        $context["fieldNamespace"] = ("fields" . twig_random($this->env));
        // line 3
        echo "

<form method=\"post\" accept-charset=\"UTF-8\">
\t<input type=\"hidden\" name=\"fieldsLocation\" value=\"";
        // line 6
        if (isset($context["fieldNamespace"])) { $_fieldNamespace_ = $context["fieldNamespace"]; } else { $_fieldNamespace_ = null; }
        echo twig_escape_filter($this->env, $_fieldNamespace_, "html", null, true);
        echo "\">
\t";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('craft')->getCsrfInputFunction(), "html", null, true);
        echo "

\t";
        // line 9
        if (isset($context["section"])) { $_section_ = $context["section"]; } else { $_section_ = null; }
        if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
        if ((($this->getAttribute($_section_, "type") != "single") && $this->getAttribute($_entryType_, "hasTitleField"))) {
            // line 10
            echo "\t\t";
            if (isset($context["forms"])) { $_forms_ = $context["forms"]; } else { $_forms_ = null; }
            if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
            echo $_forms_->gettextField(array("label" => \Craft\Craft::t($this->getAttribute($_entryType_, "titleLabel")), "id" => "title", "name" => "title", "required" => true, "first" => true));
            // line 16
            echo "
\t";
        }
        // line 18
        echo "
\t";
        // line 19
        if (isset($context["fieldNamespace"])) { $_fieldNamespace_ = $context["fieldNamespace"]; } else { $_fieldNamespace_ = null; }
        $_namespace = $_fieldNamespace_;
        if ($_namespace) {
            $_originalNamespace = \Craft\craft()->templates->getNamespace();
            \Craft\craft()->templates->setNamespace(\Craft\craft()->templates->namespaceInputName($_namespace));
            ob_start();
            try {
                // line 20
                echo "\t\t";
                if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($_entryType_, "getFieldLayout", array(), "method"), "getFields", array(), "method"));
                foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                    // line 21
                    echo "\t\t\t";
                    if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
                    if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
                    if (($this->getAttribute($_field_, "required") || twig_in_filter($this->getAttribute($_field_, "fieldId"), $this->getAttribute($_settings_, "fields")))) {
                        // line 22
                        echo "\t\t\t\t";
                        if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
                        $this->env->loadTemplate("_includes/field")->display(array("field" => $this->getAttribute($_field_, "getField", array(), "method"), "required" => $this->getAttribute($_field_, "required"), "entry" => null));
                        // line 27
                        echo "\t\t\t";
                    }
                    // line 28
                    echo "\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "\t";
            } catch (Exception $e) {
                ob_end_clean();

                throw $e;
            }
            echo \Craft\craft()->templates->namespaceInputs(ob_get_clean(), $_namespace);
            \Craft\craft()->templates->setNamespace($_originalNamespace);
        } else {
            // line 20
            echo "\t\t";
            if (isset($context["entryType"])) { $_entryType_ = $context["entryType"]; } else { $_entryType_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($_entryType_, "getFieldLayout", array(), "method"), "getFields", array(), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 21
                echo "\t\t\t";
                if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
                if (isset($context["settings"])) { $_settings_ = $context["settings"]; } else { $_settings_ = null; }
                if (($this->getAttribute($_field_, "required") || twig_in_filter($this->getAttribute($_field_, "fieldId"), $this->getAttribute($_settings_, "fields")))) {
                    // line 22
                    echo "\t\t\t\t";
                    if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
                    $this->env->loadTemplate("_includes/field")->display(array("field" => $this->getAttribute($_field_, "getField", array(), "method"), "required" => $this->getAttribute($_field_, "required"), "entry" => null));
                    // line 27
                    echo "\t\t\t";
                }
                // line 28
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "\t";
        }
        unset($_originalNamespace, $_namespace);
        // line 30
        echo "
\t<div class=\"buttons\">
\t\t<input type=\"submit\" class=\"btn submit\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, \Craft\Craft::t("Post"), "html", null, true);
        echo "\">
\t\t<div class=\"spinner hidden\"></div>
\t</div>
</form>
";
    }

    public function getTemplateName()
    {
        return "_components/widgets/QuickPost/body";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 30,  120 => 29,  114 => 28,  107 => 22,  102 => 21,  96 => 20,  86 => 29,  77 => 27,  73 => 22,  68 => 21,  62 => 20,  54 => 19,  51 => 18,  47 => 16,  38 => 9,  33 => 7,  23 => 3,  21 => 2,  19 => 1,  757 => 244,  754 => 243,  746 => 251,  737 => 249,  734 => 248,  730 => 246,  728 => 243,  724 => 241,  717 => 238,  714 => 237,  711 => 236,  700 => 235,  697 => 234,  694 => 233,  691 => 232,  688 => 231,  682 => 227,  678 => 226,  674 => 225,  670 => 224,  667 => 223,  661 => 217,  654 => 214,  651 => 213,  649 => 212,  646 => 211,  639 => 209,  636 => 208,  633 => 207,  628 => 204,  615 => 202,  610 => 201,  606 => 199,  602 => 198,  599 => 197,  590 => 285,  584 => 283,  581 => 282,  576 => 281,  570 => 278,  565 => 277,  561 => 275,  558 => 274,  553 => 271,  545 => 269,  540 => 268,  528 => 265,  517 => 263,  514 => 262,  507 => 261,  496 => 260,  490 => 256,  488 => 223,  483 => 220,  481 => 197,  475 => 193,  469 => 192,  459 => 190,  455 => 189,  450 => 188,  446 => 187,  436 => 179,  428 => 176,  422 => 174,  419 => 173,  416 => 172,  407 => 171,  401 => 170,  396 => 169,  383 => 165,  370 => 159,  362 => 157,  357 => 156,  351 => 155,  345 => 152,  341 => 150,  336 => 147,  329 => 145,  313 => 143,  308 => 142,  301 => 140,  296 => 139,  291 => 137,  283 => 134,  280 => 133,  277 => 132,  274 => 131,  271 => 130,  268 => 129,  265 => 128,  262 => 127,  254 => 123,  247 => 122,  239 => 121,  236 => 120,  232 => 119,  229 => 118,  226 => 117,  222 => 116,  218 => 115,  214 => 114,  210 => 113,  207 => 112,  204 => 111,  201 => 110,  198 => 109,  195 => 108,  192 => 107,  189 => 106,  186 => 105,  182 => 104,  177 => 103,  174 => 102,  168 => 98,  165 => 97,  162 => 96,  159 => 95,  155 => 93,  145 => 91,  140 => 90,  137 => 89,  133 => 88,  129 => 87,  126 => 86,  123 => 85,  117 => 83,  112 => 82,  109 => 81,  30 => 4,  28 => 6,  128 => 32,  122 => 35,  119 => 34,  111 => 27,  106 => 29,  94 => 27,  88 => 26,  84 => 25,  80 => 28,  76 => 23,  71 => 22,  66 => 21,  63 => 20,  56 => 16,  52 => 14,  48 => 13,  45 => 12,  42 => 10,  31 => 7,  29 => 6,  27 => 3,  25 => 2,);
    }
}

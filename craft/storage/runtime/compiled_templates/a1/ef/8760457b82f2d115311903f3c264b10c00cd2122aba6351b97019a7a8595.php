<?php

/* _elements/sources */
class __TwigTemplate_a1ef8760457b82f2d115311903f3c264b10c00cd2122aba6351b97019a7a8595 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul>
\t";
        // line 2
        if (isset($context["sources"])) { $_sources_ = $context["sources"]; } else { $_sources_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_sources_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["source"]) {
            // line 3
            echo "\t\t";
            if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
            if ($this->getAttribute($_source_, "heading", array(), "any", true, true)) {
                // line 4
                echo "\t\t\t<li class=\"heading\"><span>";
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_source_, "heading"), "html", null, true);
                echo "</span></li>
\t\t";
            } else {
                // line 6
                echo "\t\t\t<li>
\t\t\t\t<a data-key=\"";
                // line 7
                if (isset($context["key"])) { $_key_ = $context["key"]; } else { $_key_ = null; }
                echo twig_escape_filter($this->env, $_key_, "html", null, true);
                echo "\"";
                // line 8
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                if (($this->getAttribute($_source_, "hasThumbs", array(), "any", true, true) && $this->getAttribute($_source_, "hasThumbs"))) {
                    echo " data-has-thumbs";
                }
                // line 9
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                if (($this->getAttribute($_source_, "structureId", array(), "any", true, true) && $this->getAttribute($_source_, "structureId"))) {
                    echo " data-has-structure";
                }
                // line 10
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                if ($this->getAttribute($_source_, "data", array(), "any", true, true)) {
                    // line 11
                    if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($_source_, "data"));
                    foreach ($context['_seq'] as $context["dataKey"] => $context["dataVal"]) {
                        echo " data-";
                        if (isset($context["dataKey"])) { $_dataKey_ = $context["dataKey"]; } else { $_dataKey_ = null; }
                        echo twig_escape_filter($this->env, $_dataKey_, "html", null, true);
                        echo "=\"";
                        if (isset($context["dataVal"])) { $_dataVal_ = $context["dataVal"]; } else { $_dataVal_ = null; }
                        echo twig_escape_filter($this->env, $_dataVal_, "html", null, true);
                        echo "\"";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['dataKey'], $context['dataVal'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                }
                // line 13
                echo ">";
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_source_, "label"), "html", null, true);
                echo "</a>
\t\t\t\t";
                // line 14
                if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                if (($this->getAttribute($_source_, "nested", array(), "any", true, true) && (!twig_test_empty($this->getAttribute($_source_, "nested"))))) {
                    // line 15
                    echo "\t\t\t\t\t<div class=\"toggle\"></div>
\t\t\t\t\t";
                    // line 16
                    if (isset($context["source"])) { $_source_ = $context["source"]; } else { $_source_ = null; }
                    $this->env->loadTemplate("_elements/sources")->display(array_merge($context, array("sources" => $this->getAttribute($_source_, "nested"))));
                    // line 19
                    echo "\t\t\t\t";
                }
                // line 20
                echo "\t\t\t</li>
\t\t";
            }
            // line 22
            echo "\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['source'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "_elements/sources";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 23,  106 => 20,  103 => 19,  100 => 16,  94 => 14,  88 => 13,  68 => 10,  58 => 8,  54 => 7,  40 => 3,  22 => 2,  177 => 55,  174 => 54,  172 => 52,  168 => 50,  165 => 49,  159 => 45,  140 => 43,  135 => 42,  128 => 39,  125 => 38,  122 => 37,  117 => 36,  114 => 35,  110 => 22,  105 => 33,  101 => 32,  93 => 30,  71 => 11,  66 => 23,  62 => 22,  56 => 19,  53 => 18,  50 => 17,  43 => 12,  37 => 11,  27 => 8,  21 => 3,  19 => 1,  102 => 35,  97 => 15,  92 => 33,  87 => 26,  81 => 31,  78 => 30,  72 => 18,  70 => 17,  67 => 16,  63 => 9,  60 => 14,  55 => 40,  51 => 6,  49 => 25,  46 => 24,  44 => 4,  41 => 11,  38 => 8,  35 => 7,  33 => 10,  29 => 4,  32 => 7,  30 => 9,  28 => 5,  26 => 3,  24 => 2,);
    }
}

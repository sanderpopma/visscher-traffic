<?php

/* _components/widgets/RecentEntries/body */
class __TwigTemplate_7ae1809224da4e6cbd3a72234ecd1e25e762cd1e32b87f706a39ec9734570d35 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"recententries-container\">
\t";
        // line 2
        if (isset($context["entries"])) { $_entries_ = $context["entries"]; } else { $_entries_ = null; }
        if (twig_length_filter($this->env, $_entries_)) {
            // line 3
            echo "\t\t<table class=\"data fullwidth\">
\t\t\t";
            // line 4
            if (isset($context["entries"])) { $_entries_ = $context["entries"]; } else { $_entries_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_entries_);
            foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
                // line 5
                echo "\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<a href=\"";
                // line 7
                if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "getCpEditUrl", array(), "method"), "html", null, true);
                echo "\">";
                if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_entry_, "title"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t<span class=\"light\">
\t\t\t\t\t\t\t";
                // line 9
                if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
                if ($this->getAttribute($_entry_, "postDate")) {
                    // line 10
                    echo "\t\t\t\t\t\t\t\t";
                    if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_entry_, "postDate"), "localeDate", array(), "method"), "html", null, true);
                    echo "
\t\t\t\t\t\t\t";
                }
                // line 12
                echo "\t\t\t\t\t\t\t";
                if (isset($context["CraftEdition"])) { $_CraftEdition_ = $context["CraftEdition"]; } else { $_CraftEdition_ = null; }
                if (isset($context["CraftClient"])) { $_CraftClient_ = $context["CraftClient"]; } else { $_CraftClient_ = null; }
                if (($_CraftEdition_ >= $_CraftClient_)) {
                    // line 13
                    echo "\t\t\t\t\t\t\t\t";
                    if (isset($context["entry"])) { $_entry_ = $context["entry"]; } else { $_entry_ = null; }
                    echo twig_escape_filter($this->env, (($this->getAttribute($_entry_, "author")) ? (\Craft\Craft::t("by {author}", array("author" => $this->getAttribute($this->getAttribute($_entry_, "author"), "username")))) : ("")), "html", null, true);
                    echo "
\t\t\t\t\t\t\t";
                }
                // line 15
                echo "\t\t\t\t\t\t</span>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "\t\t</table>
\t";
        } else {
            // line 21
            echo "\t\t<p>";
            echo twig_escape_filter($this->env, \Craft\Craft::t("No entries exist yet."), "html", null, true);
            echo "</p>
\t";
        }
        // line 23
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "_components/widgets/RecentEntries/body";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 23,  81 => 21,  61 => 13,  82 => 19,  79 => 18,  74 => 16,  70 => 14,  65 => 13,  49 => 10,  46 => 9,  37 => 7,  32 => 5,  22 => 2,  124 => 30,  120 => 29,  114 => 28,  107 => 22,  102 => 21,  96 => 20,  86 => 20,  77 => 19,  73 => 22,  68 => 15,  62 => 12,  54 => 19,  51 => 18,  47 => 16,  38 => 9,  33 => 5,  23 => 3,  21 => 2,  19 => 1,  757 => 244,  754 => 243,  746 => 251,  737 => 249,  734 => 248,  730 => 246,  728 => 243,  724 => 241,  717 => 238,  714 => 237,  711 => 236,  700 => 235,  697 => 234,  694 => 233,  691 => 232,  688 => 231,  682 => 227,  678 => 226,  674 => 225,  670 => 224,  667 => 223,  661 => 217,  654 => 214,  651 => 213,  649 => 212,  646 => 211,  639 => 209,  636 => 208,  633 => 207,  628 => 204,  615 => 202,  610 => 201,  606 => 199,  602 => 198,  599 => 197,  590 => 285,  584 => 283,  581 => 282,  576 => 281,  570 => 278,  565 => 277,  561 => 275,  558 => 274,  553 => 271,  545 => 269,  540 => 268,  528 => 265,  517 => 263,  514 => 262,  507 => 261,  496 => 260,  490 => 256,  488 => 223,  483 => 220,  481 => 197,  475 => 193,  469 => 192,  459 => 190,  455 => 189,  450 => 188,  446 => 187,  436 => 179,  428 => 176,  422 => 174,  419 => 173,  416 => 172,  407 => 171,  401 => 170,  396 => 169,  383 => 165,  370 => 159,  362 => 157,  357 => 156,  351 => 155,  345 => 152,  341 => 150,  336 => 147,  329 => 145,  313 => 143,  308 => 142,  301 => 140,  296 => 139,  291 => 137,  283 => 134,  280 => 133,  277 => 132,  274 => 131,  271 => 130,  268 => 129,  265 => 128,  262 => 127,  254 => 123,  247 => 122,  239 => 121,  236 => 120,  232 => 119,  229 => 118,  226 => 117,  222 => 116,  218 => 115,  214 => 114,  210 => 113,  207 => 112,  204 => 111,  201 => 110,  198 => 109,  195 => 108,  192 => 107,  189 => 106,  186 => 105,  182 => 104,  177 => 103,  174 => 102,  168 => 98,  165 => 97,  162 => 96,  159 => 95,  155 => 93,  145 => 91,  140 => 90,  137 => 89,  133 => 88,  129 => 87,  126 => 86,  123 => 85,  117 => 83,  112 => 82,  109 => 81,  30 => 4,  28 => 4,  128 => 32,  122 => 35,  119 => 34,  111 => 27,  106 => 29,  94 => 27,  88 => 26,  84 => 25,  80 => 28,  76 => 23,  71 => 22,  66 => 21,  63 => 20,  56 => 12,  52 => 10,  48 => 13,  45 => 12,  42 => 7,  31 => 7,  29 => 6,  27 => 3,  25 => 3,);
    }
}

<?php

/* _includes/forms/errorList */
class __TwigTemplate_ea171ea2f951579f5ad953e90d976c1e17e08f91c44767933050b2548ce981e5 extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
        if ($_errors_) {
            // line 2
            echo "\t<ul class=\"errors\">
\t\t";
            // line 3
            if (isset($context["errors"])) { $_errors_ = $context["errors"]; } else { $_errors_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_errors_);
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 4
                echo "\t\t\t<li>";
                if (isset($context["error"])) { $_error_ = $context["error"]; } else { $_error_ = null; }
                echo twig_escape_filter($this->env, $_error_, "html", null, true);
                echo "</li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 6
            echo "\t</ul>";
        }
    }

    public function getTemplateName()
    {
        return "_includes/forms/errorList";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  140 => 31,  136 => 30,  129 => 28,  120 => 25,  108 => 22,  101 => 20,  97 => 19,  62 => 11,  32 => 5,  112 => 24,  94 => 22,  89 => 21,  78 => 19,  49 => 15,  34 => 13,  1062 => 185,  1056 => 184,  1045 => 183,  1030 => 179,  1019 => 178,  1004 => 174,  993 => 173,  978 => 169,  967 => 168,  952 => 164,  941 => 163,  929 => 159,  922 => 157,  919 => 156,  914 => 155,  894 => 154,  890 => 153,  879 => 152,  864 => 148,  853 => 147,  838 => 143,  827 => 142,  812 => 138,  801 => 137,  786 => 133,  775 => 132,  760 => 128,  749 => 127,  734 => 123,  723 => 122,  708 => 118,  697 => 117,  682 => 113,  671 => 112,  656 => 108,  645 => 107,  631 => 103,  619 => 102,  606 => 95,  595 => 94,  582 => 90,  571 => 89,  558 => 85,  547 => 84,  534 => 80,  523 => 79,  510 => 75,  499 => 74,  486 => 70,  475 => 69,  462 => 65,  451 => 64,  438 => 60,  427 => 59,  414 => 55,  403 => 54,  390 => 50,  379 => 49,  366 => 45,  355 => 44,  342 => 40,  331 => 39,  318 => 35,  307 => 34,  283 => 29,  259 => 24,  246 => 20,  235 => 19,  211 => 14,  199 => 10,  188 => 9,  176 => 2,  165 => 1,  159 => 181,  155 => 176,  151 => 171,  143 => 161,  139 => 150,  135 => 145,  131 => 140,  127 => 135,  119 => 125,  115 => 120,  111 => 115,  103 => 105,  91 => 92,  83 => 20,  75 => 72,  71 => 67,  59 => 10,  55 => 47,  43 => 32,  39 => 27,  35 => 6,  23 => 7,  19 => 1,  86 => 16,  81 => 22,  64 => 12,  57 => 9,  51 => 42,  46 => 22,  41 => 21,  24 => 3,  22 => 2,  300 => 89,  294 => 30,  281 => 86,  276 => 85,  270 => 25,  265 => 83,  257 => 79,  251 => 78,  245 => 76,  239 => 75,  236 => 74,  231 => 73,  227 => 72,  222 => 15,  217 => 70,  212 => 69,  207 => 68,  202 => 67,  197 => 66,  192 => 65,  187 => 64,  182 => 63,  177 => 62,  172 => 61,  162 => 59,  157 => 58,  147 => 166,  138 => 54,  134 => 53,  130 => 52,  126 => 27,  122 => 50,  117 => 49,  107 => 110,  102 => 39,  99 => 23,  93 => 36,  88 => 34,  85 => 32,  82 => 14,  79 => 13,  76 => 29,  74 => 28,  72 => 27,  70 => 18,  67 => 62,  65 => 12,  60 => 10,  56 => 16,  54 => 9,  48 => 23,  45 => 11,  42 => 14,  36 => 9,  30 => 4,  171 => 61,  167 => 60,  152 => 57,  145 => 48,  142 => 55,  133 => 41,  128 => 40,  123 => 130,  116 => 35,  113 => 34,  109 => 33,  104 => 24,  95 => 97,  90 => 17,  87 => 87,  69 => 26,  66 => 25,  63 => 17,  58 => 23,  47 => 37,  44 => 13,  38 => 8,  33 => 6,  31 => 17,  29 => 4,  27 => 12,  25 => 3,);
    }
}

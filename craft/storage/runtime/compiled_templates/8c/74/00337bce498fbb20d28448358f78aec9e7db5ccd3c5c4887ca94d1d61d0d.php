<?php

/* settings/plugins/_settings */
class __TwigTemplate_8c7400337bce498fbb20d28448358f78aec9e7db5ccd3c5c4887ca94d1d61d0d extends Craft\BaseTemplate
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_layouts/cp");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_layouts/cp";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((!array_key_exists("plugin", $context))) {
            // line 2
            if (isset($context["craft"])) { $_craft_ = $context["craft"]; } else { $_craft_ = null; }
            if (isset($context["pluginClass"])) { $_pluginClass_ = $context["pluginClass"]; } else { $_pluginClass_ = null; }
            $context["plugin"] = $this->getAttribute($this->getAttribute($_craft_, "plugins"), "getPlugin", array(0 => $_pluginClass_), "method");
            // line 3
            if (isset($context["plugin"])) { $_plugin_ = $context["plugin"]; } else { $_plugin_ = null; }
            if ((!$_plugin_)) {
                throw new \Craft\HttpException(404);
            }
        }
        // line 6
        $context["crumbs"] = array(0 => array("label" => \Craft\Craft::t("Settings"), "url" => \Craft\UrlHelper::getUrl("settings")), 1 => array("label" => \Craft\Craft::t("Plugins"), "url" => \Craft\UrlHelper::getUrl("settings/plugins")));
        // line 13
        if (isset($context["plugin"])) { $_plugin_ = $context["plugin"]; } else { $_plugin_ = null; }
        $context["title"] = $this->getAttribute($_plugin_, "name");
        // line 14
        if (isset($context["title"])) { $_title_ = $context["title"]; } else { $_title_ = null; }
        $context["docTitle"] = (($_title_ . " - ") . \Craft\Craft::t("Plugins"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "\t<form method=\"post\" accept-charset=\"UTF-8\" data-saveshortcut>
\t\t<input type=\"hidden\" name=\"action\" value=\"plugins/savePluginSettings\">
\t\t<input type=\"hidden\" name=\"pluginClass\" value=\"";
        // line 20
        if (isset($context["plugin"])) { $_plugin_ = $context["plugin"]; } else { $_plugin_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_plugin_, "classHandle"), "html", null, true);
        echo "\">
\t\t<input type=\"hidden\" name=\"redirect\" value=\"settings/plugins\">
\t\t";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('craft')->getCsrfInputFunction(), "html", null, true);
        echo "

\t\t";
        // line 24
        $_namespace = "settings";
        if ($_namespace) {
            $_originalNamespace = \Craft\craft()->templates->getNamespace();
            \Craft\craft()->templates->setNamespace(\Craft\craft()->templates->namespaceInputName($_namespace));
            ob_start();
            try {
                // line 25
                echo "\t\t\t";
                if (isset($context["plugin"])) { $_plugin_ = $context["plugin"]; } else { $_plugin_ = null; }
                echo $this->getAttribute($_plugin_, "getSettingsHtml", array(), "method");
                echo "
\t\t";
            } catch (Exception $e) {
                ob_end_clean();

                throw $e;
            }
            echo \Craft\craft()->templates->namespaceInputs(ob_get_clean(), $_namespace);
            \Craft\craft()->templates->setNamespace($_originalNamespace);
        } else {
            echo "\t\t\t";
            if (isset($context["plugin"])) { $_plugin_ = $context["plugin"]; } else { $_plugin_ = null; }
            echo $this->getAttribute($_plugin_, "getSettingsHtml", array(), "method");
            echo "
\t\t";
        }
        unset($_originalNamespace, $_namespace);
        // line 27
        echo "
\t\t<div class=\"buttons\">
\t\t\t<input class=\"btn submit\" type=\"submit\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, \Craft\Craft::t("Save"), "html", null, true);
        echo "\">
\t\t</div>
\t</form>
";
    }

    public function getTemplateName()
    {
        return "settings/plugins/_settings";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 29,  94 => 27,  73 => 25,  66 => 24,  61 => 22,  55 => 20,  51 => 18,  48 => 17,  42 => 14,  39 => 13,  37 => 6,  31 => 3,  27 => 2,  25 => 1,);
    }
}

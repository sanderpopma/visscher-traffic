<?php
namespace Craft;

class ContactFormModel extends BaseModel
{
	protected function defineAttributes()
	{
		return array(
			'fromName'   => array(AttributeType::String, 'label' => 'De naam'),
			'fromEmail'  => array(AttributeType::Email,  'required' => true, 'label' => 'Het emailadres'),
			'message'    => array(AttributeType::String, 'required' => true, 'label' => 'Bericht'),
			'subject'    => array(AttributeType::String, 'label' => 'Onderwerp'),
			'attachment' => AttributeType::Mixed,
		);
	}
}

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
?>
<?php header("Content-type: text/css"); ?>
body {
  color : #000000;
  background : #ffffff;
  font-family : <?=$GLOBALS['TekstLettertype']?>;
  font-size : 12pt;
}
a {
  text-decoration : underline;
  color : <?=$GLOBALS['KleurKlantBlauw']?>;
  font-weight : bold;
}
h1
{
  color : <?=$GLOBALS['KleurKlantBlauw']?>;
}
b
{
	color : black;
}
#menucontainer, #rightpatroon, #bottom
{
	display : none;
}
.headermenu
{
	display : none;
}
#bottomcontact
{
	display : inline;
}
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
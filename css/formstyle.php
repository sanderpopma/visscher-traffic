<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
?>
<?php header("Content-type: text/css"); ?>

.txt
{
background-color: <?=$GLOBALS['KleurBGInputText']?>;
border: solid 1px <?=$GLOBALS['KleurKlantBlauw']?>;
height: 14px;
margin-top: 2px;
margin-bottom: 2px;
font-size: 11px;
}

.txtklein
{
background-color: <?=$GLOBALS['KleurKlantWit']?>;
border: solid 1px black;
height: 14px;
font-size: 10px;
width: 20px;
}

.txtarea
{
border: solid 1px <?=$GLOBALS['KleurKlantBlauw']?>;
background-color: <?=$GLOBALS['KleurBGInputText']?>;
margin-top: 2px;
margin-bottom: 2px;
font-size: 11px;
}

.btn
{
background-color: <?=$GLOBALS['KleurKlantBlauw']?>;
border-style: solid;
border-width: 1px;
border-color: black;
font-size: 11px;
color: <?=$GLOBALS['KleurBGInputText']?>;
margin-top: 2px;
margin-bottom: 2px;
height: 20px;
}

.btnklein
{
background-color: black;
border-style: solid;
border-width: 1px;
border-color: black;
font-size: 10px;
color: white;
height: 18px;
margin-top: 2px;
margin-bottom: 2px;
}

.frmheader b
{
	color: <?=$GLOBALS['KleurKlantOranje']?>;
	font-weight: bold;
}
.frmheader
{
	padding-top: 5px;
	height: 20px;
}
#extraopmerkingen
{
	display: none;
}
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
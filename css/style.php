<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
?>
<?php header("Content-type: text/css"); ?>
*
{
    font-family: <?=$GLOBALS['TekstLettertype']?>;
    font-size: <?=$GLOBALS['TekstLettergrootte']?>;
    color: <?=$GLOBALS['TekstLetterKleur']?>;
}
body
{
    margin: 0px;
    background-color: <?=$GLOBALS['KleurBGWebsite']?>;
}

a
{
    color: <?=$GLOBALS['LinkKleur']?>;
    font-weight: bold;
	text-decoration: none;
}
a:hover
{
    color: <?=$GLOBALS['LinkOverKleur']?>;
	text-decoration: underline;
}
a.menu
{
	color: <?=$GLOBALS['TekstMenuKleur']?>;
	font-weight: bold;
	font-size: <?=$GLOBALS['TekstMenuLettergrootte']?>;
	text-decoration: none;
}
a.menusel
{
	color: <?=$GLOBALS['TekstMenuSelKleur']?>;
	font-weight: bold;
	font-size: <?=$GLOBALS['TekstMenuLettergrootte']?>;
	text-decoration: none;
}
a.menu:hover, a.menusel:hover
{
	text-decoration: underline;
}
a.submenu
{
    color: <?=$GLOBALS['TekstSubMenuKleur']?>;
    font-weight: bold;
    font-size: <?=$GLOBALS['TekstSubMenuLettergrootte']?>;
    text-decoration: none;
}
a.submenusel
{
    color: <?=$GLOBALS['TekstSubMenuSelKleur']?>;
    font-weight: bold;
    font-size: <?=$GLOBALS['TekstSubMenuLettergrootte']?>;
    text-decoration: none;
}
a.submenu:hover, a.submenusel:hover
{
    text-decoration: underline;
    color: <?=$GLOBALS['TekstSubMenuKleurOver']?>;
}
a.faqquestion
{

}
span.answer {
display: none;
padding: 5px 12px;
}
hr.streep
{
	height: 1px;
	width: 100%;
	border-top:1px #CCCCCC dashed;
	border-bottom: none;
	border-left: none;
	border-right: none;
}
h1
{
    font-size: <?=$GLOBALS['TekstH1Lettergrootte']?>;
    font-weight: bold;
    color: <?=$GLOBALS['TekstH1Kleur']?>;
}

ul.submenu li
{
    color: <?=$GLOBALS['TekstSubMenuKleur']?>;
	list-style: disc outside;
    font-size: <?=$GLOBALS['TekstSubMenuLettergrootte']?>;
    text-indent: -10px;
}
.menuadres
{
    color: <?=$GLOBALS['TekstMenuAdresKleur']?>;
    text-decoration: none;
    font-size: <?=$GLOBALS['TekstMenuAdresLettergrootte']?>;
    font-family: <?=$GLOBALS['TekstMenuAdresLettertype']?>;
}
.absmiddle
{
  vertical-align: middle;
}
.border
{
    border: solid black 1px;
}
.noborder
{
    border: 0;
}
td.filekop
{
    color : #000000;
    height : 24px;
    padding : 4px;
    padding-right : 12px;
    font-weight : bold;
	background-color : <?=$GLOBALS['KleurDownloadBox']?>;
	border : 1px solid <?=$GLOBALS['KleurDownloadKader']?>;
	width : 100%;
}
a.filekop
{
	color: <?=$GLOBALS['KleurDownloadLinkTekst']?>;
}
.slide
{
	position: absolute;
	top: 0;
	left: 0;
}

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
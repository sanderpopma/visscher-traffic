<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");

$artsize = "11px";
$KleurKlantBasis = "#000000";
$KleurKlantSteun = "#cccccc";
?>
<?php header("Content-type: text/css"); ?>
html
{
	overflow-y: scroll;
}
body
{
	background-color: #c4c4c4;
}
a
{
	text-decoration: underline;
	font-weight: normal;
}
a.klein
{
	font-weight: normal;
	font-size: 10px;
}
a.ilink, a.elink
{
	font-weight: bold;
	color: <?=$GLOBALS['ILinkKleur']?>;
	text-decoration: none;
}
a.ilink:hover, a.elink:hover
{
	text-decoration: underline;
}
hr.streep
{
	border: 0;
	border-top: solid 1px black;
}
.submenuheader
{
	font-weight: bold;
}
div.menustreep
{
	height: 1px;
	width: 150px;
	background-color: <?=$GLOBALS['KleurWit']?>;
}
div.menuspacer
{
	clear: both;
	height: 3px;
	width: 100%;
}
.breadcrumb
{
	color: #000000;
	font-size: 10px;
	font-weight: normal;
	text-decoration: none;
}
a.breadcrumb:hover
{
	color: #000000;
	font-weight: normal;
	text-decoration: underline;
}
div.artikelbox
{
	background-color: white;
}
a.artikelbox, a.artikelbox:hover
{
	color: black;
	font-weight: bold;
	text-decoration: underline;
	text-align: center;
}
.artikelboxbeschr
{
	font-size: 10px;
	width: 100%;
	text-align: left;
}
a.bestel
{
	font-weight: bold;
	font-size: 13px;
}
.arttabel
{
	border: solid 1px <?=$GLOBALS['KleurKlantBasis']?>;
}
.artkop
{
	font-size: <?=$artsize?>;
	font-weight: bold;
	background-color: <?=$GLOBALS['KleurKlantBasis']?>;
	color: <?=$GLOBALS['KleurKlantWit']?>;
}
.artregel
{
	font-size: <?=$artsize?>;
	border-bottom: solid 1px <?=$GLOBALS['KleurKlantBasis']?>;
}
.artregelsel
{
	font-size: <?=$artsize?>;
	border-bottom: solid 1px <?=$GLOBALS['KleurKlantBasis']?>;
	color: <?=$GLOBALS['KleurKlantSteun']?>;
}
.artvoet
{
	font-size: <?=$artsize?>;
	background-color: <?=$GLOBALS['KleurKlantWit']?>;
	color: <?=$GLOBALS['KleurKlantWit']?>;
}
#pagecontainer
{
	background-color: <?=$GLOBALS['KleurBGTekst']?>;
	margin: auto;
	top: 0;
	left: 0;
	border: 0;
}
#header1
{
	height: <?=$GLOBALS['HoogteWebsiteBoven1']?>px;
	background-color: <?=$GLOBALS['KleurBGBoven1']?>;
	background: url(<?=$GLOBALS['AppImgRoot']?>/vtboven1.jpg) no-repeat;
}
#header2
{
	width: <?=$GLOBALS['BreedteWebsite']?>px;
	background-color: <?=$GLOBALS['KleurBGBoven2']?>;
	height: <?=$GLOBALS['HoogteWebsiteBoven2']?>px;

	}
#contentcontainer
{
	width: <?=$GLOBALS['BreedteWebsite']?>px;
	background-color: <?=$GLOBALS['KleurBGTekst']?>;
	position: relative;
}
#menucontainer
{
	width: <?=$GLOBALS['BreedteWebsiteLinks']?>px;
	background: url(<?=$GLOBALS['AppImgRoot']?>/bgmenu.png) no-repeat;
	background-color: <?=$GLOBALS['KleurBGMenu']?>;
	position: relative;
}
#menu
{
	width: <?=$GLOBALS['BreedteWebsiteLinks']?>px;
	text-align: left;
	min-height: 520px;
	padding-top: 6px;
}
#menu ul
{
	list-style: none;
	padding: 0;
	margin: 0;
}
#menu ul li
{
    margin: 0;
    padding: 0;
    padding-left: 16px;
    padding-right: 3px;
}
#menu ul ul li
{
    margin: 0;
    padding: 0;
    padding-left: 6px;
    padding-right: 3px;
}
#menu ul ul ul li
{
    margin: 0;
    padding: 0;
    padding-left: 20px;
    padding-right: 3px;
}
.menu, a.menu
{
    line-height: 28px;
    font-weight: normal;
    font-size: 18px;
}
.menusel, a.menusel
{
    line-height: 28px;
    font-weight: normal;
	text-decoration: none;
	font-size: 18px;
}
.menuvaltop
{
    line-height: 20px;
    font-weight: bold;
    text-decoration: none;
}
.submenu
{
    padding-left: 24px;
    line-height: 20px;
    font-weight: bold;
}
.submenusel
{
    padding-left: 24px;
    line-height: 20px;
    font-weight: bold;
}
.subsubmenu
{
    padding-left: 32px;
    line-height: 20px;
	color: <?=$GLOBALS['TekstSubSubMenuKleur']?>;
	font-size: <?=$GLOBALS['TekstSubSubMenuLettergrootte']?>;
	font-weight: normal;
	text-decoration: none;
}
.subsubmenusel
{
    padding-left: 32px;
    line-height: 20px;
	font-size: <?=$GLOBALS['TekstSubSubMenuLettergrootte']?>;
	color: <?=$GLOBALS['TekstSubSubMenuSelKleur']?>;
	font-weight: normal;
	text-decoration: underline
}
.subsubmenu:hover, .subsubmenusel:hover
{
	color: <?=$GLOBALS['TekstSubSubMenuKleurOver']?>;
}

#textcontainer, #textcontainerhome
{
	width: <?=$GLOBALS['BreedteWebsiteTekstdeel']-72?>px;
	padding: 20px;
	padding-left: 36px;
	padding-right: 36px;
	background: url(<?=$GLOBALS['AppImgRoot']?>/vtmain.jpg) no-repeat;
	background-color: <?=$GLOBALS['KleurBGTekst']?>;
	height: 325px;
}
#rightcontainer
{
	width: <?=$GLOBALS['BreedteWebsiteRechts']?>px;
	background-color: <?=$GLOBALS['KleurBGRechts']?>;
}
#refcontainer
{
	width: <?=$GLOBALS['BreedteWebsiteTekstdeel']+$GLOBALS['BreedteWebsiteRechts']+16-10-40?>px;
	padding: 20px;
}
.kadercontainer
{
	padding-top: 5px;
	padding-left: 5px;
	padding-right: 5px;
}
#bottom
{
	height: <?=$GLOBALS['HoogteWebsiteOnder']?>px;
	line-height: <?=$GLOBALS['HoogteWebsiteOnder']?>px;
	text-align: center;
	font-size: 10px;
	color: <?=$GLOBALS['TekstOnderKleur']?>;
	background-color: <?=$GLOBALS['KleurBGOnder']?>;
	background: url(<?=$GLOBALS['AppImgRoot']?>/vtonder.jpg) no-repeat;
}
#bottom a
{
	font-size: 10px;
	font-weight: normal;
	color: white;
	text-decoration: none;
}
h1
{
	margin: 0;
	padding: 0;
	font-family: <?=$GLOBALS['TekstH1Lettertype']?>;
	font-size: <?=$GLOBALS['TekstH1Lettergrootte']?>;
	font-weight: normal;
	margin-top: -5px;
	margin-bottom: 10px;
	color: <?=$GLOBALS['TekstH1Kleur']?>;
}
h2
{
	margin: 0;
	padding: 0;
	color: <?=$GLOBALS['TekstH2Kleur']?>;
	font-size: <?=$GLOBALS['TekstH2Lettergrootte']?>;
	font-weight: normal;
}
.zwart
{
	color: black;
}
#adresmenu
{
	width: 140px;
	margin-left: 20px;
	margin-right: 20px;
	color: <?=$GLOBALS['TekstMenuAdresKleur']?>;
	font-size: <?=$GLOBALS['TekstMenuAdresLettergrootte']?>;
	font-family: <?=$GLOBALS['TekstMenuAdresLettertype']?>;
}
#adresmenu b, #adresmenu a
{
	color: <?=$GLOBALS['TekstMenuAdresKleur']?>;
	font-size: <?=$GLOBALS['TekstMenuAdresLettergrootte']?>;
}
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
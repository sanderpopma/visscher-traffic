<?
// ========== ALGEMENE FUNCTIES ============
function OpenWebForm($PostenNaar, $Formnaam, $Form_Breedte)
{
	$tmpNaam = "mijnform";
	if ($Formnaam!="")
	{
		$tmpNaam = $Formnaam;
	}
	return "<form id='" . $tmpNaam . "' name='" . $tmpNaam . "' action='" . $PostenNaar . "' method='post'>" .
		"<table border='0' cellpadding='0' cellspacing='0' width='" . $Form_Breedte . "'>" . "";
}
// ============== FORMULIER OPENEN =============
function FrmWebTekstRegel($Titel, $Waarde)
{
	return "<tr><td>" . $Titel . "</td><td>" . $Waarde . "</td></tr>";
}
function FrmWebKoptekst($Titel)
{
	return "<tr><td colspan='3' class='frmheader'><b>" . $Titel . "</b></td></tr>";
}
// ========================= TEXT ========================
function FrmWebText($Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr><td>" . $Titel . "</td><td><input class='txt' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}
function FrmWebOnEventText($OnEvent, $OnEventFunctienaam, $Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr><td>" . $Titel . "</td><td><input " . $OnEvent . "='" . $OnEventFunctienaam . "' class='txt' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}
function FrmWebDatum($Titel, $Veldnaam, $Wrd)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	if ($Waarde!="") {
		$Waarde = MaakDatum($Waarde);
	}
	return "<tr><td>" . MaakDatumLabel($Titel) . "</td><td><input class='txt' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='16' maxlength='10'> <img src='" . $GLOBALS['ApplicatieRoot'] . "/sybit/icons/kalender2.gif' width='16' height='16' alt='Kies Datum' border='0' onClick=\"popUpCalendar(this, document.getElementById('" . $Veldnaam . "'), 'd-m-yyyy', 0, 0)\"></td></tr>";
}
function FrmWebPswdText($Titel, $Veldnaam, $Waarde, $Breedte, $MaxLengte)
{
	return "<tr><td>" . $Titel . "</td><td><input class='txt' type='password' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}

// ========================= TEXTAREA ========================

function FrmWebTextArea($Titel, $Veldnaam, $Waarde, $Rijen, $Kolommen)
{
    return "<tr valign='top'><td>" . $Titel . "</td><td><textarea class='txtarea' id='" . $Veldnaam . "' name='" . $Veldnaam . "' rows='" . $Rijen . "' cols='" . $Kolommen . "'>" . $Waarde . "</textarea></td></tr>";
}

function FrmWebSelectByQuery($Titel, $Veldnaam, $CurrValue, $sqlQuery, $OptionValue, $OptionDescription, $MetLegeWaarde)
{
	$HuidigeWaarde = $CurrValue;
	if ($HuidigeWaarde==null) {
	 	$HuidigeWaarde="";
	 }
	$ReturnWaarde = "<tr valign='top'><td>" . $Titel . "</td><td><select name='" . $Veldnaam . "' size=1 class='slct' id='" . $Veldnaam . "'>";
	if ($MetLegeWaarde==true) {
		$ReturnWaarde = $ReturnWaarde . "<option value=''></option>";

	 }
	$query_rsKeuze = $sqlQuery;
	$rsKeuze = mysql_query($query_rsKeuze, $GLOBALS['conn']) or die(mysql_error());
	$row_rsKeuze = mysql_fetch_assoc($rsKeuze);
	do
	{
		$SelTxt = "";
		if ($row_rsKeuze[$OptionValue].""==$HuidigeWaarde."") {
		 	$SelTxt = " SELECTED ";
		 }
		$ReturnWaarde = $ReturnWaarde . "<option value='" . $row_rsKeuze[$OptionValue] . "'" . $SelTxt . ">" . $row_rsKeuze[$OptionDescription] . "</option>";
	}
	while ($row_rsKeuze = mysql_fetch_assoc($rsKeuze));
	$ReturnWaarde = $ReturnWaarde . "</select></td></tr>";
	return $ReturnWaarde;
}

// ====================== OVERIGE ==================
function FrmWebCheckbox($lbl_waarde, $veld_naam, $veld_waarde)
{
	$chk=false;
	$tmpWrd = " ";
	if($veld_waarde==1)
	{
		$chk=true;
		$tmpWrd = " CHECKED ";
	}
    ?>
    <tr>
    <td><?=$lbl_waarde?></td>
    <td><input name="<?=$veld_naam?>" <?=$tmpWrd?> type="checkbox" class="chk"></td>
    </tr>
    <?
}
function FrmWebEventCheckbox($lbl_waarde, $veld_naam, $veld_waarde, $event_naam, $links_rechts)
{
	$chk=false;
	$tmpWrd = " ";
	if($veld_waarde==1)
	{
		$chk=true;
		$tmpWrd = " CHECKED ";
	}
	$lbllinks = $lbl_waarde;
	$lblrechts = "";
	if ($links_rechts=="right") {
		$lblrechts = "&nbsp;&nbsp;" . $lbl_waarde;
		$lbllinks = "";
	}
    ?>
    <tr>
    <td><?=$lbllinks?></td>
    <td><input name="<?=$veld_naam?>" id="<?=$veld_naam?>" onclick="<?=$event_naam?>" <?=$tmpWrd?> type="checkbox" class="chk"><?=$lblrechts?></td>
    </tr>
    <?
}
// ========================= SUBMIT / BUTTON ========================
function FrmWebSubmit($Waarde)
{
	$tmpWrd=Vertaal("verstuur");
	if ($Waarde!="") $tmpWrd = $Waarde;
	return "<tr valign='top'><td colspan='3' align='right'><input class='btn' id='VerstuurKnop' name='VerstuurKnop' type='submit' value='" . $tmpWrd . "'></td></tr>";
}
function FrmWebButton($Waarde, $KnopNaam, $ClickFunctie)
{
	return "<tr valign='top'><td colspan='3' align='right'><input id='" . $KnopNaam . "' name='" . $KnopNaam . "' class='btn' type='button' onclick='" . $ClickFunctie . "' value='" . $Waarde . "'></td></tr>";
}

function SluitWebForm()
{
    return "</table></form>";
}
?>
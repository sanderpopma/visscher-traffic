<?
function ToonWebshopProefaanvraag(){
?>
	<?
	$prodUrl = $GLOBALS['HMURL'] . "?pagid=" . $GLOBALS['WebshopPagId'] . "&taalcode=" . $GLOBALS['GekozenTaal'] . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid'] . "&artid=" . $GLOBALS['artid'];
	if ($GLOBALS['artid']>0) {
		$query_rs = "SELECT * FROM artikelen WHERE artikelid=".$GLOBALS['artid']." AND art_publiceren<>0 LIMIT 1;";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		if (mysql_num_rows($rs)>0)
		{
			if (toInt($row_rs['art_afbeeldingid'])>0) {
			?>
			<img name="prodafb" id="prodafb" src="<?=GeefAfbeeldingSrc($row_rs['art_afbeeldingid'])?>" class="border" alt="" align="right" hspace="10" />
			<?
			}
			?>
			<b>Productgroep: </b><?=GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid'])?><br />
			<b>Subcategorie: </b><?=GeefDbWaarde("productnaam_nl", "producten", "productid=" . $GLOBALS['prodid'])?><br />
			<b>Artikel: </b><?=$row_rs['art_naam']?><br />
		<div style="clear:both;"></div>
		<?
		}
		mysql_free_result($rs);
	}
	?>

	<hr class="streep" /><h2>Proef aanvragen</h2>
	<form method="post" id="proefaanvraag" name="proefaanvraag" action="artikel-proefverzonden.php?pagid=<?=GeefPaginaIdByModule("webshopproefverzonden")?>&amp;taalcode=<?=$GLOBALS['GekozenTaal']?>&amp;prodgrpid=<?=$GLOBALS['prodgrpid']?>&amp;prodid=<?=$GLOBALS['prodid']?>&amp;artid=<?=$GLOBALS['artid']?>">
	<table cellpadding="0" cellspacing="0">
		<tr><td colspan="10"><b>Adres- en contactgegevens</b></td></tr>
		<tr><td><?=Vertaal("naam")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="tav" name="tav" size="40" maxlength="100" /></td></tr>
		<tr><td>Adres&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="afleveradres" name="afleveradres" size="40" maxlength="100" /></td></tr>
		<tr><td>Postcode&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="afleverpostcode" name="afleverpostcode" size="20" maxlength="50" /></td></tr>
		<tr><td>Plaats&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="afleverplaats" name="afleverplaats" size="40" maxlength="100" /></td></tr>
		<tr><td><?=Vertaal("emailadres")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="emailadres" name="emailadres" size="40" maxlength="50" /></td></tr>
		<tr><td>Tel.nr.&nbsp;</td><td><input class="txt" type="text" id="telefoonnummer" name="telefoonnummer" size="40" maxlength="50" /></td></tr>

		<tr><td colspan="10">&nbsp;</td></tr>
		<tr valign="top"><td>Opmerkingen&nbsp;</td><td><textarea class="txtarea" id="be_opmerkingen" name="be_opmerkingen" rows="7" cols="56"></textarea></td></tr>
		<tr><td colspan="10" align="right"><input type="submit" class="btn" value="<?=Vertaal("verstuur")?>" /></td></tr>
	</table>
	</form>
	<?=ZetFocus("tav")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("proefaanvraag");
	frmvalidator.addValidation("tav","req","Naam <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleveradres","req","Adres <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleverpostcode","req","Postcode <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleverplaats","req","Plaats <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("emailadres","req","E-mailadres <?=Vertaal("isverplicht")?>");
	</script>
	<?=Icoon("stop")?><a href="<?=$prodUrl?>">&lt; Annuleer aanvraag (terug naar vorige)</a>

<?
}
function ToonWebshopProefverzonden(){
	?>
	<a href="<?=$GLOBALS['WebshopHomeURL']?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>">&lt;&nbsp;Terug naar overzicht</a>
	<?
}
function GeefBestellingDetails($bestel_id){
	$ret = "";
	$mytaal = strtolower($GLOBALS['GekozenTaal']);

	$query_afb = "SELECT * FROM winkel_bestellingen WHERE bestellingid=$bestel_id LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret .= "==============================================\r\n";
		$query_art = "SELECT * FROM winkel_bestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_bestelregels.br_artikelid WHERE br_bestellingid=" . $bestel_id;
		$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
		$row_rsart = mysql_fetch_assoc($rsart);
		if (mysql_num_rows($rsart)>0) {
			do
			{
				$regelprijs = ($row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"])*($row_rsart["art_prijs"]);
				//$ret .= GeefProductgroepNaam("website", GeefDbWaarde("pr_prodgrpid", "producten", "productid=".$row_rsart['art_prodid']), $mytaal, false) . " - " . GeefProductNaam("website",$row_rsart['art_prodid'],$mytaal,false)."\r\n";
				$ret .= ($row_rsart['br_aantal']*$row_rsart["br_bestelhoeveelheid"]) ."x [" . $row_rsart['art_naam'] .  "] = " . MaakBedragTxt($regelprijs)."\r\n\r\n";
			}
			while ($row_rsart = mysql_fetch_assoc($rsart));
		}
		mysql_free_result($rsart);
		$ret .= "==============================================\r\n";
		$ret .= Vertaal("totaal").": " . MaakBedragTxt($row_rsafb['totaalbedragregels']) . "\r\n";
		$ret .= Vertaal("verzendkosten").": " . MaakBedragTxt($row_rsafb['indicatieverzendkosten']) . "\r\n";
		$ret .= Vertaal("totaal")." (" . Vertaal("exclbtw") . "): " . MaakBedragTxt($row_rsafb['totaalexclbtw']) . "\r\n";
		$ret .= Vertaal("btw")." (" . MaakPercentage($row_rsafb['btwpercentage']) . "): " . MaakBedragTxt($row_rsafb['btwbedrag']) . "\r\n";
		$ret .= Vertaal("totaal")." (" . Vertaal("inclbtw") . "): " . MaakBedragTxt($row_rsafb['totaalinclbtw']) . "\r\n";
		$ret .= "==============================================\r\n";
	}
	mysql_free_result($rsafb);

	return $ret;
}

function GeefVerzendkosten($tot_aantal){
	$ret = 0;
	$query_art = "SELECT * FROM winkel_verzendkosten WHERE (vz_aantaldozen=" . $tot_aantal . ") AND vz_publiceren<>0 LIMIT 1";
	$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
	$row_rsart = mysql_fetch_assoc($rsart);
	if (mysql_num_rows($rsart)>0) {
		$ret = $row_rsart['vz_prijs'];
	}
	mysql_free_result($rsart);

	if ($ret==0) {
		$query_art = "SELECT * FROM winkel_verzendkosten WHERE (vz_aantaldozen<=" . $tot_aantal . " AND vz_ofmeer<>0) AND vz_publiceren<>0 LIMIT 1";
		$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
		$row_rsart = mysql_fetch_assoc($rsart);
		if (mysql_num_rows($rsart)>0) {
			$ret = $row_rsart['vz_prijs'];
		}
		mysql_free_result($rsart);
	}
	return $ret;
}

function ToonBestellen4(){
	?>
	<script type="text/javascript">
	function BestellingPlaatsen(){
		if (confirm('Deze bestelling plaatsen?')) {
			location.href = "bestellen4.php?pagid=<?=$GLOBALS['pagid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>";
		}
	}
	</script>
	<table width="100%" cellpadding="2" cellspacing="0" class="arttabel">
	<?
	$query_afb = "SELECT * FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."' LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
	?>
	<tr>
		<td class="artkop">Omschrijving</td>
		<td class="artkop" width="70">Prijs</td>
		<td class="artkop" width="70">Aantal</td>
		<td class="artkop" width="70" align="right">Per</td>
		<td class="artkop" width="70" align="right">Aantal</td>
		<td class="artkop" width="70" align="right">Prijs</td>
	</tr>
	<?
	$query_art = "SELECT * FROM winkel_tmpbestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_tmpbestelregels.br_artikelid WHERE br_bestellingid=" . $row_rsafb['bestellingid'];
	$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
	$row_rsart = mysql_fetch_assoc($rsart);
	if (mysql_num_rows($rsart)>0) {
		$totprijs = 0;
		$totaantal = 0;
		$totdozen = 0;
		do
		{
			$prijsper = 1;

			$regelprijs = ($row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"])*($row_rsart["art_prijs"]/$prijsper);
			$regelaantal = $row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"];
			$totprijs = $totprijs + $regelprijs;
			$totaantal = $totaantal + $regelaantal;
			$totdozen = $totdozen + $row_rsart["br_aantal"];
			$tdclass = "artregel";
			$txtregelprijs = MaakBedrag($regelprijs);
			if ($regelprijs==0) {
				$txtregelprijs = $GLOBALS['StdValutaTeken'] . " 0,00";
			}
			$txtartprijs = $GLOBALS['StdValutaTeken'] . " " . MaakDecimal($row_rsart["art_prijs"]);
			if ($row_rsart["art_prijs"]==0) {
				$txtartprijs = $GLOBALS['StdValutaTeken'] . " 0,00";
			}
			?>
			<tr>
				<td class="<?=$tdclass?>"><?=$row_rsart['art_naam']?></td>
				<td class="<?=$tdclass?>"><?=$txtartprijs?></td>
				<td class="<?=$tdclass?>" nowrap><?=$row_rsart["br_aantal"]?></td>
				<td class="<?=$tdclass?>" align="right"><?=$row_rsart["br_bestelhoeveelheid"]?></td>
				<td class="<?=$tdclass?>" align="right"><?=$regelaantal?></td>
				<td class="<?=$tdclass?>" align="right"><?=$txtregelprijs?></td>
			</tr>
			<?
		}
		while ($row_rsart = mysql_fetch_assoc($rsart));

		$indicatieverzendkosten = 0;
		$query_tar = "SELECT * FROM winkel_verzendwijzen WHERE verzendwijzecode=" . SQLStr($row_rsafb['be_verzendwijzecode']) . ";";
		$rstar = mysql_query($query_tar, $GLOBALS['conn']) or die(mysql_error());
		$row_rstar = mysql_fetch_assoc($rstar);
		if (mysql_num_rows($rstar)>0) {
			$indicatieverzendkosten = $row_rstar['verzendwijzekosten'];
		}
		mysql_free_result($rstar);

		$query_tar = "SELECT * FROM tarieven WHERE tarievenactief<>0;";
		$rstar = mysql_query($query_tar, $GLOBALS['conn']) or die(mysql_error());
		$row_rstar = mysql_fetch_assoc($rstar);
		if (mysql_num_rows($rstar)>0) {
			$btwpercentage = $row_rstar['btwpercentage'];
			$geenverzendkostenboven = $row_rstar['geenverzendkostenboven'];
		}
		mysql_free_result($rstar);

		$plustxtverzendkst = "";
		if ($totprijs>$geenverzendkostenboven) {
			$indicatieverzendkosten = 0;
			$plustxtverzendkst = "<br />Deze bestelling heeft een totaalbedrag van " . MaakBedrag($geenverzendkostenboven) . " of meer. Er worden geen verzendkosten in rekening gebracht";
		}

		$btwbedrag = ($totprijs+$indicatieverzendkosten)*($btwpercentage/100);
		$totinclbtw = $btwbedrag + $totprijs + $indicatieverzendkosten;
	}
	mysql_free_result($rsart);

	if ($indicatieverzendkosten>0) {
		$TxtVerzendKst = MaakBedrag($indicatieverzendkosten);
	}
	else
	{
		$TxtVerzendKst = $GLOBALS['StdValutaTeken'] . " 0,00";
	}
	?>
	</table>
	<table width="100%" cellpadding="2" cellspacing="0" class="arttabel">
	<tr>
		<td class="artkop" colspan="6"><?=Vertaal("totaalvanuwbestelling")?></td>
	</tr>
	<tr>
		<td class="artvoet" width="150"><b><?=Vertaal("totaal")?></b></td>
		<td class="artvoet" align="right" width="80"><b><?=MaakBedrag($totprijs)?></b></td>
		<td class="artvoet" width="20"></td>
		<td class="artvoet" width="<?=($GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteLinks']-40-250)?>"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("verzendkosten")?></b></td>
		<td class="artvoet" align="right" width="80" style="border-bottom: dashed 1px black;"><b><?=$TxtVerzendKst?></b></td>
		<td class="artvoet"><b>+</b></td>
		<td class="artvoet"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("totaal")?> (<?=Vertaal("exclbtw")?>)</b></td>
		<td class="artvoet" align="right" width="80"><b><?=MaakBedrag($totprijs+$indicatieverzendkosten)?></b></td>
		<td class="artvoet"></td>
		<td class="artvoet"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("btw")?> (<?=MaakPercentage($btwpercentage)?>)</b></td>
		<td class="artvoet" align="right" width="80" style="border-bottom: dashed 1px black;"><b><?=MaakBedrag($btwbedrag)?></b></td>
		<td class="artvoet"><b>+</b></td>
		<td class="artvoet"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("totaal")?> (<?=Vertaal("inclbtw")?>)</b></td>
		<td class="artvoet" align="right" width="80"><b><?=MaakBedrag($totinclbtw)?></b></td>
		<td class="artvoet"></td>
		<td class="artvoet"></td>
	</tr>

	<tr>
		<td class="artvoet" colspan="100">&nbsp;</td>
	</tr>

	<tr>
		<td class="artkop" colspan="10"><?=Vertaal("uwgegevens")?></td>
	</tr>
	<tr><td><b><?=Vertaal("bedrijfsnaam")?></b></td><td colspan="5"><?=$row_rsafb['bedrijfsnaam']?></td></tr>
	<tr><td><b><?=Vertaal("tav")?></b></td><td colspan="5"><?=$row_rsafb['tav']?></td></tr>
	<tr><td><b><?=Vertaal("afleveradres")?></b></td><td colspan="5"><?=$row_rsafb['afleveradres']?></td></tr>
	<tr><td><b><?=Vertaal("postcode")?> / <?=Vertaal("plaats")?></b></td><td colspan="5"><?=$row_rsafb['afleverpostcode']?>&nbsp;&nbsp;<?=$row_rsafb['afleverplaats']?>&nbsp;<? if ($row_rsafb['afleverland']!="") { echo "(".$row_rsafb['afleverland'].")";} ?></td></tr>
	<tr><td><b><?=Vertaal("factuuradres")?></b></td><td colspan="5"><?=$row_rsafb['factuuradres']?></td></tr>
	<tr><td><b><?=Vertaal("postcode")?> / <?=Vertaal("plaats")?></b></td><td colspan="5"><?=$row_rsafb['factuurpostcode']?>&nbsp;&nbsp;<?=$row_rsafb['factuurplaats']?>&nbsp;<? if ($row_rsafb['factuurland']!="") { echo "(".$row_rsafb['factuurland'].")";} ?></td></tr>
	<tr><td><b><?=Vertaal("emailadres")?></b></td><td colspan="5"><?=$row_rsafb['emailadres']?></td></tr>
	<tr><td><b><?=Vertaal("telnr")?></b></td><td colspan="5"><?=$row_rsafb['telefoonnummer']?></td></tr>
	<tr><td><b><?=Vertaal("faxnr")?></b></td><td colspan="5"><?=$row_rsafb['faxnummer']?></td></tr>
	<tr valign="top"><td><b><?=Vertaal("gewensteleverdatum")?></b></td><td colspan="5"><?=MaakDatum($row_rsafb['gewensteleverdatum'])?><br /><?=Icoon($voorraadicon)?><?=$txtvoorraad?></td></tr>
	<tr><td><b><?=Vertaal("betaalwijze")?></b></td><td colspan="5"><?=GeefDbWaarde("betaalwijzenaam_".strtolower($GLOBALS['GekozenTaal']), "winkel_betaalwijzen", "betaalwijzecode='".$row_rsafb['be_betaalwijzecode']."'")?></td></tr>
	<tr valign="top"><td><b><?=Vertaal("verzendwijze")?></b></td><td colspan="5"><?=GeefDbWaarde("verzendwijzenaam_".strtolower($GLOBALS['GekozenTaal']), "winkel_verzendwijzen", "verzendwijzecode='".$row_rsafb['be_verzendwijzecode']."'")?><?=$plustxtverzendkst?></td></tr>
	<tr valign="top"><td><b><?=Vertaal("opmerkingen")?></b></td><td colspan="5"><?=VervangBRMemo($row_rsafb['be_opmerkingen'])?></td></tr>

	<tr>
		<td class="artkop" colspan="100">&nbsp;</td>
	</tr>

	</table>

	<br />
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="33%" align="left"><?=Icoon("pijl_links")?><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefDbWaarde("paginaid", "paginas", "moduletype='webshopbestellen3' AND paginataalcode='".$GLOBALS['GekozenTaal']."'")?>&taalcode=<?=$GLOBALS['GekozenTaal']?>"><?=Vertaal("vorige")?></a></td>
			<td width="34%" align="center"><?=Icoon("printer")?><a href="#" onclick="window.print(); return false;">Print bestelling</a></td>
			<td width="33%" align="right"><a href="#" onclick="BestellingPlaatsen(); return false;"><?=Vertaal("bestellingplaatsen")?></a><?=Icoon("gereed")?></td>
		</tr>
	</table>

	<?
	}
	mysql_free_result($rsafb);
}

function ToonBestellen3(){
	$query_afb = "SELECT * FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."' LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
	?>
	<?=OpenWebForm("bestellen3.php?pagid=" . $GLOBALS['pagid'] . "&taalcode=" . $GLOBALS['GekozenTaal'], "bestel3", "500")?>
		<?=FrmWebKoptekst(Vertaal("betaalwijze"))?>
		<?=FrmWebSelectByQuery(Vertaal("betaalwijze"), "be_betaalwijzecode", $row_rsafb['be_betaalwijzecode'], "SELECT betaalwijzecode, betaalwijzenaam_".strtolower($GLOBALS['GekozenTaal'])." FROM winkel_betaalwijzen", "betaalwijzecode", "betaalwijzenaam_".strtolower($GLOBALS['GekozenTaal']), true)?>
		<?=FrmLegeRegel()?>
		<?=FrmWebSubmit("Opslaan & Volgende =&gt;")?>

	<?=SluitWebForm()?>
	<?=ZetFocus("be_betaalwijzecode")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("bestel3");
	frmvalidator.addValidation("be_betaalwijzecode","req","<?=Vertaal("betaalwijze")?> <?=Vertaal("isverplicht")?>");
	</script>
	<br />
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" align="left"><?=Icoon("pijl_links")?><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefDbWaarde("paginaid", "paginas", "moduletype='webshopbestellen2' AND paginataalcode='".$GLOBALS['GekozenTaal']."'")?>&taalcode=<?=$GLOBALS['GekozenTaal']?>"><?=Vertaal("vorige")?></a></td>
			<td width="50%" align="right"><a href="#" onclick="javascript:document.getElementById('VerstuurKnop').click(); return false;"><?=Vertaal("bestellen")?></a><?=Icoon("pijl_rechts")?></td>
		</tr>
	</table>
	<?
	}
	mysql_free_result($rsafb);
}

function ToonBestellen2(){
	$query_afb = "SELECT * FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."' LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
	?>
	<?=OpenWebForm("bestellen2.php?pagid=" . $GLOBALS['pagid'] . "&taalcode=" . $GLOBALS['GekozenTaal'], "bestel2", "500")?>

	<?=FrmWebKoptekst(Vertaal("verzendwijze"))?>
	<?=FrmWebSelectByQuery(Vertaal("verzendwijze"), "be_verzendwijzecode", $row_rsafb['be_verzendwijzecode'], "SELECT verzendwijzecode, verzendwijzenaam_".strtolower($GLOBALS['GekozenTaal'])." FROM winkel_verzendwijzen", "verzendwijzecode", "verzendwijzenaam_".strtolower($GLOBALS['GekozenTaal']), true)?>
	<?=FrmLegeRegel()?>
	<?=FrmWebSubmit("Opslaan & Volgende =&gt;")?>

	<?=SluitWebForm()?>
	<?=ZetFocus("be_verzendwijzecode")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("bestel2");
	frmvalidator.addValidation("be_verzendwijzecode","req","<?=Vertaal("verzendwijze")?> <?=Vertaal("isverplicht")?>");
	</script>
	<br />
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" align="left"><?=Icoon("pijl_links")?><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefDbWaarde("paginaid", "paginas", "moduletype='webshopbestellen1' AND paginataalcode='".$GLOBALS['GekozenTaal']."'")?>&taalcode=<?=$GLOBALS['GekozenTaal']?>"><?=Vertaal("vorige")?></a></td>
			<td width="50%" align="right"><a href="#" onclick="javascript:document.getElementById('VerstuurKnop').click(); return false;"><?=Vertaal("bestellen")?></a><?=Icoon("pijl_rechts")?></td>
		</tr>
	</table>
	<?
	}
	mysql_free_result($rsafb);
}

function ToonBestellen1(){
	$query_afb = "SELECT * FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."' LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$bedrijfsnaam = $row_rsafb['bedrijfsnaam'];
		$tav = $row_rsafb['tav'];
		$afleveradres = $row_rsafb['afleveradres'];
		$afleverpostcode = $row_rsafb['afleverpostcode'];
		$afleverplaats = $row_rsafb['afleverplaats'];
		$afleverland = $row_rsafb['afleverland'];
		$factuuradres = $row_rsafb['factuuradres'];
		$factuurpostcode = $row_rsafb['factuurpostcode'];
		$factuurplaats = $row_rsafb['factuurplaats'];
		$factuurland = $row_rsafb['factuurland'];
		$factuurzelfdealsafleveradres = $row_rsafb['factuurzelfdealsafleveradres'];
		$emailadres = $row_rsafb['emailadres'];
		$telefoonnummer = $row_rsafb['telefoonnummer'];
		$faxnummer = $row_rsafb['faxnummer'];
		$gewensteleverdatum = $row_rsafb['gewensteleverdatum'];
		$be_opmerkingen = $row_rsafb['be_opmerkingen'];
	}
	mysql_free_result($rsafb);

	$query_kl = "SELECT * FROM klanten WHERE klantid=" . $_SESSION[$GLOBALS['SessionPrefix'].'LoginLidID'] ." LIMIT 1";
	$rskl = mysql_query($query_kl, $GLOBALS['conn']) or die(mysql_error());
	$row_rskl = mysql_fetch_assoc($rskl);
	if (mysql_num_rows($rskl)>0) {
		if ($bedrijfsnaam=="") { $bedrijfsnaam = $row_rskl['bedrijfsnaam']; }
		if ($afleveradres=="") { $afleveradres = $row_rskl['bedrijfsadres']; }
		if ($afleverpostcode=="") { $afleverpostcode = $row_rskl['bedrijfspostcode']; }
		if ($afleverplaats=="") { $afleverplaats = $row_rskl['bedrijfsplaats']; }
		if ($afleverland=="") { $afleverland = "Nederland"; }
		if ($factuuradres=="") { $factuuradres = $row_rskl['bedrijfsadres']; }
		if ($factuurpostcode=="") { $factuurpostcode = $row_rskl['bedrijfspostcode']; }
		if ($factuurplaats=="") { $factuurplaats = $row_rskl['bedrijfsplaats']; }
		if ($factuurland=="") { $factuurland = "Nederland"; }
		if ($emailadres=="") { $emailadres = $row_rskl['emailadres']; }
	}
	mysql_free_result($rskl);

	?>
	<script type="text/javascript">
	function KopieerAfleveradres(){
		//document.getElementById("factuuradres").disabled = (document.getElementById("factuurzelfdealsafleveradres").checked);
		//document.getElementById("factuurpostcode").disabled = (document.getElementById("factuurzelfdealsafleveradres").checked);
		//document.getElementById("factuurplaats").disabled = (document.getElementById("factuurzelfdealsafleveradres").checked);
		//document.getElementById("factuurland").disabled = (document.getElementById("factuurzelfdealsafleveradres").checked);
		if (document.getElementById("factuurzelfdealsafleveradres").checked) {
			document.getElementById("factuuradres").value = document.getElementById("afleveradres").value;
			document.getElementById("factuurpostcode").value = document.getElementById("afleverpostcode").value;
			document.getElementById("factuurplaats").value = document.getElementById("afleverplaats").value;
			document.getElementById("factuurland").value = document.getElementById("afleverland").value;
		}
	}
	</script>
	<?
	GebruikDatumObjecten();
	?>

	<?=OpenWebForm("bestellen1.php?pagid=" . $GLOBALS['pagid'] . "&taalcode=" . $GLOBALS['GekozenTaal'], "bestel1", "100%")?>
		<?=FrmWebKoptekst(Vertaal("bedrijfsgegevens"))?>
		<?=FrmWebText(Vertaal("bedrijfsnaam") . " " . VerplichtSter(),"bedrijfsnaam", $bedrijfsnaam,60 ,100 )?>
		<?=FrmWebText(Vertaal("tav"),"tav", $tav,40 ,50 )?>

		<?=FrmLegeRegel()?>
		<?=FrmWebKoptekst(Vertaal("afleveradres"))?>
		<?=FrmWebOnEventText("onchange", "KopieerAfleveradres();", Vertaal("adres"). " " . VerplichtSter(),"afleveradres", $afleveradres,60 ,100 )?>
		<?=FrmWebOnEventText("onchange", "KopieerAfleveradres();", Vertaal("postcode"). " " . VerplichtSter(),"afleverpostcode", $afleverpostcode,20 ,50 )?>
		<?=FrmWebOnEventText("onchange", "KopieerAfleveradres();", Vertaal("plaats"). " " . VerplichtSter(),"afleverplaats", $afleverplaats ,60 ,100 )?>
		<?=FrmWebOnEventText("onchange", "KopieerAfleveradres();", Vertaal("land"),"afleverland", $afleverland,60 ,50 )?>

		<?=FrmLegeRegel()?>
		<?=FrmWebKoptekst(Vertaal("factuuradres"))?>
		<?=FrmWebText(Vertaal("adres"). " " . VerplichtSter(),"factuuradres", $factuuradres,60 ,100 )?>
		<?=FrmWebText(Vertaal("postcode"). " " . VerplichtSter(),"factuurpostcode", $factuurpostcode,20 ,50 )?>
		<?=FrmWebText(Vertaal("plaats"). " " . VerplichtSter(),"factuurplaats", $factuurplaats,60 ,100 )?>
		<?=FrmWebText(Vertaal("land"),"factuurland", $factuurland,60 ,50 )?>
		<?=FrmWebEventCheckbox(Vertaal("zelfdealsafleveradres"), "factuurzelfdealsafleveradres", Int2Bool($factuurzelfdealsafleveradres), "KopieerAfleveradres();", "right")?>

		<?=FrmLegeRegel()?>
		<?=FrmWebKoptekst(Vertaal("contactgegevens"))?>
		<?=FrmWebText(Vertaal("emailadres"). " " . VerplichtSter(),"emailadres", $emailadres,60 ,150 )?>
		<?=FrmWebText(Vertaal("telnr"),"telefoonnummer", $telefoonnummer,40 ,50 )?>
		<?=FrmWebText(Vertaal("faxnr"),"faxnummer", $faxnummer,40 ,50 )?>

		<?=FrmLegeRegel()?>
		<?=FrmWebKoptekst(Vertaal("gewensteleverdatum"))?>
		<?=FrmWebDatum(Vertaal("leverdatum"), "gewensteleverdatum", $gewensteleverdatum)?>

		<?=FrmLegeRegel()?>
		<?=FrmWebKoptekst(Vertaal("opmerkingen"))?>
		<?=FrmWebTextArea(Vertaal("opmerkingen"),"be_opmerkingen", $be_opmerkingen,6 ,60 )?>

		<?=FrmWebSubmit("Opslaan & Volgende =&gt;")?>

	<?=SluitWebForm()?>
	<?=ZetFocus("bedrijfsnaam")?>
	<script type="text/javascript">
		KopieerAfleveradres();
	</script>
	<script type="text/javascript">
	var frmvalidator  = new Validator("bestel1");
	frmvalidator.addValidation("bedrijfsnaam","req","<?=Vertaal("bedrijfsnaam")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleveradres","req","<?=Vertaal("adres")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleverpostcode","req","<?=Vertaal("postcode")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("afleverplaats","req","<?=Vertaal("plaats")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("factuuradres","req","<?=Vertaal("adres")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("factuurpostcode","req","<?=Vertaal("postcode")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("factuurplaats","req","<?=Vertaal("plaats")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("emailadres","req","<?=Vertaal("emailadres")?> <?=Vertaal("isverplicht")?>");
	</script>
	<br />
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" align="left"><?=Icoon("pijl_links")?><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefDbWaarde("paginaid", "paginas", "moduletype='webshopwinkelwagen' AND paginataalcode='".$GLOBALS['GekozenTaal']."'")?>&taalcode=<?=$GLOBALS['GekozenTaal']?>"><?=Vertaal("winkelwagen")?></a></td>
			<td width="50%" align="right"><a href="#" onclick="javascript:document.getElementById('VerstuurKnop').click(); return false;"><?=Vertaal("bestellen")?></a><?=Icoon("pijl_rechts")?></td>
		</tr>
	</table>
	<?
}
function GeefAantalWinkelwagenItemsText(){
	$aantItems = toInt(TelRecords("SELECT * FROM winkel_tmpbestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_tmpbestelregels.br_artikelid WHERE br_bestellingid=(SELECT bestellingid FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."')"));
	if ($aantItems>0)
	{
		return "&nbsp;(" . $aantItems . ")";
	}
}
function ToonWinkelwagen(){
	$WebshopPagId = GeefPaginaIdByModule("WEBSHOP");
	$MyBaseUrl = $GLOBALS['HMURL'] . "?pagid=" . $WebshopPagId;
	if (TelRecords("SELECT * FROM winkel_tmpbestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_tmpbestelregels.br_artikelid WHERE br_bestellingid=(SELECT bestellingid FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."')")<1) {
		?>
		Er zitten (nog) geen artikelen in uw winkelwagen.<br />
		<br />
		<a href="<?=$MyBaseUrl?>">Bezoek de webshop</a>
	<?
	}
	else
	{
		?>
		<script type="text/javascript">
		function VerwijderRegel(br_id){
			if (confirm('Deze regel verwijderen?')) {
				location.href = "bestelregel-verwijderen.php?pagid=<?=$GLOBALS['pagid']?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&brid=" + br_id + "&taalcode=<?=$GLOBALS['GekozenTaal']?>";
			}
		}
		</script>
		<?
		$query_afb = "SELECT * FROM winkel_tmpbestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_tmpbestelregels.br_artikelid WHERE br_bestellingid=(SELECT bestellingid FROM winkel_tmpbestellingen WHERE be_sessionid='" . session_id()."')";
		$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
		$row_rsafb = mysql_fetch_assoc($rsafb);
		if (mysql_num_rows($rsafb)>0) {
			$totprijs = 0;
			$totaantal = 0;
			$totdozen = 0;
			?>
			<table width="100%" cellpadding="2" cellspacing="0" class="arttabel">
			<tr>
				<td class="artkop">Omschrijving</td>
				<td class="artkop" width="70">Prijs</td>
				<td class="artkop" width="70">Aantal</td>
				<td class="artkop" width="70" align="right">Per</td>
				<td class="artkop" width="70" align="right">Aantal</td>
				<td class="artkop" width="70" align="right">Prijs</td>
				<td class="artkop" width="20"></td>
			</tr>
			<?
			do
			{
				$regelprijs = ($row_rsafb["br_aantal"]*$row_rsafb["br_bestelhoeveelheid"])*($row_rsafb["art_prijs"]);
				$regelaantal = $row_rsafb["br_aantal"]*$row_rsafb["br_bestelhoeveelheid"];
				$tdclass="artregel";
				if ($GLOBALS['artid']==$row_rsafb['artid']) {
					$tdclass="artregelsel";
				}
				$txtregelprijs = MaakBedrag($regelprijs);
				if ($regelprijs==0) {
					$txtregelprijs = $GLOBALS['StdValutaTeken'] . " 0,00";
				}
				$txtartprijs = $GLOBALS['StdValutaTeken'] . " " . MaakDecimal($row_rsafb["art_prijs"]);
				if ($row_rsafb["art_prijs"]==0) {
					$txtartprijs = $GLOBALS['StdValutaTeken'] . " 0,00";
				}
			?>
			<tr><form method="post" action="bestelregel-wijzigen.php?pagid=<?=$GLOBALS['pagid']?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&artid=<?=$row_rsafb['artikelid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&brid=<?=$row_rsafb['bestelregelid']?>" id="formregel<?=$row_rsafb['bestelregelid']?>" name="formregel<?=$row_rsafb['bestelregelid']?>">
				<td class="<?=$tdclass?>"><?=$row_rsafb['art_naam']?></td>
				<td class="<?=$tdclass?>"><?=$txtartprijs?></td>
				<td class="<?=$tdclass?>" nowrap><input type="text" size="2" maxlength="3" value="<?=VervangFrmWaarde($row_rsafb["br_aantal"])?>" class="txtklein" id="br_aantal" name="br_aantal" />&nbsp;<input class="btnklein" type="submit" value="OK" /></td>
				<td class="<?=$tdclass?>" align="right"><?=$row_rsafb["br_bestelhoeveelheid"]?></td>
				<td class="<?=$tdclass?>" align="right"><?=$regelaantal?></td>
				<td class="<?=$tdclass?>" align="right"><?=$txtregelprijs?></td>
				<td class="<?=$tdclass?>"><a class="klein" href="#" onclick="VerwijderRegel('<?=$row_rsafb['bestelregelid']?>'); return false;"><?=Icoon("prullenbak")?></a></td>
				</form>
				<script type="text/javascript">
				var frmvalidator  = new Validator("formregel<?=$row_rsafb['bestelregelid']?>");
				frmvalidator.addValidation("br_aantal","req","Aantal is verplicht");
				frmvalidator.addValidation("br_aantal","num","Aantal heeft een ongeldige waarde");
				frmvalidator.addValidation("br_aantal","gt=0","Aantal heeft een ongeldige waarde");
				</script>
			</tr>
			<?
				$totprijs = $totprijs + $regelprijs;
				$totaantal = $totaantal + $regelaantal;
			}
			while ($row_rsafb = mysql_fetch_assoc($rsafb));

			$query_tar = "SELECT * FROM tarieven WHERE tarievenactief<>0;";
			$rstar = mysql_query($query_tar, $GLOBALS['conn']) or die(mysql_error());
			$row_rstar = mysql_fetch_assoc($rstar);
			if (mysql_num_rows($rstar)>0) {
				$btwpercentage = $row_rstar['btwpercentage'];
			}
			mysql_free_result($rstar);
			$btwbedrag = ($totprijs)*($btwpercentage/100);
			$totinclbtw = $btwbedrag + $totprijs;
			?>
			<tr>
				<td class="artvoet" colspan="4" align="right"><b>Totaal</b></td>
				<td class="artvoet" align="right"><b></b></td>
				<td class="artvoet" align="right" style="border-bottom: dashed 1px black;"><b><?=MaakBedrag($totprijs)?></b></td>
				<td class="artvoet"></td>
			</tr>
			<tr>
				<td class="artvoet" colspan="4" align="right"><b>Totaal (Excl. BTW)</b></td>
				<td class="artvoet" align="right"><b></b></td>
				<td class="artvoet" align="right"><b><?=MaakBedrag($totprijs)?></b></td>
				<td class="artvoet"></td>
			</tr>
			<tr>
				<td class="artvoet" colspan="4" align="right"><b>BTW (<?=MaakPercentage($btwpercentage)?>)</b></td>
				<td class="artvoet" align="right"></td>
				<td class="artvoet" align="right" style="border-bottom: dashed 1px black;"><b><?=MaakBedrag($btwbedrag)?></b></td>
				<td class="artvoet"><b>+</b></td>
			</tr>
			<tr>
				<td class="artvoet" colspan="4" align="right"><b>Totaal (Incl. BTW)</b></td>
				<td class="artvoet" align="right"></td>
				<td class="artvoet" align="right"><b><?=MaakBedrag($totinclbtw)?></b></td>
				<td class="artvoet"></td>
			</tr>

			<tr>
				<td class="artvoet" colspan="100">&nbsp;</td>
			</tr>
			</table>
			<br />
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" align="left"><?=Icoon("pijl_links")?><a href="<?=$MyBaseUrl?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>">Verder winkelen</a></td>
					<td width="50%" align="right"><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefDbWaarde("paginaid", "paginas", "moduletype='webshopbestellen1' AND paginataalcode='".$GLOBALS['GekozenTaal']."'")?>&taalcode=<?=$GLOBALS['GekozenTaal']?>">Bestellen</a><?=Icoon("pijl_rechts")?></td>
				</tr>
			</table>
			<?
		}
		mysql_free_result($rsafb);
	}
}
?>
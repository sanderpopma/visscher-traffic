<?
function ToonFotoboek($fb_id){
    $query_rs2 = "SELECT * FROM fotoboeken WHERE fotoboekid=".$fb_id."";
    $rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
    $row_rs2 = mysql_fetch_assoc($rs2);
    if ($row_rs2['fotoboekid']>0)
    {
    	?>
    	<h1><?=$row_rs2['fbtitel']?></h1>
		<?
		if ($row_rs2['fbdatum']!="") {
			echo "<span style='font-size: 10px;'>Datum: " . MaakDatum($row_rs2['fbdatum']) . "</span><br />";
		}
		?>
		<?
			$query_rs = "SELECT * FROM afbeeldingen INNER JOIN fotoboeken_fotos ON fotoboeken_fotos.fbf_afbeeldingid=afbeeldingen.afbeeldingid WHERE fotoboeken_fotos.fbf_fotoboekid=". $fb_id . " ORDER BY fotoboeken_fotos.fbf_volgorde, fotoboekfotoid;";
			$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
			$row_rs = mysql_fetch_assoc($rs);
			$AantFotos = mysql_num_rows($rs);
			if (mysql_num_rows($rs)>0) {
				$ftel = 0;
				?>
				<br /><br />
				<table width="96%" cellpadding="0" cellspacing="0" align="center">
				<?
				do
				{
					if ($ftel % $GLOBALS['AantalFotosPerRij']==0) {
						echo "<tr valign=\"top\">";
					}
					?>
					<td width="<?=100/$GLOBALS['AantalFotosPerRij']?>%" align="center"><a href="<?=GeefAfbeeldingSrc($row_rs['afbeeldingid'])?>" rel="lightbox"><img border="0" style="border: solid 1px black;" id="tn<?=$row_rs['fotoboekfotoid']?>" src="<?=GeefAfbeeldingSrc($row_rs['afbeeldingid'])?>" width="<?=$GLOBALS['BreedteFotoboekFoto']?>" alt="<?=$row_rs['fbf_onderschrift']?>" /></a><br /><div style="width: <?=$GLOBALS['BreedteFotoboekFoto']+20?>px; font-size: 9px; color: #444444;"><?=$row_rs['fbf_onderschrift']?></div><br /><br /></td>
					<?
					$ftel = $ftel + 1;
					if ($ftel % $GLOBALS['AantalFotosPerRij']==0) {
						echo "</tr>";
					}
				}
				while ($row_rs = mysql_fetch_assoc($rs));
				?>
				</table>
				<?
			}
			mysql_free_result($rs);

		?>
    	<?
    }
	else
	{
		die;
	}
    mysql_free_result($rs2);
	?>
	<hr class="streep">
	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>">Terug naar vorige pagina</a>
	<?
}

function ToonKaderAfbeelding(){
	return;
	switch(strtoupper(GeefPaginaModuleType($GLOBALS['pagid']))){
		case "NIEUWS":
			if ($GLOBALS['nwsid']>0) {
				ToonEersteKaderAfbeelding("NIEUWS");
				break;
			}
			else
			{
				ToonEersteKaderAfbeelding("PAGINA");
				break;
			}
		case "VACATURES":
			if ($GLOBALS['vacid']>0) {
				ToonEersteKaderAfbeelding("VACATURE");
				break;
			}
			else
			{
				ToonEersteKaderAfbeelding("PAGINA");
				break;
			}
		default:
			ToonEersteKaderAfbeelding("PAGINA");
	}
}
function ToonEersteKaderAfbeelding($soort_pag){
	switch(strtoupper($soort_pag)){
		case "NIEUWS":
			$WhereDeel = "pa_nieuwsid=".$GLOBALS['nwsid'];
			break;
		case "VACATURE":
			$WhereDeel = "pa_vacatureid=".$GLOBALS['vacid'];
			break;
		default:
			$WhereDeel = "pa_paginaid=".$GLOBALS['pagid'];
	}

	$query_par = "SELECT * FROM paragrafen WHERE " . $WhereDeel . " AND pa_publiceren<>0 AND paragraaftype IN('TEKST') AND pa_afbeeldingid>0 ORDER BY pa_volgorde LIMIT 1;";
	$rspar = mysql_query($query_par, $GLOBALS['conn']) or die(mysql_error());
	$row_rspar = mysql_fetch_assoc($rspar);
	if (mysql_num_rows($rspar)>0) {
		?>
		<br />
		<center>
		<img width="<?=$GLOBALS['AfbeeldingMaxKlein']?>" alt="" src="<?=GeefAfbeeldingSrc($row_rspar['pa_afbeeldingid'])?>" class="border" /><br /><br />
		</center>
		<?
	}
	mysql_free_result($rspar);
}
function ToonHoofdMenu($Taal_Code){
	$hmenutel = 0;
	$query_hm = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND publiceren<>0 AND valtonderpaginaid=0 AND opnemeninsubmenu<>0 ORDER BY hoofdmenuvolgorde;";
	$rshm = mysql_query($query_hm, $GLOBALS['conn']) or die(mysql_error());
	$row_rshm = mysql_fetch_assoc($rshm);
	if (mysql_num_rows($rshm)>0) {
		?>
		<?
		do
		{
			$mywidth = ($GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteLinks'])/5;
			$myclass="menu";
			if ($GLOBALS['hmid']==$row_rshm['paginaid']) {
				$myclass="menusel";
			}
			$myMenuNaam = strtoupper($row_rshm['paginamenunaam']);
			$myMenuNaam = str_replace("&amp;", "&", $myMenuNaam);
			$myMenuNaam = str_replace("&", "&amp;", $myMenuNaam);

			?>
				<div style="height: 32px; float: left; margin-right: 12px;"><img src="<?=$GLOBALS['EmptyVar']?>/siteimg/hmli.gif" style="margin-left: 6px; margin-right: 6px;" /><a class="<?=$myclass?>" target="_top" href="<?=GeefPaginaURL($row_rshm['paginaid'])?>"><?=$myMenuNaam?></a></div>
			<?
			$hmenutel = $hmenutel + 1;
		}
		while ($row_rshm = mysql_fetch_assoc($rshm));
		?>
		<?
	}
	mysql_free_result($rshm);
}

function ToonSubMenu($Taal_Code){

	$query_hhm = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND publiceren<>0 AND valtonderpaginaid=0 AND ledenloginnodig=0 ORDER BY hoofdmenuvolgorde;";
	$rshhm = mysql_query($query_hhm, $GLOBALS['conn']) or die(mysql_error());
	$row_rshhm = mysql_fetch_assoc($rshhm);
	if (mysql_num_rows($rshhm)>0) {
		do
		{
			$hoofdmenuvaltop = Int2Bool($row_rshhm['hoofdmenuvaltop']);
			$myclasshhm="menu";
			if (GeefPaginaHMID($GLOBALS['pagid'])==$row_rshhm['paginaid']) {
				$myclasshhm="menusel";
			}

			$myMenuNaam = ($row_rshhm['paginamenunaam']);
			$myMenuNaam = str_replace("&amp;", "&", $myMenuNaam);
			$myMenuNaam = str_replace("&", "&amp;", $myMenuNaam);
			$toonsubmenu = false;
			if ($GLOBALS['pagid']==$row_rshhm['paginaid'] || $GLOBALS['hmid']==$row_rshhm['paginaid']) {
				$myclasshhm="menusel";
				$toonsubmenu = true;
			}
			if ($hoofdmenuvaltop==true) {
				$myclasshhm = "menuvaltop";
			}
			?>
				<li><a <?=GeefTarget("pagina", $row_rshhm['paginaid'])?> href="<?=GeefPaginaURL($row_rshhm['paginaid'])?>" class="<?=$myclasshhm?>"><?=$myMenuNaam?></a></li>
			<?

			if ($toonsubmenu==true) {
				?>
				<ul>
				<?
				$query_hm = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND paginaid=" . $row_rshhm['paginaid'] . " AND publiceren<>0 AND valtonderpaginaid=0 AND ledenloginnodig=0 ORDER BY hoofdmenuvolgorde;";
				$rshm = mysql_query($query_hm, $GLOBALS['conn']) or die(mysql_error());
				$row_rshm = mysql_fetch_assoc($rshm);
				if (mysql_num_rows($rshm)>0) {
					?>
					<?
					if (strtoupper(GeefPaginaModuleType($row_rshhm['paginaid']))!="WEBSHOP")
					{
						$query_sm = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $row_rshhm['paginaid'] . " AND opnemeninsubmenu<>0 AND ledenloginnodig=0 AND publiceren<>0 ORDER BY submenuvolgorde;";
						$rssm = mysql_query($query_sm, $GLOBALS['conn']) or die(mysql_error());
						$row_rssm = mysql_fetch_assoc($rssm);
						if (mysql_num_rows($rssm)>0) {
							do
							{
								$toonsubsubmenu = false;
								$myclass="submenu";
								$liclass = "navsubmenu";
								if ($GLOBALS['pagid']==$row_rssm['paginaid'] || $GLOBALS['smid']==$row_rssm['paginaid']) {
									$myclass="submenusel";
									$toonsubsubmenu = true;
									$liclass = "subsel";
								}
								$mysubmenunaam = $row_rssm['paginamenunaam'];
								$mysubmenunaam = str_replace("&amp;", "&", $mysubmenunaam);
								$mysubmenunaam = str_replace("&", "&amp;", $mysubmenunaam);
							?>
							<li class="<?=$liclass?>"><a <?=GeefTarget("pagina", $row_rssm['paginaid'])?> href="<?=GeefPaginaURL($row_rssm['paginaid'])?>" class="<?=$myclass?>"><?=$mysubmenunaam?></a></li>
							<?
							if ($toonsubsubmenu==true) {
							?>
							<ul>
							<?
								$query_prod = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $row_rssm['paginaid'] . " AND opnemeninsubmenu<>0 AND publiceren<>0 ORDER BY submenuvolgorde;";
								$rsprod = mysql_query($query_prod, $GLOBALS['conn']) or die(mysql_error());
								$row_rsprod = mysql_fetch_assoc($rsprod);
								if (mysql_num_rows($rsprod)>0) {
									do
									{
										$myclass="subsubmenu";
										if ($GLOBALS['pagid']==$row_rsprod['paginaid']) {
											$myclass="subsubmenusel";
										}
										$mysubsubmenunaam = $row_rsprod['paginamenunaam'];
										$mysubsubmenunaam = str_replace("&amp;", "&", $mysubsubmenunaam);
										$mysubsubmenunaam = str_replace("&", "&amp;", $mysubsubmenunaam);
									?>
									<li class="navsubsubmenu"><a href="<?=GeefPaginaURL($row_rsprod['paginaid'])?>" class="<?=$myclass?>"><?=$mysubsubmenunaam?></a></li>
									<?
									}
									while ($row_rsprod = mysql_fetch_assoc($rsprod));
								}
							?>
							</ul>
							<?
							}
							?>
							<?
							}
							while ($row_rssm = mysql_fetch_assoc($rssm));
						}
					}
					else
					{
						$query_sm = "SELECT * FROM productgroepen WHERE prodgrp_publiceren<> 0 ORDER BY prodgrp_volgorde;";
						$rssm = mysql_query($query_sm, $GLOBALS['conn']) or die(mysql_error());
						$row_rssm = mysql_fetch_assoc($rssm);
						if (mysql_num_rows($rssm)>0) {
							do
							{
								$toonproducten = false;
								$myclass="submenu";
								if ($GLOBALS['prodgrpid']==$row_rssm['prodgrpid']) {
									$myclass="submenusel";
									$toonproducten = true;
								}
								$mysubmenunaam = $row_rssm['prodgrp_naam_nl'];
							?>
							<li class="navsubmenu"><?=ApplicatieIcoon("submenuitem")?><a <?=GeefTarget("prodgrp", $row_rssm['prodgrpid'])?> href="<?=$GLOBALS['HMURL']?>?pagid=<?=$WebshopPagId?>&prodgrpid=<?=$row_rssm['prodgrpid']?>" class="<?=$myclass?>"><?=$mysubmenunaam?></a></li>
							<?
							if ($toonproducten==true) {
								$query_prod = "SELECT * FROM producten WHERE pr_prodgrpid=" . $row_rssm['prodgrpid'] . " AND pr_publiceren<> 0 ORDER BY pr_volgorde, productnaam_nl;";
								$rsprod = mysql_query($query_prod, $GLOBALS['conn']) or die(mysql_error());
								$row_rsprod = mysql_fetch_assoc($rsprod);
								if (mysql_num_rows($rsprod)>0) {
									do
									{
										$myclass="submenu";
										if ($GLOBALS['prodid']==$row_rsprod['productid']) {
											$myclass="submenusel";
										}
										$mysubmenunaam = $row_rsprod['productnaam_nl'];
									?>
									<li class="navsubmenu">&nbsp;&nbsp;<?=ApplicatieIcoon("shopsubitem")?><a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$WebshopPagId?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$row_rsprod['productid']?>" class="<?=$myclass?>"><?=$mysubmenunaam?></a></li>
									<?
									}
									while ($row_rsprod = mysql_fetch_assoc($rsprod));
								}
							}
							?>
							<?
							}
							while ($row_rssm = mysql_fetch_assoc($rssm));
						}
					}
					?>
					<?
				}
				mysql_free_result($rshm);
			?>
			</ul>
			<?
			}
		}
		while ($row_rshhm = mysql_fetch_assoc($rshhm));
	}
	mysql_free_result($rshhm);

}

function ToonMenu($Taal_Code){
	$query_hm = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND publiceren<>0 AND valtonderpaginaid=0 AND ledenloginnodig=0 ORDER BY hoofdmenuvolgorde;";
	if ($GLOBALS['LidIsIngelogd']==true) {
		$query_hm = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND publiceren<>0 AND valtonderpaginaid=0 AND verbergindieningelogd=0 ORDER BY hoofdmenuvolgorde;";
	}
	$rshm = mysql_query($query_hm, $GLOBALS['conn']) or die(mysql_error());
	$row_rshm = mysql_fetch_assoc($rshm);
	$numrec = mysql_num_rows($rshm);
	if (mysql_num_rows($rshm)>0) {
		$numtel = 1;
		do
		{
			$toonitem = true;
			$toonsubmenu = false;
			$myclass="menu";
			$myliclass = "navmainmenu";
			if ($GLOBALS['hmid']==$row_rshm['paginaid']) {
				$myclass="menusel";
				$toonsubmenu = true;
			}
			if ($numrec==$numtel) {
				$myliclass = "navmainmenu";
			}
			if ($row_rshm['moduletype']=="vacatures") {
				$toonitem = false;
				if (TelRecords("SELECT * FROM vacatures WHERE vacpubliceren<>0")>0) {
					$toonitem = true;
				}
			}
			if ($toonitem==true) {
				?>
				<li class="<?=$myliclass?>"><a <?=GeefTarget("pagina", $row_rshm['paginaid'])?> href="<?=GeefPaginaURL($row_rshm['paginaid'])?>" class="<?=$myclass?>"><?=strtoupper($row_rshm['paginamenunaam'])?></a></li>
	<?
				if ($toonsubmenu) {
					$query_sm = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $row_rshm['paginaid'] . " AND opnemeninsubmenu<>0 and publiceren<>0 ORDER BY submenuvolgorde;";
					$rssm = mysql_query($query_sm, $GLOBALS['conn']) or die(mysql_error());
					$row_rssm = mysql_fetch_assoc($rssm);
					if (mysql_num_rows($rssm)>0) {
					do
					{
						$myclass="submenu";
						if ($GLOBALS['pagid']==$row_rssm['paginaid']) {
							$myclass="submenusel";
						}
						$mysubmenunaam = $row_rssm['paginamenunaam'];
						?>
				<li class="navsubmenu">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a <?=GeefTarget("pagina", $row_rssm['paginaid'])?> href="<?=GeefPaginaURL($row_rssm['paginaid'])?>" class="<?=$myclass?>"><?=strtolower($mysubmenunaam)?></a></li>
	<?
					}
					while ($row_rssm = mysql_fetch_assoc($rssm));
					}
				}
				$numtel = $numtel + 1;
			}
			?>
			<?
			if ($toonitem==true) {
			?>
			<div class="menustreep" /></div>
			<div class="menuspacer" /></div>
			<?
			}
			?>
			<?
		}
		while ($row_rshm = mysql_fetch_assoc($rshm));
		?>
			<li class="navmainmenu">&nbsp;</li>
		<?
	}
	mysql_free_result($rshm);
}

function ToonWebshopInloggen(){
?>
	<hr class="streep" /><h2>Inloggen</h2>
	<form method="post" id="inloggen" name="inloggen" action="applogin.php?prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&artid=<?=$GLOBALS['artid']?>">
	<table cellpadding="0" cellspacing="0">
		<tr><td>Loginnaam&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="klantloginnaam" name="klantloginnaam" size="40" maxlength="50" /></td></tr>
		<tr><td>Wachtwoord&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="password" id="klantwachtwoord" name="klantwachtwoord" size="40" maxlength="50" /></td></tr>
		<tr><td colspan="10" align="right"><input type="submit" class="btn" value="Login" /></td></tr>
	</table>
	</form>
	<?=ZetFocus("klantloginnaam")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("inloggen");
	frmvalidator.addValidation("klantloginnaam","req","Loginnaam <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("klantwachtwoord","req","Wachtwoord <?=Vertaal("isverplicht")?>");
	</script>
<?
}
function ToonWebshopLoginaanvraag(){
?>
	<hr class="streep" /><h2>Loginnaam aanvragen</h2>
	<form method="post" id="loginaanvragen" name="loginaanvragen" action="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefPaginaIdByModule("webshoploginaanvraagverzonden")?>&amp;taalcode=<?=$GLOBALS['GekozenTaal']?>">
	<table cellpadding="0" cellspacing="0">
		<tr><td colspan="10"><b>Bedrijfsgegevens</b></td></tr>
		<tr><td>Bedrijfsnaam&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="bedrijfsnaam" name="bedrijfsnaam" size="40" maxlength="100" /></td></tr>
		<tr><td>Vestigingsadres&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="adres" name="adres" size="40" maxlength="100" /></td></tr>
		<tr><td>Postcode&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="postcode" name="postcode" size="40" maxlength="100" /></td></tr>
		<tr><td>Plaats&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="plaats" name="plaats" size="40" maxlength="100" /></td></tr>

		<tr><td colspan="10"><b>Contactgegevens</b></td></tr>
		<tr><td>Contactpersoon&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="contactpersoon" name="contactpersoon" size="40" maxlength="100" /></td></tr>
		<tr><td><?=Vertaal("emailadres")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="emailadres" name="emailadres" size="40" maxlength="50" /></td></tr>
		<tr><td>Tel.nr&nbsp;</td><td><input class="txt" type="text" id="telefoonnummer" name="telefoonnummer" size="40" maxlength="50" /></td></tr>
		<tr><td colspan="10">&nbsp;</td></tr>
		<tr valign="top"><td>Opmerkingen&nbsp;</td><td><textarea class="txtarea" id="opmerkingen" name="opmerkingen" rows="4" cols="56"></textarea></td></tr>
		<tr><td colspan="10" align="right"><input type="submit" class="btn" value="<?=Vertaal("verstuur")?>" /></td></tr>
	</table>
	</form>
	<?=ZetFocus("bedrijfsnaam")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("loginaanvragen");
	frmvalidator.addValidation("bedrijfsnaam","req","Bedrijfsnaam <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("adres","req","Adres <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("plaats","req","Plaats <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("contactpersoon","req","Contactpersoon <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("emailadres","req","<?=Vertaal("emailadres")?> <?=Vertaal("isverplicht")?>");
	</script>
<?
}
function VerstuurWebshopLoginaanvraag(){
	$retval = "";
	$vals = array();
	foreach($_POST as $key => $value){
		if(!empty($_POST[$key])){
			switch($key){
				case "opmerkingen":
					$keyval = "\n".Vertaal($key);
					$valval = $value;
					break;
				default:
					if (Vertaal($key)!=$key) {
						$keyval = Vertaal($key);
					}
					else{
						$keyval = $key;
					}
					$valval = $value;
			} // switch
			$vals[] = $keyval.": ".$valval."\n";
		}
	}

	for($i = 0; $i < count($vals); $i++){
		$retval .=$vals[$i];
	}
	if ($retval!="") {
		$retvalvmcs = $retval . "\n".date('l jS \of F Y h:i:s A') ."\n\n".$GLOBALS['ServerRoot']."/login/index.php?hmid=" . GeefDbWaarde("admmenuid", "adminmenu", "admpaginaurl='cmsinfoaanvraag'");
		$toadres = GeefDBWaarde("taremailadres", "tarieven", "tarievenactief<>0");
		$fromadres = $_POST['emailadres'];
		$qry1="INSERT INTO informatieaanvragen (bedrijfsnaam, adres, postcode, plaats, contactpersoon, emailadres, telefoonnummer, opmerkingen, datumontvangen) VALUES( ";
		$qry2 = "".SQLStr($_POST['bedrijfsnaam']).", ".
			"".SQLStr($_POST['adres']).", ".
			"".SQLStr($_POST['postcode']).", ".
			"".SQLStr($_POST['plaats']).", ".
			"".SQLStr($_POST['contactpersoon']).", ".
			"".SQLStr($_POST['emailadres']).", ".
			"".SQLStr($_POST['telefoonnummer']).", ".
			"".SQLStr($_POST['opmerkingen']).", ".
			"".SQLDat(MaakDatum(Now))."";
		$qry3=")";
		$query_rs = $qry1.$qry2.$qry3;
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

		if ($toadres!="") {
			StuurMail($fromadres, $toadres, "Loginnaam aangevraagd " . $GLOBALS['ApplicatieNaam'], $retvalvmcs);
		}
		if ($fromadres!="") {
			$klantbericht = "LS,\r\n\r\nBedankt voor het aanvragen van een loginnaam. Indien deze wordt goedgekeurd, zullen wij contact met u opnemen. Onderstaand zijn de door ons ontvangen gegevens opgenomen.\r\n\r\n";
			StuurMail($fromadres, $toadres, $GLOBALS['ApplicatieNaam'] . " - Bedankt voor uw aanvraag", $klantbericht);
		}
	}
}
function ToonInformatieAanvraag(){
?>
	<table width="100%"><tr valign="top"><td width="60%">
	<h2><?=Vertaal("informatieaanvraag")?></h2>
	<form method="post" id="informatie" name="informatie" action="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefPaginaIdByModule("contactsent")?>&amp;taalcode=<?=$GLOBALS['GekozenTaal']?>">
	<table cellpadding="0" cellspacing="0">
		<tr><td><?=Vertaal("naam")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="contactpersoon" name="contactpersoon" size="40" maxlength="100" /></td></tr>
		<tr><td><?=Vertaal("emailadres")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="emailadres" name="emailadres" size="40" maxlength="50" /></td></tr>
		<tr><td><?=Vertaal("telefoonnummer")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="telefoonnummer" name="telefoonnummer" size="40" maxlength="50" /></td></tr>
		<tr><td><?=Vertaal("adres")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="adres" name="adres" size="40" maxlength="50" /></td></tr>
		<tr><td><?=Vertaal("postcode")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="postcode" name="postcode" size="20" maxlength="20" /></td></tr>
		<tr><td><?=Vertaal("plaats")?>&nbsp;<?=$GLOBALS['VerplichtSter']?>&nbsp;</td><td><input class="txt" type="text" id="plaats" name="plaats" size="40" maxlength="50" /></td></tr>

		<tr><td colspan="10">&nbsp;</td></tr>
		<tr valign="top"><td width="150">Vragen/Opmerkingen&nbsp;</td><td><textarea class="txtarea" id="opmerkingen" name="opmerkingen" style="width: 350px; height: 75px;"></textarea><textarea class="txtarea" id="extraopmerkingen" name="extraopmerkingen" style="width: 350px; height: 75px;"></textarea></td></tr>
		<tr><td colspan="10" align="right"><input type="submit" class="btn" value="<?=Vertaal("verstuur")?>" /></td></tr>
	</table>
	</form>
	<?=ZetFocus("contactpersoon")?>
	<script type="text/javascript">
	var frmvalidator  = new Validator("informatie");
	frmvalidator.addValidation("contactpersoon","req","<?=Vertaal("naam")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("emailadres","req","<?=Vertaal("emailadres")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("telefoonnummer","req","<?=Vertaal("telefoonnummer")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("adres","req","<?=Vertaal("adres")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("postcode","req","<?=Vertaal("postcode")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("plaats","req","<?=Vertaal("plaats")?> <?=Vertaal("isverplicht")?>");
	frmvalidator.addValidation("opmerkingen","req","<?=Vertaal("opmerkingen")?> <?=Vertaal("isverplicht")?>");
	</script>
	</td>
	<td width="40%" align="right">
	<?=ToonContactGegevens($GLOBALS['GekozenTaal'])?>
	</td>
	</tr>
	</table>
<?
}
function VerstuurInformatieAanvraag(){
	$retval = "";
	$vals = array();
	if ($_POST['extraopmerkingen']!="")
	{
		//spam
	}
	else
	{
		foreach($_POST as $key => $value){
			if(!empty($_POST[$key])){
				switch($key){
					case "opmerkingen":
						$keyval = "\n".Vertaal($key);
						$valval = $value;
						break;
					default:
						if (Vertaal($key)!=$key) {
							$keyval = Vertaal($key);
						}
						else{
							$keyval = $key;
						}
						$valval = $value;
				} // switch
				$vals[] = $keyval.": ".$valval."\n";
			}
		}

		for($i = 0; $i < count($vals); $i++){
			$retval .=$vals[$i];
		}
		if ($retval!="") {
			$retvalklant = "Wij hebben de volgende gegevens van u ontvangen:\n\n" . $retval;
			$retval .= "\n".date('l jS \of F Y h:i:s A') ."\n\n".$GLOBALS['ServerRoot']."/login/index.php?hmid=21";
			$toadres = GeefDBWaarde("taremailadres", "tarieven", "tarievenactief<>0");
			$fromadres = $_POST['emailadres'];
			$qry1="INSERT INTO informatieaanvragen (contactpersoon, emailadres, telefoonnummer, adres, postcode, plaats, opmerkingen, datumontvangen) VALUES( ";
			$qry2 = "".SQLStr($_POST['contactpersoon']).", ".
				"".SQLStr($_POST['emailadres']).", ".
				"".SQLStr($_POST['telefoonnummer']).", ".
				"".SQLStr($_POST['adres']).", ".
				"".SQLStr($_POST['postcode']).", ".
				"".SQLStr($_POST['plaats']).", ".
				"".SQLStr($_POST['opmerkingen']).", ".
				"".SQLDat(MaakDatum(Now))."";
			$qry3=")";
			$query_rs = $qry1.$qry2.$qry3;
			$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

			if ($toadres!="") {
				StuurMail($fromadres, $toadres, "Informatieaanvraag Website " . $GLOBALS['ApplicatieNaam'], $retval);
			}
			if ($fromadres!="") {
				StuurMail($toadres, $fromadres, "Bedankt voor uw bericht | " . $GLOBALS['ApplicatieNaam'], $retvalklant);
			}
		}
	}
}
?>
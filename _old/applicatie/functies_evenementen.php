<?
function ToonEvenementenLijstLeden($aant_ev){
	if (toInt($aant_ev)>0) {
		$aantfilter = " LIMIT " . $aant_ev . " ";
	}
	echo "<hr class='streep' />";

	$query_rsev = "SELECT * FROM evenementen WHERE (evvandatum>=NOW() OR evtotdatum>=NOW()) ".
		"AND evtaalcode='" . $GLOBALS['GekozenTaal'] . "' AND evpubliceren<>0 AND evalleenleden<>0 ORDER BY evvandatum, evtotdatum, eventid " . $aantfilter . ";";
	$rsev = mysql_query($query_rsev, $GLOBALS['conn']) or die(mysql_error());
	$row_rsev = mysql_fetch_assoc($rsev);
	if (mysql_num_rows($rsev)>0) {
		do
		{
		   	$projURL = $GLOBALS['HMURL']."?pagid=" . GeefPaginaIdByModule("EVENTSLEDEN") . "&amp;taalcode=".$GLOBALS['GekozenTaal'] . "&amp;evid=".$row_rsev['eventid'] . "&amp;evjaar=".$GLOBALS['evjaar']."&amp;evmaand=".$GLOBALS['evmaand'];
            $MijnDatumVan = $row_rsev["evvandatum"];
            $MijnDatumTot = $row_rsev["evtotdatum"];
            $MijnDatumTxt = MaakVolledigeDatum($MijnDatumVan);
            if ($MijnDatumTot!="" && $MijnDatumTot>$MijnDatumVan) {
            	$MijnDatumTxt = $MijnDatumTxt . " t/m " . MaakVolledigeDatum($MijnDatumTot);
            }
            $MijnDatDag = datDay($row_rsev['evvandatum']);
            $MijnDatMaand = datMonth($row_rsev['evvandatum']);
			?>
			<div align="center" style="float: left; filter: Shadow(Color=#cccccc, Direction=135, Strength=5); font-size: 40px; font-weight: bold; line-height: 50px; width: 120px; height: 50px; border: solid 1px #aaa;"><?=$MijnDatDag?>-<?=$MijnDatMaand?></div>
			<div style="float: left; margin-left: 10px; width: <?=$GLOBALS['BreedteWebsiteTekstdeel']-180?>px;">
			<a class="ilink" href="<?=$projURL?>"><b><?=$MijnDatumTxt?>: </b><?=$row_rsev['evtitel']?></a><br />
			<?=$row_rsev['evinleiding']?><br />
			<br />
			<? if (TelRecords("SELECT * FROM paragrafen WHERE pa_eventid=".$row_rsev['eventid']." AND pa_publiceren<>0")>0) {
				?>
				<a href="<?=$projURL?>">Meer informatie &gt;&gt;</a>
			<?
			}
			?>
			</div>
			<?
			echo "<div style='clear: both;'></div>";
			echo "<hr class='streep' />";
		}
		while ($row_rsev = mysql_fetch_assoc($rsev));
	}
	else
	{
		echo "Er zijn (nog) geen evenementen bekend voor leden.";
		echo "<hr class='streep' />";
	}
	mysql_free_result($rsev);

}

function ToonEvenementenLijst(){

	echo "<br />";
	echo "<hr class='streep' />";
	echo ToonEvenementenVorigeVolgende();
	echo "<hr class='streep' />";

	$query_rsev = "SELECT * FROM evenementen WHERE ((MONTH(evvandatum)=" . $GLOBALS['evmaand'] . " AND YEAR(evvandatum)=" . $GLOBALS['evjaar'] . ") ".
		"OR (MONTH(evtotdatum)=" . $GLOBALS['evmaand'] . " AND YEAR(evtotdatum)=" . $GLOBALS['evjaar'] . ") ".
		"OR (evvandatum<'" . $GLOBALS['evjaar']. "-" . $GLOBALS['evmaand'] . "-1' AND evtotdatum>'" . $GLOBALS['evjaar']. "-" . $GLOBALS['evmaand'] . "-1')) ".
		"AND evtaalcode='" . $GLOBALS['GekozenTaal'] . "' AND evpubliceren<>0 AND evalleenleden=0 ORDER BY evvandatum, evtotdatum, eventid;";
	$rsev = mysql_query($query_rsev, $GLOBALS['conn']) or die(mysql_error());
	$row_rsev = mysql_fetch_assoc($rsev);
	if (mysql_num_rows($rsev)>0) {
		do
		{
		   	$projURL = $GLOBALS['HMURL']."?pagid=" . GeefPaginaIdByModule("EVENTS") . "&amp;taalcode=".$GLOBALS['GekozenTaal'] . "&amp;evid=".$row_rsev['eventid'] . "&amp;evjaar=".$GLOBALS['evjaar']."&amp;evmaand=".$GLOBALS['evmaand'];
            $MijnDatumVan = $row_rsev["evvandatum"];
            $MijnDatumTot = $row_rsev["evtotdatum"];
            $MijnDatumTxt = MaakVolledigeDatum($MijnDatumVan);
            if ($MijnDatumTot!="" && $MijnDatumTot>$MijnDatumVan) {
            	$MijnDatumTxt = $MijnDatumTxt . " t/m " . MaakVolledigeDatum($MijnDatumTot);
            }
            $MijnDatDag = toInt(datDay($row_rsev['evvandatum']));
            if ($GLOBALS['evmaand']!=toInt(datMonth($row_rsev['evvandatum']))) {
            	$MijnDatDag = 1;
            }
			?>
			<div align="center" style="float: left; filter: Shadow(Color=#cccccc, Direction=135, Strength=5); font-size: 40px; font-weight: bold; line-height: 50px; width: 50px; height: 50px; border: solid 1px #aaa;"><?=$MijnDatDag?></div>
			<div style="float: left; margin-left: 10px; width: <?=$GLOBALS['BreedteWebsiteTekstdeel']-110?>px;">
			<a class="ilink" href="<?=$projURL?>"><b><?=$MijnDatumTxt?>: </b><?=$row_rsev['evtitel']?></a><br />
			<?=$row_rsev['evinleiding']?><br />
			<br />
			<? if (TelRecords("SELECT * FROM paragrafen WHERE pa_eventid=".$row_rsev['eventid']." AND pa_publiceren<>0")>0) {
				?>
				<a href="<?=$projURL?>">Meer informatie &gt;&gt;</a>
			<?
			}
			?>
			</div>
			<?
			echo "<div style='clear: both;'></div>";
			echo "<hr class='streep' />";
		}
		while ($row_rsev = mysql_fetch_assoc($rsev));
	}
	else
	{
		echo "Er zijn (nog) geen evenementen bekend in " . strtolower(GeefDbWaarde("maandnaam".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$GLOBALS['evmaand'])) . " " . $GLOBALS['evjaar'];
		echo "<hr class='streep' />";
	}
	mysql_free_result($rsev);

	echo ToonEvenementenTijdslijn();
	echo "<hr class='streep' />";

}
function ToonEvenementenTijdslijn(){
	$evURL = $GLOBALS['HMURL']."?pagid=" . GeefPaginaIdByModule("EVENTS") . "&amp;taalcode=".$GLOBALS['GekozenTaal'];
	?>
	<table align="center">
		<tr>
			<td><a href="<?=$evURL?>&amp;evjaar=<?=$GLOBALS['evjaar']-1?>&amp;evmaand=12">&lt;&lt;&nbsp;<?=$GLOBALS['evjaar']-1?></a></td>
			<td>|</td>
			<?
			for($i=1;$i<13;$i++)
			{
			?>
				<? if ($i==$GLOBALS['evmaand']) {
					?>
			<td><b><?=strtolower(GeefDbWaarde("maandkort".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$i))?></b></td>
					<?
				}
				else{
				?>
			<td><a href="<?=$evURL?>&amp;evjaar=<?=$GLOBALS['evjaar']?>&amp;evmaand=<?=$i?>"><?=strtolower(GeefDbWaarde("maandkort".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$i))?></a></td>
				<?
				}
				?>
			<td>|</td>
			<?
			}
			?>
			<td><a href="<?=$evURL?>&amp;evjaar=<?=$GLOBALS['evjaar']+1?>&amp;evmaand=1"><?=$GLOBALS['evjaar']+1?>&nbsp;&gt;&gt;</a></td>
		</tr>
	</table>
	<?
}

function ToonEvenementenVorigeVolgende(){
	$evURL = $GLOBALS['HMURL']."?pagid=" . GeefPaginaIdByModule("EVENTS") . "&amp;taalcode=".$GLOBALS['GekozenTaal'];
	$Vorigjaar = $GLOBALS['evjaar'];
	$Vorigmaand = $GLOBALS['evmaand']-1;
	$Volgendjaar = $GLOBALS['evjaar'];
	$Volgendmaand = $GLOBALS['evmaand']+1;
	if ($GLOBALS['evmaand']==1) {
		$Vorigjaar = $GLOBALS['evjaar']-1;
		$Vorigmaand = 12;
	}
	if ($GLOBALS['evmaand']==12) {
		$Volgendjaar = $GLOBALS['evjaar']+1;
		$Volgendmaand = 1;
	}
	?>
	<table align="center">
		<tr>
			<td><a href="<?=$evURL?>&amp;evjaar=<?=$Vorigjaar?>&amp;evmaand=<?=$Vorigmaand?>">&lt;&lt;&nbsp;<?=GeefDbWaarde("maandnaam".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$Vorigmaand)?>&nbsp;<?=$Vorigjaar?></a></td>
			<td>|</td>
			<td><b><?=GeefDbWaarde("maandnaam".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$GLOBALS['evmaand'])?>&nbsp;<?=$GLOBALS['evjaar']?></b></td>
			<td>|</td>
			<td><a href="<?=$evURL?>&amp;evjaar=<?=$Volgendjaar?>&amp;evmaand=<?=$Volgendmaand?>"><?=GeefDbWaarde("maandnaam".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$Volgendmaand)?>&nbsp;<?=$Volgendjaar?>&nbsp;&gt;&gt;</a></td>
		</tr>
	</table>
	<?
}
?>
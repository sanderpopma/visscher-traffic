<?
// In de website (raamwerk) te gebruiken globale variabelen

$brid = toInt($_GET["brid"]);                       				// Bestelregel
$bestid = toInt($_GET["bestid"]);                       			// Bestelling
$homeitemid= toInt($_GET["homeitemid"]);                       		// Home-instellingen
$vertaalid= toInt($_GET["vertaalid"]);                       		// Vertalingen
$faqid = $_GET["faqid"]; 											// faqid
$pagcode = $_GET["pagcode"]; 										// T.b.v. over ons en nieuws
$nwsjaar= toInt($_GET["nwsjaar"]);                       		// Nieuwsarchief
$evjaar= toInt($_GET["evjaar"]);                       		// Evenementen
$evmaand= toInt($_GET["evmaand"]);                       		// Evenementen
$linkrubid = toInt($_GET["linkrubid"]);                           			// Links
$linkid = toInt($_GET["linkid"]);                           			// Links

$catid = toInt($_GET["catid"]);                           			// Cateogire (leden)

$parid = toInt($_GET["parid"]);                           			// Alinea's
$tariefid = toInt($_GET["tariefid"]);                           	// Tarieven / Systeeminstellingen

$prodid = toInt($_GET["prodid"]);                           		// Producten
$prodgrpid = toInt($_GET["prodgrpid"]);                           	// Producten
$artid = toInt($_GET["artid"]);                           			// Producten
$paaid = toInt($_GET["paaid"]);                           			// Producten
$projcatid = toInt($_GET["projcatid"]);                           	// Projecten

$gebruikerid = toInt($_GET["gebruikerid"]);                     	// CMS Beheerders
$afbid= toInt($_GET["afbid"]);                     					// Afbeeldingen
$afbtype = $_GET['afbtype'];										// Afbeeldingen
$avid = toInt($_GET["avid"]);                           			// informatieaanvraag
$banid = toInt($_GET["banid"]);                           			// bannerid
$fileid = toInt($_GET["fileid"]);									// bestanden
$folderid = toInt($_GET["folderid"]);									// bestanden

$fotoboekid = toInt($_GET["fotoboekid"]);									// fotoboeken
$fotoid = toInt($_GET["fotoid"]);									// fotoboeken

$orgberichtid = toInt($_GET["orgberichtid"]);                       			// Berichten
?>
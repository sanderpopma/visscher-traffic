<?
$TitelWebsite = "Visscher Traffic";
$SubTitelWebsite = "Transport";
$ApplicatieNaam = "VisscherTraffic.nl";
$SessionPrefix = "VT";

$LiveGereed = true;

if ($LiveGereed==true) {
	$ServerRoot =  "http://www.visschertraffic.nl";
}
else
{
	$ServerRoot =  "http://vt.sybit.biz";
}
$ApplicatieRoot =  "";
$AppImgRoot = $GLOBALS['ApplicatieRoot'] . "/siteimg";
$AppJsRoot = $GLOBALS['ApplicatieRoot'] . "/sybit/js";
$HMURL = "index.php";

//Onderdelen van het raamwerk aan- of uitzetten
$MetLoginOveral = false;
$MetTaalModules = false;
$MetLoginStuurWachtwoordOp = true;
$MetCMSSubpaginas = true;		// om onder een pagina subpagina's te kunnen aanmaken
$MetCMSHoofdpaginas = true;		// om in het CMS hoofdpagina's te kunnen aanmaken
$MetPaginaURL = false;			// Om extra domeinnamen te kunnen gebruiken
$MetPaginaCode = false;			// Om te kunnen werken met paginacode's
$MetVacaturesUitgebreid = false;
$MetGoogleMaps = false;

// Upload instellingen
$AfbeeldingMaxHeel = 800;
$AfbeeldingMaxHeelTonen = 500;
$AfbeeldingMaxKlein = 150;
$AfbeeldingMaxArtikel = 181;
$AfbeeldingMaxRechts = 181;
$AfbeeldingMiniWidth = 56;
$MaxBestandsGrootte = 10*1000*1000; // 10Mb

if ($LiveGereed==true) {
$AppUploadFolder = "d:\www\visschertraffic.nl\www\images";
$CheckUploadFolder = "d:\www\visschertraffic.nl\www\images";
}
else
{
$AppUploadFolder = "e:\Websites\vt.sybit.biz\www\images";
$CheckUploadFolder = "e:\\Websites\\vt.sybit.biz\\www\\images";
}
$AppImageResizer = "_resizeimagefile.php";
$UploadImageFolder = "images";
$UploadFileFolder = "files";

// HTML-instellingen
$MetW3CValidatie = true;

//E-mail instellingen
$StdEmailNaam = "Visscher Traffic";
$StdEmailAdres = "info@visschertraffic.nl";
$StdEmailNaamAdres = $GLOBALS['StdEmailNaam'] . "<" . $GLOBALS['StdEmailAdres'] . ">";
$MetMailServer = true;

// Databaseverbinding en -instellingen
if ($LiveGereed==true) {
	$hostname_dbc = "db.visschertraffic.nl";
	$database_dbc = "md151546db226688";
	$username_dbc = "md151546db226688";
	$password_dbc = "5AP8up0c";
}
else
{
	$hostname_dbc = "localhost";
	$database_dbc = "visschertraffic";
	$username_dbc = "visschertraffic";
	$password_dbc = "VisscherTraffic";
}
// Google Maps
if ($LiveGereed==true) {
	$GoogleMapsKey = "";
}
else
{
	$GoogleMapsKey = "ABQIAAAAhfeauFxT8o1fQdzuohjA8BRGVS24B-HGhkTd434sPhuhbZZEShS8VbZrby2xt_zaQ6Jpgzsr12k6jQ";
}
// Weergave instellingen
$StdValutaTeken = "&euro;";
$StdDatumNotatie = "dd-mm-jjjj";
$StdTaalcode = "NL";
$VerplichtSter = "<span style='color: red;'>*</span>";
?>
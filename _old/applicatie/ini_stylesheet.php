<?
// Basis kleurenpalet
$KleurZwart = "#000000";
$KleurWit = "#ffffff";
$KleurStatusGroen = "#008000";

$KleurKlantBlauw = "#082872";
$KleurKlantLichtBlauw = "#00ace3";

// Teksten
$TekstLettertype = "Myriad Pro, Verdana, Helvetica";
$TekstLettergrootte = "13px";
$TekstLetterKleur = $GLOBALS['KleurZwart'];
$TekstH1Kleur = $KleurKlantLichtBlauw;
$TekstH1Lettergrootte = "20px";
$TekstH1Lettertype = $TekstLettertype;
$TekstH2Kleur = $KleurKlantLichtBlauw;
$TekstH2Lettergrootte = "13px";

$TekstMenuKleur =  $KleurWit;
$TekstMenuSelKleur =  $KleurWit;
$TekstMenuLettergrootte = "11px";
$TekstMenuLettertype = $TekstLettertype;

$TekstSubMenuKleur = $KleurWit;
$TekstSubMenuSelKleur = $KleurZwart;
$TekstSubMenuKleurOver = $KleurZwart;
$TekstSubMenuLettergrootte = "11px";
$TekstSubMenuLettertype = $TekstLettertype;

$TekstSubSubMenuLettergrootte = "10px";
$TekstSubSubMenuLettertype = $TekstLettertype;
$TekstSubSubMenuKleur = $KleurWit;
$TekstSubSubMenuSelKleur = $KleurZwart;
$TekstSubSubMenuKleurOver = $KleurZwart;

$TekstMenuAdresKleur = $KleurZwart;
$TekstMenuAdresLettergrootte = "10px";
$TekstMenuAdresLettertype = $TekstLettertype;

// Scherm
$KleurBGTekst = "#ffffff";
$KleurBGWebsite = "#ffffff";

//Formulieren
$KleurBGInputText = $GLOBALS['KleurWit'];

$BreedteWebsite = 980;
$BreedtePageContainer = $BreedteWebsite;
$HoogteWebsiteBoven1 = 264;		// afbeelding boven
$HoogteWebsiteBoven2 = 0;		// menubalk
$BreedteWebsiteLinks = 0;		// menu

$BreedteWebsiteRechts = 0;		// afb. rechts
$HoogteWebsiteOnder = 98;		// contact
$HoogteWebsiteBovenFoto = 0;
$BreedteWebsiteBovenFoto = 0;

$BreedteWebsiteTekstdeel = $BreedteWebsite - $BreedteWebsiteLinks - $BreedteWebsiteRechts;
$BreedteArtikelBox = ($BreedteWebsiteTekstdeel - 90)/2;
$BreedteFotoboekFoto = 190;
$AantalFotosPerRij = 4;

$KleurBGBoven1 = $KleurWit;
$KleurBGBoven2 = $KleurWit;
$KleurBGOnder = $KleurWit;
$KleurBGMenu = $KleurWit;
$KleurBGRechts = $KleurWit;

$TekstOnderKleur = $KleurWit;

// HTML
$LinkKleur = $KleurKlantBlauw;
$LinkOverKleur = $KleurKlantLichtBlauw;
$ILinkKleur = $KleurKlantBlauw;

$KleurDownloadKader = $KleurKlantLichtBlauw;
$KleurDownloadBox = $KleurWit;
$KleurDownloadLinkTekst = $KleurKlantBlauw;

// CMS
$KlantLogoSrc = "logocms.jpg";
$KleurLoginHeader = $KleurKlantBlauw;
$KleurLoginFooter = $KleurKlantLichtBlauw;
?>
<?
function GeefBestelTekst($prodgrp_id){
	$ret = "Proef aanvragen";
	if (ArtikelViaWebshop($prodgrp_id)==true) {
		$ret = "Bestel";
	}
	return $ret;
}
function GeefBevestigBestelTekst($prodgrp_id){
	$ret = "Proef aanvragen van dit artikel?";
	if (ArtikelViaWebshop($prodgrp_id)==true) {
		$ret = "Dit artikel bestellen?";
	}
	return $ret;
}
function GeefBestelArtikelURL($prodgrp_id){
	$ret = "artikel-proef";
	if (ArtikelViaWebshop($prodgrp_id)==true) {
		$ret = "artikel-bestel";
	}
	return $ret;
}
function ArtikelViaWebshop($prodgrp_id){
	$ret = false;
	if ($prodgrp_id!="") {
		$query_rsnws = "SELECT * FROM productgroepen WHERE prodgrp_publiceren<>0 AND prodgrpid=" . $prodgrp_id . " LIMIT 1;";
		$rsnws = mysql_query($query_rsnws, $GLOBALS['conn']) or die(mysql_error());
		$row_rsnws = mysql_fetch_assoc($rsnws);
		if (mysql_num_rows($rsnws)>0) {
			if (Int2Bool($row_rsnws['prodgrp_metwinkelwagen'])==true) {
				$ret = true;
			}
		}
		mysql_free_result($rsnws);
	}
	return $ret;
}
function ScriptBestel(){
	?>
	<script type="text/javascript">
	function BestelArtikel(art_id){
		if (confirm('<?=GeefBevestigBestelTekst($GLOBALS['prodgrpid'])?>')) {
			location.href = "<?=GeefBestelArtikelURL($GLOBALS['prodgrpid'])?>.php?prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&artid=" + art_id + "&taalcode=<?=$GLOBALS['GekozenTaal']?>";
		}
	}
	</script>
	<?
}
function ToonProductenPagina(){
    if ($GLOBALS['artid']>0) {
    	ToonArtikel();
    }
    else
    {
        if ($GLOBALS['prodid']>0) {
        	ToonProduct();
        	ToonArtikelenLijst();
        }
        else
        {
            if ($GLOBALS['prodgrpid']>0) {
            	ToonProductGroep();
            	ToonProductenLijst();
            }
            else
            {
				ToonProductgroepenLijst();			// functies_producten
			}
		}
	}

}

function ToonProductgroepenLijst(){
	$naamveld = "prodgrp_naam_".strtolower($GLOBALS['GekozenTaal']);
	$query_rsnws = "SELECT * FROM productgroepen WHERE prodgrp_publiceren<>0 ORDER BY prodgrp_volgorde;";
	$rsnws = mysql_query($query_rsnws, $GLOBALS['conn']) or die(mysql_error());
	$row_rsnws = mysql_fetch_assoc($rsnws);
	if (mysql_num_rows($rsnws)>0) {
		do
		{
			?>
			<li><a class="nwstitel" href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['WebshopPagId']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&prodgrpid=<?=$row_rsnws['prodgrpid']?>"><?=$row_rsnws[$naamveld]?></a><br /></li>
			<?
		}
		while ($row_rsnws = mysql_fetch_assoc($rsnws));
	}
	mysql_free_result($rsnws);
}

function ToonProductgroep(){
?>
	<?
	$query_rs = "SELECT * FROM artikelen WHERE art_productid IN(SELECT productid FROM producten WHERE pr_prodgrpid=".$GLOBALS['prodgrpid'].") AND art_publiceren<>0 AND art_afbeeldingid>0 ORDER BY rand() LIMIT 1;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
	?>
	<img src="<?=GeefAfbeeldingSrc($row_rs['art_afbeeldingid'])?>" class="border" alt="" align="right" hspace="10" />
	<?
	}
	mysql_free_result($rs);
	?>

	<?=VervangBRMemo(GeefDBWaarde("prodgrp_beschrijving_".strtolower($GLOBALS['GekozenTaal']), "productgroepen", "prodgrpid=".$GLOBALS['prodgrpid'])); ?><br /><br />
	<b>Kies uw subcategorie:</b><br />
<?
}

function ToonProductenLijst(){
	$naamveld = "productnaam_".strtolower($GLOBALS['GekozenTaal']);
	$query_rsnws = "SELECT * FROM producten WHERE pr_prodgrpid=" . $GLOBALS['prodgrpid'] . " and pr_publiceren<>0 ORDER BY pr_volgorde;";
	$rsnws = mysql_query($query_rsnws, $GLOBALS['conn']) or die(mysql_error());
	$row_rsnws = mysql_fetch_assoc($rsnws);
	if (mysql_num_rows($rsnws)>0) {
		do
		{
			?>
			<li><a class="nwstitel" href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$row_rsnws['productid']?>"><?=$row_rsnws[$naamveld]?></a><br /></li>
			<?
		}
		while ($row_rsnws = mysql_fetch_assoc($rsnws));
	}
	mysql_free_result($rsnws);
}

function ToonProduct(){
	if ($GLOBALS['prodid']>0) {
		$query_rs = "SELECT * FROM producten WHERE productid=".$GLOBALS['prodid']." AND pr_publiceren<>0 LIMIT 1;";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		if (mysql_num_rows($rs)>0) {
		$prodtxt = "productbeschrijving_" . strtolower($GLOBALS['GekozenTaal']);
			/*
			$query_rs2 = "SELECT * FROM productfotos WHERE foto_productid=".$GLOBALS['prodid']." ORDER BY rand() LIMIT 1;";
			$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
			$row_rs2 = mysql_fetch_assoc($rs2);
			if (mysql_num_rows($rs2)>0) {
			?>
			<img name="prodafb" id="prodafb" src="<?=GeefAfbeeldingSrc($row_rs2['foto_afbeeldingid'])?>" class="noborder" alt="" align="right" hspace="10" />
			<?
			}
			mysql_free_result($rs2);
			*/
		?>
		<?=VervangBRMemo($row_rs[$prodtxt])?><br /><br />
		<div style="clear:both;"></div>
		<?
		}
		mysql_free_result($rs);
	}
}

function ToonArtikelenLijst(){
	$naamveld = "art_naam";
	$query_rsnws = "SELECT * FROM artikelen WHERE art_publiceren<>0 AND art_productid=" . $GLOBALS['prodid'] ." ORDER BY art_naam;";
	$rsnws = mysql_query($query_rsnws, $GLOBALS['conn']) or die(mysql_error());
	$row_rsnws = mysql_fetch_assoc($rsnws);
	if (mysql_num_rows($rsnws)>0) {
		?>
		<?
		ScriptBestel();
		?>
		<?
		do
		{
			$txtPrijs = "";
			if ($row_rsnws['art_prijs']!="") {
				$txtPrijs = MaakBedrag($row_rsnws['art_prijs']);
			}
			$artUrl = $GLOBALS['HMURL'] . "?pagid=" . $GLOBALS['WebshopPagId'] . "&taalcode=" . $GLOBALS['GekozenTaal'] . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid'] . "&artid=" . $row_rsnws['artikelid'] . "";
			?>
			<div class="artikelbox" style="float: left; border: solid 1px #cccccc; width: <?=$GLOBALS['BreedteArtikelBox']?>px; padding: 10px; margin-bottom: 10px;"><?=GeefDbWaarde("productnaam_nl", "producten", "productid=".$GLOBALS['prodid'])?><br />
			<a class="artikelbox" href="<?=$artUrl?>">
			<?=$row_rsnws[$naamveld]?></a><br /><br />
			<table width="100%"><tr valign="top"><td width="<?=$GLOBALS['AfbeeldingMaxArtikel']?>">
			<?
			$afbSrc="";
			if (toInt($row_rsnws['art_afbeeldingid']>0)) {
				$afbSrc = GeefAfbeeldingSrc($row_rsnws['art_afbeeldingid']);
			}
			else
			{
				$afbSrc = $GLOBALS['AppImgRoot']."/leeg.gif";
			}
			if ($afbSrc=="") {
				$afbSrc = $GLOBALS['AppImgRoot']."/leeg.gif";
			}
			?>
			<a class="artikelbox" href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['WebshopPagId']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&prodgrpid=<?=$GLOBALS['prodgrpid']?>&prodid=<?=$GLOBALS['prodid']?>&artid=<?=$row_rsnws['artikelid']?>">
			<img src="<?=$afbSrc?>" border="0" class="border" width="<?=$GLOBALS['AfbeeldingMaxArtikel']?>">
			</a>
			<?
			?>
			</td>
			<td width="10"></td><td>
			<?
			if ($row_rsnws['art_omschrijving']!="") {
				echo "<div class='artikelboxbeschr'><br />" . VervangBRMemo(substr($row_rsnws['art_omschrijving'], 0, 120)) . "...<br /><br /><a class='artikelboxbeschr' href='$artUrl'>Meer...</a></div>";
			}
			?>
			<div style="text-align: right; width: 100%; font-weight: bold; font-size: 24px; color: <?=$GLOBALS['KleurKlantGroen']?>"><?=$txtPrijs?>&nbsp;</div>
			</td></tr></table>
			<div style="text-align: right; width: 100%;"><a class="bestel" href="#" onclick="BestelArtikel('<?=$row_rsnws['artikelid']?>'); return false;"><?=Icoon("winkelwagen")?><?=GeefBestelTekst($GLOBALS['prodgrpid'])?> &gt;</a>&nbsp;</div>
			</div>
			<div style="float: left; width: 5px;"></div>
			<?
		}
		while ($row_rsnws = mysql_fetch_assoc($rsnws));
	}
	mysql_free_result($rsnws);
}
function ToonArtikel(){
	$prodUrl = $GLOBALS['HMURL'] . "?pagid=" . $GLOBALS['WebshopPagId'] . "&taalcode=" . $GLOBALS['GekozenTaal'] . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid'] . "";
	if ($GLOBALS['artid']>0) {
		$query_rs = "SELECT * FROM artikelen WHERE artikelid=".$GLOBALS['artid']." AND art_publiceren<>0 LIMIT 1;";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		if (mysql_num_rows($rs)>0) {
			if (toInt($row_rs['art_afbeeldingid'])>0) {
			?>
			<img name="prodafb" id="prodafb" src="<?=GeefAfbeeldingSrc($row_rs['art_afbeeldingid'])?>" class="border" alt="" align="right" hspace="10" />
			<?
			}
		?>
		<?=VervangBRMemo($row_rs['art_omschrijving'])?><br /><br />
		<?
		ScriptBestel();
		?>
		<?
			$txtPrijs = "";
			if ($row_rs['art_prijs']!="") {
				$txtPrijs = MaakBedrag($row_rs['art_prijs']);
				?>
				<div style="font-weight: bold; font-size: 24px; color: #000000;"><?=$txtPrijs?>&nbsp;</div><br />
				<?
			}
		?>
		<a class="bestel" href="#" onclick="BestelArtikel('<?=$row_rs['artikelid']?>'); return false;"><?=Icoon("winkelwagen")?><?=GeefBestelTekst($GLOBALS['prodgrpid'])?> &gt;</a><br /><br />
		<div style="clear:both;"></div>

		<?
		}
		mysql_free_result($rs);
	}
	?>
	<a href="<?=$prodUrl?>">&lt; Terug naar vorige</a>
	<?
}

function ToonBreadcrumbProducten(){
	$MyBaseUrl = $GLOBALS['HMURL'] . "?pagid=" . $GLOBALS['WebshopPagId'];
	$ret = "";
    if ($GLOBALS['prodgrpid']>0) {
    	$ret = "<a class='breadcrumb' href='" . $MyBaseUrl . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "' />" . GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0") . "</a>";
    }
    if ($GLOBALS['prodid']>0) {
    	$ret .= "&nbsp;&gt;&nbsp;<a class='breadcrumb' href='" . $MyBaseUrl . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid'] . "' />" . GeefDbWaarde("productnaam_nl", "producten", "productid=" . $GLOBALS['prodid']. " AND pr_publiceren<>0") . "</a>";
    }
    if ($GLOBALS['artid']>0) {
    	$ret .= "&nbsp;&gt;&nbsp;<a class='breadcrumb' href='" . $MyBaseUrl . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid'] . "&artid=" . $GLOBALS['artid'] . "' />" . GeefDbWaarde("art_naam", "artikelen", "artikelid=" . $GLOBALS['artid']. " AND art_publiceren<>0") . "</a>";
    }
	return $ret;
}
function GeefProductgroepTitel(){
	$ret = "";
    if ($GLOBALS['artid']>0) {
        if ($GLOBALS['prodgrpid']>0) {
        	$ret = GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0");
        }
        if ($GLOBALS['prodid']>0) {
            $ret .= " - " . GeefDbWaarde("productnaam_nl", "producten", "productid=" . $GLOBALS['prodid']. " AND pr_publiceren<>0");
        }
        $ret .= " - " . GeefDbWaarde("art_naam", "artikelen", "artikelid=" . $GLOBALS['artid']. " AND art_publiceren<>0");
    }
    else
    {
        if ($GLOBALS['prodid']>0) {
            if ($GLOBALS['prodgrpid']>0) {
            	$ret = GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0");
            }
            $ret .= " - " . GeefDbWaarde("productnaam_nl", "producten", "productid=" . $GLOBALS['prodid']. " AND pr_publiceren<>0");
        }
        else
        {
            if ($GLOBALS['prodgrpid']>0) {
            	$ret = GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0");
            }
            else
            {
				//
			}
		}
	}
	return $ret;
}
function GeefProductgroepH1Titel(){
	$ret = "";
    if ($GLOBALS['artid']>0) {
        $ret .= GeefDbWaarde("art_naam", "artikelen", "artikelid=" . $GLOBALS['artid']. " AND art_publiceren<>0");
    }
    else
    {
        if ($GLOBALS['prodid']>0) {
            if ($GLOBALS['prodgrpid']>0) {
            	$ret = GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0");
            }
            $ret .= " - " . GeefDbWaarde("productnaam_nl", "producten", "productid=" . $GLOBALS['prodid']. " AND pr_publiceren<>0");
        }
        else
        {
            if ($GLOBALS['prodgrpid']>0) {
            	$ret = GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=" . $GLOBALS['prodgrpid']. " AND prodgrp_publiceren<>0");
            }
            else
            {
				//
			}
		}
	}
	return $ret;
}
?>
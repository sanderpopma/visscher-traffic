/*
  -------------------------------------------------------------------------
	                    JavaScript Form Validator 
                                Version 2.0.2
	Copyright 2003 JavaScript-coder.com. All rights reserved.
	You use this script in your Web pages, provided these opening credit
    lines are kept intact.
	The Form validation script is distributed free from JavaScript-Coder.com

	You may please add a link to JavaScript-Coder.com, 
	making it easy for others to find this script.
	Checkout the Give a link and Get a link page:
	http://www.javascript-coder.com/links/how-to-link.php

    You may not reprint or redistribute this code without permission from 
    JavaScript-Coder.com.
	
	JavaScript Coder
	It precisely codes what you imagine!
	Grab your copy here:
		http://www.javascript-coder.com/
    -------------------------------------------------------------------------  
*/
function Validator(frmname)
{
  this.formobj=document.forms[frmname];
	if(!this.formobj)
	{
	  alert("BUG: couldnot get Form object "+frmname);
		return;
	}
	if(this.formobj.onsubmit)
	{
	 this.formobj.old_onsubmit = this.formobj.onsubmit;
	 this.formobj.onsubmit=null;
	}
	else
	{
	 this.formobj.old_onsubmit = null;
	}
	this.formobj.onsubmit=form_submit_handler;
	this.addValidation = add_validation;
	this.setAddnlValidationFunction=set_addnl_vfunction;
	this.clearAllValidations = clear_all_validations;
}
function set_addnl_vfunction(functionname)
{
  this.formobj.addnlvalidation = functionname;
}
function clear_all_validations()
{
	for(var itr=0;itr < this.formobj.elements.length;itr++)
	{
		this.formobj.elements[itr].validationset = null;
	}
}
function form_submit_handler()
{
	for(var itr=0;itr < this.elements.length;itr++)
	{
		if(this.elements[itr].validationset &&
	   !this.elements[itr].validationset.validate())
		{
		  return false;
		}
	}
	if(this.addnlvalidation)
	{
	  str =" var ret = "+this.addnlvalidation+"()";
	  eval(str);
    if(!ret) return ret;
	}
	for(var itr=0;itr < this.elements.length;itr++)
	{
		if (this.elements[itr].name=="VerstuurKnop")
		{
			this.elements[itr].disabled=true;
			this.elements[itr].value="Bezig met verwerken...";
		}
	}
	return true;
}
function add_validation(itemname,descriptor,errstr)
{
  if(!this.formobj)
	{
	  alert("BUG: the form object is not set properly");
		return;
	}

	if (descriptor!="allreq" && descriptor!="allequal" && descriptor!="allnotequal")
	{
		var itemobj = this.formobj[itemname];
		if(!itemobj)
		{
			alert("BUG: Couldnot get the input object named: "+itemname);
			return;
		}
		if(!itemobj.validationset)
		{
			itemobj.validationset = new ValidationSet(itemobj);
		}
		itemobj.validationset.add(descriptor,errstr);
	}
	else
	{
		var tempArr = new Array();
		tempArr = itemname.split(";");
		for(var itr=0;itr<tempArr.length;itr++)
		{
			var itemobj = this.formobj[tempArr[itr]];
			if(!itemobj)
			{
				alert("BUG: Couldnot get the input object named: "+tempArr[itr]);
				return;
			}
			if(itr==0)
			{
				if(!itemobj.validationset)
				{
					itemobj.validationset = new ValidationSet(itemobj);
				}
				itemobj.validationset.add(descriptor,itemname + ';' + errstr);
			}
		}
	}  
}
function ValidationDesc(inputitem,desc,error)
{
  this.desc=desc;
	this.error=error;
	this.itemobj = inputitem;
	this.validate=vdesc_validate;
}
function vdesc_validate()
{
 if(!V2validateData(this.desc,this.itemobj,this.error))
 {
	if (this.itemobj.type!="hidden")
	{
    this.itemobj.focus();
    }
		return false;
 }
 return true;
}
function ValidationSet(inputitem)
{
    this.vSet=new Array();
	this.add= add_validationdesc;
	this.validate= vset_validate;
	this.itemobj = inputitem;
}
function add_validationdesc(desc,error)
{
  this.vSet[this.vSet.length]= 
	  new ValidationDesc(this.itemobj,desc,error);
}
function vset_validate()
{
   for(var itr=0;itr<this.vSet.length;itr++)
	 {
	   if(!this.vSet[itr].validate())
		 {
		   return false;
		 }
	 }
	 return true;
}
function validateEmailv2(email)
{
// a very simple email validation checking. 
// you can add more complex email checking if it helps 
    if(email.length <= 0)
	{
	  return true;
	}
    var splitted = email.match("^(.+)@(.+)$");
    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null) 
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if
      return true;
    }
return false;
}
function V2validateData(strValidateStr,objValue,strError) 
{ 
    var epos = strValidateStr.search("="); 
    var  command  = ""; 
    var  cmdvalue = ""; 
    if(epos >= 0) 
    { 
     command  = strValidateStr.substring(0,epos); 
     cmdvalue = strValidateStr.substr(epos+1); 
    } 
    else 
    { 
     command = strValidateStr; 
    } 
    switch(command) 
    { 
        case "req": 
        case "required": 
         { 
           if(eval(objValue.value.length) == 0) 
           { 
              objValue.style.borderColor = "#ff0000";
              objValue.style.backgroundColor = "#ffeded";
              if(!strError || strError.length ==0) 
              { 
                strError = objValue.name + " : Required Field"; 
              }//if 
              alert(strError); 
              return false; 
           }//if 
           break;             
         }//case required 
	case "reqCK":
	  {
         //var editor_data = CKEDITOR.instances.editor1.getData();
		// Get the editor instance that we want to interact with.
		//alert(CKEDITOR.instances(objValue.name).getData());
		//var oEditor = CKEDITOR.instances(objValue.name) ;
		/*if( (CKEDITOR.instances(objValue.name).getData().length<=0) || (CKEDITOR.instances(objValue.name).getData()=="<BR>"))
			{
			alert(strError);
			//oEditor.EditorDocument.body.focus();
			return false;
			}
		*/
		break;
	  }

	case "reqFCK":
	  {
		// Get the editor instance that we want to interact with.
		var oEditor = FCKeditorAPI.GetInstance(objValue.name) ;
		if( (oEditor.EditorDocument.body.innerHTML.length<=0) || (oEditor.EditorDocument.body.innerHTML=="<BR>"))
			{
			alert(strError);
			oEditor.EditorDocument.body.focus();
			return false;
			}
		break;
	  }
        case "allreq":
          {
			var tempArr = new Array();
			var EenIsGezet = false;
			tempArr = strError.split(";");
			for(var itr=0;itr<tempArr.length-1;itr++)
			{
				if(eval(document.getElementById(tempArr[itr]).value.length) != 0) 
				{
					if (document.getElementById(tempArr[itr]).type=="hidden")
					{
						if (document.getElementById(tempArr[itr]).value!="-1")
						{
							EenIsGezet = true;
						}
					}
					else
					{
						EenIsGezet = true;
					}
				}
				if (EenIsGezet==true)
				{
					return true;
				}
			}
			if (EenIsGezet==false)
			{
				alert(tempArr[tempArr.length-1]);
			    return false;
			}
            break;
          }
        case "allequal":
          {
			var tempArr = new Array();
			var VeldenZijnGelijk = true;
			tempArr = strError.split(";");
			var EersteWaarde = document.getElementById(tempArr[0]).value;
			for(var itr=1;itr<tempArr.length-1;itr++)
			{
				if (document.getElementById(tempArr[itr]).value!= EersteWaarde)
				{
					VeldenZijnGelijk = false;
				}
				else
				{
					VeldenZijnGelijk = true;
				}
			}
			if (VeldenZijnGelijk==true)
			{
				return true;
			}
			if (VeldenZijnGelijk==false)
			{
				alert(tempArr[tempArr.length-1]);
			    return false;
			}
            break;
          }
        case "allnotequal":
          {
			var tempArr = new Array();
			var VeldenZijnOnGelijk = true;
			tempArr = strError.split(";");
			var EersteWaarde = document.getElementById(tempArr[0]).value;
			for(var itr=1;itr<tempArr.length-1;itr++)
			{
				if (document.getElementById(tempArr[itr]).value!= EersteWaarde)
				{
					VeldenZijnOnGelijk = true;
				}
				else
				{
					VeldenZijnOnGelijk = false;
				}
			}
			if (VeldenZijnOnGelijk==true)
			{
				return true;
			}
			if (VeldenZijnOnGelijk==false)
			{
				alert(tempArr[tempArr.length-1]);
			    return false;
			}
            break;
          }
        case "dat": 
        case "date": 
         {
            // Initialiseren parameters
		        var tempStr;					// temporary string
		        var tempArr;					// temporary array
		        var tempDate;					// temporary datum
		        var tempDay;					// temporary day
		        var tempMonth;				// temporary month
		        var tempYear;					// temporary year
		        var lengthStr;				// lengte van de ingegeven string
		        var lengthArr;				// aantal delen van de ingegeven string gesplit op het minteken
		        var boolOK;						// boolean waarde of de check goed gaat
		        var regexp;						// string voor reguliere expressie
		        var retStr;						// string voor opgebouwde datum
		        var language;					// string voor taal
        		
		        // Zetten parameters
		        boolOK = true;
		        tempDate = new Date();
		        //language = jsStandaardInvoertaal;
			language = "NL";
		        if(language=="NL")
		        {
		          regexp = /^(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d$/;
		        }
		        else
		        {
		          regexp = /^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$/;
		        }
		        // Uit hoeveel delen bestaat de ingevulde datum
		        tempStr = objValue.value;
		        if(tempStr=="")
		        {
		          return true;
		        }
		        tempArr = new Array();
		        if(language=="NL")
		        {
		          tempArr = tempStr.split("-");
		        }
		        else
		        {
		          tempArr = tempStr.split("/");
		        }		
		        lengthStr = tempStr.length;
		        lengthArr = tempArr.length;
        		
		        // Bouw de juiste datum op naar aanleiding van de opgegeven string
		        if(lengthArr==1)						// d OF dd
		        {
		          if(lengthStr==1)						// d
		          {
		            retStr = "0" + tempStr;
			          tempMonth = tempDate.getMonth()+1;
			          if(tempMonth<10)
			          {
			            tempMonth = "0" + tempMonth;
			          }
			          if(language=="NL")
			          {
			            retStr = retStr + "-" + tempMonth + "-" + tempDate.getYear();
			          }
			          else
			          {
			            retStr = tempMonth + "/" + retStr + "/" + tempDate.getYear();
			          }
		          }
		          else if(lengthStr==2)					// dd
		          {
			          tempMonth = tempDate.getMonth()+1;
			          if(tempMonth<10)
			          {
			            tempMonth = "0" + tempMonth;
			          }
			          if(language=="NL")
			          {
			            retStr = tempStr + "-" + tempMonth + "-" + tempDate.getYear();
			          }
			          else
			          {
			            retStr = tempMonth + "/" + tempStr + "/" + tempDate.getYear();
			          }
		          }
		          else    									// niet mogelijk
		          {
		            boolOK = false;
		          }
		        }
		        else if(lengthArr==2)					// d-m OF dd-m OF d-mm OF dd-mm --- m/d OF m/dd OF mm/d OF mm/dd
		        {
		          if(lengthStr==3)						// d-m --- m/d
		          {
		            if(tempStr.charAt(1))
			          {
			            if(language=="NL")				// d-m
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + "-" + tempDate.getYear();
			            }
			            else								// m/d
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + "/" + tempDate.getYear();
			            }
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==4)					// dd-m OF d-mm --- m/dd OF mm/d
		          {
		            if(tempStr.charAt(2)=="-" && language=="NL")		// dd-m
			          {
			            retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3) + "-" + tempDate.getYear();
			          }
			          else if(tempStr.charAt(1)=="-" && language=="NL")	// d-mm
			          {
			            retStr = "0" + tempStr + "-" + tempDate.getYear();
			          }
			          else if(tempStr.charAt(1)=="/" && language!="NL")	// m/dd
			          {
			            retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + tempStr.charAt(3) + "/" + tempDate.getYear();
			          }
			          else if(tempStr.charAt(2)=="/" && language!="NL")	// mm/d
			          {
			            retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3) + "/" + tempDate.getYear();
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==5)					// dd-mm --- mm/dd
		          {
		            if(language=="NL")					// d-m
			          {
			            retStr = tempStr + "-" + tempDate.getYear();
			          }
			          else								// m/d
			          {
			            retStr = tempStr + "/" + tempDate.getYear();
			          }
		          }
		          else									// niet mogelijk
		          {
		            boolOK = false;
		          }
		        }
		        else if(lengthArr==3)					// d-m-yy OF dd-m-yy OF d-mm-yy OF d-m-yyyy OF dd-mm-yy OF dd-m-yyyy OF d-mm-yyyy OF dd-mm-yyyy
		        {										          // --- m/d/yy OF m/dd/yy OF mm/d/yy OF m/d/yyyy OF mm/dd/yy OF m/dd/yyyy OF mm/d/yyyy OF mm/dd/yyyy
		          if(lengthStr==6)						// d-m-yy --- m/d/yy
		          {
		            if(tempStr.charAt(1)=="-" && tempStr.charAt(3)=="-" && language=="NL")		// d-m-yy
			          {
			            tempYear = parseInt(tempStr.charAt(4) + tempStr.charAt(5));
			            if(tempYear<7)
			            {
			              tempYear = "200" + tempYear;
			            }
			            else
			            {
			              tempYear = "19" + tempYear;
			            }
			            retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + "-" + tempYear;
			          }
			          else if(tempStr.charAt(1)=="/" && tempStr.charAt(3)=="/" && language!="NL")	// m/d/yy
			          {
			            tempYear = parseInt(tempStr.charAt(4) + tempStr.charAt(5));
			            if(tempYear<7)
			            {
			              tempYear = "200" + tempYear;
			            }
			            else
			            {
			              tempYear = "19" + tempYear;
			            }
			            retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + "/" + tempYear;
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==7)						// dd-m-yy OF d-mm-yy --- m/dd/yy OF mm/d/yy
		          {
		            if((tempStr.charAt(2)=="-" && tempStr.charAt(4)=="-" && language=="NL") || (tempStr.charAt(1)=="-" && tempStr.charAt(4)=="-" && language=="NL") ||
			             (tempStr.charAt(2)=="/" && tempStr.charAt(4)=="/" && language!="NL") || (tempStr.charAt(1)=="/" && tempStr.charAt(4)=="/" && language!="NL"))
			          {
		              if(tempStr.charAt(2)=="-" && language=="NL")			// dd-m-yy
			            {
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3) + tempStr.charAt(4);
			              tempYear = parseInt(tempStr.charAt(5) + tempStr.charAt(6));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = retStr + tempYear;
			            }
			            else if(tempStr.charAt(1)=="-" && language=="NL")		// d-mm-yy
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + tempStr.charAt(3) + tempStr.charAt(4);
			              tempYear = parseInt(tempStr.charAt(5) + tempStr.charAt(6));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = retStr + tempYear;
			            }
			            else if(tempStr.charAt(1)=="/" && language!="NL")			// m/dd/yy
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + tempStr.charAt(3) + tempStr.charAt(4);
			              tempYear = parseInt(tempStr.charAt(5) + tempStr.charAt(6));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = retStr + tempYear;
			            }
			            else if(tempStr.charAt(2)=="/" && language!="NL")		// mm/d/yy
			            {
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3) + tempStr.charAt(4);
			              tempYear = parseInt(tempStr.charAt(5) + tempStr.charAt(6));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = retStr + tempYear;
			            }
			            else
			            {
			              boolOK = false;
			            }
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==8)						// d-m-yyyy OF dd-mm-yy --- m/d/yyyy OF mm/dd/yy
		          {
		            if((tempStr.charAt(2)=="-" && tempStr.charAt(5)=="-" && language=="NL") || (tempStr.charAt(1)=="-" && tempStr.charAt(3)=="-" && language=="NL") ||
			             (tempStr.charAt(2)=="/" && tempStr.charAt(5)=="/" && language!="NL") || (tempStr.charAt(1)=="/" && tempStr.charAt(3)=="/" && language!="NL"))
			          {
		              if(tempStr.charAt(2)=="-" && language=="NL")			// dd-mm-yy
			            {
			              tempYear = parseInt(tempStr.charAt(6) + tempStr.charAt(7));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempYear;
			            }
			            else if(tempStr.charAt(1)=="-" && language=="NL")		// d-m-yyyy
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempStr.charAt(6) + tempStr.charAt(7);
			            }
			            else if(tempStr.charAt(2)=="/" && language!="NL")		// mm/dd/yy
			            {
			              tempYear = parseInt(tempStr.charAt(6) + tempStr.charAt(7));
			              if(tempYear<7)
			              {
			                tempYear = "200" + tempYear;
			              }
			              else
			              {
			                tempYear = "19" + tempYear;
			              }
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempYear;
			            }
			            else if(tempStr.charAt(1)=="/" && language!="NL")		// m/d/yyyy
			            {
			              retStr = "0" + tempStr.charAt(0) + tempStr.charAt(1) + "0" + tempStr.charAt(2) + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempStr.charAt(6) + tempStr.charAt(7);
			            }
			            else
			            {
			              boolOK = false;
		 	            }
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==9)						// dd-m-yyyy OF d-mm-yyyy --- m/dd/yyyy OF mm/d/yyyy
		          {
		            if((tempStr.charAt(2)=="-" && tempStr.charAt(4)=="-" && language=="NL") || (tempStr.charAt(1)=="-" && tempStr.charAt(4)=="-" && language=="NL") ||
			             (tempStr.charAt(2)=="/" && tempStr.charAt(4)=="/" && language!="NL") || (tempStr.charAt(1)=="/" && tempStr.charAt(4)=="/" && language!="NL"))
			          {
		              if(tempStr.charAt(2)=="-" && language=="NL")			// dd-m-yyyy
			            {
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempStr.charAt(6);
			              retStr = retStr + tempStr.charAt(7) + tempStr.charAt(8);
			            }
			            else if(tempStr.charAt(1)=="-" && language=="NL")		// d-mm-yyyy
			            {
			              retStr = "0" + tempStr;
			            }
			            else if(tempStr.charAt(2)=="/" && language!="NL")		// mm/d/yyyy
			            {
			              retStr = tempStr.charAt(0) + tempStr.charAt(1) + tempStr.charAt(2) + "0" + tempStr.charAt(3);
			              retStr = retStr + tempStr.charAt(4) + tempStr.charAt(5) + tempStr.charAt(6);
			              retStr = retStr + tempStr.charAt(7) + tempStr.charAt(8);
			            }
			            else if(tempStr.charAt(1)=="/" && language!="NL")		// m/dd/yyyy
			            {
			              retStr = "0" + tempStr;
			            }
			            else
			            {
			              boolOK = false;
			            }
			          }
			          else
			          {
			            boolOK = false;
			          }
		          }
		          else if(lengthStr==10)				// dd-mm-yyyy --- mm/dd/yyyy
		          {
		            retStr = tempStr;
		          }
		          else									// niet mogelijk
		          {
		            boolOK = false;
		          }
		        }
		        else
		        {
		          boolOK = false;
		        }
		        
		        // Is er een goede datum ingevuld of niet (boolOK)
		        if(boolOK == false)
		        {
		          if(!strError || strError.length ==0) 
		          { 
			          strError = objValue.name+": Only digits allowed "; 
		          }//if
		          alert(strError);
		          return false;
		        }
		        
		        // Checken of er een bestaande datum is ingevuld
		        var0 = parseInt(retStr.charAt(0));
		        var1 = parseInt(retStr.charAt(1));
		        var3 = parseInt(retStr.charAt(3));
		        var4 = parseInt(retStr.charAt(4));
		        var6 = parseInt(retStr.charAt(6));
		        var7 = parseInt(retStr.charAt(7));
		        var8 = parseInt(retStr.charAt(8));
		        var9 = parseInt(retStr.charAt(9));
		        if(language=="NL")
		        {
		          tempDay = parseInt(parseInt(var0*10) + parseInt(var1*1))
		          tempMonth = parseInt(parseInt(var3*10) + parseInt(var4*1))
		          tempYear = parseInt(parseInt(var6*1000) + parseInt(var7*100) + parseInt(var8*10) + parseInt(var9*1))
		        }
		        else
		        {
		          tempMonth = parseInt(parseInt(var0*10) + parseInt(var1*1))
		          tempDay = parseInt(parseInt(var3*10) + parseInt(var4*1))
		          tempYear = parseInt(parseInt(var6*1000) + parseInt(var7*100) + parseInt(var8*10) + parseInt(var9*1))
		        }
		        if(tempMonth==1 || tempMonth==3 || tempMonth==5 || tempMonth==7 || tempMonth==8 || tempMonth==10 || tempMonth==12 )
		        {
		          if(tempDay<1 || tempDay>31)
		          {
		            boolOK = false;
		          }
		        }
		        else if(tempMonth==4 || tempMonth==6 || tempMonth==9 || tempMonth==11)
		        {
		          if(tempDay<1 || tempDay>30)
		          {
		            boolOK = false;
		          }
		        }
		        else if(tempMonth==2)
		        {
		          var checkEen = tempYear / 4;
		          var checkTwee = tempYear / 100;
		          var checkDrie = tempYear / 400;
        		  
		          if((checkEen*4==tempYear && checkTwee*4==tempYear) || (checkEen*4==tempYear && checkTwee*4==tempYear && checkDrie*4==tempYear))
		          {
		            if(tempDay<1 || tempDay>29)
			          {
			            boolOK = false;
			          }
		          }
		          else
		          {
		            if(tempDay<1 || tempDay>28)
			          {
			            boolOK = false;
			          }
		          }
		        }
		        else
		        {
		          boolOK = false;
		        }
		        
		        // Is er een goede datum ingevuld of niet (boolOK)
		        if(boolOK == false || retStr.search(regexp)==-1)
		        {
		          if(!strError || strError.length ==0) 
		          { 
			          strError = objValue.name+": Only digits allowed "; 
		          }//if
		          alert(strError);
		          return false;
		        }
            break;             
         }//case date
        case "dec": 
        case "decimal": 
         { 
            var charpos = objValue.value.search(/^-{0,1}\d*[\. \,]{0,1}\d+$/);
            if(objValue.value.length > 0 &&  charpos == -1) 
            { 
              if(!strError || strError.length ==0) 
              { 
                strError = objValue.name+": Only digits allowed "; 
              }//if               
              alert(strError); 
              return false; 
            }//if 
            break;           
         }//case decimal
        case "maxlength": 
        case "maxlen": 
          { 
             if(eval(objValue.value.length) >  eval(cmdvalue)) 
             { 
               if(!strError || strError.length ==0) 
               { 
                 strError = objValue.name + " : "+cmdvalue+" characters maximum "; 
               }//if 
               alert(strError); 
               return false; 
             }//if 
             break; 
          }//case maxlen 
        case "minlength": 
        case "minlen": 
           { 
             if(eval(objValue.value.length) <  eval(cmdvalue)) 
             { 
               if(!strError || strError.length ==0) 
               { 
                 strError = objValue.name + " : " + cmdvalue + " characters minimum  "; 
               }//if               
               alert(strError); 
               return false;                 
             }//if 
             break; 
            }//case minlen 
        case "alnum": 
        case "alphanumeric": 
           { 
              var charpos = objValue.value.search("[^A-Za-z0-9]"); 
              if(objValue.value.length > 0 &&  charpos >= 0) 
              { 
               if(!strError || strError.length ==0) 
                { 
                  strError = objValue.name+": Only alpha-numeric characters allowed "; 
                }//if 
                alert(strError); 
                return false; 
              }//if 
              break; 
           }//case alphanumeric 
        case "num": 
        case "numeric": 
           { 
              var charpos = objValue.value.search(/^-{0,1}\d+$/); 
              if(objValue.value.length > 0 &&  charpos == -1) 
              { 
                if(!strError || strError.length ==0) 
                { 
                  strError = objValue.name+": Geef een numerieke waarde"; 
                }//if               
                alert(strError); 
                return false; 
              }//if 
              break;               
           }//numeric 
        case "alphabetic": 
        case "alpha": 
           { 
              var charpos = objValue.value.search("[^A-Za-z]"); 
              if(objValue.value.length > 0 &&  charpos >= 0) 
              { 
                  if(!strError || strError.length ==0) 
                { 
                  strError = objValue.name+": Only alphabetic characters allowed "; 
                }//if                             
                alert(strError); 
                return false; 
              }//if 
              break; 
           }//alpha 
		case "alnumhyphen":
			{
              var charpos = objValue.value.search("[^A-Za-z0-9\-_]"); 
              if(objValue.value.length > 0 &&  charpos >= 0) 
              { 
                  if(!strError || strError.length ==0) 
                { 
                  strError = objValue.name+": characters allowed are A-Z,a-z,0-9,- and _"; 
                }//if                             
                alert(strError); 
                return false; 
              }//if 			
			break;
			}
        case "email": 
          { 
               if(!validateEmailv2(objValue.value)) 
               { 
                 if(!strError || strError.length ==0) 
                 { 
                    strError = objValue.name+": Enter a valid Email address "; 
                 }//if                                               
                 alert(strError); 
                 return false; 
               }//if 
           break; 
          }//case email 
        case "lt": 
        case "lessthan": 
         { 
            if(isNaN(objValue.value)) 
            { 
              alert(objValue.name+": Should be a number "); 
              return false; 
            }//if 
            if(eval(objValue.value) >=  eval(cmdvalue)) 
            { 
              if(!strError || strError.length ==0) 
              { 
                strError = objValue.name + " : value should be less than "+ cmdvalue; 
              }//if               
              alert(strError); 
              return false;                 
             }//if             
            break; 
         }//case lessthan 
        case "gt": 
        case "greaterthan": 
         { 
            if(isNaN(objValue.value)) 
            { 
              alert(objValue.name+": Should be a number "); 
              return false; 
            }//if 
             if(eval(objValue.value) <=  eval(cmdvalue)) 
             { 
               if(!strError || strError.length ==0) 
               { 
                 strError = objValue.name + " : value should be greater than "+ cmdvalue; 
               }//if               
               alert(strError); 
               return false;                 
             }//if             
            break; 
         }//case greaterthan 
        case "regexp": 
         { 
		 	if(objValue.value.length > 0)
			{
	            if(!objValue.value.match(cmdvalue)) 
	            { 
	              if(!strError || strError.length ==0) 
	              { 
	                strError = objValue.name+": Invalid characters found "; 
	              }//if                                                               
	              alert(strError); 
	              return false;                   
	            }//if 
			}
           break; 
         }//case regexp 
        case "dontselect": 
         { 
            if(objValue.selectedIndex == null) 
            { 
              alert("BUG: dontselect command for non-select Item"); 
              return false; 
            } 
            if(objValue.selectedIndex == eval(cmdvalue)) 
            { 
             if(!strError || strError.length ==0) 
              { 
              strError = objValue.name+": Please Select one option "; 
              }//if                                                               
              alert(strError); 
              return false;                                   
             } 
             break; 
         }//case dontselect 
    }//switch 
    return true; 
}
/*
	Copyright 2003 JavaScript-coder.com. All rights reserved.
*/
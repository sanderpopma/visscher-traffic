<?
// ========== ALGEMENE FUNCTIES ============
function GebruikDatumObjecten()
{
    ?>
	<script type="text/javascript" language="javascript" src="<?=$GLOBALS['AppJsRoot']?>/datepicker/popcalendar.php"></script>
    <?
}
function VerplichtSter(){
	return "<span style='color: red; font-size: 10px;'>*</span>";
}
function ZetFocus($Veld_naam)
{
    ?>
    <script type="text/javascript" language="javascript">
	    document.getElementById("<?=$Veld_naam?>").focus();
    </script>
    <?
}

function VervangFrmWaarde( $waarde )
{
    return str_replace("'", "&#39;", $waarde);
}

// ============== FORMULIER OPENEN =============
function OpenForm($PostenNaar, $Formnaam, $Form_Breedte)
{
	$tmpNaam = "mijnform";
	if ($Formnaam!="")
	{
		$tmpNaam = $Formnaam;
	}
	return "<tr><td>" .
		"<form id='" . $tmpNaam . "' name='" . $tmpNaam . "' action='" . $PostenNaar . "' method='post'>" .
		"<table border='0' width='" . $Form_Breedte . "'>" . "";
}
function OpenForm2($PostenNaar, $Formnaam)
{
	$tmpNaam = "mijnform";
	if ($Formnaam!="")
	{
		$tmpNaam = $Formnaam;
	}
	return "<form id='" . $tmpNaam . "' name='" . $tmpNaam . "' action='" . $PostenNaar . "' method='post'>";
}

function OpenDataForm($PostenNaar, $Formnaam, $FormTitel)
{
	$tmpNaam = "mijnform";
	if ($Formnaam!="")
	{
		$tmpNaam = $Formnaam;
	}
	return RegelHoofd($FormTitel) . "<tr><td>" .
		"<form enctype='multipart/form-data' id='" . $tmpNaam . "' name='" . $tmpNaam . "' action='" . $PostenNaar . "' method='post'>" .
        "<table border=0>" . "";
}

// ========================= LEGE, KOP-, OF TEKSTREGELS ========================
function FrmKoptekst($Titel)
{
	return "<tr class='regel'><td colspan='3'><b>" . $Titel . "</b></td></tr>";
}
function FrmKopregel($Titel)
{
	return "<tr class='kadervoet'><td colspan='3'><b>" . $Titel . "</b></td></tr>";
}
function FrmLegeRegel()
{
	return "<tr class='regel'><td colspan='3'>&nbsp;</td></tr>";
}
function FrmRegel($Waarde)
{
	return "<tr class='regel'><td colspan='3'>" . $Waarde . "</td></tr>";
}
function FrmTekstRegel($Titel, $Waarde)
{
	return "<tr class='regel'><td>" . $Titel . "</td><td>:</td><td>" . $Waarde . "</td></tr>";
}

// ========================= HIDDEN ========================
function FrmHidden($Veldnaam, $s)
{
	$Waarde = VervangFrmWaarde($s);
	return "<input type='hidden' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "'>";
}

// ========================= TEXT ========================
function FrmText($Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='Veld' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}
function FrmTextKleur($Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='Veld' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'>&nbsp;<span style='width: 32px; height: 20px; border: solid 1px black; background-color: " . $Wrd . "'>&nbsp;&nbsp;&nbsp;&nbsp;</span></td></tr>";
}
function FrmOnEventText($OnEvent, $OnEventFunctienaam, $Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input " . $OnEvent . "='" . $OnEventFunctienaam . "' class='Veld' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}

function FrmTextDisabled($Titel, $Veldnaam, $Wrd, $Breedte, $MaxLengte)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='VeldUitgeschakeld' disabled='true' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}

function FrmDatum($Titel, $Veldnaam, $Wrd)
{
	$Waarde = VervangFrmWaarde( $Wrd );
	return "<tr class='regel' valign='top'><td>" . MaakDatumLabel($Titel) . "</td><td>:</td><td><input class='Veld' type='text' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='16' maxlength='10'> <img src='" . $GLOBALS['ApplicatieRoot'] . "/sybit/icons/kalender2.gif' width='16' height='16' alt='Kies Datum' border='0' onClick=\"popUpCalendar(this, document.getElementById('" . $Veldnaam . "'), 'd-m-yyyy', 0, 0)\"></td></tr>";
}

function FrmFile($Titel, $Veldnaam, $Breedte)
{
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='Veld' type='file' id='" . $Veldnaam . "' name='" . $Veldnaam . "' size='" . $Breedte . "'></td></tr>";
}

function FrmPswdText($Titel, $Veldnaam, $Waarde, $Breedte, $MaxLengte)
{
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='Veld' type='password' id='" . $Veldnaam . "' name='" . $Veldnaam . "' value='" . $Waarde . "' size='" . $Breedte . "' maxlength='" . $MaxLengte . "'></td></tr>";
}

// ========================= TEXTAREA ========================

function FrmTextArea($Titel, $Veldnaam, $Waarde, $Rijen, $Kolommen)
{
    return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><textarea class='Veld' id='" . $Veldnaam . "' name='" . $Veldnaam . "' rows='" . $Rijen . "' cols='" . $Kolommen . "'>" . $Waarde . "</textarea></td></tr>";
}

function FrmFCKText($lbl_waarde, $veld_naam, $veld_waarde, $fck_height)
{
	$tmpHeight = 200;
	if ($fck_height>0) {
		$tmpHeight = $fck_height;
	}
    ?>
    <tr class="regel" valign="top">
    <td><?=$lbl_waarde?></td><td>:</td>
    <td>
	<textarea name="<?=$veld_naam?>"><?=$veld_waarde?></textarea>
	<script type="text/javascript">

	CKEDITOR.replace( '<?=$veld_naam?>',
		{
		height : <?=$tmpHeight?>
		});
	</script>
	<?php
    //$oFCKeditor = new FCKeditor($veld_naam) ;
    //$oFCKeditor->BasePath = $GLOBALS['ApplicatieRoot'].'/sybit/CKeditor/';
    //$oFCKeditor->Value = $veld_waarde;
    //$oFCKeditor->Width  = '500' ;
    //$oFCKeditor->Height = $fck_height;
    //$oFCKeditor->Create() ;
    ?>
    </td></tr>
    <?
}

// ====================== SELECT ==================
function FrmGeslacht($lbl_waarde, $veld_naam, $veld_waarde)
{
	return FrmSelectByArray($lbl_waarde, $veld_naam, $veld_waarde, "M,V", false);
}
function FrmSelectByArray($lbl_waarde, $veld_naam, $veld_waarde, $arr_string, $metlegewaarde)
{
    $wrdarr=explode(",",$arr_string);
    $maxi = sizeof($wrdarr);
    $inti = 0;
    ?>
    <tr class="regel">
    <td><?=$lbl_waarde?></td>
    <td>:</td>
    <td><select class="Veld" name="<?=$veld_naam?>" size="1">
    <?
    if($metlegewaarde)
    {
    ?>
    <option value=""></option>
    <?
    }
    ?>
    <?
    for ($inti; $inti<$maxi; $inti++)
    {
    $seltxt = "";
    if ($wrdarr[$inti]==$veld_waarde) $seltxt=" SELECTED ";
    ?>
    <option value="<?=$wrdarr[$inti]?>" <?=$seltxt?>><?=$wrdarr[$inti]?></option>
    <?
    }
    ?>
    </select>
    </td>
    </tr>
    <?
}

function FrmSelectByQuery($Titel, $Veldnaam, $CurrValue, $sqlQuery, $OptionValue, $OptionDescription, $MetLegeWaarde)
{
	$HuidigeWaarde = $CurrValue;
	if ($HuidigeWaarde==null) {
	 	$HuidigeWaarde="";
	 }
	$ReturnWaarde = "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><select name='" . $Veldnaam . "' size=1 class='veld' id='" . $Veldnaam . "'>";
	if ($MetLegeWaarde==true) {
		$ReturnWaarde = $ReturnWaarde . "<option value=''></option>";

	 }
	$query_rsKeuze = $sqlQuery;
	$rsKeuze = mysql_query($query_rsKeuze, $GLOBALS['conn']) or die(mysql_error());
	$row_rsKeuze = mysql_fetch_assoc($rsKeuze);
	do
	{
		$SelTxt = "";
		if ($row_rsKeuze[$OptionValue].""==$HuidigeWaarde."") {
		 	$SelTxt = " SELECTED ";
		 }
		$ReturnWaarde = $ReturnWaarde . "<option value='" . $row_rsKeuze[$OptionValue] . "'" . $SelTxt . ">" . $row_rsKeuze[$OptionDescription] . "</option>";
	}
	while ($row_rsKeuze = mysql_fetch_assoc($rsKeuze));
	$ReturnWaarde = $ReturnWaarde . "</select></td></tr>";
	return $ReturnWaarde;
}
function FrmSelectValueOptionList($Titel, $Veldnaam, $CurrValue, $ValueLijst, $OptionLijst, $MetLegeWaarde)
{
	$HuidigeWaarde = $CurrValue;
	$ValWaardes = explode(";",$ValueLijst);
	$OptWaardes = explode(";",$OptionLijst);
	$ret = "";
	if (sizeof($ValWaardes) == sizeof($OptWaardes))
	{
		$ret = "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><select name='" . $Veldnaam . "' size=1 class='veld' id='" . $Veldnaam . "'>";
		if ($MetLegeWaarde==true)
		{
			$ret = $ret . "<option value=''></option>";
		}
		for ($x=0;$x<sizeof($ValWaardes);$x++)
		{
			$SelTxt = "";
			if (($ValWaardes[$x]) == ($HuidigeWaarde)) {$SelTxt = " SELECTED ";}
			$ret = $ret . "<option value='" . $ValWaardes[$x] . "'" . $SelTxt . ">" . $OptWaardes[$x] . "</option>";
		}
		$ret = $ret . "</select></td></tr>";
	}
	return $ret;
}
function FrmOnEventSelectValueOptionList($OnEvent, $OnEventFunctienaam, $Titel, $Veldnaam, $CurrValue, $ValueLijst, $OptionLijst, $MetLegeWaarde)
{
	$HuidigeWaarde = $CurrValue;
	$ValWaardes = explode(";",$ValueLijst);
	$OptWaardes = explode(";",$OptionLijst);
	$ret = "";
	if (sizeof($ValWaardes) == sizeof($OptWaardes))
	{
		$ret = "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><select " . $OnEvent . "='" . $OnEventFunctienaam . "' name='" . $Veldnaam . "' size=1 class='veld' id='" . $Veldnaam . "'>";
		if ($MetLegeWaarde==true)
		{
			$ret = $ret . "<option value=''></option>";
		}
		for ($x=0;$x<sizeof($ValWaardes);$x++)
		{
			$SelTxt = "";
			if (($ValWaardes[$x]) == ($HuidigeWaarde)) {$SelTxt = " SELECTED ";}
			$ret = $ret . "<option value='" . $ValWaardes[$x] . "'" . $SelTxt . ">" . $OptWaardes[$x] . "</option>";
		}
		$ret = $ret . "</select></td></tr>";
	}
	return $ret;
}
// ====================== OVERIGE ==================
function FrmCheckbox($lbl_waarde, $veld_naam, $veld_waarde)
{
	$chk=false;
	$tmpWrd = " ";
	if($veld_waarde==1)
	{
		$chk=true;
		$tmpWrd = " CHECKED ";
	}
    ?>
    <tr class="regel">
    <td><?=$lbl_waarde?></td>
    <td>:</td>
    <td><input id="<?=$veld_naam?>" name="<?=$veld_naam?>" <?=$tmpWrd?> type="checkbox" class="Veld"></td>
    </tr>
    <?
}

function FrmOnEventCheckbox($OnEvent, $OnEventFunctienaam, $lbl_waarde, $veld_naam, $veld_waarde)
{
	$chk=false;
	$tmpWrd = " ";
	if($veld_waarde==1)
	{
		$chk=true;
		$tmpWrd = " CHECKED ";
	}
    ?>
    <tr class="regel">
    <td><?=$lbl_waarde?></td>
    <td>:</td>
    <td><input <?=$OnEvent?>='<?=$OnEventFunctienaam?>' id="<?=$veld_naam?>" name="<?=$veld_naam?>" <?=$tmpWrd?> type="checkbox" class="Veld"></td>
    </tr>
    <?
}

function FrmRadio($Titel, $Naam_Veld, $Waarde, $Checked)
{
	$Chk = "";
  if($Checked==true) {$Chk="CHECKED";}
	return "<tr class='regel' valign='top'><td>" . $Titel . "</td><td>:</td><td><input class='Veld' type='radio' id='" . $Naam_Veld . "' name='" . $Naam_Veld . "' value='" . $Waarde . "' " . $Chk . "></td></tr>";
}

// ========================= SUBMIT / BUTTON ========================
function FrmSubmit($Waarde)
{
	$tmpWrd="Verstuur";
	if ($Waarde!="") $tmpWrd = $Waarde;
	return "<tr class='regel' valign='top'><td colspan='3' align='right'><input class='Knop' id='VerstuurKnop' name='VerstuurKnop' type='SUBMIT' value='" . $tmpWrd . "'></td></tr>";
}
function FrmSubmit2($Naam, $Waarde)
{
	return "<tr class='regel' valign='top'><td colspan='3' align='right'><input class='Knop' id='" . $Naam . "' name='" & Naam & "' type='SUBMIT' value='" . $Waarde . "'></td></tr>";
}
function FrmButton($Waarde, $KnopNaam, $ClickFunctie)
{
	return "<tr class='regel' valign='top'><td colspan='3' align='right'><input id='" . $KnopNaam . "' name='" . $KnopNaam . "' class='Knop' type='button' onclick='" . $ClickFunctie . "' value='" . $Waarde . "'></td></tr>";
}

// ========================= FORMULIER SLUITEN ========================
function SluitForm()
{
    return "</table></form></td></tr>";
}
function SluitForm2()
{
    return "</form>";
}
?>
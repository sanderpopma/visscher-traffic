<?
$tmpTxtOnbekend = "Loginnaam of wachtwoord is onbekend";
$tmpGepostNaar = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];
if ($GLOBALS['actie']=="loguit")
{
    unset ($_SESSION['CMSBeheerderId']);
    unset ($_SESSION['CMSLoginnaam']);
    unset ($_SESSION['CMSAfbeeldnaam']);
}
if (!isset($_SESSION['CMSBeheerderId']))
{
    $GLOBALS['GebruikerIsIngelogd'] = false;
}
else
{
    if(toInt($_SESSION["CMSBeheerderId"])>0)
    {
        $GLOBALS['GebruikerIsIngelogd'] = true;
    }
}

if ($GLOBALS['GebruikerIsIngelogd']==false)
{
    if($_POST["Loginnaam"]!="")
    {
        if($_POST["Wachtwoord"]!="")
        {
            $query_rsl = "SELECT * FROM cmsbeheerders WHERE beheerlogin='" . ValidateString($_POST["Loginnaam"]) . "'";
            $rsl = mysql_query($query_rsl, $GLOBALS['conn']) or die(mysql_error());
            $row_rsl = mysql_fetch_assoc($rsl);
            if ($row_rsl['beheerderid']>0)
            {
                if($row_rsl["beheerlogin"]==$_POST["Loginnaam"])
                {
                    if($row_rsl["beheerwachtwoord"]==$_POST["Wachtwoord"])
                    {
                        $_SESSION["CMSBeheerderId"] = $row_rsl["beheerderid"];
                        $_SESSION["CMSLoginnaam"] = $row_rsl["beheerlogin"];
                        $_SESSION["CMSAfbeeldnaam"] = $row_rsl["beheerafbeeldnaam"];
                        $GLOBALS['GebruikerIsIngelogd'] = true;
                        mysql_query("UPDATE cmsbeheerders SET aantallogins=aantallogins+1 WHERE beheerderid=".$row_rsl['beheerderid'].";",$GLOBALS['conn']);
                    }
                    else
                    {
                        $mijnMelding = $tmpTxtOnbekend;
                    }
                }
                else
                {
                    $mijnMelding = $tmpTxtOnbekend;
                }
            }
            else
            {
                $mijnMelding = $tmpTxtOnbekend;
            }
            mysql_free_result($rsl);
        }
        else
        {
            // Login/wachtwoord onbekend
            $mijnMelding = $tmpTxtOnbekend;
        }
    }
}

if ($GLOBALS['GebruikerIsIngelogd']==false)
{
    ?>
    <?
    OpenPagina("CMS - Login", "");
    ?>
    <?
    include($_SERVER['DOCUMENT_ROOT']."/applicatie/login_header.php");
    ?>

    <table width="400" cellpadding="0" cellspacing="0" align="center" style="border: solid black 1px; border-bottom: 0;"><tr valign="top"><td>

    <?
    OpenCMSTabel("Login voor CMS");
    OpenCMSNavBalk() ;
    ?>
        <? ToonCMSNavKnop("slotdicht", "Login", "javascript:VerstuurKnop.click();") ?>
        <? ToonCMSNavKnop("stop", "Annuleren", $GLOBALS['ApplicatieRoot']); ?>
    <? SluitCMSNavBalk(); ?>

    <?=OpenForm($tmpGepostNaar, "login", 500)?>
        <?
        foreach( $_POST as $item => $value)
        {
            if ($item!="Wachtwoord" && $item!="Loginnaam" && $item!="VerstuurKnop")
            {
                FrmHidden($item, $value);
            }
        }
        ?>
        <?
        if ($mijnMelding!="")
        {
            ?>
            <?=FrmKoptekst($mijnMelding) ?>
            <?
        }
        ?>
        <?=FrmText("Loginnaam", "Loginnaam", "", 30,50) ?>
        <?=FrmPswdText("Wachtwoord", "Wachtwoord", "", 30,50) ?>
        <?=FrmSubmit("Login")?>
    <?=SluitForm();?>

    <?=ZetFocus("Loginnaam");?>

    <SCRIPT language="JavaScript">
    var frmvalidator  = new Validator("login");
    frmvalidator.addValidation("Loginnaam","req","Loginnaam is verplicht");
    frmvalidator.addValidation("Wachtwoord","req","Wachtwoord is verplicht");
    </script>

    <?=SluitCMSTabel();?>

    </td></tr></table>

    <br /><br />
    <center>
    <?
    if ($GLOBALS['MetLoginStuurWachtwoordOp']==true)
    {
        ?>
        <?=PlaatsIcoonLink("help", "Stuur mijn wachtwoord op", $GLOBALS['ApplicatieRoot'] . "/sybit/includes/wachtwoordkwijt.php?naar=" . $tmpGepostNaar, "Stuur mijn wachtwoord op");?>
        <?
    }
    ?>
    </center>

    <?
    include($_SERVER['DOCUMENT_ROOT']."/applicatie/login_footer.php");
    ?>

    <?=SluitPagina();?>
    <?
    include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
    ?>
    <?
    die;
}
?>
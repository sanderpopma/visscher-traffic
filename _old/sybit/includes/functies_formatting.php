<?
function datYear($datwrd){
	list($Year,$Month,$Day) = explode('-',$datwrd);
	return $Year;
}
function datMonth($datwrd){
	list($Year,$Month,$Day) = explode('-',$datwrd);
	return $Month;
}
function datDay($datwrd){
	list($Year,$Month,$Day) = explode('-',$datwrd);
	return $Day;
}
function MaakGUIDString($Wrd)
{
    $ret="";
    if ($Wrd!="")
    {
        $ret = $Wrd;
        //$ret = Replace(ret, "{", "");
        //$ret = Replace(ret, "}", "");
    }
    return $ret;
}

function Maak2Dig($wrd)
{
    $ret = "";
    if ($wrd!="")
    {
        $ret = $wrd;
        if (strlen($ret)==1)
		{
         	$ret = "0" . $ret;
        }
    }
    return $ret;
}

function MaakPercentage($GetalWaarde)
{
	if (toInt($GetalWaarde)>-1)
	{
		return $GetalWaarde . "%";
    }
	else
	{
		return "";
	}
}
function MaakPercentageLabel($LabelWaarde)
{
	if ($LabelWaarde!="")
	{
		return $LabelWaarde . " (%)";
	}
}

function MaakDatum($dat_wrd)
{
	if($dat_wrd!="")
	{
	return date("d-m-Y", strtotime($dat_wrd));
	}
}

function MaakDatumTijd($dat_wrd)
{
	if($dat_wrd!="")
	{
	return date("d-m-Y G:i:s", strtotime($dat_wrd));
	}
}

function MaakVolledigeDatum($dat_wrd)
	{
	$ret = "";
	$dagtxt = "";
	if($dat_wrd!="") {
		switch(strtoupper($GLOBALS['GekozenTaal']))
		{
			case "EN":
				switch(datDay($dat_wrd)){
					case 1 || 21 ||31:
						$dagtxt = "st";
						break;
					case 2 || 22:
						$dagtxt = "nd";
						break;
					case 3 || 23:
						$dagtxt = "rd";
						break;
					default:
						$dagtxt = "th";
				} // switch
				$ret = GeefMaandNaam(datMonth($dat_wrd)) . " " . toInt(datDay($dat_wrd)) . $dagtxt . ", " . datYear($dat_wrd);
				break;
			case "DE":
				$dagtxt = ".";
				$ret = toInt(datDay($dat_wrd)) . $dagtxt . GeefMaandNaam(datMonth($dat_wrd)) . " " . datYear($dat_wrd);
				break;

			default:
				$ret = toInt(datDay($dat_wrd)) . " " . strtolower(GeefMaandNaam(datMonth($dat_wrd))) . " " . datYear($dat_wrd);
		}
	}
	else{
		$ret = "";
	}
	return $ret;
}

function MaakDatumLabel($LabelWaarde)
{
	if ($LabelWaarde!="")
	{
		return $LabelWaarde . " (" . $GLOBALS['StdDatumNotatie'] . ")";
	}
}

function MaakDecimal($dec_wrd)
{
	if($dec_wrd!="")
	{
		return str_replace(".", ",", $dec_wrd);
	}
}
function MaakBedrag($wrd)
{
	if($wrd!="")
	{
		if (is_numeric($wrd)) {
			return $GLOBALS['StdValutaTeken'] . " " . MaakDecimal(sprintf("%.2f",$wrd));
		}
	}
}
function MaakBedragTxt($wrd)
{
	if($wrd!="")
	{
		if (is_numeric($wrd)) {
			return "EUR " . MaakDecimal(sprintf("%.2f",$wrd));
		}
	}
}

function MaakBedragDec($wrd, $aant_dec)
{
	$ret = "";
	if($wrd!="")
	{
		if (is_numeric($wrd)) {
			$ret = $GLOBALS['StdValutaTeken'] . " " . MaakDecimal(sprintf("%." . $aant_dec . "f",$wrd));
		}
	}
	if ($aant_dec==3) {
		if (Right($ret, 1)=="0") {
			$ret = Left($ret, strlen($ret)-1);
		}
	}
	return $ret;
}
function MaakBedragLabel($LabelWaarde)
{
	if ($LabelWaarde!="")
	{
		return $LabelWaarde . " (" . $GLOBALS['StdValutaTeken'] . ")";
	}
}
function MaakJaNee($bitwrd)
{
	if (is_numeric($bitwrd)) {
		if($bitwrd==1)
		{
			return Vertaal("ja");
		}
		else
		{
			return Vertaal("nee");
		}
	}
	else{
		if($bitwrd=="on")
		{
			return Vertaal("ja");
		}
		else
		{
			return Vertaal("nee");
		}
	}
}
?>
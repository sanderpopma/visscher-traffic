<?
function ToonContactGegevens($taal_code){
$query_rs = "SELECT * FROM tarieven WHERE tarievenactief<>0;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
?>
			<b><?=$row_rs['bedrijfsnaam']?></b><br />
			<?=$row_rs['bedrijfsadres']?><br />
			<?=$row_rs['bedrijfspostcode']?>&nbsp;&nbsp;<?=$row_rs['bedrijfsplaats']?><br />
			<br />
			<? if ($row_rs['bedrijfspostadres']!="") {
				?>
				<b><?=Vertaal("postadres")?>:</b><br />
				<?=$row_rs['bedrijfspostadres']?><br />
				<?=$row_rs['bedrijfspostpostcode']?>&nbsp;&nbsp;<?=$row_rs['bedrijfspostplaats']?><br />
				<br />
				<?
			}
			?>
			<? if ($row_rs['bedrijfstelnr']!="") {
				?>
				<?=Vertaal("telefoonnummer")?>: <?=$row_rs['bedrijfstelnr']?><br />
<?
			}
			?>
			<? if ($row_rs['bedrijfsfaxnr']!="") {
?>
				<?=Vertaal("fax")?>: <?=$row_rs['bedrijfsfaxnr']?><br />
<?
}
?>
			<? if ($row_rs['bedrijfsmobnr']!="") {
?>
				<?=Vertaal("mobnr")?>: <?=$row_rs['bedrijfsmobnr']?><br />
<?
}
?>
			<? if ($row_rs['bedrijfstelnr']!="" || $row_rs['bedrijfsfaxnr']!="" || $row_rs['bedrijfsmobnr']!="") {
				?>
				<br />
			<?
			}
			?>

			<? if ($row_rs['bedrijfsemail']!="") {
?>
				<?=Vertaal("email")?>: <a href="mailto:<?=$row_rs['bedrijfsemail']?>"><?=$row_rs['bedrijfsemail']?></a><br />
<?
}
?>
			<? if ($row_rs['bedrijfswebsite']!="") {
?>
				www: <a href="<?=$row_rs['bedrijfswebsite']?>" target="_blank"><?=$row_rs['bedrijfswebsite']?></a><br />
<?
}
?>
			<? if ($row_rs['bedrijfsemail']!="" || $row_rs['bedrijfswebsite']!="") {
				?>
				<br />
			<?
			}
			?>
			<? if ($row_rs['bedrijfslinkedin']!="") {
?>
				<?php echo Icoon("soc_linkedin"); ?>: <a href="<?=$row_rs['bedrijfslinkedin']?>" target="_blank"><?=Vertaal("onsbedrijfoplinkedin")?></a><br />
<?
}
?>
			<? if ($row_rs['bedrijfstwitter']!="") {
?>
				<?php echo Icoon("soc_twitter"); ?>: <a href="<?=$row_rs['bedrijfstwitter']?>" target="_blank"><?=Vertaal("onsbedrijfoptwitter")?></a><br />
<?
}
?>

			<? if ($row_rs['bedrijfslinkedin']!="" || $row_rs['bedrijfstwitter']!="") {
				?>
				<br />
			<?
			}
			?>

			<? if ($row_rs['kvknummer']!="") {
?>
				<?=Vertaal("kvknummer")?>: <?=$row_rs['kvknummer']?><br />
			<br />
<?
}
?>
			<? if ($row_rs['metroutelink']==true) {
?>
				<?=Vertaal("Routebeschrijving")?>: <a href="http://maps.google.com/maps?saddr=&amp;daddr=<?=$row_rs['bedrijfsadres']?>,<?=$row_rs['bedrijfsplaats']?>,Netherlands" target="_blank"><?=Vertaal("klikhiervoorroutebeschrijving")?></a><br />
<?
}
?>
		<?
}
mysql_free_result($rs);

}
?>
<?
function ToonSitemap($taal_code){
	$query_rshm = "SELECT * FROM paginas WHERE paginataalcode='" . $taal_code . "' AND publiceren<>0 AND hoofdmenuvolgorde>0 AND ledenloginnodig=0 ORDER BY hoofdmenuvolgorde;";
	if ($GLOBALS['LidIsIngelogd']==true) {
		$query_rshm = "SELECT * FROM paginas WHERE paginataalcode='" . $taal_code . "' AND publiceren<>0 AND hoofdmenuvolgorde>0 ORDER BY hoofdmenuvolgorde;";
	}

	//$query_rshm = "SELECT * FROM paginas WHERE paginataalcode='".$taal_code ."' AND hoofdmenuvolgorde>0 AND publiceren<>0 ORDER BY hoofdmenuvolgorde;";
	$rshm = mysql_query($query_rshm, $GLOBALS['conn']) or die(mysql_error());
	$row_rshm = mysql_fetch_assoc($rshm);
	if (mysql_num_rows($rshm)>0) {
		?>
		</p>
		<ul>
		<?
			do
			{
			?>
			<li><a <?=MaakPaginaLink($row_rshm['paginaid'])?>><?=$row_rshm['paginamenunaam']?></a></li>
			<?=ToonSitemapSubs($row_rshm['paginaid'])?>
			<?
			}
			while ($row_rshm = mysql_fetch_assoc($rshm));
		?>
		</ul>
		<p>
		<?
	}
	mysql_free_result($rshm);
}
function ToonSitemapSubs($pag_id){
	$query_rssm = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $pag_id . " AND publiceren<>0 AND opnemeninsubmenu<>0 AND ledenloginnodig=0 ORDER BY submenuvolgorde;";
	if ($GLOBALS['LidIsIngelogd']==true) {
		$query_rssm = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $pag_id . " AND publiceren<>0 AND opnemeninsubmenu<>0 ORDER BY submenuvolgorde;";
	}

	//$query_rssm = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $pag_id ." AND opnemeninsubmenu<>0 AND publiceren<>0 ORDER BY submenuvolgorde;";
	$rssm = mysql_query($query_rssm, $GLOBALS['conn']) or die(mysql_error());
	$row_rssm = mysql_fetch_assoc($rssm);
	if (mysql_num_rows($rssm)>0) {
		?>
		<ul>
		<?
			do
			{
			?>
			<li><a <?=MaakPaginaLink($row_rssm['paginaid'])?>><?=$row_rssm['paginamenunaam']?></a></li>
			<?=ToonSitemapSubs($row_rssm['paginaid'])?>
			<?
			}
			while ($row_rssm = mysql_fetch_assoc($rssm));
		?>
		</ul>
		<?
	}
	mysql_free_result($rssm);
}
function ToonNieuwsLijstHome($aant_ber, $taal_code, $alleen_leden){
	if (toInt($aant_ber)<1) { $aant_ber=1; }
	if ($alleen_leden==true) {
		$query_rsnws = "SELECT * FROM nieuws WHERE nwstaalcode='".$taal_code ."' AND nwspubliceren<>0 AND nwsalleenleden<>0 ORDER BY nwsdatum DESC, nieuwsid DESC LIMIT $aant_ber;";
		$myNieuwsModule = "nieuwsleden";
	}
	else{
		$query_rsnws = "SELECT * FROM nieuws WHERE nwstaalcode='".$taal_code ."' AND nwspubliceren<>0 AND nwsalleenleden=0 ORDER BY nwsdatum DESC, nieuwsid DESC LIMIT $aant_ber;";
		$myNieuwsModule = "nieuws";
	}
	$rsnws = mysql_query($query_rsnws, $GLOBALS['conn']) or die(mysql_error());
	$row_rsnws = mysql_fetch_assoc($rsnws);
	if (mysql_num_rows($rsnws)>0) {
		?>
		</p>
		<hr class="streep" />
		<?
			do
			{
			$nwsafb = toInt($row_rsnws['nwsafbeeldingid']);
			if ($nwsafb<1) {
				$query_rsprg = "SELECT * FROM paragrafen WHERE pa_nieuwsid=".$row_rsnws['nieuwsid'] ." AND pa_publiceren<>0 AND paragraaftype='TEKST' AND pa_afbeeldingid>0 LIMIT 1;";
				$rsprg = mysql_query($query_rsprg, $GLOBALS['conn']) or die(mysql_error());
				$row_rsprg = mysql_fetch_assoc($rsprg);
				if (mysql_num_rows($rsprg)>0) {
					$nwsafb = toInt($row_rsprg['pa_afbeeldingid']);
				}
				mysql_free_result($rsprg);
			}
			?>
			<div>
			<?
				if ($nwsafb>0) {
					$MyAfbSrc = GeefAfbeeldingSrc($nwsafb);
					if ($MyAfbSrc!="") {
						$myalign = "right";
						if ($GLOBALS['lastimgalign']=="right") {
							$myalign = "left";
						}
						$GLOBALS['lastimgalign']=$myalign;
					 	?>
					 	<img src="<?=$MyAfbSrc?>" class="border" width="<?=$GLOBALS['AfbeeldingMaxKlein']?>" alt="" align="<?=$myalign?>" hspace="10" />
					 	<?
					 }
				}
			?>
			<h2><?=$row_rsnws['nwstitel']?></h2>
			<?=strtoupper(GeefDbWaarde("bedrijfsplaats", "tarieven", "tarievenactief<>0"))?> - <i><?=MaakVolledigeDatum($row_rsnws['nwsdatum'], $row_rsnws['nwstaalcode'])?></i>.&nbsp;<?=$row_rsnws['nwsinleiding']?>
			&nbsp;<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefPaginaIdByModule($myNieuwsModule)?>&amp;nwsid=<?=$row_rsnws['nieuwsid']?>"><?=Vertaal("leesverder")?></a>
			</div>
			<div style="clear: both;"></div>
			<hr class="streep" />
			<?
			}
			while ($row_rsnws = mysql_fetch_assoc($rsnws));
		?>
		<div style="text-align: right;">
		<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=GeefPaginaIdByModule($myNieuwsModule)?>"><?=Vertaal("meernieuws")?></a>
		</div><div style="clear:both;"></div>
		<hr class="streep" />
		<p>
		<?
	}
	else
	{
	?>
	<?=Vertaal("erzijngeennieuwsberichten")?>
	<?
	}
	mysql_free_result($rsnws);
}
?>
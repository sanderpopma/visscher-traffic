<?

function GeefMaand($Maand_Nr){
	return GeefDBWaarde("maandkort" . $GLOBALS['GekozenTaal'], "_maanden", "maandnr=" .$Maand_Nr);
}
function GeefMaandNaam($Maand_Nr){
	return GeefDBWaarde("maandnaam" . $GLOBALS['GekozenTaal'], "_maanden", "maandnr=" .$Maand_Nr);
}

function GeefDBWaarde($select_veld, $from_tabel, $where_deel)
{
    $tmpWhere = "";
    if ($where_deel!="")
    {
        $tmpWhere = " WHERE " . $where_deel;
    }
    mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
    $query_rs = "SELECT ". $select_veld . " FROM " . $from_tabel . $tmpWhere . "";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    return $row_rs[$select_veld];
}
function Vertaal($tekst_waarde){
    $ret = "";
    if ($tekst_waarde!="") {$ret = $tekst_waarde; }
	$query_afb = "SELECT * FROM vertalingen WHERE zoekcode='" . $tekst_waarde . "';";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['tekst_'.strtolower($GLOBALS['GekozenTaal'])];
    }
	mysql_free_result($rsafb);
	if ($ret=="") {
		$query_afb = "SELECT * FROM vertalingen WHERE tekst_nl='" . $tekst_waarde . "';";
		$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
		$row_rsafb = mysql_fetch_assoc($rsafb);
		if (mysql_num_rows($rsafb)>0) {
			$ret = $row_rsafb['tekst_'.strtolower($GLOBALS['GekozenTaal'])];
	    }
		mysql_free_result($rsafb);
	}
    return $ret;
}

function TelRecords($sql_qry)
{
    $rs = mysql_query($sql_qry, $GLOBALS['conn']) or die(mysql_error());
    return toInt(mysql_num_rows($rs));
}

function SQLInsertStr($val)
{
	return $val;
}

function SQLInsertDat($val)
{
	if($val)
	{
	$DMY = explode('-', $val);
	return $DMY[2]."-".$DMY[1]."-".$DMY[0];
	}
	else
	{
		return null;
	}
}

function SQLStr($val)
{
	if($val!="")
	{
		$val =  str_replace("'", "\'",$val);
		return "'".$val."'";
	}
	else
		return "null";
}

function SQLDec($val)
{
	if($val!="")
		return "'".str_replace(",", ".",$val)."'";
	else
		return "null";
}

function SQLDat($val)
{
	if($val!="")
	{
	$DMY = explode('-', $val);
	return "'".$DMY[2]."-".$DMY[1]."-".$DMY[0]."'";
	}
	else
	{
		return "null";
	}
}

function SQLBool($val)
{
	if($val==1 || $val=="on")
		return "'1'";
	else
		return "'0'";
}
?>
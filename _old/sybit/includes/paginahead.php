<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php
	echo GeefPaginaMETADescription("PAGINA", $GLOBALS['pagid']);
?>
<meta name="author" content="Sybit | Software op Maat | www.sybit.nl" />
<meta name="copyright" content="Copyright (c) Sybit | Software op Maat" />

<link href="<?=$GLOBALS['ApplicatieRoot']?>/css/style.php" rel="stylesheet" type="text/css" media="screen" />
<link href="<?=$GLOBALS['ApplicatieRoot']?>/css/actstyle.php" rel="stylesheet" type="text/css" media="screen" />
<link href="<?=$GLOBALS['ApplicatieRoot']?>/css/formstyle.php" rel="stylesheet" type="text/css" media="screen" />
<link href="<?=$GLOBALS['ApplicatieRoot']?>/css/printstyle.php" rel="stylesheet" type="text/css" media="print" />
<?
if (strpos($_SERVER["PHP_SELF"], "/login/").""!="" || $GLOBALS['MetCMSCSS']==true)
{
?>
<link href="<?=$GLOBALS['ApplicatieRoot']?>/login/css/stylecms.php" rel="stylesheet" type="text/css" />
<?
}
?>
<link rel="icon" href="<?=$GLOBALS['ApplicatieRoot']?>/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?=$GLOBALS['ApplicatieRoot']?>/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="<?=$GLOBALS['ApplicatieRoot']?>/sybit/js/gen_validator.js"></script>
<script type="text/javascript" src="<?=$GLOBALS['ApplicatieRoot']?>/sybit/js/jssybit.js"></script>
<script type="text/javascript" src="<?=$GLOBALS['ApplicatieRoot']?>/applicatie/js/appjs.js"></script>
<?
if (strpos($_SERVER["PHP_SELF"], "/login/").""!="" || $GLOBALS['MetCMSCSS']==true)
{
	?>
	<script type="text/javascript" src="/sybit/ckeditor/ckeditor.js"></script>
	<?
}
else
{
?>
	<link rel="stylesheet" href="/css/roundedstyle.css" type="text/css" media="screen" />
	<script type="text/javascript" src="/applicatie/js/lib/jquery.js"></script>
	<script type="text/javascript" src="/applicatie/js/main.js"></script>
    <script type="text/javascript" src="/applicatie/js/jcarousellite_1.0.1.js"></script>
	<script type="text/javascript" src="/applicatie/js/jquery.cycle.all.min.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {
		    $('.picsslideshow').cycle({
				fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
			});
		});
		</script>

	<script type="text/javascript" src="<?=$GLOBALS['ApplicatieRoot']?>/sybit/LightBox/lightbox.js"></script>
	<link rel="stylesheet" href="<?=$GLOBALS['ApplicatieRoot']?>/sybit/LightBox/lightbox.css" type="text/css" media="screen" />

	<!--[if lt IE 7]>
		<script type="text/javascript" src="/sybit/js/unitpngfix.js"></script>
	<![endif]-->

<?
}
?>
<?
if ($GLOBALS['MetWebsiteFrames']==true)
{
  include($_SERVER['DOCUMENT_ROOT']."/applicatie/frameset.php");
}

if ($GLOBALS['MetCMSFrames']==true)
{
  include($_SERVER['DOCUMENT_ROOT']."/login/includes/cmsframeset.php");
}
?>
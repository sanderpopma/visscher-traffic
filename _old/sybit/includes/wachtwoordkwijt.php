<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
?>
<?
// Inlezen en verwerken paginaparameters
$naar = $_GET['naar'];
$MetCMSCSS = true;
?>
<?
OpenPagina("CMS - Wachtwoord opsturen", "");
?>

    <?
    include($_SERVER['DOCUMENT_ROOT']."/applicatie/login_header.php");
    ?>

    <table width="400" cellpadding="0" cellspacing="0" align="center" style="border: solid black 1px; border-bottom: 0px;"><tr valign="top"><td>

    <?=OpenCMSTabel("Stuur mijn wachtwoord op")?>
    <?=OpenCMSNavBalk()?>
        <?=ToonCMSNavKnop("gereed", "Verstuur mijn wachtwoord", "javascript:VerstuurKnop.click();")?>
        <?=ToonCMSNavKnop("stop", "Annuleren", $naar)?>
    <?=SluitCMSNavBalk()?>


    <?=OpenForm("wachtwoordkwijt2.php", "wachtwoord", 500)?>
        <?=FrmKoptekst("Vul uw e-mailadres in en uw wachtwoord wordt opgestuurd")?>
        <?=FrmHidden("naar", $naar)?>
        <?=FrmText("E-mailadres", "BeheerEmailadres", "", 40,150)?>
        <?=FrmSubmit("Verstuur")?>
    <?=SluitForm()?>

    <?=ZetFocus("BeheerEmailadres")?>

    <SCRIPT language="JavaScript">
    var frmvalidator  = new Validator("wachtwoord");
    frmvalidator.addValidation("BeheerEmailadres","req","E-mailadres is verplicht");
    </script>

    <?=SluitCMSTabel()?>

    </td></tr></table>

<div align="center" style=" color: #DDDDDD;">
    <br /><br />
    <br /><br />
</div>

    <?
    include($_SERVER['DOCUMENT_ROOT']."/applicatie/login_footer.php");
    ?>

<?=SluitPagina()?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
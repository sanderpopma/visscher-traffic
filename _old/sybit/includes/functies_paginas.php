<?
function GeefPaginaMETADescription($pag_type, $item_id){
	$ret = "";
	switch($pag_type){
		case "NIEUWS":
			//;
			break;
		default:
			$pag_type = "PAGINA";
			$ret = GeefDbWaarde("paginagooglebeschrijving", "paginas", "paginaid=".$item_id);
	} // switch
	if ($ret!="") {
		$ret = str_replace("'","\'" ,$ret );
		$ret = "<meta name='description' content='" . $ret . "' />";
	}
	return $ret;
}
function EncodeTaalURL($van_taal, $naar_taal){
	if ($van_taal==$naar_taal) {
		$naarURL = GeefPaginaURL(GeefEersteHoofdmenuID($naar_taal));
		return $naarURL;
	}
	else
	{
		if ($van_taal==$GLOBALS['StdTaalcode']) {
			// Van Nederlands naar andere taal
			$naarpagid = toInt(GeefDBWaarde("paginaid", "paginas", "hoortbijnlpaginaid=".$GLOBALS['pagid']." AND paginataalcode='".$naar_taal."'"));
			if ($naarpagid>0) {
				$naarURL = GeefPaginaURL($naarpagid)."&prodgrpid=".$GLOBALS['prodgrpid']."&prodid=".$GLOBALS['prodid']."&msmid=".$GLOBALS['msmid'];
				return $naarURL;
			}
			else
			{
				$naarpagid = toInt(GeefDBWaarde("paginaid", "paginas", "hoortbijnlpaginaid=".$GLOBALS['hmid']." AND paginataalcode='".$naar_taal."'"));
				if ($naarpagid>0) {
					$naarURL = GeefPaginaURL($naarpagid);
					return $naarURL;
				}
				else
				{
					$naarURL = GeefPaginaURL(GeefEersteHoofdmenuID($naar_taal));
					return $naarURL;
				}
			}
		}
		else
		{
			if ($naar_taal==$GLOBALS['StdTaalcode']) {
				// Van andere taal naar Nederlands
				$naarpagid = toInt(GeefDBWaarde("hoortbijnlpaginaid", "paginas", "paginaid=".$GLOBALS['pagid']));
				if ($naarpagid>0) {
					$naarURL = GeefPaginaURL($naarpagid)."&prodgrpid=".$GLOBALS['prodgrpid']."&prodid=".$GLOBALS['prodid']."&msmid=".$GLOBALS['msmid'];
					return $naarURL;
				}
				else
				{
					$naarpagid = toInt(GeefDBWaarde("hoortbijnlpaginaid", "paginas", "paginaid=".GeefPaginaHMID($GLOBALS['pagid'])));
					if ($naarpagid>0) {
						$naarURL = GeefPaginaURL($naarpagid);
						return $naarURL;
					}
					else
					{
						$naarURL = GeefPaginaURL(GeefEersteHoofdmenuID($naar_taal));
						return $naarURL;
					}
				}
			}
			else
			{
				// Van andere taal naar andere taal
				$naarnlpagid = toInt(GeefDBWaarde("hoortbijnlpaginaid", "paginas", "paginaid=".$GLOBALS['pagid']));
				if ($naarnlpagid<1) {
					$naarnlpagid = toInt(GeefDBWaarde("hoortbijnlpaginaid", "paginas", "paginaid=".GeefPaginaHMID($GLOBALS['pagid'])));
					if ($naarnlpagid<1) {
						$naarnlpagid = GeefEersteHoofdmenuID($GLOBALS['StdTaalcode']);
					}
				}
				$naarpagid = toInt(GeefDBWaarde("paginaid", "paginas", "hoortbijnlpaginaid=".$naarnlpagid." AND paginataalcode='".$naar_taal."'"));
				if ($naarpagid>0) {
					$naarURL = GeefPaginaURL($naarpagid)."&prodgrpid=".$GLOBALS['prodgrpid']."&prodid=".$GLOBALS['prodid']."&msmid=".$GLOBALS['msmid'];
					return $naarURL;
				}
				else
				{
					$naarpagid = toInt(GeefDBWaarde("paginaid", "paginas", "hoortbijnlpaginaid=".GeefPaginaHMID($naarnlpagid)." AND paginataalcode='".$naar_taal."'"));
					if ($naarpagid>0) {
						$naarURL = GeefPaginaURL($naarpagid);
						return $naarURL;
					}
					else
					{
						$naarURL = GeefPaginaURL(GeefEersteHoofdmenuID($naar_taal));
						return $naarURL;
					}
				}
			}
		}
	}
}
function MaakPaginaLink($pag_id){
	$ret = "";
	$tmptarget="_self";
	if ($pag_id!="") {
		$query_pag = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . " and publiceren<>0;";
		$rspag = mysql_query($query_pag, $GLOBALS['conn']) or die(mysql_error());
		$row_rspag = mysql_fetch_assoc($rspag);
		if (mysql_num_rows($rspag)>0) {
			if ($row_rspag['paginaurl']!="") {
				$tmptarget="_top";
				$ret = "href='" .$row_rspag['paginaurl'] . "'";
				if ($row_rspag['paginanieuwvenster']!=0) {
					$tmptarget="_blank";
				}
			}
			else
			{
				$ret = "href='" .$GLOBALS['HMURL']."?pagid=".$row_rspag['paginaid']."&amp;taalcode=".$row_rspag['paginataalcode'] . "'";
			}
			//$ret = "href='" . GeefPaginaURL($pag_id) . "'";
	    }
		mysql_free_result($rspag);
	}
	else
	{
		$ret = "href='" . $GLOBALS['HMURL'] . "'";
	}
	return $ret . " target='" . $tmptarget . "'";
}
function GeefPaginaURL($pag_id){
	$ret = "";
	if ($pag_id!="") {
		$query_pag = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . " and publiceren<>0;";
		$rspag = mysql_query($query_pag, $GLOBALS['conn']) or die(mysql_error());
		$row_rspag = mysql_fetch_assoc($rspag);
		if (mysql_num_rows($rspag)>0) {
			if ($row_rspag['paginaurl']!="") {
				$ret = $row_rspag['paginaurl'];
			}
			else
			{
				$ret = $GLOBALS['HMURL']."?pagid=".$row_rspag['paginaid']."&amp;taalcode=".$row_rspag['paginataalcode'] . "";
			}
	    }
		mysql_free_result($rspag);
	}
	else
	{
		$ret = "";
	}
	return $ret;
}
function GeefProductgroepURL($prodgrp_id){
	$ret = "";
	if ($prodgrp_id!="") {
		$tmpPagId = GeefPaginaIdByModule("PRODUCTEN");
		if ($tmpPagId<1) { $tmpPagId = $GLOBALS['pagid']; }
		$tmpHmId = GeefPaginaHMID($tmpPagId);
		$tmpSmId = GeefPaginaSMID($tmpPagId);
		if ($tmpHmId<1) { $tmpHmId = $GLOBALS['hmid']; }
		if ($tmpSmId<1) { $tmpSmId = $GLOBALS['smid']; }
		if ( $GLOBALS['pagid']==GeefPaginaIdByModule("OFFERTE")) {
			$tmpPagId = $GLOBALS['pagid'];
		}
		$tbl_url = "prodgrp_url_" . strtolower($GLOBALS['GekozenTaal']);
		$query_pag = "SELECT * FROM productgroepen WHERE prodgrpid=" . $prodgrp_id . " and prodgrp_publiceren<>0;";
		$rspag = mysql_query($query_pag, $GLOBALS['conn']) or die(mysql_error());
		$row_rspag = mysql_fetch_assoc($rspag);
		if (mysql_num_rows($rspag)>0) {
			if ($row_rspag[$tbl_url]!="") {
				$ret = $row_rspag[$tbl_url];
			}
			else
			{
				$ret = $GLOBALS['HMURL']."?hmid=".$tmpHmId."&amp;smid=".$tmpSmId."&amp;pagid=".$tmpPagId."&amp;taalcode=".$GLOBALS['GekozenTaal']."&amp;prodgrpid=".$prodgrp_id;
			}
	    }
		mysql_free_result($rspag);
	}
	else
	{
		$ret = "";
	}
	return $ret;
}
function GeefMarktsegmentURL($msm_id){
	$ret = "";
	if ($msm_id!="") {
		$tmpPagId = GeefPaginaIdByModule("MARKTEN");
		$tbl_url = "marktsegment_url_" . strtolower($GLOBALS['GekozenTaal']);
		$query_pag = "SELECT * FROM marktsegmenten WHERE marktsegmentid=" . $msm_id . " and marktsegment_publiceren<>0;";
		$rspag = mysql_query($query_pag, $GLOBALS['conn']) or die(mysql_error());
		$row_rspag = mysql_fetch_assoc($rspag);
		if (mysql_num_rows($rspag)>0) {
			if ($row_rspag[$tbl_url]!="") {
				$ret = $row_rspag[$tbl_url];
			}
			else
			{
				$ret = $GLOBALS['HMURL']."?hmid=".$GLOBALS['hmid']."&amp;smid=".$GLOBALS['smid']."&amp;pagid=".$GLOBALS['pagid']."&amp;taalcode=".$GLOBALS['GekozenTaal']."&amp;msmid=".$msm_id;
			}
	    }
		mysql_free_result($rspag);
	}
	else
	{
		$ret = "";
	}
	return $ret;
}
function GeefTarget($tbl_type, $item_id){
	switch($tbl_type){
		case "prodgrp":
			$tbl_naam = "productgroepen";
			$tbl_id = "prodgrpid";
			$tbl_url = "prodgrp_url_" . strtolower($GLOBALS['GekozenTaal']);
			$tbl_newwin = "";
			break;
		case "msm":
			$tbl_naam = "marktsegmenten";
			$tbl_id = "marktsegmentid";
			$tbl_url = "marktsegment_url_" . strtolower($GLOBALS['GekozenTaal']);
			$tbl_newwin = "";
			break;
		default:
			$tbl_naam = "paginas";
			$tbl_id = "paginaid";
			$tbl_url = "paginaurl";
			$tbl_newwin = "paginanieuwvenster";
	} // switch
	$ret = "";
	if ($item_id!="") {
		$query_pag = "SELECT * FROM " . $tbl_naam . " WHERE " . $tbl_id . "=" . $item_id . "";
		$rspag = mysql_query($query_pag, $GLOBALS['conn']) or die(mysql_error());
		$row_rspag = mysql_fetch_assoc($rspag);
		if (mysql_num_rows($rspag)>0) {
			if ($row_rspag[$tbl_url]!="") {
				if ($tbl_newwin!="") {
					if ($row_rspag[$tbl_newwin]!=0) {
						$ret = "target='_blank'";
					}
					else{
						$ret = "target='_parent'";
					}
				}
				else{
					$ret = "target='_parent'";
				}
			}
			else
			{
				if ($tbl_newwin!="") {
					if ($row_rspag[$tbl_newwin]!=0) {
						$ret = "target='_blank'";
					}
					else{
						$ret = "target='_parent'";
					}
				}
				else{
					$ret = "target='_parent'";
				}
			}
	    }
		mysql_free_result($rspag);
	}
	else
	{
		$ret = "";
	}
	return $ret;
}
function ToonAfbeelding($afb_id) {
if (toInt($afb_id)>0){
	$query_afb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=" . $afb_id . ";";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
        $AfbGUID = MaakGUIDString($row_rsafb['afbeeldingguid']);
        $AfbMaand = Maak2Dig(datMonth($row_rsafb['afbdatum']));
        $AfbJaar = datYear($row_rsafb['afbdatum']);
        $AfbExt = $row_rsafb['afbeeldingextentie'];
    ?>
    <img src="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=$AfbJaar?>/<?=$AfbMaand?>/<?=$AfbGUID?>.<?=$AfbExt?>" class="border" /><br />
    <?
    }
	mysql_free_result($rsafb);
	}
}
function GeefAfbeeldingSrc($afb_id) {
if (toInt($afb_id)>0){
	$query_afb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=" . $afb_id . ";";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
        $AfbGUID = MaakGUIDString($row_rsafb['afbeeldingguid']);
        $AfbMaand = Maak2Dig(datMonth($row_rsafb['afbdatum']));
        $AfbJaar = datYear($row_rsafb['afbdatum']);
        $AfbExt = $row_rsafb['afbeeldingextentie'];
    	return $GLOBALS['ApplicatieRoot'] . "/". $GLOBALS['UploadImageFolder'] . "/" . $AfbJaar . "/" . $AfbMaand . "/" . $AfbGUID . "." . $AfbExt;
    }
	mysql_free_result($rsafb);
	}
}

function ToonPaginaAfbeeldingen($Module_type, $Item_Id) {
	switch(strtoupper($Module_type)){
		case "NIEUWS":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_nieuwsid=" . $Item_Id . " AND pa_publiceren<>0 AND pa_afbeeldingid>0 ORDER BY pa_volgorde;";
			break;
		case "VACATURE":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_vacatureid=" . $Item_Id . " AND pa_publiceren<>0 AND pa_afbeeldingid>0 ORDER BY pa_volgorde;";
			break;
		case "EVENT":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_eventid=" . $Item_Id . " AND pa_publiceren<>0 AND pa_afbeeldingid>0 ORDER BY pa_volgorde;";
			break;
		case "PROJECT":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_projectid=" . $Item_Id . " AND pa_publiceren<>0 AND pa_afbeeldingid>0 ORDER BY pa_volgorde;";
			break;
		default:
			$Module_type="PAGINA";
			$sqlst = "SELECT * FROM paragrafen WHERE pa_paginaid=" . $Item_Id . " AND pa_publiceren<>0 AND pa_afbeeldingid>0 ORDER BY pa_volgorde;";

	} // switch
	$rsprg = mysql_query($sqlst, $GLOBALS['conn']) or die(mysql_error());
	$row_rsprg = mysql_fetch_assoc($rsprg);
	if (mysql_num_rows($rsprg)>0) {
		do
		{
	        $afbSrc = "";
	        //$afbTxt = $row_rsprg["pa_afbeeldingonderschrift"];
        	switch(strtoupper($row_rsprg['paragraaftype'])){
        		case "TEKST":
        			if (toInt($row_rsprg['pa_afbeeldingid'])>0) {
        				$tmpWidth="";
						$query_afb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=" . $row_rsprg['pa_afbeeldingid'] . " AND afbeeldinggereed<>0;";
						$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
						$row_rsafb = mysql_fetch_assoc($rsafb);
						if (mysql_num_rows($rsafb)>0) {
	                        $AfbGUID = $row_rsafb['afbeeldingguid'];
	                        if ($row_rsafb['afbeeldingformaat']== "HEEL"){
								$tmpWidth=" width='" . $GLOBALS['AfbeeldingMaxKlein'] . "' ";
	                        }
	                        $afbSrc = $GLOBALS['ApplicatieRoot'] . "/" . $GLOBALS['UploadImageFolder'] . "/" . datYear($row_rsafb['afbdatum']) . "/" . Maak2Dig(datMonth($row_rsafb['afbdatum'])) . "/" . $AfbGUID . "." . $row_rsafb['afbeeldingextentie'];
	                    }
	                   	mysql_free_result($rsafb);
        			}
        			break;
        		default:
        			//
        	} // switch
			if ($afbSrc!="") {
				?>
				<div style="width: 175px; padding-top: 10px;">
				<center><img name="kaderafb<?=$row_rsprg['pa_afbeeldingid']?>" id="kaderafb<?=$row_rsprg['pa_afbeeldingid']?>" <?=$tmpWidth?> src="<?=$afbSrc?>"  class="border"/><br /></center>
		        </div>
		        <br />
		        <div style="clear:both;"></div>
				<?
			}
		}
		while ($row_rsprg = mysql_fetch_assoc($rsprg));
	}
	mysql_free_result($rsprg);
}

function ToonPagina($Pag_Id){
	$tmpMyModule =strtoupper(GeefPaginaModuleType($Pag_Id));
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $Pag_Id . ";";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
        if ($row_rsafb['ledenloginnodig']==true){
            //if Session("ZSOLoginLidID")<>"" then
            // Deze pagina is alleen voor leden en het lid is ingelogd
            //    if rsPag("Pagina_SoortLidCode")<>"" then
            //        if GeefDBWaarde("Lid_SoortLidCode", "qryLedenGegevens", "LidID=" & Session("ZSOLoginLidID"))=rsPag("Pagina_SoortLidCode") then
            //            ' Dit lid mag deze pagina bekijken
            //        else
            //            Response.End
            //        end if
            //    end if
            //else
            // Deze pagina is alleen voor leden en het lid is NIET ingelogd
            //    Response.End
            //end if
        }
        $MijnTitel = $row_rsafb['paginatitel'];
        $MijnDatum = "";
        $MijnDatumTxt = "";
        $MijnOnderTitel = "";
        if ($GLOBALS['nwsid']>0 && $tmpMyModule=="NIEUWS"){
            $MijnTitel = GeefDBWaarde("nwstitel", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden=0");
            $MijnDatum = GeefDBWaarde("nwsdatum", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden=0");
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM nieuws WHERE nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden=0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
			if (mysql_num_rows($rsproj)>0 && $row_rsproj['nieuwsid']>0) {
				if ($row_rsproj['nwsafbeeldingid']>0) {
					$afbProjSrc = GeefAfbeeldingSrc($row_rsproj['nwsafbeeldingid']);
					$MijnOnderTitel.= "<img src='".$afbProjSrc."' alt='' style='border: solid 1px black;' align='right' width='" .$GLOBALS['AfbeeldingMaxKlein']. "' hspace='10' />";
				}
        	}
        	mysql_free_result($rsproj);

            $MijnOnderTitel .= GeefDBWaarde("nwsinleiding", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden=0");
        }
        if ($GLOBALS['vacid']>0 && $tmpMyModule=="VACATURES"){
            $MijnTitel = GeefDBWaarde("vactitel", "vacatures", "vacatureid=" . $GLOBALS['vacid'] . " AND vacpubliceren<>0");
            $MijnDatum = GeefDBWaarde("vacdatumgeplaatst", "vacatures", "vacatureid=" . $GLOBALS['vacid'] . " AND vacpubliceren<>0");
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM vacatures WHERE vacatureid=" . $GLOBALS['vacid'] . " AND vacpubliceren<>0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
        	mysql_free_result($rsproj);

            $MijnOnderTitel .= GeefDBWaarde("vacinleiding", "vacatures", "vacatureid=" . $GLOBALS['vacid'] . " AND vacpubliceren<>0");
        }
        if ($GLOBALS['nwsid']>0 && $tmpMyModule=="NIEUWSLEDEN"){
            $MijnTitel = GeefDBWaarde("nwstitel", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden<>0");
            $MijnDatum = GeefDBWaarde("nwsdatum", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden<>0");
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM nieuws WHERE nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden<>0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
			if (mysql_num_rows($rsproj)>0 && $row_rsproj['nieuwsid']>0) {
				if ($row_rsproj['nwsafbeeldingid']>0) {
					$afbProjSrc = GeefAfbeeldingSrc($row_rsproj['nwsafbeeldingid']);
					$MijnOnderTitel.= "<img src='".$afbProjSrc."' alt='' style='border: solid 1px black;' align='right' width='" .$GLOBALS['AfbeeldingMaxKlein']. "' hspace='10' />";
				}
        	}
        	mysql_free_result($rsproj);

            $MijnOnderTitel .= GeefDBWaarde("nwsinleiding", "nieuws", "nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden<>0");
        }
        if ($GLOBALS['nwsjaar']>0){
            $MijnTitel = $MijnTitel . " " . $GLOBALS['nwsjaar'];
        }
        if ($GLOBALS['evid']>0 && $tmpMyModule=="EVENTS"){
            $MijnTitel = GeefDBWaarde("evtitel", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0");

            $MijnDatumVan = GeefDBWaarde("evvandatum", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0");
            $MijnDatumTot = GeefDBWaarde("evtotdatum", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0");
            $MijnDatumTxt = MaakVolledigeDatum($MijnDatumVan);
            if ($MijnDatumTot!="" && $MijnDatumTot>$MijnDatumVan) {
            	$MijnDatumTxt = $MijnDatumTxt . " t/m " . MaakVolledigeDatum($MijnDatumTot);
            }
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM evenementen WHERE eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
			if (mysql_num_rows($rsproj)>0 && $row_rsproj['eventid']>0) {
				if ($row_rsproj['evafbeeldingid']>0) {
					$afbProjSrc = GeefAfbeeldingSrc($row_rsproj['evafbeeldingid']);
					$MijnOnderTitel.= "<img src='".$afbProjSrc."' alt='' style='border: solid 1px black;' align='right' width='" .$GLOBALS['AfbeeldingMaxKlein']. "' hspace='10' />";
				}
        	}
        	mysql_free_result($rsproj);

            $MijnOnderTitel .= GeefDBWaarde("evinleiding", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0");
        }
        if ($GLOBALS['evid']>0 && $tmpMyModule=="EVENTSLEDEN"){
            $MijnTitel = GeefDBWaarde("evtitel", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0");

            $MijnDatumVan = GeefDBWaarde("evvandatum", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0");
            $MijnDatumTot = GeefDBWaarde("evtotdatum", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0");
            $MijnDatumTxt = MaakVolledigeDatum($MijnDatumVan);
            if ($MijnDatumTot!="" && $MijnDatumTot>$MijnDatumVan) {
            	$MijnDatumTxt = $MijnDatumTxt . " t/m " . MaakVolledigeDatum($MijnDatumTot);
            }
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM evenementen WHERE eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
			if (mysql_num_rows($rsproj)>0 && $row_rsproj['eventid']>0) {
				if ($row_rsproj['evafbeeldingid']>0) {
					$afbProjSrc = GeefAfbeeldingSrc($row_rsproj['evafbeeldingid']);
					$MijnOnderTitel.= "<img src='".$afbProjSrc."' alt='' style='border: solid 1px black;' align='right' width='" .$GLOBALS['AfbeeldingMaxKlein']. "' hspace='10' />";
				}
        	}
        	mysql_free_result($rsproj);

            $MijnOnderTitel .= GeefDBWaarde("evinleiding", "evenementen", "eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0");
        }
		if ($GLOBALS['evmaand']>0 && $GLOBALS['evid']<1 && ($tmpMyModule=="EVENTS")){
            $MijnTitel = $MijnTitel . " " . strtolower(GeefDbWaarde("maandnaam".strtolower($GLOBALS['GekozenTaal']), "_maanden", "maandnr=".$GLOBALS['evmaand']));
        }
        if ($GLOBALS['evjaar']>0 && $GLOBALS['evid']<1 && ($tmpMyModule=="EVENTS")){
            $MijnTitel = $MijnTitel . " " . $GLOBALS['evjaar'];
        }
        if ($GLOBALS['lidid']>0) {
            //$MijnTitel = GeefDBWaarde("lidnaam", "qryledengegevens", "lidid=" . $GLOBALS['lidid'] . "");
        }
        if ($GLOBALS['projid']>0){
            $MijnTitel = GeefDBWaarde("proj_naam", "projecten", "projid=".$GLOBALS['projid']." AND proj_publiceren<>0") . " - " . GeefDBWaarde("proj_subtitel", "projecten", "projid=".$GLOBALS['projid']." AND proj_publiceren<>0");
            $MijnOnderTitel = "";
			$query_proj = "SELECT * FROM projecten WHERE projid=" . $GLOBALS['projid'] . " AND proj_publiceren<>0;";
			$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
			$row_rsproj = mysql_fetch_assoc($rsproj);
			if (mysql_num_rows($rsproj)>0 && $row_rsproj['projid']>0) {
				if ($row_rsproj['proj_afbeeldingid']>0) {
					$afbProjSrc = GeefAfbeeldingSrc($row_rsproj['proj_afbeeldingid']);
					$MijnOnderTitel.= "<img src='".$afbProjSrc."' alt='' style='border: solid 1px black;' align='right' width='" .$GLOBALS['AfbeeldingMaxKlein']. "' hspace='10' />";
				}
        	}
        	mysql_free_result($rsproj);
            $MijnOnderTitel .= GeefDBWaarde("proj_kortebeschrijving", "projecten", "projid=".$GLOBALS['projid']." AND proj_publiceren<>0");
        }
		if ($GLOBALS['prodgrpid']>0 && ($tmpMyModule=="WEBSHOP")){
            $MijnTitel = GeefProductgroepH1Titel();		// functies_producten
        }

    ?>
    <? if ($MijnTitel!="") {
    	?>
	    <h1><?=$MijnTitel?></h1>
    	<?
    }
    ?>
    <?
    if ($MijnDatum!="") {
        echo "<div style='font-size:11px; margin-top: -10px;'>" . MaakVolledigeDatum($MijnDatum) . "</div><br>";
    }
    if ($MijnDatumTxt!="") {
        echo $MijnDatumTxt . "<br>";
    }
    ?>
    <?
    if ($MijnOnderTitel!="") {
        echo "<b>" . $MijnOnderTitel . "</b><br>";
    }
    ?>
    <?
    if ($_SESSION['MeldTekst']!="") {
    	echo "<span style='color: red; font-weight: bold;'>" . $_SESSION['MeldTekst'] . "<br><br></span>";
    	$_SESSION['MeldTekst'] = "";
    }
    ?>
    <?
    if ($GLOBALS['nwsid']<1 && $GLOBALS['vacid']<1 && $GLOBALS['prodgrpid']<1 && $GLOBALS['prodid']<1 && $GLOBALS['artid']<1) {
        ToonParagrafen("PAGINA", $Pag_Id);
    }
    ?>
    <?
    	switch(strtoupper($row_rsafb['moduletype'])){
    		case "PAGINA":
                // Verder niets doen. Is al afgehandeld
    			break;
    		case "WEBSHOP":
				ToonProductenPagina();		// functies_producten
    			break;
    		case "WEBSHOPPROEFAANVRAAG":
    		case "WEBSHOPPROEFVERZONDEN":
			    ToonParagrafen("PAGINA", $Pag_Id);
    			break;
    		case "NIEUWS":
                if ($GLOBALS['nwsid']>0) {
					$query_proj = "SELECT * FROM nieuws WHERE nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden=0;";
					$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
					$row_rsproj = mysql_fetch_assoc($rsproj);
					if (mysql_num_rows($rsproj)>0 && $row_rsproj['nieuwsid']>0) {
	                	ToonParagrafen("NIEUWS", $GLOBALS['nwsid']);

	                	//ToonNieuwsLeden($GLOBALS['nwsid']);			// functies_flevoboulevard
	                	mysql_query("UPDATE nieuws SET nwsgelezen=nwsgelezen+1 WHERE nieuwsid=".$GLOBALS['nwsid'].";",$GLOBALS['conn']);
	                	?>
	                	<hr class="streep" />
	                	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>"><?=Vertaal("terugnaarnieuwsoverzicht")?></a>
	                	<?
		        	}
		        	mysql_free_result($rsproj);
                }
                else
				{
					ToonNieuwsLijst();  //functies_nieuws
				}
    			break;
    		case "VACATURES":
                if ($GLOBALS['vacid']>0) {
					$query_proj = "SELECT * FROM vacatures WHERE vacatureid=" . $GLOBALS['vacid'] . " AND vacpubliceren<>0;";
					$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
					$row_rsproj = mysql_fetch_assoc($rsproj);
					if (mysql_num_rows($rsproj)>0 && $row_rsproj['vacatureid']>0) {
	                	ToonParagrafen("VACATURE", $GLOBALS['vacid']);

	                	mysql_query("UPDATE vacatures SET vacgelezen=vacgelezen+1 WHERE vacatureid=".$GLOBALS['vacid'].";",$GLOBALS['conn']);
	                	?>
	                	<hr class="streep" />
	                	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>"><?=Vertaal("terugnaarvacaturesoverzicht")?></a>
	                	<?
		        	}
		        	mysql_free_result($rsproj);
                }
                else
				{
					ToonVacaturesLijst();  //functies_nieuws
				}
    			break;
    		case "NIEUWSLEDEN":
                if ($GLOBALS['nwsid']>0) {
					$query_proj = "SELECT * FROM nieuws WHERE nieuwsid=" . $GLOBALS['nwsid'] . " AND nwspubliceren<>0 AND nwsalleenleden<>0;";
					$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
					$row_rsproj = mysql_fetch_assoc($rsproj);
					if (mysql_num_rows($rsproj)>0 && $row_rsproj['nieuwsid']>0) {
	                	ToonParagrafen("NIEUWS", $GLOBALS['nwsid']);

	                	ToonNieuwsLeden($GLOBALS['nwsid']);			// functies_flevoboulevard
	                	mysql_query("UPDATE nieuws SET nwsgelezen=nwsgelezen+1 WHERE nieuwsid=".$GLOBALS['nwsid'].";",$GLOBALS['conn']);
	                	?>
	                	<hr class="streep" />
	                	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>"><?=Vertaal("terugnaarnieuwsoverzicht")?></a>
	                	<?
		        	}
		        	mysql_free_result($rsproj);
                }
                else
				{
					ToonNieuwsLijstAlleenLeden();  //functies_nieuws
				}
    			break;
    		case "NIEUWSARCHIEF":
                if ($GLOBALS['nwsid']>0) {
                	ToonParagrafen("NIEUWS", $GLOBALS['nwsid']);
                }
                break;
    		case "EVENTS":
                if ($GLOBALS['evid']>0) {
					$query_proj = "SELECT * FROM evenementen WHERE eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden=0;";
					$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
					$row_rsproj = mysql_fetch_assoc($rsproj);
					if (mysql_num_rows($rsproj)>0 && $row_rsproj['eventid']>0) {
	                	ToonParagrafen("EVENT", $GLOBALS['evid']);

	                	ToonEvenementenLeden($GLOBALS['evid']);			// functies_flevoboulevard
	                	mysql_query("UPDATE evenementen SET evgelezen=evgelezen+1 WHERE eventid=".$GLOBALS['evid'].";",$GLOBALS['conn']);
	                	?>
	                	<hr class="streep" />
	                	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&amp;evjaar=<?=$GLOBALS['evjaar']?>&amp;evmaand=<?=$GLOBALS['evmaand']?>"><?=Vertaal("terugnaarevenementenoverzicht")?></a>
	                	<?
		        	}
		        	mysql_free_result($rsproj);
                }
                else
				{
					ToonEvenementenLijst();  //functies_events
				}
    			break;
    		case "EVENTSLEDEN":
                if ($GLOBALS['evid']>0) {
					$query_proj = "SELECT * FROM evenementen WHERE eventid=" . $GLOBALS['evid'] . " AND evpubliceren<>0 AND evalleenleden<>0;";
					$rsproj = mysql_query($query_proj, $GLOBALS['conn']) or die(mysql_error());
					$row_rsproj = mysql_fetch_assoc($rsproj);
					if (mysql_num_rows($rsproj)>0 && $row_rsproj['eventid']>0) {
	                	ToonParagrafen("EVENT", $GLOBALS['evid']);

	                	ToonEvenementenLeden($GLOBALS['evid']);			// functies_flevoboulevard
	                	mysql_query("UPDATE evenementen SET evgelezen=evgelezen+1 WHERE eventid=".$GLOBALS['evid'].";",$GLOBALS['conn']);
	                	?>
	                	<hr class="streep" />
	                	<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&amp;evjaar=<?=$GLOBALS['evjaar']?>&amp;evmaand=<?=$GLOBALS['evmaand']?>"><?=Vertaal("terugnaaragenda")?></a>
	                	<?
		        	}
		        	mysql_free_result($rsproj);
                }
                else
				{
					ToonEvenementenLijstLeden(0);  //functies_events
				}
    			break;
    		default:
    			//echo strtoupper($row_rsafb['moduletype']);
    	} // switch
	}
	mysql_free_result($rsafb);
}
function GeefVorigeParagraafType($Module_type, $Item_Id, $par_id)
{
	switch(strtoupper($Module_type)){
		case "EVENT":
			$sqlstp = "SELECT * FROM paragrafen WHERE pa_eventid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "PROJECT":
			$sqlstp = "SELECT * FROM paragrafen WHERE pa_projectid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "NIEUWS":
			$sqlstp = "SELECT * FROM paragrafen WHERE pa_nieuwsid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "VACATURE":
			$sqlstp = "SELECT * FROM paragrafen WHERE pa_vacatureid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		default:
			$Module_type = "PAGINA";
			$sqlstp = "SELECT * FROM paragrafen WHERE pa_paginaid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
	} // switch
	$laatstetype = "";
	$retvorigepar = "geen";
	$rsp = mysql_query($sqlstp, $GLOBALS['conn']) or die(mysql_error());
	$row_rsp = mysql_fetch_assoc($rsp);
	if (mysql_num_rows($rsp)>0) {
		do
		{
			if (toInt($par_id)==toInt($row_rsp['paragraafid'])) {
				$retvorigepar = $laatstetype;
			}
			$laatstetype = $row_rsp['paragraaftype'];
		}
		while ($row_rsp = mysql_fetch_assoc($rsp));
	}
	mysql_free_result($rsp);
	//echo $retvorigepar . "<br />";
	return strtoupper($retvorigepar);
}
function GeefVolgendeParagraafType($Module_type, $Item_Id, $par_id)
{
	$ret = "";
	switch(strtoupper($Module_type)){
		case "EVENT":
			$sqlstptype = "SELECT * FROM paragrafen WHERE pa_eventid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "PROJECT":
			$sqlstptype = "SELECT * FROM paragrafen WHERE pa_projectid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "NIEUWS":
			$sqlstptype = "SELECT * FROM paragrafen WHERE pa_nieuwsid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "VACATURE":
			$sqlstptype = "SELECT * FROM paragrafen WHERE pa_vacatureid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		default:
			$Module_type = "PAGINA";
			$sqlstptype = "SELECT * FROM paragrafen WHERE pa_paginaid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
	} // switch
	$laatstetype = "";
	$retvolgendepar = "geen";
	$rsptype = mysql_query($sqlstptype, $GLOBALS['conn']) or die(mysql_error());
	$row_rsptype = mysql_fetch_assoc($rsptype);
	if (mysql_num_rows($rsptype)>0) {
		do
		{
			if ($laatstetype!="") {
				$retvolgendepar = $row_rsptype['paragraaftype'];
				$laatstetype = "";
			}
			if (toInt($par_id)==toInt($row_rsptype['paragraafid'])) {
				$laatstetype = $row_rsptype['paragraaftype'];
			}
		}
		while ($row_rsptype = mysql_fetch_assoc($rsptype));
	}
	mysql_free_result($rsptype);
	//echo $retvolgendepar . "<br />";
	return strtoupper($retvolgendepar);
}

function ToonParagraaf($Module_type, $Item_Id, $par_id, $met_afb){
	$sqlst = "SELECT * FROM paragrafen WHERE paragraafid=" . $par_id . " AND pa_publiceren<>0;";
	$rsprg = mysql_query($sqlst, $GLOBALS['conn']) or die(mysql_error());
	$row_rsprg = mysql_fetch_assoc($rsprg);
	if (mysql_num_rows($rsprg)>0) {
	$tmpPaTekst = $row_rsprg['pa_tekst'];
	?>
	<?php
	$beginPstr = "";
	$eindPstr = "";
	$lilinkstr = "";
	$lilinkstrEind = "";
	$vorigePar = GeefVorigeParagraafType($Module_type,$Item_Id ,$par_id);
	$volgendePar = GeefVolgendeParagraafType($Module_type,$Item_Id ,$par_id);

	switch(strtoupper($row_rsprg['paragraaftype']))
	{
		case "ILINK":
		case "ELINK":
		case "EMAIL":

			if ($vorigePar!="ILINK" && $vorigePar!="ELINK" && $vorigePar!="EMAIL") {
				$beginPstr = "<p>";
				if ($volgendePar=="ILINK" || $volgendePar=="ELINK" || $volgendePar=="EMAIL")
				{
					$beginPstr = $beginPstr . "<ul class='ilinklist'>";
				}
			}
			else
			{
				$lilinkstr = "<li class='ilinklistitem'>";
				$lilinkstrEind = "</li>";
			}
			if ($volgendePar!="ILINK" && $volgendePar!="ELINK" && $volgendePar!="EMAIL") {
				$eindPstr = "</p><div style='clear:both;'></div>";
				if ($vorigePar=="ILINK" || $vorigePar=="ELINK" || $vorigePar=="EMAIL")
				{
					$eindPstr = "</ul>" . $eindPstr;
				}
			}
			else
			{
				$lilinkstr = "<li class='ilinklistitem'>";
				$lilinkstrEind = "</li>";
			}

			break;
		default:
			$beginPstr = "<p>";
			$eindPstr = "</p><div style='clear:both;'></div>";
	}
	?>
    <?=$beginPstr?>
    <?
	switch(strtoupper($row_rsprg['paragraaftype']))
	{
		case "TEKST":
			if (toInt($row_rsprg['pa_afbeeldingid'])>0) {
			$tmpAlign="right";
			$tmpMargin= "left";
			$tmpWidth = "";
			if (strtoupper($row_rsprg['pa_afbeeldinglr'])=="L") {
							$tmpAlign="left";
							$tmpMargin= "right";
						}
			$query_afb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=" . $row_rsprg['pa_afbeeldingid'] . " AND afbeeldinggereed<>0;";
			$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
			$row_rsafb = mysql_fetch_assoc($rsafb);
			if (mysql_num_rows($rsafb)>0) {
			$AfbGUID = $row_rsafb['afbeeldingguid'];
			//if ($row_rsafb['afbeeldingformaat']== "HEEL"){
				$tmpWidth=" width='" . $GLOBALS['AfbeeldingMaxKlein'] . "' ";
			//				}
			?>
				                        <a rel="lightbox" href="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=datYear($row_rsafb['afbdatum'])?>/<?=Maak2Dig(datMonth($row_rsafb['afbdatum']))?>/<?=$AfbGUID?>.<?=$row_rsafb['afbeeldingextentie']?>" target="_blank"><img style="margin-<?=$tmpMargin?>: 10px;" <?=$tmpWidth?> align="<?=$tmpAlign?>" alt="" src="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=datYear($row_rsafb['afbdatum'])?>/<?=Maak2Dig(datMonth($row_rsafb['afbdatum']))?>/<?=$AfbGUID?>.<?=$row_rsafb['afbeeldingextentie']?>" class="border" /></a>
				                    <?
			}
			mysql_free_result($rsafb);
			}
			echo VervangBRMemo($tmpPaTekst);
			break;
		case "ILINK":
			if ($row_rsprg['pa_linktekst']!="") {
					$tmpTitel = $row_rsprg['pa_linktekst'];
				}
			else
				{
					$tmpTitel = GeefPaginaTitel($row_rsprg['pa_ilinkid']);
				}
			?>
	                <?=$lilinkstr?><a class="ilink" <?=MaakPaginaLink($row_rsprg['pa_ilinkid'])?>><?=$tmpTitel?></a><?=$lilinkstrEind?>
	                <?
			break;
		case "ELINK":
			if ($row_rsprg['pa_linktekst']!="") {
					$tmpTitel = $row_rsprg['pa_linktekst'];
				}
			else
				{
					$tmpTitel = $row_rsprg['pa_elinkurl'];
				}
			?>
				    <?=$lilinkstr?><a class="elink" href="<?=$row_rsprg['pa_elinkurl']?>" target="_blank"><?=$tmpTitel?></a><?=$lilinkstrEind?>
				    <?
			break;
		case "EMAIL":
			if ($row_rsprg['pa_linktekst']!="") {
					$tmpTitel = $row_rsprg['pa_linktekst'];
				}
			else
				{
					$tmpTitel = $row_rsprg['pa_emailadres'];
				}
			?>
				    <?=$lilinkstr?><a class="emaillink" href="mailto:<?=$row_rsprg['pa_emailadres']?>"><?=$tmpTitel?></a><?=$lilinkstrEind?>
				    <?
			break;
		case "FOTO":
			if ($tmpPaTekst!="") {
					$tmpOnderschrift = "<i>" . $tmpPaTekst . "</i>";
				}
			else
				{
					$tmpOnderschrift = "";
				}
			$query_afb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=" . $row_rsprg['pa_afbeeldingid'] . " AND afbeeldinggereed<>0;";
			$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
			$row_rsafb = mysql_fetch_assoc($rsafb);
			if (mysql_num_rows($rsafb)>0) {
			$AfbGUID = $row_rsafb['afbeeldingguid'];
			$AfbClass = "border";
			$myalign1="";
			$myalign2="";
			switch($row_rsprg['pa_afbeeldinglr']){
				case "L":
					$myalign1="</p><div align='left'>";
					$myalign2="</div><p>";
					break;
				case "C":
					$myalign1="</p><div align='center'>";
					$myalign2="</div><p>";
					break;
				case "R":
					$myalign1="</p><div align='right'>";
					$myalign2="</div><p>";
					break;
			} // switch
			$elinktxt1 = "";
			$elinktxt2 = "";
			if (Int2Bool($row_rsprg['pa_afbeeldingnoborder'])==true) {
				$AfbClass = "noborder";
			}
			$txtImgA1 = "";
			$txtImgA2 = "";
			$ImgSizeAfbeelden = $GLOBALS['AfbeeldingMaxHeelTonen'];
			if (toInt($row_rsprg['pa_afbeeldingformaat'])>=1) {
				$txtImgA1 = "<a href=\"" . $GLOBALS['ApplicatieRoot']. "/" . $GLOBALS['UploadImageFolder'] . "/". datYear($row_rsafb['afbdatum']) . "/" . Maak2Dig(datMonth($row_rsafb['afbdatum'])) . "/" . $AfbGUID . "." . $row_rsafb['afbeeldingextentie'] . "\" rel=\"lightbox\">";
				$txtImgA2 = "</a>";
				$ImgSizeAfbeelden = $GLOBALS['AfbeeldingMaxHeelTonen'] / $row_rsprg['pa_afbeeldingformaat'];
			}

			if ($row_rsprg['pa_elinkurl']!="") {
				$elinktxt1 = "<a href='". $row_rsprg['pa_elinkurl'] . "' target='_blank'>";
				$elinktxt2 = "</a>";
				$txtImgA1 = "";
				$txtImgA2 = "";
			}
			?>
					<?=$myalign1?><?=$elinktxt1?><?=$txtImgA1?><img <?=$myalign?> onload="ResAfb('i<?=$AfbGUID?>-<?=$row_rsprg['paragraafid']?>', '<?=$ImgSizeAfbeelden?>');" id="i<?=$AfbGUID?>-<?=$row_rsprg['paragraafid']?>" alt="" src="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=datYear($row_rsafb['afbdatum'])?>/<?=Maak2Dig(datMonth($row_rsafb['afbdatum']))?>/<?=$AfbGUID?>.<?=$row_rsafb['afbeeldingextentie']?>" class="<?=$AfbClass?>" /><?=$txtImgA2?><?=$elinktxt2?><br />
					<?=$tmpOnderschrift?><?=$myalign2?>
				<?
			}
			mysql_free_result($rsafb);
			break;
		case "FILM":
			if ($row_rsprg['pa_afbeeldingonderschrift']!="") {
					$tmpOnderschrift = "<i>" . $row_rsprg['pa_afbeeldingonderschrift'] . "</i>";
				}
			else
				{
					$tmpOnderschrift = "";
				}
			?>
				<?=$tmpPaTekst?><br>
               	<?=$tmpOnderschrift?>
				<?
			break;


		case "FILE":
			if ($row_rsprg['pa_linktekst']!="") {
				$tmpTitel = $row_rsprg['pa_linktekst'];
			}
			else
			{
				$tmpTitel = "";
			}
			$query_afb = "SELECT * FROM bestanden WHERE bestandid=" . $row_rsprg['pa_bestandid'] . " AND bestandgereed<>0;";
			$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
			$row_rsafb = mysql_fetch_assoc($rsafb);
			if (mysql_num_rows($rsafb)>0) {
				$tmpBestandsnaam = $row_rsafb['bestandsnaam'];
				$tmpBestandsIcoon = GeefApplicatieIcoonViaExtentie($row_rsafb['bestandsextentie']);
				$tmpBestandsGUID = $row_rsafb['bestandsguid'];
				if ($tmpTitel=="") { $tmpTitel = $tmpBestandsnaam; }
			?>
				</p>
				<table cellpadding="0" cellspacing="0" width="300"><tr><td class="filekop">
				<span style="font-size: 9px; color: #BBBBBB;">Download:<br /><br /></span>
				<?=Icoon($tmpBestandsIcoon)?><a class="filekop" href="<?=$ApplicatieRoot?>/sybit/bestand.php?bestid=<?=$row_rsprg['pa_bestandid']?>&amp;bestguid=<?=$tmpBestandsGUID?>" target="filedown"><?=$tmpTitel?></a><br />
				</td></tr></table>
				<p>
            <?
			}
			mysql_free_result($rsafb);
			break;

		case "FOTOBOEK":
			if ($row_rsprg['pa_fotoboekid']>0) {
				$query_afb = "SELECT * FROM fotoboeken WHERE fotoboekid=" . $row_rsprg['pa_fotoboekid'] . ";";
				$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
				$row_rsafb = mysql_fetch_assoc($rsafb);
				if (mysql_num_rows($rsafb)>0) {
					?>
					<?
					$query_afb2 = "SELECT * FROM fotoboeken_fotos WHERE fbf_fotoboekid=" . $row_rsprg['pa_fotoboekid'] . " ORDER BY RAND();";
					$rsafb2 = mysql_query($query_afb2, $GLOBALS['conn']) or die(mysql_error());
					$row_rsafb2 = mysql_fetch_assoc($rsafb2);
					if (mysql_num_rows($rsafb2)>0) {
						$AantFotos = toInt(mysql_num_rows($rsafb2));
						$FotoTxt = "foto";
						if ($AantFotos>1) {
							$FotoTxt = "foto's";
						}
						?>
						<table width="100%" cellpadding="0" cellspacing="5">
						<tr valign="top">
						<td width="<?=$GLOBALS['BreedteFotoboekFoto']+20?>">
						<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&fotoboekid=<?=$row_rsafb['fotoboekid']?>"><img src="<?=GeefAfbeeldingSrc($row_rsafb2['fbf_afbeeldingid'])?>" class="border" width="<?=$GLOBALS['BreedteFotoboekFoto']?>" /></a>
						</td>
						<td>
						<b><?=$row_rsafb['fbtitel']?></b><br />
						<?
						if ($row_rsafb['fbdatum']!="") {
							echo "<span style='font-size: 10px;'>Datum: " . MaakDatum($row_rsafb['fbdatum']) . "</span><br />";
						}
						?>
						<br />
						<?=$AantFotos?>&nbsp;<?=$FotoTxt?><br />
						<br />
						<a href="<?=$GLOBALS['HMURL']?>?pagid=<?=$GLOBALS['pagid']?>&taalcode=<?=$GLOBALS['GekozenTaal']?>&fotoboekid=<?=$row_rsafb['fotoboekid']?>">Bekijk fotoboek =&gt;</a>
						</td>
						</tr></table>
						<?
					}
					mysql_free_result($rsafb2);

				}
				mysql_free_result($rsafb);
			}
			break;
		case "BLANK":
			$aantuitvoeren = toInt($row_rsprg['pa_aantalkeer']);
			for($i=0;$i<$aantuitvoeren;$i++)
			{
				echo "<br />";
			}
			break;
		case "NIEUWSHOME":
			$aantuitvoeren = toInt($row_rsprg['pa_aantalkeer']);
			$alleenleden = Int2Bool($row_rsprg['pa_alleenleden']);
			ToonNieuwsLijstHome($aantuitvoeren, $GLOBALS['GekozenTaal'], $alleenleden);			// functies_modules
			break;
		case "NIEUWSBHZ":
			$aantuitvoeren = toInt($row_rsprg['pa_aantalkeer']);
			$geeninleiding = Int2Bool($row_rsprg['pa_nwsgeeninleiding']);
			ToonNieuwsLijstBHZNet($aantuitvoeren, "", $geeninleiding);			// functies_modules
			break;
		case "NIEUWSBHZFB":
			$aantuitvoeren = toInt($row_rsprg['pa_aantalkeer']);
			$geeninleiding = Int2Bool($row_rsprg['pa_nwsgeeninleiding']);
			ToonNieuwsLijstBHZNet($aantuitvoeren, "Rubriek", $geeninleiding);			// functies_modules
			break;
		case "CONTACT":
			ToonContactgegevens($GLOBALS['GekozenTaal']);		// functies_modules
			break;
		case "SITEMAP":
			ToonSitemap($GLOBALS['GekozenTaal']);		// functies_modules
			break;
		case "FLEVOBOULEVARD":
			ToonOntdekFlevoboulevard($GLOBALS['GekozenTaal']);		// functies_flevoboulevard
			break;
		default:
			//
		}
		?>
		<?=$eindPstr?>
	        <?
}
mysql_free_result($rsprg);

}
function ToonParagrafen($Module_type, $Item_Id) {
	$partel = 1;
	switch(strtoupper($Module_type)){
		case "EVENT":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_eventid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "PROJECT":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_projectid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "NIEUWS":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_nieuwsid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		case "VACATURE":
			$sqlst = "SELECT * FROM paragrafen WHERE pa_vacatureid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
			break;
		default:
			$Module_type = "PAGINA";
			$sqlst = "SELECT * FROM paragrafen WHERE pa_paginaid=" . $Item_Id . " AND pa_publiceren<>0 ORDER BY pa_volgorde;";
	} // switch
	$rsprg2 = mysql_query($sqlst, $GLOBALS['conn']) or die(mysql_error());
	$row_rsprg2 = mysql_fetch_assoc($rsprg2);
	if (mysql_num_rows($rsprg2)>0) {
		?>
        <script type="text/javascript">
        	function ResAfb(afb_elemid, max_formaat){
            	if (document.getElementById(afb_elemid).width > max_formaat) {
            		document.getElementById(afb_elemid).width = max_formaat;
            	}
        	}
		</script>
		<?
		do
		{
			$MetAfbeelding = true;
			if ($partel==1) { $MetAfbeelding = false; }
			ToonParagraaf($Module_type,$Item_Id ,$row_rsprg2['paragraafid'], $MetAfbeelding);
			$partel = $partel + 1;
		}
		while ($row_rsprg2 = mysql_fetch_assoc($rsprg2));
	}
	mysql_free_result($rsprg2);
}

function IsPagOnzichtBaar($pag_id) {
    $ret = true;
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . " AND publiceren<>0;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = false;
    }
	mysql_free_result($rsafb);
    return $ret;
}
function IsPagAlleenLeden($pag_id) {
    $ret = false;
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . " AND ledenloginnodig<>0;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		if ($GLOBALS['LidIsIngelogd']==false) {
			$ret = true;
		}
    }
	mysql_free_result($rsafb);
    return $ret;
}
function GeefEersteHoofdmenuID($Taal_Code) {
    $ret = "";
	$query_afb = "SELECT * FROM paginas WHERE paginataalcode='" . $Taal_Code . "' AND hoofdmenuvolgorde>0 AND publiceren<>0 ORDER BY hoofdmenuvolgorde;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['paginaid'];
    }
	mysql_free_result($rsafb);
    return $ret;
}
function GeefPaginaHMID($pag_id) {
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . ";";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		if ($row_rsafb['hoofdmenuvolgorde']>0) {
			return $row_rsafb['paginaid'];
		}
		else
		{
			return GeefPaginaHMID($row_rsafb['valtonderpaginaid']);
		}
    }
	mysql_free_result($rsafb);
}
function GeefPaginaSMID($pag_id) {
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $pag_id . ";";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		if ($row_rsafb['valtonderpaginaid']>0) {
			return $row_rsafb['valtonderpaginaid'];
		}
		else
		{
			if ($row_rsafb['submenuvolgorde']>0) {
				return $row_rsafb['paginaid'];
			}
			else
			{
				return -1;
			}
		}
    }
	mysql_free_result($rsafb);
}
function GeefPaginaTitel($Pag_Id){
    $ret = "";
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $Pag_Id . " AND publiceren<>0;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['paginatitel'];
    }
	mysql_free_result($rsafb);
    return $ret;
}
function GeefGooglePaginaTitel($Pag_Id){
    $ret = "";
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $Pag_Id . " AND publiceren<>0;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['paginagoogletitel'];
    }
	mysql_free_result($rsafb);
    return $ret;
}
function GeefPaginaModuleType($Pag_Id) {
    $ret = "";
	$query_afb = "SELECT * FROM paginas WHERE paginaid=" . $Pag_Id . " AND publiceren<>0;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['moduletype'];
    }
	mysql_free_result($rsafb);
    return $ret;
}
function GeefPaginaIdByModule($mod_type) {
    $ret = "";
	$query_afb = "SELECT * FROM paginas WHERE moduletype='" . $mod_type . "' AND publiceren<>0 AND paginataalcode='" . $GLOBALS['GekozenTaal'] . "';";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$ret = $row_rsafb['paginaid'];
    }
	mysql_free_result($rsafb);
    return $ret;
}

function ToonSubmenuProducten($Taal_Code){
	$oldHMURL = $GLOBALS['HMURL'];
	$query_afb = "SELECT * FROM productgroepen WHERE prodgrp_publiceren<>0 ORDER BY prodgrp_volgorde, prodgrp_menunaam;";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		?>
		<ul class="submenu">
		<?
		do
		{
	        $ToonRegel = true;
	        if ($ToonRegel==true)
			{
				$myClass = "submenu";
	        	if ($GLOBALS['prodgrpid'].""== $row_rsafb['prodgrpid']."")
				{
					$myClass = "submenusel";
				}
			    if ($row_rsafb['prodgrp_url']!="")
				{
					$GLOBALS['HMURL'] = $row_rsafb['prodgrp_url'];
				}
				else
				{
					$GLOBALS['HMURL'] = $OldHMURL;
				}
			    if (Left($GLOBALS['HMURL'],7)!="http://") {
			        $SubMenuURL = $GLOBALS['HMURL'] . "?pagid=" . $GLOBALS['pagid'] . "&hmid=" . $GLOBALS['hmid'] . "&smid=" . $row_rsafb['smid'] . "&prodgrpid=" . $row_rsafb['prodgrpid'] . "&taalcode=" . $Taal_Code;
			    }
			    else
			    {
			        $SubMenuURL = $GLOBALS['HMURL'];
			    }
			    $ToonMenuRegel = true;
			    if ($ToonMenuRegel==true) {
			        if ($GLOBALS['prodgrpid']==$row_rsafb['prodgrpid']) {
			        ?>
			            <li><a class="<?=$myClass?>" href="<?=$SubMenuURL?>" target="_top"><?=strtoupper($row_rsafb['prodgrp_menunaam'])?></a></li><br />
			            <br />
			        <?
			        }
			        else
			        {
			        ?>
			            <li><a class="<?=$myClass?>" href="<?=$SubMenuURL?>" target="_top"><?=strtoupper($row_rsafb['prodgrp_menunaam'])?></a></li><br />
			            <br />
			        <?
			        }
			    }
	        }
		}
		while ($row_rsafb = mysql_fetch_assoc($rsafb));
		?>
		</ul>
		<?
	}
	mysql_free_result($rsafb);
}
// Te gebruiken bij het openen van een pagina
function OpenPagina($ExtraPaginaTitel, $BodyClass)
{
	if ($ExtraPaginaTitel!="")
    {
	    $tmpTitel = $GLOBALS['TitelWebsite'] . " - " . $ExtraPaginaTitel . " - " . $GLOBALS['SubTitelWebsite'];
    }
	else
    {
	    $tmpTitel = $GLOBALS['TitelWebsite'] . " - " . $GLOBALS['SubTitelWebsite'];
    }
	if ($BodyClass!="")
	{
		$tmpClass = "class='" . $BodyClass . "' ";
		$tmpBodyId = "id='" . $BodyClass . "' ";
	}
	else
	{
		$tmpClass = "";
		$tmpBodyId = "id='body'";
	}
	if ($GLOBALS['GooglePagTitel']!="") {
	    $tmpTitel = $GLOBALS['GooglePagTitel'] . " - " . $GLOBALS['TitelWebsite'] . " - " . $GLOBALS['SubTitelWebsite'];
	}

    if ($GLOBALS['MetW3CValidatie']==true)
    {
		if (strpos($_SERVER["PHP_SELF"], "/login/").""!="" || $GLOBALS['MetCMSCSS']==true)
		{
        ?>
		<html>
		<?
		}
		else
		{
        ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?
		}
    }
    else
    {
        ?>
<html>
<?
    }
    ?>

<head>
<title><?=$tmpTitel?></title>
<?
        include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/paginahead.php");
        ?>
</head>

<body <?=$tmpClass?><?=$tmpBodyId?>>
<?
}

function SluitPagina()
{
    ?>
</body>
</html>
<?
}
?>
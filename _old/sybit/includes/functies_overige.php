<?
function StuurMailHTML($Van_Naam, $Van_Adres, $Aan_Naam, $Aan_Adres, $Ber_Titel, $Ber_Bericht){

$to      = $Aan_Adres;
$subject = $Ber_Titel;
$message = $Ber_Bericht;

ini_set("sendmail_from",$GLOBALS['StdEmailAdres']);

$headers = "From : " . $GLOBALS['StdEmailAdres'] . "\r\n";
// To send HTML mail, the Content-type header must be set
//$headers = "From: " .$Van_Adres ."\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";


//$headers = "From: ".$Van_Adres."\r\n";
//$headers .= "MIME-Version: 1.0\r\n";
//$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
//$headers .= "Reply-to: \"" . $Van_Naam . "\" <".$Van_Adres.">\r\n";
//$headers .= "Return-Path: <".$Van_Adres.">\r\n";
mail($to, $subject, $message, $headers);
}

function StuurMail($Van_Adres, $Aan_Adres, $Ber_Titel, $Ber_Bericht){

$to      = $Aan_Adres;
$subject = $Ber_Titel;
$message = $Ber_Bericht;

ini_set("sendmail_from", $Van_Adres);

$headers = "From : " . $Van_Adres . "\r\n";
// To send HTML mail, the Content-type header must be set
//$headers = "From: " .$Van_Adres ."\n";
//$headers .= "MIME-Version: 1.0\r\n";
//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
mail($to, $subject, $message, $headers);
}

function MaakDir($dirnaam){
	if (!file_exists($dirnaam)) {
			mkdir($dirnaam);
		}
}
function right($value, $count){
    $value = substr($value, (strlen($value) - $count), strlen($value));
    return $value;
}
function left($string, $count){
    return substr($string, 0, $count);
}

function GeefGUID(){
     if (function_exists('com_create_guid')){
         return com_create_guid();
     }else{
         mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
         $charid = strtoupper(md5(uniqid(rand(), true)));
         $hyphen = chr(45);// "-"
         $uuid = chr(123)// "{"
                 .substr($charid, 0, 8).$hyphen
                 .substr($charid, 8, 4).$hyphen
                 .substr($charid,12, 4).$hyphen
                 .substr($charid,16, 4).$hyphen
                 .substr($charid,20,12)
                 .chr(125);// "}"
         return $uuid;
     }
}
function MaakGUID($gval){
	$ret = str_replace("{", "" , $gval);
	$ret = str_replace("}", "" , $ret);
	return $ret;
}
function redirect($url){
	header('Location: ' . $url);
}

function VervangBRMemo($wrd){
	$ret = $wrd;
	if ($ret!="") {
		//$ret = str_replace("\n", "<br />",$ret);
	}
	return $ret;
}
function VervangBRMemoHTML($wrd){
	$ret = $wrd;
	if ($ret!="") {
		$ret = htmlspecialchars($ret);
		$ret = str_replace("\n", "<br />",$ret);
	}
	return $ret;
}
function ToonGereed($bitwrd)
{
	if($bitwrd==1)
	{
	    return Icoon("gereed");
	}
	else
	{
	    return Icoon("nietgereed");
	}
}

function Icoon($Icoon_naam)
{
	if ($Icoon_naam!="")
	{
		return "&nbsp;<img src='".$GLOBALS['ApplicatieRoot']."/sybit/icons/".$Icoon_naam.".gif' width='16' height='16' class='absmiddle' border='0' alt='' />&nbsp;";
	}
}

function ApplicatieIcoon($Icoon_naam)
{
	if ($Icoon_naam!="")
	{
		return "&nbsp;<img src='".$GLOBALS['ApplicatieRoot']."/applicatie/icons/".$Icoon_naam.".gif' width='16' height='16' class='absmiddle' border='0' alt='' />&nbsp;";
	}
}

function IcoonAlt($IcoonNaam, $AltWaarde)
{
	if ($IcoonNaam!="")
	{
		return "&nbsp;<img src='" . $GLOBALS['ApplicatieRoot'] . "/sybit/icons/" . $IcoonNaam . ".gif' width='16' height='16' class='absmiddle' border='0' alt='" . $AltWaarde . "' />&nbsp;";
	}
}

function MaakBestandsGrootte($size_bytes){

$ext = "b";
$ret = $size_bytes;
	if ($size_bytes>1024*1024) {
		$ext = "Mb";
		$ret = $size_bytes/1024/1024;
	}
	else
	{
		if ($size_bytes>1024) {
			$ext = "Kb";
			$ret = $size_bytes/1024;
		}
	}
	return round($ret,2) . "&nbsp;" . $ext;
}

function GeefApplicatieIcoonViaExtentie($Extentie_Naam){
	$query_rs = "SELECT * FROM bestandsinstellingen WHERE bestandstype='" . $Extentie_Naam . "';";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		$IconName = $row_rs['bestandsicoon'];
	}
	else
	{
		$IconName = "app_download";
	}
	mysql_free_result($rs);

	//IconName = "app_word"
	//IconName = "app_zip"
	//IconName = "app_image"
	//IconName = "app_jpg"
	//IconName = "app_pdf"
	//IconName = "app_html"
	//IconName = "app_excel"
	//IconName = "app_ppt"
	//IconName = "app_access"
	//IconName = "app_text"
	return $IconName;
}

function PlaatsIcoonLink($IcoonNaam, $Linktekst, $LinkURL, $AltWaarde)
{
	return "<a class='icoon' href='" . $LinkURL . "' class='kleur'>" . IcoonAlt($IcoonNaam, $AltWaarde) . $Linktekst . "</a>";
}
function PlaatsIcoonLinkBlank($IcoonNaam, $Linktekst, $LinkURL, $AltWaarde)
{
	return "<a class='icoon' href='" . $LinkURL . "' class='kleur' target='_blank'>" . IcoonAlt($IcoonNaam, $AltWaarde) . $Linktekst . "</a>";
}

function PlaatsVerwijderLink($Link_tekst, $Link_url, $meld_tekst)
{
    if ($meld_tekst!="")
    {
        $mld = $meld_tekst;
    }
    else
    {
        $mld = "Weet u zeker dat u dit wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	}
    $mld = str_replace("'", "", $mld);
    $mld = str_replace("\"", "", $mld);
	?>
	<a href="<?=$Link_url?>" onclick="return confirm('<?=$mld?>');" class="icoon"><?=Icoon("verwijderen")?><?=$Link_tekst?></a>
	<?
}
function PlaatsPrullenbakLink($Link_tekst, $Link_url, $meld_tekst)
{
    if ($meld_tekst!="")
    {
        $mld = $meld_tekst;
    }
	else
	{
        $mld = "Weet u zeker dat u dit wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
    }
    $mld = str_replace("'", "", $mld);
    $mld = str_replace("\"", "", $mld);
	?>
	<a href="<?=$Link_url?>" onclick="return confirm('<?=$mld?>');" class="icoon"><?=Icoon("prullenbak")?><?=$Link_tekst?></a>
	<?
}
?>
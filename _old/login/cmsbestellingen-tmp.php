<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Onafgeronde Bestellingen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("stop", "Terug", "cmsbestellingen.php") ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="100"><center><b>Status</b></td>
    <td width="96"><b>Datum</b></td>
    <td><b>Bedrijfsnaam</b></td>
    <td><b>T.a.v.</b></td>
    <td><b>Plaats</b></td>
</tr>

<?
$query_rs = "SELECT * FROM winkel_tmpbestellingen WHERE be_sessionid NOT IN(SELECT be_sessionid FROM winkel_bestellingen) ORDER BY be_datumtoegevoegd DESC, bestellingid DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
	    $WijzigURL = "cmsbestellingen-tmp-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["bestellingid"];
	    $VerwijderURL = "cmsbestellingen-tmp-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["bestellingid"];
	    $VerwijderMelding = "Weet u zeker dat u deze bestelling wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		$mycol = $GLOBALS['KleurRegelInv'];;
		?>
			<tr class="regel">
			    <td style="background-color: <?=$mycol?>;"><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><center>Tijdelijk</td>
			    <td><a href="<?=$WijzigURL?>"><?=MaakDatum($row_rs["be_datumtoegevoegd"])?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsnaam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["tav"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["afleverplaats"]?></a></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Klanten")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe klant", "cmsklanten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Bedrijfsnaam</b></td>
    <td><b>Adres</b></td>
    <td><b>Plaats</b></td>
    <td><b>Loginnaam</b></td>
    <td width="100" align="center"><b>Logins</b></td>
    <td width="100" align="center"><b>Bestellingen</b></td>
    <td width="100" align="center"><b>Toegang</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM klanten ORDER BY heefttoegang, bedrijfsnaam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
		$MagVerwijderen = false;
	    $WijzigURL = "cmsklanten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["klantid"];
	    $VerwijderURL = "cmsklanten-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["klantid"];
	    $VerwijderMelding = "Weet u zeker dat u deze klant wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantBestellingen = toInt(TelRecords("SELECT * FROM winkel_bestellingen WHERE be_klantid=" . $row_rs['klantid'].""));
	    if ($AantProducten<1) {
	    	$MagVerwijderen = true;
	    	$AantProducten = 0;
	    }
    	$txtbestel = $AantProducten . " bestellingen";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsnaam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsadres"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsplaats"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["klantloginnaam"]?></a></td>
			    <td align="center"><?=$row_rs["aantallogins"]?></td>
			    <td align="center"><?=$txtbestel?></td>
			    <td align="center"><?=ToonGereed($row_rs["heefttoegang"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
			    	<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>

				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
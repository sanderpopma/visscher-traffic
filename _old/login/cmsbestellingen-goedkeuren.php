<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['itemid']<1) {
	redirect("cmsbestellingen.php");
	die;
}

// Inlezen en verwerken paginaparameters
    $query_rs = "SELECT * FROM winkel_bestellingen WHERE bestellingid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['bestellingid']>0)
    {
		$MagVerder = true;
		if ($row_rs['bestelstatus']!="gepl") {
			$MagVerder = false;
		}
    }
    else
    {
	    $MagVerder = false;
	}
    mysql_free_result($rs);

if ($GLOBALS['itemid']<1) $MagVerder = false;

if (!strpos(@$_SERVER["HTTP_REFERER"], "cmsbestellingen-edit.php")) {
	$MagVerder = false;
}
if ($MagVerder==true) {

	$query_afb = "SELECT * FROM winkel_bestellingen WHERE bestellingid=" . $GLOBALS['itemid'] . " LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
		$mytaalcode = $row_rsafb['be_taalcode'];
		$myverkoper = $row_rsafb['be_verkopercode'];

		// Sturen e-mailbericht
		$query_rs = "SELECT * FROM winkel_emailberichten WHERE em_zoekcode='bestellinggoedgekeurd_" . strtolower($mytaalcode)."'";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		if ($row_rs['emailid']>0)
		{
			$MailBer = $row_rs['em_soortaanhef'];
			//$MailBer = str_replace("{Voornaam}", MaakVoornaam($voorletters, $voornaam, $tussenvoegsels, $achternaam), $MailBer);
			$MailBer .= "\r\n\r\n";
			$emBer = $row_rs['em_bericht'];
			//$emBer = str_replace("{Nickname}", $nickname, $emBer);
			$MailBer .= $emBer."\r\n\r\n";
			$MailBer .= GeefDbWaarde("ondertekening_bevestigingsmail", "winkel_verkopers", "verkopercode='".$myverkoper."'");

			$MailBer .= "\r\n\r\n";
			$MailBer .= GeefBestellingDetails($GLOBALS['itemid']);

			if (Int2Bool($row_rs['em_metlinkonderbericht'])==true) {
				$MailBer .= "\r\n\r\n".$GLOBALS['ApplicatieRoot'];
			}
			$MailTitel = $GLOBALS['ApplicatieNaam']. " - " . $row_rs['em_titel'];
		}
		mysql_free_result($rs);
		$MailVan = GeefDbWaarde("emailadres_bevestigingsmail", "winkel_verkopers", "verkopercode='".$myverkoper."'");
		$MailAan = $row_rsafb['emailadres'];
		if ($MailAan=="") { $MailAan = $MailVan; }
		//StuurMail($MailVan, $MailAan, $MailTitel, $MailBer);

		// Reserveren voorraad
		$query_art = "SELECT * FROM winkel_bestelregels INNER JOIN winkel_artikelen ON winkel_artikelen.artid=winkel_bestelregels.br_artikelid WHERE br_bestellingid=" . $GLOBALS['itemid'];
		$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
		$row_rsart = mysql_fetch_assoc($rsart);
		if (mysql_num_rows($rsart)>0) {
			do
			{
			$regelaantal = $row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"];

			$query = "UPDATE winkel_artikelen SET art_voorraadinbestelling=art_voorraadinbestelling+$regelaantal WHERE artid=" . $row_rsart['br_artikelid'] ." LIMIT 1;";
			$rs = mysql_query($query, $GLOBALS['conn']) or die(mysql_error());
			}
			while ($row_rsart = mysql_fetch_assoc($rsart));
		}
		mysql_free_result($rsart);

		// Bijwerken bestelling
		$query_rs = "UPDATE winkel_bestellingen SET bestelstatus='beh', datum_inbehandeling=NOW(), door_inbehandeling_mdwid=" . SQLStr(GeefHuidigeUserId()) . " WHERE bestellingid=".$GLOBALS['itemid']." LIMIT 1";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	}

	redirect("cmsbestellingen.php");
}
else
{
	die;
}

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
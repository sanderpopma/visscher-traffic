<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
$MagVerder = true;
$partype = $_GET["partype"];
$itemid = $_GET["itemid"];

switch(strtoupper($partype)){
	case "NIEUWS":
		$CheckVanaf = "cmsnieuws-edit.php";
		$RedirLink = "cmsnieuws-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&nwsid=" . $itemid;
		break;
	case "VACATURE":
		$CheckVanaf = "cmsvacatures-edit.php";
		$RedirLink = "cmsvacatures-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vacid=" . $itemid;
		break;
	case "EVENT":
		$CheckVanaf = "cmsevents-edit.php";
		$RedirLink = "cmsevents-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&evid=" . $itemid;
		break;
	case "PROJECT":
		$CheckVanaf = "cmsprojecten-edit.php";
		$RedirLink = "cmsprojecten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projid=" . $itemid;
		break;
	default:
		$partype="PAGINA";
		$CheckVanaf = "cmspaginas-edit.php";
		$RedirLink = "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $itemid;
} // switch

if ($GLOBALS['parid']<1) $MagVerder = false;

if (!strpos(@$_SERVER["HTTP_REFERER"], $CheckVanaf)) {
	$MagVerder = false;
}
if ($MagVerder==true) {
	$query_rs = "DELETE FROM paragrafen WHERE paragraafid=".$GLOBALS['parid']."";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	redirect($RedirLink);
}
else
{
	die;
}

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
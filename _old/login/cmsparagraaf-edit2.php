<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
$partype = $_GET['partype'];
$itemid = $_GET['itemid'];
switch(strtoupper($partype)){
	case "NIEUWS":
        $RedirLink = "cmsnieuws-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&nwsid=" . $itemid;
		break;
	case "VACATURE":
        $RedirLink = "cmsvacatures-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&vacid=" . $itemid;
		break;
	case "EVENT":
        $RedirLink = "cmsevents-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&evid=" . $itemid;
		break;
	case "PROJECT":
		$RedirLink = "cmsprojecten-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&projid=" . $itemid;
		break;
	default:
        $partype = "PAGINA";
        $RedirLink = "cmspaginas-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&pagid=" . $itemid;
} // switch

$tmpELink = Post2URL($_POST["pa_elinkurl"]);

$PA_Naam = $_POST["pa_naam"];
$PA_Volgorde = $_POST["pa_volgorde"];
$PA_Publiceren = $_POST["pa_publiceren"];

$PA_Tekst = $_POST["pa_tekst"];

$PA_AfbeeldingID = $_POST["pa_afbeeldingid"];
$PA_AfbeeldingLR = $_POST["pa_afbeeldinglr"];
$PA_AfbeeldingOnderschrift = $_POST["pa_afbeeldingonderschrift"];
$PA_AfbeeldingNoBorder = $_POST["pa_afbeeldingnoborder"];
$pa_afbeeldingformaat =  toInt($_POST["pa_afbeeldingformaat"]);
if ($pa_afbeeldingformaat<1) {
	$pa_afbeeldingformaat=1;
}
$PA_LinkTekst = $_POST["pa_linktekst"];
$PA_Emailadres = $_POST["pa_emailadres"];
$PA_ILinkID = $_POST["pa_ilinkid"];
$PA_EventLinkID = $_POST["pa_eventlinkid"];

$PA_FormulierID = $_POST["pa_formulierid"];
$PA_BestandID = $_POST["pa_bestandid"];
$PA_FotoboekID = $_POST["pa_fotoboekid"];

$pa_aantalkeer = $_POST["pa_aantalkeer"];
$pa_nwsgeeninleiding = $_POST["pa_nwsgeeninleiding"];

$pa_alleenleden = $_POST["pa_alleenleden"];

if ($GLOBALS['parid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE paragrafen SET ";
	$qry2 = "pa_naam=".SQLStr($PA_Naam).", ".
		"pa_volgorde=".SQLStr($PA_Volgorde).", ".
		"pa_tekst=".SQLStr($PA_Tekst).", ".
		"pa_afbeeldingid=".SQLStr($PA_AfbeeldingID).", ".
		"pa_afbeeldinglr=".SQLStr($PA_AfbeeldingLR).", ".
		"pa_afbeeldingonderschrift=".SQLStr($PA_AfbeeldingOnderschrift).", ".
		"pa_afbeeldingnoborder=".SQLBool($PA_AfbeeldingNoBorder).", ".
		"pa_afbeeldingformaat=".SQLStr($pa_afbeeldingformaat).", ".
		"pa_linktekst=".SQLStr($PA_LinkTekst).", ".
		"pa_elinkurl=".SQLStr($tmpELink).", ".
		"pa_emailadres=".SQLStr($PA_Emailadres).", ".
		"pa_ilinkid=".SQLStr($PA_ILinkID).", ".
		"pa_eventlinkid=".SQLStr($PA_EventLinkID).", ".
		"pa_formulierid=".SQLStr($PA_FormulierID).", ".
		"pa_bestandid=".SQLStr($PA_BestandID).", ".
		"pa_fotoboekid=".SQLStr($PA_FotoboekID).", ".
		"pa_aantalkeer=".SQLStr($pa_aantalkeer).", ".
		"pa_nwsgeeninleiding=".SQLBool($pa_nwsgeeninleiding).", ".
		"pa_alleenleden=".SQLBool($pa_alleenleden).", ".
		"pa_publiceren=".SQLBool($PA_Publiceren)."";

	$qry3=" WHERE paragraafid=".$GLOBALS['parid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

redirect($RedirLink);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
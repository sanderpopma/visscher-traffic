<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
// if Not HeeftRechten(GeefHuidigeUserId, "LEDEN") then Response.End

$nickname = $_POST['nickname'];
$voorletters = strtoupper($_POST['voorletters']);
$voornaam = $_POST['voornaam'];
$tussenvoegsels = $_POST['tussenvoegsels'];
$achternaam = $_POST['achternaam'];
$geslacht = strtoupper($_POST['geslacht']);
$burgerlijke_staat = $_POST['burgerlijke_staat'];
$geboortedatum = $_POST['geboortedatum'];
$landvanherkomst = $_POST['landvanherkomst'];

$adres = $_POST['adres'];
$postcode = strtoupper($_POST['postcode']);
$woonplaats = $_POST['woonplaats'];
$woonplaats_website = $_POST['woonplaats_website'];
$provincie = $_POST['provincie'];
$land = $_POST['land'];
$telefoonnummer = $_POST['telefoonnummer'];

$emailadres = $_POST['emailadres'];
$wachtwoord = $_POST['wachtwoord'];

$geloof_praktiserend = $_POST['geloof_praktiserend'];
$geloof_richting = $_POST['geloof_richting'];
$geloof_beleving = $_POST['geloof_beleving'];

$gewoonte_roken = $_POST['gewoonte_roken'];
$gewoonte_alcohol = $_POST['gewoonte_alcohol'];

//$isgoedgekeurd = $_POST['isgoedgekeurd'];
$isactief = $_POST['isactief'];

$datumtoegangtot = $_POST['datumtoegangtot'];
if (SQLBool($isactief)=="'1'" && $datumtoegangtot=="") {
	switch(date('m')){
		case 1:
		case 2:
		case 3:
			$datumtoegangtot = "31-3-".date('Y');
			break;
		case 4:
		case 5:
		case 6:
			$datumtoegangtot = "30-6-".date('Y');
			break;
		case 7:
		case 8:
		case 9:
			$datumtoegangtot = "30-9-".date('Y');
			break;
		case 10:
		case 11:
		case 12:
			$datumtoegangtot = "31-12-".date('Y');
			break;
		default:
			;
	} // switch
}

if ($GLOBALS['lidid']>0)
{
	$PgMode = "WIJZIG";
}
else
{
	$PgMode = "TOEVOEG";
}
if ($PgMode=="TOEVOEG") {
	$qry1="INSERT INTO inschrijvingen (datumingeschreven, datumlaatstelogin, nickname) VALUES(NOW(), NOW(), ";
	$qry2 = "".SQLStr($nickname)."";
	$qry3=")";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$GLOBALS['lidid'] = mysql_insert_id();
}

if ($GLOBALS['lidid']>0)
{
	$qry1="UPDATE inschrijvingen SET ";
	$qry2 = "nickname=".SQLStr($nickname).", ".
		"voorletters=".SQLStr($voorletters).", ".
		"voornaam=".SQLStr($voornaam).", ".
		"tussenvoegsels=".SQLStr($tussenvoegsels).", ".
		"achternaam=".SQLStr($achternaam).", ".
		"geslacht=".SQLStr($geslacht).", ".
		"burgerlijke_staat=".SQLStr($burgerlijke_staat).", ".
		"geboortedatum=".SQLDat($geboortedatum).", ".
		"landvanherkomst=".SQLStr($landvanherkomst).", ".
		"adres=".SQLStr($adres).", ".
		"postcode=".SQLStr($postcode).", ".
		"woonplaats=".SQLStr($woonplaats).", ".
		"woonplaats_website=".SQLStr($woonplaats_website).", ".
		"provincie=".SQLStr($provincie).", ".
		"land=".SQLStr($land).", ".
		"telefoonnummer=".SQLStr($telefoonnummer).", ".
		"emailadres=".SQLStr($emailadres).", ".
		"wachtwoord=".SQLStr($wachtwoord).", ".
		"geloof_praktiserend=".SQLStr($geloof_praktiserend).", ".
		"geloof_richting=".SQLStr($geloof_richting).", ".
		"geloof_beleving=".SQLStr($geloof_beleving).", ".
		"gewoonte_roken=".SQLStr($gewoonte_roken).", ".
		"gewoonte_alcohol=".SQLStr($gewoonte_alcohol).", ".
		"uiterlijk_lengte=".SQLStr($_POST['uiterlijk_lengte']).", ".
		"uiterlijk_postuur=".SQLStr($_POST['uiterlijk_postuur']).", ".
		"uiterlijk_kleurogen=".SQLStr($_POST['uiterlijk_kleurogen']).", ".
		"uiterlijk_kleurhaar=".SQLStr($_POST['uiterlijk_kleurhaar']).", ".
		"uiterlijk_huidskleur=".SQLStr($_POST['uiterlijk_huidskleur']).", ".
		"uiterlijk_kledingstijl=".SQLStr($_POST['uiterlijk_kledingstijl']).", ".
		"uiterlijk_overige=".SQLStr($_POST['uiterlijk_overige']).", ".
		"cv_opleiding=".SQLStr($_POST['cv_opleiding']).", ".
		"cv_beroep=".SQLStr($_POST['cv_beroep']).", ".
		"cv_rijbewijs=".SQLStr($_POST['cv_rijbewijs']).", ".
		"cv_hobbys=".SQLStr($_POST['cv_hobbys']).", ".
		"cv_sport=".SQLStr($_POST['cv_sport']).", ".
		"cv_talen=".SQLStr($_POST['cv_talen']).", ".
		"datumtoegangtot=".SQLDat($datumtoegangtot).", ".
		"isalingeschreven=".SQLBool($_POST['isalingeschreven']).", ".
		"isactief=".SQLBool($isactief)."";

	$qry3=" WHERE inschrijvingid=".$GLOBALS['lidid']." LIMIT 1";

//		"isgoedgekeurd=".SQLBool($isgoedgekeurd).", ".

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
}

if ($PgMode=="TOEVOEG") {
	redirect("cmsleden.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);
	//redirect("cmsleden-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&lidid=".$GLOBALS['lidid']);
}
else{
	redirect("cmsleden.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);
}

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
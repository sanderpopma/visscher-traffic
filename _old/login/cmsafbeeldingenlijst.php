<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
$query_rs = "SELECT * FROM afbeelding_soorten WHERE afbsoort='" . $GLOBALS['afbtype'] . "';";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel($row_rs['afbsoortomschrijving'] . " beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsafbeeldingen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"></td>
    <td width="80" align="center"><b>Gebruikt</b></td>
    <td width="80" align="center"><b>Verwijder</b></td>
    <td align="left"><b>Afbeelding</b></td>
</tr>

<?
$x = 1;
$query_rs2 = "SELECT * FROM afbeeldingen WHERE afbeeldingformaat='" . $GLOBALS['afbtype'] . "' ORDER BY afbdatum;";
$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
$row_rs2 = mysql_fetch_assoc($rs2);
if (mysql_num_rows($rs2)>0) {
	do
	{
		$AantGebruikt = 0;
	    $regelclass="regel";
	    if ($x % 2==0) { $regelclass="kadervoet"; }
	    $VerwijderURL = "cmsafbeeldingen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&afbid=" . $row_rs2['afbeeldingid'] . "&afbtype=" . $GLOBALS['afbtype'];
	    $VerwijderMelding = "Weet u zeker dat u deze afbeelding wilt verwijderen (deze wordt ook verwijderd van de server)? (Dit kan niet ongedaan worden gemaakt!)";
	    $MagVerwijderen = true;
	    	$Tel1 = TelRecords("SELECT * FROM paragrafen WHERE pa_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel1>0) { $AantGebruikt = $AantGebruikt + $Tel1; }
			$Tel3 = TelRecords("SELECT * FROM paginas WHERE pagina_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel3>0) { $AantGebruikt = $AantGebruikt + $Tel3; }
			$Tel3 = TelRecords("SELECT * FROM afbeeldingen_boven WHERE ab_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel3>0) { $AantGebruikt = $AantGebruikt + $Tel3; }
			$Tel3 = TelRecords("SELECT * FROM pagina_afbeeldingen WHERE paa_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel3>0) { $AantGebruikt = $AantGebruikt + $Tel3; }
			$Tel4 = TelRecords("SELECT * FROM nieuws WHERE nwsafbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel4>0) { $AantGebruikt = $AantGebruikt + $Tel4; }
			$Tel4 = TelRecords("SELECT * FROM googlemapsreferenties WHERE ref_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel4>0) { $AantGebruikt = $AantGebruikt + $Tel4; }
			$Tel4 = TelRecords("SELECT * FROM artikelen WHERE art_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel4>0) { $AantGebruikt = $AantGebruikt + $Tel4; }
			$Tel4 = TelRecords("SELECT * FROM fotoboeken_fotos WHERE fbf_afbeeldingid=" . $row_rs2['afbeeldingid'].";");
			if ($Tel4>0) { $AantGebruikt = $AantGebruikt + $Tel4; }
	    if ($AantGebruikt<1) {
	    	$AantGebruikt = 0;
	    }
	    if ($AantGebruikt>0) {
	    	$MagVerwijderen = false;
	    }
		?>
		<tr class="<?=$regelclass?>">
		    <td></td>
		    <td align="center"><?=$AantGebruikt?>x</td>
		    <td align="center">
		    <? if ($MagVerwijderen==true) { ?>
		        <?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
		    <? } else { ?>
		        <?=Icoon("stop")?>
		    <? } ?>
		    </td>
		    <td align="left">
		    <?=ToonAfbeelding($row_rs2['afbeeldingid'], false, false) ?>
		    </td>
		</tr>
		<?
	$x = $x + 1;
	}
	while ($row_rs2 = mysql_fetch_assoc($rs2));
}
mysql_free_result($rs2);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>
<?
}
mysql_free_result($rs);
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
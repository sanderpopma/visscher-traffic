<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "LINKS")) { die; }

if ($GLOBALS['linkrubid']<1) {
	redirect("cmslinkrub.php");
}
$LinkRubNaam = GeefDBWaarde("rubnaam_nl", "links_rubrieken", "rubriekid=".$GLOBALS['linkrubid']);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Links in de rubriek ".$LinkRubNaam)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe link", "cmslinks-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&linkrubid=".$GLOBALS['linkrubid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmslinkrub.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Rubriek</b></td>
    <td><b>Naam link<?=Icoon("NL")?></b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM links WHERE link_rubriekid=".$GLOBALS['linkrubid']." ORDER BY linktitel_nl;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmslinks-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" .$row_rs["link_rubriekid"]."&linkid=".$row_rs['linkid'];
	    $VerwijderURL = "cmslinks-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" .$row_rs["link_rubriekid"]."&linkid=".$row_rs['linkid'];
	    $VerwijderMelding = "Weet u zeker dat u deze link wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Link wijzigen")?></td>
			    <td><?=$LinkRubNaam?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["linktitel_nl"]?></a></td>
			    <td><center><?=ToonGereed($row_rs["linkpubliceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['pagid']<1) {
	redirect("cmspaginas.php");
	die;
}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Afbeeldingen (rechterkant) beheren [" .GeefPaginaTitel($GLOBALS['pagid'])."]")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe afbeelding", "cmspagina-afbeeldingen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&pagid=".$GLOBALS['pagid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar pagina", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&pagid=".$GLOBALS['pagid']) ?>
<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="80" align="center"><b>Volgorde</b></td>
    <td><b>Afbeelding</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM pagina_afbeeldingen WHERE paa_paginaid=" . $GLOBALS['pagid'] . " ORDER BY paa_volgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmspagina-afbeeldingen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=".$GLOBALS['pagid'] . "&paaid=" .$row_rs["paaid"];
	    $VerwijderURL = "cmspagina-afbeeldingen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=".$GLOBALS['pagid'] . "&paaid=" .$row_rs["paaid"];
	    $VerwijderMelding = "Weet u zeker dat u deze afbeelding niet langer wilt gebruiken op deze pagina? (De afbeelding zelf blijft wel bewaard)";
		?>
			<tr class="regel" valign="top">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td align="center"><?=$row_rs["paa_volgorde"]?></td>
			    <td><img width="<?=$GLOBALS['AfbeeldingMaxArtikel']?>" src="<?=GeefAfbeeldingSrc($row_rs["paa_afbeeldingid"])?>" border="1" /></td>
			    <td align="center"><?=ToonGereed($row_rs["paa_publiceren"])?></td>
			    <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
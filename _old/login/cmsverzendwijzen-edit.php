<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM winkel_verzendwijzen WHERE verzendwijzeid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['verzendwijzeid']>0)
    {
        $verzendwijzecode = $row_rs['verzendwijzecode'];
        $verzendwijzenaam_nl = $row_rs['verzendwijzenaam_nl'];
        $verzendwijzekosten = $row_rs['verzendwijzekosten'];
        $verzendwijzevolgorde = $row_rs['verzendwijzevolgorde'];
        $verzendwijzeactief = Int2Bool($row_rs['verzendwijzeactief']);
     }
    mysql_free_result($rs);
}
else
{
		$verzendwijzekosten = 0;
        $verzendwijzeactief = true;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Verzendwijze " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsverzendwijzen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsverzendwijzen-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid'], "gebruiker", 700)?>
    <?=FrmText("Zoekcode", "verzendwijzecode", $verzendwijzecode, 30, 50) ?>
    <?=FrmText("Omschrijving", "verzendwijzenaam_nl", $verzendwijzenaam_nl, 60, 250) ?>

    <?=FrmText("Volgorde", "verzendwijzevolgorde", $verzendwijzevolgorde, 4, 4) ?>
    <?=FrmText(MaakBedragLabel("Kosten verzendwijze"), "verzendwijzekosten", $verzendwijzekosten, 10, 10) ?>
    <?=FrmCheckbox("Verzendwijze actief", "verzendwijzeactief", $verzendwijzeactief) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("verzendwijzecode") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("gebruiker");
frmvalidator.addValidation("verzendwijzecode","req","Zoekcode is verplicht");
frmvalidator.addValidation("verzendwijzenaam_nl","req","Omschrijving is verplicht");
frmvalidator.addValidation("verzendwijzekosten","req","Kosten is verplicht");
frmvalidator.addValidation("verzendwijzekosten","dec","Kosten heeft een ongeldige waarde");
frmvalidator.addValidation("verzendwijzevolgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("verzendwijzevolgorde","num","Volgorde heeft een ongeldige waarde");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
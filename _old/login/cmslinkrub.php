<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "LINKS")) { die; }
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Link Rubrieken")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe rubriek", "cmslinkrub-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Rubriek</b></td>
    <td width="100"><b>Links</b></td>
    <td width="80" align="center" nowrap><b>Nieuw</b></td>
    <td width="60"><center><b>Volgorde</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM links_rubrieken ORDER BY rubvolgorde, rubnaam_nl;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
    	$LinkTxt = "links";
	    $WijzigURL = "cmslinkrub-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" .$row_rs["rubriekid"];
	    $VerwijderURL = "cmslinkrub-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" .$row_rs["rubriekid"];
	    $VerwijderMelding = "Weet u zeker dat u deze rubriek wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantLinks = toInt(TelRecords("SELECT * FROM links WHERE link_rubriekid=" . $row_rs['rubriekid'].""));
	    $LinkURL = "cmslinks.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" .$row_rs["rubriekid"];
	    if ($AantLinks<1) {
	    	$MagVerwijderen = true;
	    	$AantLinks = 0;
	    }
	    if ($AantLinks==1) {
	    	$LinkTxt = "link";
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Rubriek wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["rubnaam_nl"]?></a></td>
			    <td><a href="<?=$LinkURL?>"><?=$AantLinks?>&nbsp;<?=$LinkTxt?></a></td>
				<td nowrap><?=PlaatsIcoonLink("toevoegen", "Nieuwe link", "cmslinks-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['=smid'] . "&linkrubid=" .$row_rs["rubriekid"]. "", "Nieuwe link")?></td>
				<td><center><?=$row_rs["rubvolgorde"]?></td>
			    <td><center><?=ToonGereed($row_rs["rubpubliceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
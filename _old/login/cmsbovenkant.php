<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Afbeeldingen bovenkant ".GeefDbWaarde("paginamenunaam", "paginas", "paginaid=".$GLOBALS['parid']))?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Foto toevoegen", "cmsbovenkant-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&parid=".$GLOBALS['parid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar pagina", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&pagid=".$GLOBALS['parid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Afbeelding</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="100" align="center"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM afbeeldingen_boven WHERE ab_hmid=" . $GLOBALS['parid'] ." ORDER BY ab_publiceren DESC, abid DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsbovenkant-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["abid"]."&parid=".$GLOBALS['parid'];
	    $VerwijderURL = "cmsbovenkant-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["abid"]."&parid=".$GLOBALS['parid'];
	    $VerwijderMelding = "Weet u zeker dat u deze afbeelding niet langer wilt gebruiken? (Dit kan niet ongedaan worden gemaakt!)";
	    $MagVerwijderen = true;
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><img src="<?=GeefAfbeeldingSrc($row_rs['ab_afbeeldingid'])?>" width="<?=$GLOBALS['BreedteWebsiteBovenFoto']?>" height="<?=$GLOBALS['HoogteWebsiteBovenFoto']?>" border="0" alt="" /></a></td>
			    <td align="center"><?=ToonGereed($row_rs["ab_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen) {
			    	?>
				<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
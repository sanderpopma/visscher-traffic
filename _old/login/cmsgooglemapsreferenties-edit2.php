<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}
$ref_naam = $_POST['ref_naam'];
$ref_adres = $_POST['ref_adres'];
$ref_postcode = $_POST['ref_postcode'];
$ref_plaats = $_POST['ref_plaats'];
$ref_land = $_POST['ref_land'];
$ref_beschrijving = $_POST['ref_beschrijving'];
$ref_publiceren = $_POST['ref_publiceren'];
$ref_lat = $_POST['ref_lat'];
$ref_lng = $_POST['ref_lng'];
$ref_afbeeldingid = $_POST["ref_afbeeldingid"];

if ($GLOBALS['itemid']>0)
{
	$PgMode = "WIJZIG";
}
else
{
	$PgMode = "TOEVOEG";
}
if ($PgMode=="TOEVOEG") {
	$qry1="INSERT INTO googlemapsreferenties (ref_naam) VALUES(";
	$qry2 = "".SQLStr($ref_naam)."";
	$qry3=")";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$GLOBALS['itemid'] = mysql_insert_id();
}

if ($GLOBALS['itemid']>0)
{
	$qry1="UPDATE googlemapsreferenties SET ";
	$qry2 = "ref_naam=".SQLStr($ref_naam).", ".
		"ref_adres=".SQLStr($ref_adres).", ".
		"ref_postcode=".SQLStr($ref_postcode).", ".
		"ref_plaats=".SQLStr($ref_plaats).", ".
		"ref_land=".SQLStr($ref_land).", ".
		"ref_lat=".SQLStr($ref_lat).", ".
		"ref_lng=".SQLStr($ref_lng).", ".
		"ref_beschrijving=".SQLStr($ref_beschrijving).", ".
		"ref_afbeeldingid=".SQLStr($ref_afbeeldingid).", ".
		"ref_publiceren=".SQLBool($ref_publiceren)."";

	$qry3=" WHERE referentieid=".$GLOBALS['itemid']." LIMIT 1";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
}

if ($PgMode=="TOEVOEG") {
	redirect("cmsgooglemapsreferenties.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);
	//redirect("cmsgooglemapsreferenties-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&itemid=".$GLOBALS['itemid']);
}
else{
	redirect("cmsgooglemapsreferenties.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);
}

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
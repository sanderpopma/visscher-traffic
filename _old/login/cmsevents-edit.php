<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['evid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM evenementen WHERE eventid=".$GLOBALS['evid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['eventid']>0)
    {
        $evvandatum = MaakDatum($row_rs["evvandatum"]);
        $evtotdatum = MaakDatum($row_rs["evtotdatum"]);
        $evpubliceren = Int2Bool($row_rs["evpubliceren"]);
        $evtitel = $row_rs["evtitel"];
        $evlocatie = $row_rs["evlocatie"];
        $eventree = $row_rs["eventree"];
        $evtijd = $row_rs["evtijd"];
        $evinleiding = $row_rs["evinleiding"];
        $evtaalcode = $row_rs['evtaalcode'];
        $evafbeeldingid = $row_rs['evafbeeldingid'];
        $evalleenleden = Int2Bool($row_rs["evalleenleden"]);
    }
    mysql_free_result($rs);
}
else
{
    $evvandatum = MaakDatum(Now);
    $evpubliceren = false;
    $evtitel = "";
    $evinleiding = "";
    $evlocatie = "";
    $eventree = "";
    $evtijd = "";
    $evtaalcode = $GLOBALS['StdTaalcode'];
    $evafbeeldingid = -1;
    $evalleenleden = false;
}
$evafbeeldingid = toInt($evafbeeldingid);
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Evenement " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsevents.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsevents-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&evid=" . $GLOBALS['evid'], "evenementen", 700)?>
    <?=FrmText("Naam evenement", "evtitel", $evtitel, 60, 100) ?>
    <?=FrmTextArea("Korte omschrijving", "evinleiding", $evinleiding, 5, 60) ?>
	<?=FrmSelectByQuery("Taal", "evtaalcode", $evtaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>

    <?=FrmDatum("Op/Van Datum", "evvandatum", $evvandatum) ?>
    <?=FrmDatum("T/m Datum", "evtotdatum", $evtotdatum) ?>
    <?=FrmText("Tijdstip", "evtijd", $evtijd, 60, 100) ?>
    <?=FrmText("Locatie", "evlocatie", $evlocatie, 60, 100) ?>
    <?=FrmText("Entree", "eventree", $eventree, 60, 100) ?>

    <?=FrmCheckbox("Publiceren", "evpubliceren", $evpubliceren) ?>
    <?=FrmCheckbox("Alleen voor leden", "evalleenleden", $evalleenleden) ?>

	<? if (1==2) {
		?>
	<?=FrmKopregel("Kleine afbeelding (evenementenlijst)")?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoklein.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("evafbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("evafbeeldingid", $evafbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $evafbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('evafbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('evafbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>
	<?
	}
	?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("evtitel") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("evenementen");
frmvalidator.addValidation("evtitel","req","Titel is verplicht");
frmvalidator.addValidation("evinleiding","req","Inleiding is verplicht");
frmvalidator.addValidation("evtaalcode","req","Taal is verplicht");
frmvalidator.addValidation("evvandatum","req","Van/Op Datum is verplicht");
frmvalidator.addValidation("evvandatum","date","Van/Op Datum heeft een ongeldige waarde");
frmvalidator.addValidation("evtotdatum","date","T/m Datum heeft een ongeldige waarde");
</script>


<? if ($PgMode=="WIJZIG") {
?>
	<?=FrmKopregel("Dit evenement gaat over de volgende leden:")?>
<tr><td colspan="100"><table>

	<?
	$query_rs = "SELECT * FROM evenementen_leden WHERE el_eventid=" . $GLOBALS['evid'] ." ORDER BY el_lidid;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		?>
		<tr class="regel">
			<td><b>Naam lid</b></td>
			<td><b>Verwijder</b></td>
		</tr>
		<?
		do
		{
			$VerwijderURL = "cmsevents-leden-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&lidid=" . $row_rs['el_lidid']."&evid=".$row_rs['el_eventid'];
			$VerwijderMelding = "Weet u zeker dat u dit lid wilt verwijderen bij dit evenement?"
		?>
		<tr class="regel">
			<td><?=GeefDbWaarde("lid_naam", "leden", "lidid=".$row_rs['el_lidid'])?></td>
			<td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
		</tr>
		<?
		}
	while ($row_rs = mysql_fetch_assoc($rs));
	}
	mysql_free_result($rs);
	?>
</table></td></tr>
<tr><td colspan="100"><table>
	<?=FrmKoptekst("Lid toevoegen aan evenement:")?>
<?=OpenForm("cmsevents-leden-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&evid=" . $GLOBALS['evid'], "lidcat", 700)?>
	<?=FrmSelectByQuery("Lid", "el_lidid", "", "SELECT lidid, lid_naam FROM leden WHERE lidid NOT IN(SELECT el_lidid FROM evenementen_leden WHERE el_eventid=" . $GLOBALS['evid'] .") AND lidpubliceren<>0 ORDER BY lid_nummerkaart", "lidid", "lid_naam", true)?>
    <?=FrmSubmit("Voeg toe")?>
<?=SluitForm()?>
</table></td></tr>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("lidcat");
frmvalidator.addValidation("el_lidid","req","Lid is verplicht");
</script>
<?
}
?>

<?=SluitCMSTabel(); ?>

<br /><br />

<script language="javascript">
function openSorter(page_type, sub_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&subtype=' + sub_type + '&paginaid=' + page_id , 'sorteren', 'width=676, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>
<?
if ($PgMode=="WIJZIG"){
    ToonParagrafenLijst("EVENT", $GLOBALS['evid']);
}
?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
?>
<?php header("Content-type: text/css"); ?>
*, html
{
    font-family: Verdana;
    font-size: 12px;
    color: Black;
}
body, #body
{
    margin: 0px;
    background-color: #ffffff;
    background: none;
}

body.CMSsubmenu
{
    background-color: <?=$GLOBALS['KleurCMSSubmenu']?>;
}
body.CMShoofdmenu
{
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalkover.gif');
	background-repeat: repeat-x;
}
b
{
	color: black;
}
a
{
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
    font-weight: bold;
    text-decoration: underline;
}
a:hover
{
    color: <?=$GLOBALS['KleurSybitLichtBlauw']?>;
    font-weight: bold;
    text-decoration: underline;
}
a.icoon
{
    color: black;
    font-weight: normal;
    font-size: 10px;
    text-decoration: none;
}
a.navpad
{
    font-size: 9px;
    text-decoration: none;
    font-weight: normal;
}
a.navpad:hover
{
    text-decoration: underline;
}
a.cmssm
{
    font-size: 9px;
    font-weight: normal;
    text-decoration: none;
    color: #222222;
}
a.cmssm:hover
{
    text-decoration: none;
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
}
.paging
{
	font-size: 10px;
	color: #000000;
	font-weight: normal;
}
a.paging:hover
{
	font-size: 10px;
	color: #000000;
	font-weight: normal;
}
b.paging
{
	font-weight: bold;
}
.cmsdivhm
{
    border-bottom: solid black 1px;
    height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    width: <?=$GLOBALS['BreedteCMSSubmenu']?>px;
    position: absolute;
    left: 0;
    top: 0;
    text-align: center;
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalk.gif');
    color: black;
    font-size: 10px;
    font-weight: bold;
    line-height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    float: left;
}
.cmsdivhmover
{
    border-bottom: solid black 1px;
    height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    width: <?=$GLOBALS['BreedteCMSSubmenu']?>px;
    position: absolute;
    left: 0;
    top: 0;
    text-align: center;
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalkover.gif');
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
    font-size: 10px;
    font-weight: bold;
    line-height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    float: left;
}
.cmsdivhmsel
{
    border-bottom: solid black 1px;
    height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    width: <?=$GLOBALS['BreedteCMSSubmenu']?>px;
    position: absolute;
    left: 0;
    top: 0;
    text-align: center;
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalksel.gif');
    color: black;
    font-size: 10px;
    font-weight: bold;
    line-height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    float: left;
}
.cmsdivhmselover
{
    border-bottom: solid black 1px;
    height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    width: <?=$GLOBALS['BreedteCMSSubmenu']?>px;
    position: absolute;
    left: 0;
    top: 0;
    text-align: center;
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalkselover.gif');
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
    font-size: 10px;
    font-weight: bold;
    line-height: <?=$GLOBALS['HoogteCMSHMKnoppen']?>px;
    float: left;
}
td.cmsknopsm
{
    padding-left: 0px;
    border: solid <?=$GLOBALS['KleurCMSSubmenu']?> 1px;
    font-size: 9px;
}
td.cmsknopsmover
{
    padding-left: 0px;
    border: solid <?=$GLOBALS['KleurCMSSubmenuBorder']?> 1px;
    background-color: <?=$GLOBALS['KleurCMSSubmenuOver']?>;
    font-size: 9px;
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
}
td.cmsknopsmsel
{
    padding-left: 0px;
    border: solid <?=$GLOBALS['KleurCMSSubmenuBorder']?> 1px;
    background-color: <?=$GLOBALS['KleurCMSSubmenuSel']?>;
    font-size: 9px;
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
}
td.kaderkop
{
    background-image: url('<?=$GLOBALS['CMSImgRoot']?>/cmshmbalkover.gif');
    color: <?=$GLOBALS['KleurSybitBlauw']?>;
    font-size: 9px;
    font-weight: bold;
    text-decoration: underline;
}
tr.kadernav
{
	height: 24px;
	background-color: <?=$GLOBALS['KleurNavBalk']?>;
}
tr.kadertitel
{
	height: 24px;
	background-color: <?=$GLOBALS['KleurTitelBalk']?>;
}
tr.regel
{
	height: 20px;
}
tr.regel td
{
	padding: 2px;
}
tr.regelinv
{
	height: 20px;
	background-color: <?=$GLOBALS['KleurRegelInv']?>;
}
tr.kleurregel
{
	height: 24px;
	background-color: <?=$GLOBALS['KleurSybitBlauw']?>;
}
tr.kleurregel td
{
	padding: 2px;
	color: <?=$KleurWit?>;
}
tr.kadervoet
{
	height: 24px;
	background-color: <?=$GLOBALS['KleurOnderBalk']?>;
}
td.navknop
{
	border: solid 1px <?=$GLOBALS['KleurNavBalk']?>;
	font-size: 10px;
}
td.navknopover
{
	border: solid 1px black;
	background: <?=$GLOBALS['KleurNavKnopOver']?>;
	font-size: 10px;
}
.absmiddle
{
  vertical-align: middle;
}
.border
{
    border: solid black 1px;
}
.Veld
{
	border: solid 1px <?=$GLOBALS['KleurSybitBlauw']?>;
}
.Knop
{
	background-color: <?=$GLOBALS['KleurKnoppen']?>;
	color: <?=$GLOBALS['TekstKleurKnoppen']?>;
	font-weight: bold;
}
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
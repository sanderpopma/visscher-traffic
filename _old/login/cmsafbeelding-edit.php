<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$afbid = toInt($_GET["afbid"]);
if ($afbid>0)
{
	$PgMode = "WIJZIG";
	$FrmTitel = "Afbeelding wijzigen";
}
else
{
	$PgMode = "TOEVOEG";
	$FrmTitel = "Afbeelding toevoegen";
}
?>
<?=OpenPagina("CMS", "")?>

<form method="post" action="cmsafbeelding-edit2.php?OpenerFieldToUpdate=<?=$OpenerFieldToUpdate?>&afbid=<?=$afbid?>" name="afbeelding" enctype="multipart/form-data">
<?=OpenCMSTabel($FrmTitel)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close();") ?>
<?=SluitCMSNavBalk()?>

<?=FrmHidden("MAX_FILE_SIZE", $GLOBALS['MaxBestandsGrootte'])?>

<tr>
	<td>Kies afbeelding</td>
	<td>:</td>
	<td><input class="Veld" type="file" name="afb_url" size="40"></td>
</tr>
<tr>
	<td colspan="2"></td><td>Let op: alleen JPG of GIF bestanden kunnen worden gebruikt</td>
</tr>

    <?=FrmSubmit("Opslaan")?>

<?=SluitCMSTabel()?>
</form>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("afbeelding");
frmvalidator.addValidation("afb_url","req","Kies een bestand");
</script>

<script language="javascript">
window.focus();
</script>
<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
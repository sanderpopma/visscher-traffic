<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['banid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM banners WHERE bannerid=".$GLOBALS['banid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['bannerid']>0)
    {
        $bannernaam = $row_rs['bannernaam'];
        $bannerlinkprod_nl = $row_rs['bannerlinkprod_nl'];
        $bannerlinkinternid_nl = toInt($row_rs['bannerlinkinternid_nl']);
        $bannerlinkextern_nl = $row_rs['bannerlinkextern_nl'];
        $banneractief_nl = Int2Bool($row_rs['banneractief_nl']);
        $bannerafbeeldingid = toInt($row_rs['bannerafbeeldingid']);
        $bannertaalcode = $row_rs['bannertaalcode'];
    }
    mysql_free_result($rs);
}
else
{
	$bannerid = 0;
    $bannernaam = "";
    $bannerlinkprod_nl = "";
    $bannerlinkinternid_nl = 0;
    $bannerlinkextern_nl = "";
    $banneractief_nl = false;
    $bannerafbeeldingid = -1;
    $bannertaalcode = $GLOBALS['StdTaalcode'];
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Banner " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsbanners.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsbanners-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] .  "&banid=".$GLOBALS['banid'], "banner", 700)?>
    <?=FrmText("Naam banner", "bannernaam", $bannernaam, 60, 100) ?>
	<?=FrmSelectByQuery("Taal", "bannertaalcode", $bannertaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>

    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotogeen.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function toonFlash()
	{
		var flashloc = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
		//alert(document.getElementById("flashcont").value);
		//alert(document.getElementById("flashsrc").src);
        //document.getElementById("flashcont").value = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
		//alert(document.getElementById("flashsrc").src);
        //document.getElementById("flashsrc").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        //document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
        // GEBRUIK DIV met INNERHTML!
        //document.getElementById("vrbbanner").innerHTML = "<object width='180' height='480'><param name='flashcont' id='flashcont' value='<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"'><embed id='flashsrc' name='flashsrc' src='somefilename.swf' width='180' height='480'>				</embed>				</object>';
        document.getElementById("vrbbanner").innerHTML = "<object width='180' height='480'><param name='flashcont' id='flashcont' value='"+flashloc+"'><embed id='flashsrc' name='flashsrc' src='"+flashloc+"' width='180' height='480'></embed></object>";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("bannerafbeeldingid").value = "-1";
			document.getElementById("vrbbanner").innerHTML = "";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("bannerafbeeldingid", $bannerafbeeldingid)?>
    <tr class='regel' valign='top'>
        <td>Afbeelding</td>
        <td>:</td>
        <td>
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $bannerafbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	$AfbExt = $row_rsafb["afbeeldingextentie"];
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('bannerafbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
				<div id="vrbbanner">
				</div>

	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('bannerafbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
	        	<? if ($AfbExt=="swf") {
					?>
                	toonFlash();
					<?
	        	}
	        	else {
	        	?>
                toonFoto();
                <?
                }
                ?>
            </script>
        <? } ?>
        </td>

    </tr>
	<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding wordt niet automatisch verkleind bij toevoegen. Zorg dat deze vooraf maximaal 180px breed is.");?>
	<?=FrmKopregel("Instellingen") ?>
    <?=FrmCheckbox("Actief / activeren", "banneractief_nl", $banneractief_nl) ?>
   	<?=FrmKopregel("Link binnen de website") ?>
    <script language="javascript">
    function kiesILink(fld_toupdate)
    {
        result = window.open('kies_ilink.php?CurVal=' + document.getElementById("bannerlinkinternid_nl").value + '&GekozenPaginaTaal=' + document.getElementById("bannertaalcode").value + '&OpenerFieldToUpdate=' + fld_toupdate, 'kiesilink', 'width=500, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    </script>
    <?
    $PA_ILinkTekst = "";
    if ($bannerlinkinternid_nl>0) {
	    $query_rsi = "SELECT * FROM paginas WHERE paginaid=". $bannerlinkinternid_nl .";";
	    $rsi = mysql_query($query_rsi, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsi = mysql_fetch_assoc($rsi);
	    if ($row_rsi['paginaid']>0)
	    {
    		$PA_ILinkTekst = $row_rsi['paginatitel'];
    	}
	    mysql_free_result($rsi);
    }
    ?>
    <?=FrmHidden("bannerlinkinternid_nl", $bannerlinkinternid_nl)?>
    <?=FrmHidden("pa_ilinktekst", $PA_ILinkTekst)?>
    <tr class='regel' valign='top'>
        <td>Link naar pagina</td>
        <td>:</td>
        <td>
        <span id="ilinktekst"><?=$PA_ILinkTekst?></span><br />
        <a onclick="kiesILink('bannerlinkinternid_nl'); return false;" href="">Klik hier om de interne link te kiezen of te wijzigen</a></td>
    </tr>
	<?=FrmSelectByQuery("Link naar productgroep", "bannerlinkprod_nl", $bannerlinkprod_nl, "SELECT prodgrpid, prodgrp_naam_nl FROM productgroepen", "prodgrpid", "prodgrp_naam_nl", true)?>
   	<?=FrmKopregel("Link naar een andere website") ?>
	<?=FrmText("Externe link/URL", "bannerlinkextern_nl", $bannerlinkextern_nl, 80, 255)?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("bannernaam") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("banner");
frmvalidator.addValidation("bannernaam","req","Naam is verplicht");
frmvalidator.addValidation("bannerafbeeldingid","gt=0","Afbeelding is verplicht");
</script>
<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
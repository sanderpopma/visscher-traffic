<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
//Inlezen en verwerken paginaparameters
$HoofdURL = "hoofd";

if ($GLOBALS['smid']>0)
{
	if ($GLOBALS['hmid']==GeefEersteAdmHoofdmenuID())
	{
		$HoofdURL = "cmspaginas";
	}
	else
	{
    	$HoofdURL = GeefDBWaarde("admpaginaurl", "adminmenu", "admmenuid=".$GLOBALS['smid']." AND admpubliceren<>0");
    }
}
else
{
    if ($GLOBALS['hmid']<1)
	{
        $HoofdURL = "hoofd";
	}
    else
	{
        $HoofdURL = GeefDBWaarde("admpaginaurl", "adminmenu", "admmenuid=".$GLOBALS['hmid']." AND admpubliceren<>0");
	}
}
// Tijdelijke code (i.v.m. eventuele hoofd.php)
if ($GLOBALS['hmid']<1)
{
    $GLOBALS['hmid'] = GeefEersteAdmHoofdmenuID();
    $HoofdURL = GeefDBWaarde("admpaginaurl", "adminmenu", "admmenuid=".$GLOBALS['hmid']." AND admpubliceren<>0");
}
if ($HoofdURL=="") {
	$HoofdURL = "cmspaginas";
}
?>
<?
$MetCMSFrames = true;
OpenPagina("CMS", "");
?>
<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("HOME-INSTELLINGEN")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe regel", "cmshome-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Taal</b></td>
    <td><b>Soort</b></td>
    <td><b>Volgnummer</b></td>
    <td><b>Titel</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM homeinstellingen ORDER BY homeitemtaalcode, homeitemsoort, homeitemvolgnr;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmshome-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&homeitemid=".$row_rs['homeitemid'];
	    $VerwijderURL = "cmshome-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&homeitemid=".$row_rs['homeitemid'];
	    $VerwijderMelding = "Weet u zeker dat u deze regel wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel" valign="top">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Regel wijzigen")?></td>
			    <td><?=Icoon($row_rs["homeitemtaalcode"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["homeitemsoort"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["homeitemvolgnr"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["homeitem_titel"]?></a></td>
			    <td><center><?=ToonGereed($row_rs["regelpubliceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Marktsegmenten")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw marktsegment", "cmsmarktsegmenten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="100"><b>Naam in menu</b></td>
    <td><b>Naam marktsegment</b></td>
    <!--
    <td width="100"><b>Producten</b></td>
    -->
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="70"><center><b>Volgorde</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM marktsegmenten ORDER BY marktsegment_volgorde, marktsegment_naam_nl;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
    	$ProdTxt = "afbeeldingen";
	    $WijzigURL = "cmsmarktsegmenten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&msmid=" .$row_rs["marktsegmentid"];
	    $VerwijderURL = "cmsmarktsegmenten-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&msmid=" .$row_rs["marktsegmentid"];
	    $VerwijderMelding = "Weet u zeker dat u dit marktsegment wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantProducten = 0;
	    //$AantProducten = toInt(TelRecords("SELECT * FROM producten WHERE pr_marktsegmentid=" . $row_rs['marktsegmentid'].""));
	    //$ProdURL = "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&msmid=" .$row_rs["marktsegmentid"];
	    if ($AantProducten<1) {
	    	$MagVerwijderen = true;
	    	$AantProducten = 0;
	    }
	    if ($AantProducten==1) {
	    	$ProdTxt = "afbeelding";
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Marktsegment wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["marktsegment_menunaam_nl"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["marktsegment_naam_nl"]?></a></td>
			    <!--
			    <td><a href="<?=$ProdURL?>"><?=$AantProducten?>&nbsp;<?=$ProdTxt?></a></td>
			    -->
			    <td><center><?=ToonGereed($row_rs["marktsegment_publiceren"])?></td>
			    <td><center><?=$row_rs["marktsegment_volgorde"]?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
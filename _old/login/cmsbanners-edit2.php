<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
// if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
$bannernaam = $_POST['bannernaam'];
$bannerlinkinternid_nl = toInt($_POST['bannerlinkinternid_nl']);
$bannerlinkprod_nl = $_POST['bannerlinkprod_nl'];
$bannerlinkextern_nl = $_POST['bannerlinkextern_nl'];
if ($bannerlinkextern_nl!="") {
	$bannerlinkprod_nl="";
	$bannerlinkinternid_nl="";
}
if ($bannerlinkprod_nl!="") {
	$bannerlinkinternid_nl="";
}
$banneractief_nl = $_POST['banneractief_nl'];
$bannerafbeeldingid = toInt($_POST['bannerafbeeldingid']);
$bannertaalcode = $_POST['bannertaalcode'];
$PgMode = "TOEVOEG";
$qry1 = "INSERT INTO banners (bannernaam, bannertaalcode, ";
$qry1 .= "bannerlinkprod_nl, bannerlinkinternid_nl, bannerlinkextern_nl, ";
$qry1 .= "banneractief_nl, ";
$qry1 .= "bannerafbeeldingid) VALUES( ";
$qry2 = "".SQLStr($bannernaam).", ".
	"".SQLDec($bannertaalcode).", ".
	"".SQLDec($bannerlinkprod_nl).", ".
	"".SQLDec($bannerlinkinternid_nl).", ".
	"".SQLStr($bannerlinkextern_nl).", ".
	"".SQLBool($banneractief_nl).", ".
	"".SQLDec($bannerafbeeldingid)."";
$qry3=")";

if ($GLOBALS['banid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE banners SET ";
	$qry2 = "bannernaam=".SQLStr($bannernaam).", ".
		"bannertaalcode=".SQLDec($bannertaalcode).", ".
		"bannerlinkprod_nl=".SQLDec($bannerlinkprod_nl).", ".
		"bannerlinkinternid_nl=".SQLDec($bannerlinkinternid_nl).", ".
		"bannerlinkextern_nl=".SQLStr($bannerlinkextern_nl).", ".
		"banneractief_nl=".SQLBool($banneractief_nl).", ".
		"bannerafbeeldingid=".SQLDec($bannerafbeeldingid)."";
	$qry3=" WHERE bannerid=".$GLOBALS['banid'];
}

$query_rs = $qry1.$qry2.$qry3;
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

// eventueel banner activeren voor alle talen
if ($PgMode == "TOEVOEG")
{
	$GLOBALS['banid'] = mysql_insert_id();
}
if ($banneractief_nl)
{
	$query_rs = "UPDATE banners SET banneractief_nl=0 WHERE banneractief_nl<>0 AND bannerid<>".$GLOBALS['banid']." AND bannertaalcode='" . $bannertaalcode."';";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
}

redirect("cmsbanners.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
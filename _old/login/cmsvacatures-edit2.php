<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
$vacdatumgeplaatst = $_POST["vacdatumgeplaatst"];
$vactitel = $_POST["vactitel"];
$vacinleiding = $_POST["vacinleiding"];
$vacpubliceren = $_POST["vacpubliceren"];
$vactaalcode = strtoupper($_POST["vactaalcode"]);
$vacaantaluren = $_POST["vacaantaluren"];
$vacstandplaats = $_POST["vacstandplaats"];
$vacprovincie = $_POST["vacprovincie"];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO vacatures (vactitel, vacinleiding, vacdatumgeplaatst, vactaalcode, vacaantaluren, vacstandplaats, vacprovincie, vacpubliceren) VALUES( ";
$qry2 = "".SQLStr($vactitel).", ".
	"".SQLStr($vacinleiding).", ".
	"".SQLDat($vacdatumgeplaatst).", ".
	"".SQLStr($vactaalcode).", ".
	"".SQLStr($vacaantaluren).", ".
	"".SQLStr($vacstandplaats).", ".
	"".SQLStr($vacprovincie).", ".
	"".SQLBool($vacpubliceren)."";
$qry3=")";

if ($GLOBALS['vacid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE vacatures SET ";
	$qry2 = "vactitel=".SQLStr($vactitel).", ".
		"vacinleiding=".SQLStr($vacinleiding).", ".
		"vacdatumgeplaatst=".SQLDat($vacdatumgeplaatst).", ".
		"vactaalcode=".SQLStr($vactaalcode).", ".
		"vacaantaluren=".SQLStr($vacaantaluren).", ".
		"vacstandplaats=".SQLStr($vacstandplaats).", ".
		"vacprovincie=".SQLStr($vacprovincie).", ".
		"vacpubliceren=".SQLBool($vacpubliceren)."";

	$qry3=" WHERE vacatureid=".$GLOBALS['vacid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

if ($PgMode=="TOEVOEG")
{
    $query_rs = "SELECT * FROM vacatures ORDER BY vacatureid DESC";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['vacatureid']>0)
    {
        $GLOBALS['vacid'] = toInt($row_rs["vacatureid"]);
    }
    mysql_free_result($rs);
}
redirect("cmsvacatures-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&vacid=".$GLOBALS['vacid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
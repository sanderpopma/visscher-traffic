<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM winkel_emailberichten WHERE emailid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['emailid']>0)
    {
        $em_zoekcode = $row_rs['em_zoekcode'];
        $em_omschrijving = $row_rs['em_omschrijving'];
        $em_soortaanhef = $row_rs['em_soortaanhef'];
        $em_titel = $row_rs['em_titel'];
        $em_bericht = $row_rs['em_bericht'];
        //$em_metlogingegevens = Int2Bool($row_rs['em_metlogingegevens']);
        $em_metlinkonderbericht = Int2Bool($row_rs['em_metlinkonderbericht']);
     }
    mysql_free_result($rs);
}
else
{
        $em_metlinkonderbericht = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Bericht " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsemails.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsemails-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid'], "gebruiker", 700)?>
    <?=FrmText("Zoekcode", "em_zoekcode", $em_zoekcode, 30, 50) ?>
    <?=FrmText("Omschrijving", "em_omschrijving", $em_omschrijving, 60, 250) ?>

	<?=FrmKopregel("Inhoud e-mailbericht")?>
    <?=FrmText("Titel bericht", "em_titel", $em_titel, 60, 100) ?>
    <?=FrmText("Aanhef", "em_soortaanhef", $em_soortaanhef, 60, 50) ?>
    <?=FrmTextArea("E-mailbericht", "em_bericht", $em_bericht, 12, 60) ?>
    <?=FrmCheckbox("Met link onderaan bericht", "em_metlinkonderbericht", $em_metlinkonderbericht) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("em_zoekcode") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("gebruiker");
frmvalidator.addValidation("em_zoekcode","req","Zoekcode is verplicht");
frmvalidator.addValidation("em_omschrijving","req","Omschrijving is verplicht");
frmvalidator.addValidation("em_soortaanhef","req","Soort aanhef is verplicht");
frmvalidator.addValidation("em_titel","req","Titel is verplicht");
frmvalidator.addValidation("em_bericht","req","Bericht is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Evenementen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw evenement", "cmsevents-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="50" align="center"><b>Taal</b></td>
    <td width="100"><b>Op/Van</b></td>
    <td width="100"><b>T/m</b></td>
    <td><b>Titel</b></td>
    <td width="100" align="center"><b>Gelezen</b></td>
    <td width="100" align="center"><b>AlleenLeden</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM evenementen ORDER BY evpubliceren, evvandatum DESC, evtotdatum DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsevents-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&evid=" .$row_rs["eventid"];
	    $VerwijderURL = "cmsevents-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&evid=" .$row_rs["eventid"];
	    $VerwijderMelding = "Weet u zeker dat u dit evenement, inclusief de bijbehorende alinea's, wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Evenement wijzigen")?></td>
			    <td align="center"><?=Icoon($row_rs["evtaalcode"])?></td>
			    <td><?=MaakDatum($row_rs["evvandatum"])?></td>
			    <td><?=MaakDatum($row_rs["evtotdatum"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["evtitel"]?></a></td>
			    <td align="center"><?=$row_rs["evgelezen"]?></td>
			    <td align="center"><?=ToonGereed($row_rs["evalleenleden"])?></td>
			    <td align="center"><?=ToonGereed($row_rs["evpubliceren"])?></td>
			    <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters

if ($GLOBALS['homeitemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM homeinstellingen WHERE homeitemid=".$GLOBALS['homeitemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['homeitemid']>0)
    {
    	$homeitemtaalcode = $row_rs['homeitemtaalcode'];
    	$homeitemsoort = $row_rs['homeitemsoort'];
        $homeitemvolgnr = $row_rs['homeitemvolgnr'];
        $homeitem_titel = $row_rs['homeitem_titel'];
        $homeitem_txt = $row_rs['homeitem_txt'];
        $homeitem_pagid = toInt($row_rs['homeitem_pagid']);
        $homeitem_prodgrpid = toInt($row_rs['homeitem_prodgrpid']);
        $regelpubliceren = Int2Bool($row_rs['regelpubliceren']);
    }
    mysql_free_result($rs);
}
else
{
    	$homeitemsoort = "home";
        $homeitemvolgnr = 1;
        $homeitem_afbid = -1;
        $regelpubliceren = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Home-instelling " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmshome.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmshome-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&homeitemid=".$GLOBALS['homeitemid'], "homeitem", 700)?>
	<?=FrmSelectByArray("Soort", "homeitemsoort", $homeitemsoort, "home,onder", false)?>
	<?=FrmSelectByQuery("Taal", "homeitemtaalcode", $homeitemtaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0", "taalcode", "taalnaam", true)?>
    <?=FrmText("VolgNr", "homeitemvolgnr", $homeitemvolgnr, 4, 4) ?>
	<?=FrmSelectByQuery("Linkt naar pagina", "homeitem_pagid", $homeitem_pagid, "SELECT paginaid, CONCAT(paginatitel,' (',paginamenunaam,')') FROM paginas", "paginaid", "CONCAT(paginatitel,' (',paginamenunaam,')')", true)?>
	<?=FrmSelectByQuery("Linkt naar productgroep", "homeitem_prodgrpid", $homeitem_prodgrpid, "SELECT prodgrpid, CONCAT(prodgrpid,' (',prodgrp_naam_nl,')') FROM productgroepen", "prodgrpid", "CONCAT(prodgrpid,' (',prodgrp_naam_nl,')')", true)?>
    <?=FrmText("Titel / zoals afgebeeld", "homeitem_titel", $homeitem_titel, 80, 100) ?>
    <?=FrmText("Tekst / zoals afgebeeld", "homeitem_txt", $homeitem_txt, 80, 100) ?>
    <?=FrmCheckbox("Publiceren", "regelpubliceren", $regelpubliceren) ?>
    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("homeitemsoort") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("homeitem");
frmvalidator.addValidation("homeitemsoort","req","Soort is verplicht");
frmvalidator.addValidation("homeitem_titel","req","Titel is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
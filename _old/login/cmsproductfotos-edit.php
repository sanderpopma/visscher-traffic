<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['prodfotoid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM productfotos WHERE prodfotoid=".$GLOBALS['prodfotoid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['prodfotoid']>0)
    {
        $foto_afbeeldingid = toInt($row_rs['foto_afbeeldingid']);
        $foto_productid = toInt($row_rs['foto_productid']);
        $foto_marktsegmentid = toInt($row_rs['foto_marktsegmentid']);
        $foto_publiceren = Int2Bool($row_rs['foto_publiceren']);
    }
    mysql_free_result($rs);
}
else
{
    $foto_afbeeldingid = -1;
    $foto_productid = $GLOBALS['prodid'];
    $foto_marktsegmentid = -1;
    $foto_publiceren = true;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Foto " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsproductfotos.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=" . $GLOBALS['prodgrpid']. "&prodid=" . $GLOBALS['prodid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsproductfotos-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" . $GLOBALS['prodgrpid']. "&prodid=" . $GLOBALS['prodid'].  "&prodfotoid=".$GLOBALS['prodfotoid'], "foto", 700)?>
	<?=FrmTekstRegel("Hoort bij Product", GeefDBWaarde("productnaam_nl", "producten", "productid=".$GLOBALS['prodid']."")); ?>
	<?=FrmHidden("foto_productid", $foto_productid); ?>
	<?=FrmSelectByQuery("Hoort bij Martksegment", "foto_marktsegmentid", $foto_marktsegmentid, "SELECT marktsegmentid, marktsegment_naam_nl FROM marktsegmenten ORDER BY marktsegment_volgorde, marktsegment_naam_nl", "marktsegmentid", "marktsegment_naam_nl", true)?>
    <?=FrmCheckbox("Zichtbaar", "foto_publiceren", $foto_publiceren) ?>

    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoart.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("foto_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("foto_afbeeldingid", $foto_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td>Afbeelding</td>
        <td>:</td>
        <td>
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $foto_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('foto_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('foto_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>
    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("foto");
frmvalidator.addValidation("foto_afbeeldingid","req","Afbeelding is verplicht");
frmvalidator.addValidation("foto_afbeeldingid","gt=0","Afbeelding is verplicht");
</script>
<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
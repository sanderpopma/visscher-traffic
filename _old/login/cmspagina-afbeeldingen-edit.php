<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PAGINAS") then Response.End
if ($GLOBALS['paaid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM pagina_afbeeldingen WHERE paaid=".$GLOBALS['paaid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['paaid']>0)
    {
        $paa_paginaid = $row_rs['paa_paginaid'];
        $paa_afbeeldingid = $row_rs['paa_afbeeldingid'];
        $paa_volgorde = $row_rs['paa_volgorde'];
        $paa_publiceren = Int2Bool($row_rs['paa_publiceren']);
    }
    mysql_free_result($rs);
}
else
{
	$query_rs = "SELECT MAX(paa_volgorde) as maxnr FROM pagina_afbeeldingen WHERE paa_paginaid=".$GLOBALS['pagid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $paa_volgorde = toInt($row_rs['maxnr']);
        if ($paa_volgorde<0) { $paa_volgorde=0;}
	}
	mysql_free_result($rs);
    $paa_volgorde=$paa_volgorde+1;
    $paa_paginaid = $GLOBALS['pagid'];
    $paa_publiceren = true;
    $paa_afbeeldingid = -1;
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Afbeelding " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmspagina-afbeeldingen.php?hmid=" . $GLOBALS['hmid'] . "&smid=".$GLOBALS['smid']."&pagid=".$GLOBALS['pagid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmspagina-afbeeldingen-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&paaid=" . $GLOBALS['paaid'], "pagina", 700)?>
    <?=FrmTekstregel("Hoort bij pagina", GeefPaginaTitel($paa_paginaid)) ?>
    <?=FrmText("Afbeeldvolgorde", "paa_volgorde", $paa_volgorde, 4, 4) ?>
    <?=FrmCheckbox("Zichtbaar", "paa_publiceren", $paa_publiceren) ?>
    <?=FrmKopregel("Afbeelding rechterkant") ?>
	<?=FrmHidden("paa_paginaid", $paa_paginaid)?>

    <script type="text/javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoart.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("afbklein").style.width = "<?=$GLOBALS['AfbeeldingMaxArtikel']?>px";
    }
    </script>
	<?=FrmHidden("paa_afbeeldingid", $paa_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
$query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $paa_afbeeldingid .";";
$rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
$row_rsafb = mysql_fetch_assoc($rsafb);
if ($row_rsafb['afbeeldingid']>0)
{
$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('paa_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
}
else
{
?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('paa_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
}
mysql_free_result($rsafb);
?>
            <br /><br />
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script type="text/javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>

    <?=FrmSubmit("Opslaan")?>


<?=SluitForm()?>

<?=ZetFocus("paa_volgorde") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("pagina");
frmvalidator.addValidation("paa_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("paa_afbeeldingid","req","Afbeelding is verplicht");
frmvalidator.addValidation("paa_afbeeldingid","gt=0","Afbeelding is verplicht");

</script>

<?=SluitCMSTabel(); ?>
<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
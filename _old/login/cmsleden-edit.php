<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "LEDEN") then Response.End
if ($GLOBALS['lidid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM inschrijvingen WHERE inschrijvingid=".$GLOBALS['lidid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['inschrijvingid']>0)
    {
        $nickname = $row_rs['nickname'];
        $datumingeschreven = MaakDatum($row_rs['datumingeschreven']);
        $voorletters = $row_rs['voorletters'];
        $voornaam = $row_rs['voornaam'];
        $tussenvoegsels = $row_rs['tussenvoegsels'];
        $achternaam = $row_rs['achternaam'];
        $geslacht = $row_rs['geslacht'];
        $geboortedatum = MaakDatum($row_rs['geboortedatum']);
        $burgerlijke_staat = $row_rs['burgerlijke_staat'];
		$landvanherkomst = $row_rs['landvanherkomst'];

        $woonplaats_website = $row_rs['woonplaats_website'];
        $provincie = $row_rs['provincie'];
        $land = $row_rs['land'];

        $adres = $row_rs['adres'];
        $postcode = $row_rs['postcode'];
        $woonplaats = $row_rs['woonplaats'];
		$telefoonnummer = $row_rs['telefoonnummer'];

        $emailadres = $row_rs['emailadres'];
        $wachtwoord = $row_rs['wachtwoord'];

        $geloof_praktiserend = $row_rs['geloof_praktiserend'];
        $geloof_richting = $row_rs['geloof_richting'];
        $geloof_beleving = $row_rs['geloof_beleving'];

		$gewoonte_roken = $row_rs['gewoonte_roken'];
		$gewoonte_alcohol = $row_rs['gewoonte_alcohol'];

        $isgoedgekeurd = Int2Bool($row_rs['isgoedgekeurd']);
        $isafgekeurd = Int2Bool($row_rs['isafgekeurd']);
        $isactief = Int2Bool($row_rs['isactief']);
        $isalingeschreven = Int2Bool($row_rs['isalingeschreven']);
        $datumtoegangtot = MaakDatum($row_rs['datumtoegangtot']);
        $aantallogins = $row_rs['aantallogins'];
        $datumlaatstelogin = MaakDatum($row_rs['datumlaatstelogin']);

        $uiterlijk_lengte = $row_rs['uiterlijk_lengte'];
        $uiterlijk_postuur = $row_rs['uiterlijk_postuur'];
        $uiterlijk_kleurogen = $row_rs['uiterlijk_kleurogen'];
        $uiterlijk_kleurhaar = $row_rs['uiterlijk_kleurhaar'];
        $uiterlijk_huidskleur = $row_rs['uiterlijk_huidskleur'];
        $uiterlijk_kledingstijl = $row_rs['uiterlijk_kledingstijl'];
        $uiterlijk_overige = $row_rs['uiterlijk_overige'];

        $cv_opleiding = $row_rs['cv_opleiding'];
        $cv_beroep = $row_rs['cv_beroep'];
        $cv_rijbewijs = $row_rs['cv_rijbewijs'];
        $cv_hobbys = $row_rs['cv_hobbys'];
        $cv_sport = $row_rs['cv_sport'];
        $cv_talen = $row_rs['cv_talen'];

    }
    mysql_free_result($rs);
}
else
{
    $land = GeefDbWaarde("landnaam", "_landen", "land_taalcode='" . $GLOBALS['StdTaalcode']."' AND isstandaardland<>0 AND land_publiceren<>0");
    $isgoedgekeurd = true;
    $isactief = false;
    $datumtoegangtot = date('d-m-Y', strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " +1 year"));
	$aantallogins = 0;
    $datumlaatstelogin = MaakDatum(date('d-m-Y'));
    $datumingeschreven = MaakDatum(date('d-m-Y'));
    $isalingeschreven = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?
	GebruikDatumObjecten();
?>
<script type="text/javascript">
function ToonFotos(){
	var ret = window.open('cmsledenfotos.php?lidid=<?=$GLOBALS['lidid']?>', 'profielfotos', 'scrollbars=yes,toolbars=no, width=800, height=450, top=10, left=10');
}
</script>
<?=OpenCMSTabel("Inschrijving " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
	<?
	if ($PgMode=="WIJZIG")
	{
	?>
	    <?=ToonCMSNavKnop("fotos", "Toon Profielfoto's", "javascript:ToonFotos();") ?>
	<?
	}
	?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsleden.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsleden-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&lidid=" . $GLOBALS['lidid'], "leden", 700)?>
	<?=FrmKopregel("Persoonlijke gegevens")?>
	<?=FrmTekstRegel("Ingeschreven", $datumingeschreven)?>
    <?=FrmText("Nickname", "nickname", $nickname, 40, 50) ?>
    <?=FrmText("Voorletters", "voorletters", $voorletters, 20, 50) ?>
    <?=FrmText("Voornaam", "voornaam", $voornaam, 40, 50) ?>
    <?=FrmText("Tussenvoegsels", "tussenvoegsels", $tussenvoegsels, 20, 50) ?>
    <?=FrmText("Achternaam", "achternaam", $achternaam, 80, 100) ?>
	<?=FrmGeslacht("Geslacht", "geslacht", $geslacht) ?>
    <?=FrmSelectByQuery("Burgerlijke staat", "burgerlijke_staat", $burgerlijke_staat, "SELECT bscode, bsnaam FROM _burgerlijkestaat ORDER BY bsvolgorde", "bscode", "bsnaam", true)?>
    <?=FrmDatum("Geb.datum", "geboortedatum", $geboortedatum) ?>
    <?=FrmText("Land van herkomst", "landvanherkomst", $landvanherkomst, 80, 100) ?>

	<?=FrmKopregel("Geloof")?>
	<tr>
		<td>Geloof&nbsp;</td><td>:</td>
		<td><select id="geloof_praktiserend" name="geloof_praktiserend" size="1"><option></option>
			<option value="Praktiserend" <? if ($geloof_praktiserend=="Praktiserend") { echo "selected";}?>>Praktiserend</option>
			<option value="Niet Praktiserend" <? if ($geloof_praktiserend=="Niet Praktiserend") { echo "selected";}?>>Niet Praktiserend</option>
		</select></td>
	</tr>

    <?=FrmText("Richting", "geloof_richting", $geloof_richting, 80, 100) ?>
    <?=FrmTextArea("Beleving", "geloof_beleving", $geloof_beleving, 4, 60) ?>

	<?=FrmKopregel("Contactgegevens - zichtbaar op website")?>
    <?=FrmText("Plaats", "woonplaats_website", $woonplaats_website, 80, 100) ?>
    <?=FrmSelectByQuery("Provincie", "provincie", $provincie, "SELECT provincienaam FROM _provincies WHERE prov_landnaam='" . $land . "' AND prov_publiceren<>0", "provincienaam", "provincienaam", true)?>
    <?=FrmSelectByQuery("Land", "land", $land, "SELECT landnaam FROM _landen WHERE land_publiceren<>0", "landnaam", "landnaam", false)?>

	<?=FrmKopregel("Contactgegevens - voor onze administratie")?>
    <?=FrmText("Adres", "adres", $adres, 80, 100) ?>
    <?=FrmText("Postcode", "postcode", $postcode, 20, 50) ?>
    <?=FrmText("Plaats", "woonplaats", $woonplaats, 80, 100) ?>
    <?=FrmText("Telefoonnummer", "telefoonnummer", $telefoonnummer, 40, 100) ?>

	<?=FrmKopregel("Instellingen website")?>
    <?=FrmText("E-mailadres/Loginnaam", "emailadres", $emailadres, 80, 255) ?>
    <?=FrmText("Wachtwoord", "wachtwoord", $wachtwoord, 40, 100) ?>

	<?=FrmKopregel("Gewoonten")?>
    <?=FrmSelectByQuery("Roken", "gewoonte_roken", $gewoonte_roken, "SELECT freqcode, freqnaam FROM _frequenties ORDER BY freqvolgorde", "freqcode", "freqnaam", true)?>
    <?=FrmSelectByQuery("Drinken", "gewoonte_alcohol", $gewoonte_alcohol, "SELECT freqcode, freqnaam FROM _frequenties ORDER BY freqvolgorde", "freqcode", "freqnaam", true)?>

	<?=FrmKopregel("Uiterlijk")?>
    <?=FrmText("Lengte", "uiterlijk_lengte", $uiterlijk_lengte, 40, 50) ?>
    <?=FrmText("Postuur", "uiterlijk_postuur", $uiterlijk_postuur, 40, 50) ?>
    <?=FrmText("Kleur ogen", "uiterlijk_kleurogen", $uiterlijk_kleurogen, 40, 50) ?>
    <?=FrmText("Kleur haar", "uiterlijk_kleurhaar", $uiterlijk_kleurhaar, 40, 50) ?>
    <?=FrmText("Huidskleur", "uiterlijk_huidskleur", $uiterlijk_huidskleur, 40, 50) ?>
    <?=FrmText("Kledingstijl", "uiterlijk_kledingstijl", $uiterlijk_kledingstijl, 40, 50) ?>
    <?=FrmTextArea("Overige", "uiterlijk_overige", $uiterlijk_overige, 4, 60) ?>

	<?=FrmKopregel("CV")?>
    <?=FrmText("Opleiding", "cv_opleiding", $cv_opleiding, 40, 50) ?>
    <?=FrmText("Beroep", "cv_beroep", $cv_beroep, 40, 50) ?>
    <?=FrmText("Rijbewijs", "cv_rijbewijs", $cv_rijbewijs, 40, 50) ?>
    <?=FrmText("Hobby's", "cv_hobbys", $cv_hobbys, 60, 100) ?>
    <?=FrmText("Sport(en)", "cv_sport", $cv_sport, 60, 100) ?>
    <?=FrmText("Talen", "cv_talen", $cv_talen, 60, 100) ?>

	<?=FrmKopregel("Betaling en administratie")?>
	<?=FrmTekstRegel("Aantal logins", $aantallogins)?>
	<?=FrmTekstRegel("Laatste login", $datumlaatstelogin)?>
	<? if ($PgMode=="WIJZIG") {
		?>
		<tr valign="top"><td>Goedkeuren</td><td>:</td><td>
		<script type="text/javascript">
		function NaarActie(vraag_confirm, naar_url){
			if (confirm(vraag_confirm + "(Let op: in het formulier aangebrachte wijzigingen worden NIET opgeslagen)")) {
				location.href = naar_url + ".php?lidid=<?=$GLOBALS['lidid']?>";
			}
		}
		</script>
		<? if ($isgoedgekeurd==true) {
			?>
			<?=Icoon("gereed")?>Dit profiel is al goedgekeurd (<a href="#" onclick="NaarActie('Goedkeuring opheffen?', 'cmsleden-goedkeuren-opheffen'); return false;">goedkeuring opheffen</a>)
			<?
		}
		else
		{
			if ($isafgekeurd==false) {
			?>
			<?=Icoon("wijzigen")?><a href="#" onclick="NaarActie('Dit profiel GOEDKEUREN en de inschrijver bericht hiervan sturen?', 'cmsleden-goedkeuren'); return false;">Dit profiel goedkeuren (de inschrijver ontvangt hiervan bericht per e-mail)</a><br />
			<?
			}
		}
		?>
		</td></tr>
		<tr valign="top"><td>Afkeuren</td><td>:</td><td>
		<? if ($isafgekeurd==true) {
			?>
			<?=Icoon("stop")?>Dit profiel is afgekeurd (<a href="#" onclick="NaarActie('Afkeuring opheffen?', 'cmsleden-afkeuren-opheffen'); return false;">afkeuring opheffen</a>)
			<?
		}
		else
		{
			if ($isgoedgekeurd==false) { ?>
			<?=Icoon("stop")?><a href="#" onclick="NaarActie('Dit profiel AFKEUREN en de inschrijver bericht hiervan sturen?', 'cmsleden-afkeuren'); return false;">Dit profiel afkeuren (de inschrijver ontvangt hiervan bericht per e-mail)</a><br />
			<? }
		}
		?>
		</td></tr>
		<?
	}
	else
	{
		?>
	    <?=FrmCheckbox("Goedkeuren", "isgoedgekeurd", $isgoedgekeurd) ?>
		<?
	}
	?>
    <?=FrmCheckbox("Is al ingeschreven bij Oulfa", "isalingeschreven", $isalingeschreven) ?>
    <?=FrmCheckbox("Actief / Toegang", "isactief", $isactief) ?>
    <?=FrmDatum("Toegang tot", "datumtoegangtot", $datumtoegangtot) ?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("nickname") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("leden");
frmvalidator.addValidation("nickname","req","Nickname is verplicht");
frmvalidator.addValidation("voorletters","req","Voorletter(s) is/zijn verplicht");
frmvalidator.addValidation("voornaam","req","Voornaam is verplicht");
frmvalidator.addValidation("achternaam","req","Achternaam is verplicht");
frmvalidator.addValidation("burgerlijke_staat","req","Burgerlijke staat is verplicht");
frmvalidator.addValidation("geboortedatum","req","Geboortedatum is verplicht");
frmvalidator.addValidation("geboortedatum","dat","Geboortedatum heeft een ongeldige waarde");
frmvalidator.addValidation("geslacht","req","Geslacht is verplicht");
frmvalidator.addValidation("woonplaats_website","req","Woonplaats is verplicht");
frmvalidator.addValidation("provincie","req","Provincie is verplicht");
frmvalidator.addValidation("land","req","Land is verplicht");
frmvalidator.addValidation("adres","req","Adres is verplicht");
frmvalidator.addValidation("postcode","req","Postcode is verplicht");
frmvalidator.addValidation("woonplaats","req","Woonplaats is verplicht");
frmvalidator.addValidation("telefoonnummer","req","Telefoonnummer is verplicht");
frmvalidator.addValidation("emailadres","req","E-mailadres is verplicht");
frmvalidator.addValidation("wachtwoord","req","Wachtwoord is verplicht");
frmvalidator.addValidation("datumtoegangtot","dat","Toegang tot heeft een ongeldige waarde");
</script>

<? if ($PgMode=="WIJZIG") {
?>

<?
}
?>


<?=SluitCMSTabel(); ?>

<br /><br />
<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "PROJECTEN")) { die; }
if ($GLOBALS['projid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM projecten WHERE projid=".$GLOBALS['projid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['projid']>0)
    {
		$proj_taalcode = $row_rs['proj_taalcode'];
		$proj_categorieid = $row_rs['proj_categorieid'];
        $proj_naam = $row_rs['proj_naam'];
        $proj_subtitel = $row_rs['proj_subtitel'];
        $proj_kortebeschrijving = $row_rs['proj_kortebeschrijving'];
        $proj_publiceren = Int2Bool($row_rs['proj_publiceren']);
    	$proj_datum = MaakDatum($row_rs["proj_datum"]);
    	$proj_prijs = MaakDecimal($row_rs['proj_prijs']);
		$proj_verkocht = Int2Bool($row_rs['proj_verkocht']);
		$proj_datumverkocht = MaakDatum($row_rs["proj_datumverkocht"]);
        $proj_afbeeldingid = $row_rs['proj_afbeeldingid'];
		$proj_opgeleverd = Int2Bool($row_rs['proj_opgeleverd']);
    }
    mysql_free_result($rs);
}
else
{
	$proj_datum = MaakDatum(Now);
    $proj_publiceren = false;
    $proj_categorieid = $GLOBALS['projcatid'];
	$proj_taalcode = $GLOBALS['StdTaalcode'];
	$proj_verkocht = false;
    $proj_afbeeldingid = -1;
    $proj_opgeleverd = false;
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Project " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsprojecten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&projcatid=".$GLOBALS['projcatid']) ?>
<?=SluitCMSNavBalk();?>
<?=OpenForm("cmsprojecten-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" . $GLOBALS['projcatid'] . "&projid=".$GLOBALS['projid'], "project", 700)?>
	<?=FrmSelectByQuery("Taal", "proj_taalcode", $proj_taalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>
    <?=FrmText("Naam project", "proj_naam", $proj_naam, 60, 100) ?>
    <?=FrmText("Subtitel/onderschrift", "proj_subtitel", $proj_subtitel, 60, 100) ?>
	<?=FrmSelectByQuery("Categorie", "proj_categorieid", $proj_categorieid, "SELECT projcatid, projcat_naam FROM project_categorieen ORDER BY projcat_volgorde", "projcatid", "projcat_naam", true)?>
    <?=FrmCheckbox("Publiceren", "proj_publiceren", $proj_publiceren) ?>

	<?=FrmKopregel("Kleine afbeelding (hoofdpagina, projectenlijst)")?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoklein.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("proj_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("proj_afbeeldingid", $proj_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $proj_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('proj_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('proj_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>


	<?=FrmKopregel("Verkoopinformatie") ?>
    <?=FrmDatum("Datum te koop", "proj_datum", $proj_datum) ?>
    <?=FrmText(MaakBedragLabel("Prijs"), "proj_prijs", $proj_prijs, 10, 10) ?>
    <?=FrmCheckbox("Verkocht", "proj_verkocht", $proj_verkocht) ?>
    <?=FrmDatum("Datum verkocht", "proj_datumverkocht", $proj_datumverkocht) ?>
    <?=FrmCheckbox("Opgeleverd", "proj_opgeleverd", $proj_opgeleverd) ?>

    <?=FrmKopregel("Beschrijving (kort)") ?>
    <?=FrmFCKText("Beschrijving", "proj_kortebeschrijving", $proj_kortebeschrijving, 130) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("proj_naam") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("project");
frmvalidator.addValidation("proj_naam","req","Naam is verplicht");
frmvalidator.addValidation("proj_taalcode","req","Taal is verplicht");
frmvalidator.addValidation("proj_categorieid","req","Categorie is verplicht");
frmvalidator.addValidation("proj_categorieid","gt=-1","Categorie is verplicht");
frmvalidator.addValidation("proj_datum","date","Datum heeft een ongeldige waarde");
frmvalidator.addValidation("proj_prijs","dec","Prijs heeft een ongeldige waarde");
frmvalidator.addValidation("proj_datumverkocht","date","Datum verkocht heeft een ongeldige waarde");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<script language="javascript">
function openSorter(page_type, sub_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&subtype=' + sub_type + '&paginaid=' + page_id , 'sorteren', 'width=676, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>
<?
if ($PgMode=="WIJZIG"){
	ToonParagrafenLijst("PROJECT", $GLOBALS['projid']);
}
?>
<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
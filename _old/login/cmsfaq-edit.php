<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "FAQ") then Response.End
if ($GLOBALS['faqid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM faq WHERE faqid=".$GLOBALS['faqid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['faqid']>0)
    {
    	$faqtaalcode = $row_rs['faqtaalcode'];
        $vraag = $row_rs['vraag'];
        $antwoord = $row_rs['antwoord'];
        $volgorde = toInt($row_rs['volgorde']);
        $publiceren = Int2Bool($row_rs['publiceren']);
        $faq_faqcatid = toInt($row_rs['faq_faqcatid']);
    }
    mysql_free_result($rs);
}
else
{
    $query_rs = "SELECT MAX(volgorde) as maxnr FROM faq";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $volgorde = toInt($row_rs['maxnr']);
        if ($volgorde<0) { $volgorde=0;}
	}
	mysql_free_result($rs);
    $volgorde=$volgorde+1;
    $publiceren = false;
    $faqtaalcode = $GLOBALS['StdTaalcode'];
    $faq_faqcatid = 0;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Vraag " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsfaq.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsfaq-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&faqid=" . $GLOBALS['faqid'], "faq", 700)?>
    <?=FrmText("Vraag Nr", "volgorde", $volgorde, 4, 4) ?>
	<?=FrmSelectByQuery("Taal", "faqtaalcode", $faqtaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>
	<?=FrmSelectByQuery("Categorie", "faq_faqcatid", $faq_faqcatid, "SELECT faqcatid, faqcat_naam FROM faq_categorieen WHERE faqcat_publiceren<>0 ORDER BY faqcat_volgorde", "faqcatid", "faqcat_naam", true)?>
    <?=FrmText("Vraag", "vraag", $vraag, 80, 255) ?>
    <?=FrmTextArea("Antwoord", "antwoord", $antwoord, 8, 80) ?>
    <?=FrmCheckbox("Publiceren", "publiceren", $publiceren) ?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("vraag") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("faq");
frmvalidator.addValidation("vraag","req","Vraag is verplicht");
frmvalidator.addValidation("antwoord","req","Antwoord is verplicht");
frmvalidator.addValidation("volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("volgorde","numeric","Volgorde is ongeldig");
frmvalidator.addValidation("faq_faqcatid","req","Categorie is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['fileid']>0) {
	$query_rs = "SELECT * FROM bestanden WHERE bestandid=" . $GLOBALS['fileid'] . ";";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		$mijnFolderId = $row_rs['be_mapid'];
		}
	$_SESSION['myfileid'] = $GLOBALS['fileid'];
	mysql_free_result($rs);
}
else
{
	if ($_SESSION['myfileid']!="") {
		$GLOBALS['fileid'] = toInt($_SESSION['myfileid']);
	}
	$mijnFolderId = $GLOBALS['folderid'];
}
if ($mijnFolderId<1) { $mijnFolderId = 0; }
$MagMapVerwijderen = true;
if ($mijnFolderId==0) { $MagMapVerwijderen = false; }
if ($MagMapVerwijderen==true) {
	if (TelRecords("SELECT * FROM bestanden WHERE be_mapid=".$mijnFolderId.";")>0) { $MagMapVerwijderen = false; }
}
if ($MagMapVerwijderen==true) {
	if (TelRecords("SELECT * FROM bestanden_mappen WHERE bm_valtondermapid=".$mijnFolderId.";")>0) { $MagMapVerwijderen = false; }
}

$openerfieldtoupdate = $_GET["openerfieldtoupdate"];
$openerfieldno = $_GET["openerfieldno"];
$UploadURL = "bestanden-upload.php?onderfolderid=" . $mijnFolderId . "&openerfieldtoupdate=" . $openerfieldtoupdate ."&openerfieldno=".$openerfieldno;
?>
<?
OpenPagina("CMS", "");
?>
<script language="javascript">
function selecteerBestand(best_id, best_naam, best_icoon, best_guid)
{
    if(window.opener)
    {
    window.opener.document.getElementById("<?=$openerfieldtoupdate?>").value = best_id;
    window.opener.document.getElementById("tmpbestandsnaam").value = best_naam;
    window.opener.document.getElementById("tmpbestandsicoon").value = best_icoon;
    window.opener.document.getElementById("tmpbestandsguid").value = best_guid;
    window.opener.toonFileVoorbeeld();
    window.close();
    }
    else
    {
        document.getElementById("bestdownload_" + best_id).click();
    }
}
</script>
<?=OpenCMSTabel("Bestanden kiezen of uploaden")?>
<?=OpenCMSNavBalk()?>
    <? if ($mijnFolderId>0) { ?>
        <?=ToonCMSNavKnop("folder_wijzigen", "Wijzig map", "bestanden-mappen-edit.php?folderid=" . $mijnFolderId . "&openerfieldtoupdate=" . $openerfieldtoupdate ."&openerfieldno=".$openerfieldno) ?>
    <? } ?>
    <? if ($MagMapVerwijderen==true) { ?>
        <?=ToonCMSNavKnop("folder_verwijderen", "Verwijder map", "bestanden-mappen-del.php?folderid=" . $mijnFolderId . "&openerfieldtoupdate=" . $openerfieldtoupdate ."&openerfieldno=".$openerfieldno) ?>
    <? } ?>
    <?=ToonCMSNavKnop("folder_nieuw", "Nieuwe map", "bestanden-mappen-edit.php?onderfolderid=" . $mijnFolderId . "&openerfieldtoupdate=" . $openerfieldtoupdate ."&openerfieldno=".$openerfieldno) ?>
    <?=ToonCMSNavKnop("document_nieuw", "Nieuw bestand", $UploadURL) ?>
    <?=ToonCMSNavKnop("stop", "Sluit", "javascript:window.close()") ?>
<?=SluitCMSNavBalk()?>
<?
$MijnMapNaam = "Home";
$MyMapStruct = "";
function ToonMappenstructuur($map_id){
	$query_rs = "SELECT * FROM bestanden_mappen WHERE mapid=" . $map_id . ";";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		if (toInt($row_rs['bm_valtondermapid'])>0) {
			$MyMapStruct = $MyMapStruct . ToonMappenstructuur($row_rs['bm_valtondermapid']);
		}
		$GLOBALS['MijnMapNaam'] = $row_rs['mapnaam'];
		$MyMapStruct = $MyMapStruct . "&nbsp;&gt;&nbsp;<a class='navpad' href='bestanden.php?openerfieldtoupdate=" . $GLOBALS['openerfieldtoupdate'] . "&openerfieldno=" . $GLOBALS['openerfieldno'] . "&folderid=" . $map_id . "'>" . $row_rs['mapnaam'] . "</a>";
	}
	mysql_free_result($rs);
	return $MyMapStruct;
}
?>
<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td style="font-size: 10px;" colspan="20">
    <a class="navpad" href="bestanden.php?openerfieldtoupdate=<?=$openerfieldtoupdate?>&openerfieldno=<?=$openerfieldno?>&folderid=0">Home</a><?=ToonMappenstructuur($mijnFolderId)?>
    </td>
</tr>
<tr class="kadernav">
    <td width="30">&nbsp;<?=Icoon("folder_open")?></td>
    <td><b style="font-size: 10px;"><?=$MijnMapNaam?></b></td>
    <td width="10" style="font-size: 10px;">&nbsp;|&nbsp;</td>
    <td style="font-size: 10px;" width="50" align="center"><b style="font-size: 10px;">Gebruikt</b></td>
    <td width="10" style="font-size: 10px;">&nbsp;|&nbsp;</td>
    <td style="font-size: 10px;" width="50" align="right"><b style="font-size: 10px;">Grootte</b></td>
    <td width="10" style="font-size: 10px;">&nbsp;|&nbsp;</td>
    <td style="font-size: 10px;" width="70" align="right"><b style="font-size: 10px;">Gewijzigd</b></td>
    <td width="10" style="font-size: 10px;">&nbsp;|&nbsp;</td>
    <td style="font-size: 10px;" width="70" align="right"><b style="font-size: 10px;">Gemaakt</b></td>
    <td width="30">&nbsp;</td>
    <td width="30">&nbsp;</td>
</tr>

<?
$GeenRes = true;
?>

<?
//1 Omhoogbladeren
if ($mijnFolderId>0) {
	$query_rs = "SELECT * FROM bestanden_mappen WHERE mapid=" . $mijnFolderId . ";";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
	$GeenRes = false;
	$KiesURL = "bestanden.php?openerfieldtoupdate=" . $openerfieldtoupdate . "&openerfieldno=" . $openerfieldno . "&folderid=" .$row_rs['bm_valtondermapid'];
	?>
	<tr class="regel">
	    <td><?=PlaatsIcoonLink("folder_op", "", $KiesURL, "")?></td>
	    <td><a href="<?=$KiesURL?>">..</a></td>
	            <td></td>
	    <td style="font-size: 10px;"></td>
	            <td></td>
	    <td style="font-size: 10px;"></td>
	            <td></td>
	    <td style="font-size: 10px;" align="right"></td>
	            <td></td>
	    <td style="font-size: 10px;" align="right"></td>
	    <td style="font-size: 10px;"></td>
	    <td style="font-size: 10px;"></td>
	</tr>
			<?
	}
	mysql_free_result($rs);
}
?>
<?
//2 Onderliggende mappen
$query_rs = "SELECT * FROM bestanden_mappen WHERE bm_valtondermapid=" . $mijnFolderId . " ORDER BY mapnaam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	$GeenRes = false;
	do{
		$KiesURL = "bestanden.php?openerfieldtoupdate=" . $openerfieldtoupdate . "&openerfieldno=" . $openerfieldno . "&folderid=" .$row_rs['mapid'];
		?>
<tr class="regel">
    <td><?=PlaatsIcoonLink("folder_dicht", "", $KiesURL, "")?></td>
    <td><a href="<?=$KiesURL?>"><?=$row_rs['mapnaam']?></a></td>
            <td></td>
    <td style="font-size: 10px;"></td>
            <td></td>
    <td style="font-size: 10px;"></td>
            <td></td>
    <td style="font-size: 10px;" align="right"><?=MaakDatum($row_rs['bm_datumgewijzigd'])?></td>
            <td></td>
    <td style="font-size: 10px;" align="right"><?=MaakDatum($row_rs['bm_datumaangemaakt'])?></td>
    <td style="font-size: 10px;"></td>
    <td style="font-size: 10px;"></td>
</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>
<?
//3 Bestanden
$query_rs = "SELECT * FROM bestanden WHERE be_mapid=" . $mijnFolderId . " AND uploadgereed<>0 AND bestandgereed<>0 ORDER BY bestandsnaam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
$GeenRes = false;
do{
	$trclass = "regel";
	$MagVerwijderen = true;
	if ($GLOBALS['fileid']==$row_rs['bestandid']) {
		$trclass = "regelinv";
		$MagVerwijderen = false;
	}
	$tmpIcoon = GeefApplicatieIcoonViaExtentie($row_rs['bestandsextentie']);
	$tmpGUID = $row_rs['bestandsguid'];
	$AantGebruikt = TelRecords("SELECT * FROM paragrafen WHERE pa_bestandid=" . $row_rs['bestandid'] . ";");
	$VerwijderMelding = "Weet u zeker dat u dit bestand wilt verwijderen?";
		if ($AantGebruikt>0) {
			$VerwijderMelding = "Als u dit bestand verwijdert, worden ook alle verwijzingen (alineas) naar dit bestand verwijderd. Weet u zeker dat u dit bestand wilt verwijderen?";
		}
		else
		{
			$AantGebruikt = 0;
		}
	?>
<tr class="<?=$trclass?>">
    <td><?=PlaatsIcoonLink($tmpIcoon, "", $KiesURL, "")?></td>
    <td><a href="javascript:selecteerBestand(<?=$row_rs['bestandid']?>, '<?=$row_rs['bestandsnaam']?>', '<?=$tmpIcoon?>', '<?=$tmpGUID?>')"><?=substr($row_rs['bestandsnaam'],0,30)?></a></td>
            <td></td>
    <td style="font-size: 10px;" align="center"><?=$AantGebruikt?></td>
            <td></td>
    <td style="font-size: 10px;" align="right"><?=MaakBestandsGrootte($row_rs['bestandsgrootte'])?></td>
            <td></td>
    <td style="font-size: 10px;" align="right"><?=MaakDatum($row_rs['be_datumgewijzigd'])?></td>
            <td></td>
    <td style="font-size: 10px;" align="right"><?=MaakDatum($row_rs['be_datumaangemaakt'])?></td>
    <td style="font-size: 10px;">
        <a target="#filedownload" class="icoon" name="bestdownload_<?=$row_rs['bestandid']?>" id="bestdownload_<?=$row_rs['bestandid']?>" href="<?=$GLOBALS['ApplicatieRoot']?>/sybit/bestand.php?bestid=<?=$row_rs['bestandid']?>&bestguid=<?=$row_rs['bestandsguid']?>"><?=Icoon("app_download")?></a>
    </td>
    <td style="font-size: 10px;">
    <? if ($MagVerwijderen==true) { ?>
        <a class="icoon" onclick="return confirm('<?=$VerwijderMelding?>');" href="bestanden-del.php?bestid=<?=$row_rs['bestandid']?>&bestguid=<?=$row_rs['bestandsguid']?>&folderid=<?=$mijnFolderId?>&openerfieldtoupdate=<?=$openerfieldtoupdate?>&openerfieldno=<?=$openerfieldno?>"><?=Icoon("delete")?></a>
    <? } ?>
    </td>
</tr>
<?
}
while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>
<?
if ($GeenRes==true) {
?>
<tr class="regel">
    <td></td>
    <td>Er zijn nog geen bestanden of mappen</td>
</tr>
<?
}
?>

<?
SluitCMSTabel();
?>

<br /><br />

<script language="javascript">
window.focus();
</script>
<script language="javascript">
    window.resizeTo(800, 600);
</script>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
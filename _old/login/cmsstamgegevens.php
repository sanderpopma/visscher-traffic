<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Stamgegevens beheren")?>
<?=OpenCMSNavBalk()?>

<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Omschrijving</b></td>
</tr>
<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM adminmenu WHERE hoortbijadmmenuid=" . $GLOBALS['hmid'] . " AND admpubliceren<>0 ORDER BY admvolgorde";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
do
{
    $WijzigURL = $row_rs["admpaginaurl"] . ".php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'];
    $ItemIcoon = $row_rs["admmoduletype"];
    $ItemNaam = $row_rs["admmenunaam"];
?>
<tr class="regel">
    <td><?=PlaatsIcoonLink($ItemIcoon, "", $WijzigURL, $ItemNaam)?></td>
    <td><a href="<?=$WijzigURL?>"><?=$ItemNaam?></a></td>
</tr>
<?
}
while ($row_rs = mysql_fetch_assoc($rs));
mysql_free_result($rs);
?>

<?=SluitCMSTabel()?>

<?=SluitPagina()?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!IsAdministrator(GeefHuidigeUserId())) {die;}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Verzendwijzen beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw bericht", "cmsverzendwijzen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Zoekcode</b></td>
    <td><b>Omschrijving</b></td>
    <td width="100"><b>Volgorde</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>


<?
$query_rs = "SELECT * FROM winkel_verzendwijzen ORDER BY verzendwijzevolgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsverzendwijzen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["verzendwijzeid"];
	    $VerwijderURL = "cmsverzendwijzen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["verzendwijzeid"];
	    $VerwijderMelding = "Weet u zeker dat u deze verzendwijze wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
    	//$MagVerwijderen = false;
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["verzendwijzecode"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["verzendwijzenaam_nl"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["verzendwijzevolgorde"]?></a></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
$evvandatum = $_POST["evvandatum"];
$evtotdatum = $_POST["evtotdatum"];
$evtitel = $_POST["evtitel"];
$evinleiding = $_POST["evinleiding"];
$evpubliceren = $_POST["evpubliceren"];
$evalleenleden = $_POST["evalleenleden"];
$evtaalcode = strtoupper($_POST["evtaalcode"]);
$evafbeeldingid = $_POST["evafbeeldingid"];
$evlocatie = $_POST["evlocatie"];
$eventree = $_POST["eventree"];
$evtijd = $_POST["evtijd"];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO evenementen (evtitel, evinleiding, evvandatum, evtotdatum, evtaalcode, evlocatie, eventree, evtijd, evafbeeldingid, evalleenleden, evpubliceren) VALUES( ";
$qry2 = "".SQLStr($evtitel).", ".
	"".SQLStr($evinleiding).", ".
	"".SQLDat($evvandatum).", ".
	"".SQLDat($evtotdatum).", ".
	"".SQLStr($evtaalcode).", ".
	"".SQLStr($evlocatie).", ".
	"".SQLStr($eventree).", ".
	"".SQLStr($evtijd).", ".
	"".SQLStr($evafbeeldingid).", ".
	"".SQLBool($evalleenleden).", ".
	"".SQLBool($evpubliceren)."";
$qry3=")";

if ($GLOBALS['evid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE evenementen SET ";
	$qry2 = "evtitel=".SQLStr($evtitel).", ".
		"evinleiding=".SQLStr($evinleiding).", ".
		"evvandatum=".SQLDat($evvandatum).", ".
		"evtotdatum=".SQLDat($evtotdatum).", ".
		"evtaalcode=".SQLStr($evtaalcode).", ".
		"evlocatie=".SQLStr($evlocatie).", ".
		"eventree=".SQLStr($eventree).", ".
		"evtijd=".SQLStr($evtijd).", ".
		"evafbeeldingid=".SQLStr($evafbeeldingid).", ".
		"evalleenleden=".SQLBool($evalleenleden).", ".
		"evpubliceren=".SQLBool($evpubliceren)."";

	$qry3=" WHERE eventid=".$GLOBALS['evid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

if ($PgMode=="TOEVOEG")
{
    $GLOBALS['evid'] = mysql_insert_id();
}
redirect("cmsevents-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&evid=".$GLOBALS['evid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$afbSize = $_GET["afbSize"];

if ($afbSize!="KLEIN" && $afbSize!="ART" && $afbSize!="GEEN") {$afbSize="HEEL";}

$afbformaat = $afbSize;
$resizetype="wh";
$toonfunctie = "toonFoto";
$toonflashfunctie = "toonFlash";
$veldguid = "AfbGUID";
$veldext = "AfbExt";
$veldmaand = "AfbMaand";
$veldjaar = "AfbJaar";

$afbtype = $_FILES['afb_url']['type'];
$afbnaam = $_FILES['afb_url']['name'];

$afbresizemode = "";

switch($afbSize){
	case "KLEIN":
		$afbpxsize = $GLOBALS['AfbeeldingMaxHeel'];
		break;
	case "ART":
		$afbpxsize = $GLOBALS['AfbeeldingMaxArtikel'];
		$afbresizemode = "w";
		break;
	default:
        $afbpxsize = $GLOBALS['AfbeeldingMaxHeel'];
} // switch

if (toInt($_FILES['afb_url']['size'])<1)
{
echo "Bestand is te groot (maximaal ".($GLOBALS['MaxBestandsGrootte']/1000)."Kb)";
die;
}

$magverder=false;
if ($magverder==false && toInt(strpos($afbtype, "jpeg"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbtype, "jpg"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbtype, "gif"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbnaam, "jpg"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbnaam, "swf"))>0) $magverder=true;

if($magverder==false)
{
echo "Geen JPG of GIF gebruikt<br>";
echo $afbtype."<br>";
echo $afbnaam."<br>";
echo $_FILES['afb_url']['size']."<br>";
die;
}
switch($afbtype)
{
	case "image/gif":
		$bestype="gif";
		break;
	case "image/jpeg":
		$bestype="jpg";
		break;
	case "image/jpg":
		$bestype="jpg";
		break;
	case "image/pjpeg":
		$bestype="jpg";
		break;
	case "application/x-shockwave-flash":
		$bestype="swf";
		break;
}

    if (is_uploaded_file($_FILES['afb_url']['tmp_name']))
	{
	$afbguid = MaakGUID(GeefGUID());
	$query_rs = "INSERT INTO afbeeldingen(afbeeldingguid, afbdatum) VALUES('" . $afbguid . "', NOW())";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

	$query_rs = "SELECT * FROM afbeeldingen WHERE afbeeldingguid='" . $afbguid . "' ORDER BY afbeeldingid DESC";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	$totalRows_rs = mysql_num_rows($rs);
	$afbid = $row_rs['afbeeldingid'];
	$afbmaand = Maak2Dig(datMonth($row_rs['afbdatum']));
	$afbjaar = datYear($row_rs['afbdatum']);
	mysql_free_result($rs);

	$query_rs = "UPDATE afbeeldingen SET afbeeldingnaam=".SQLStr($afbnaam).", uploadgereed=1, afbeeldinggereed=1, afbeeldingextentie=" . SQLStr($bestype) . ", afbeeldingformaat='$afbformaat' WHERE afbeeldingid=".$afbid."";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

		MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar");
		MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand");
        copy($_FILES['afb_url']['tmp_name'], "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);

	if ($afbSize!="GEEN") {
		include_once("resizeimage.inc.php");
		$rimg=new RESIZEIMAGE("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
		echo $rimg->error();
		//$rimg->resize_percentage(50);
		if ($afbresizemode=="w") {
			$rimg->resize_limitwh($GLOBALS['AfbeeldingMaxHeelTonen'],$GLOBALS['AfbeeldingMaxHeelTonen'], "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/full_".$afbguid.".".$bestype);
			$rimg->resize_fixedw($afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
		}
		else
		{
			if ($resizetype=="hw")
			{
				$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
			}
			else
			{
				$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
			}
		}
		$rimg->close();

	    } else {
	        echo "Mogelijke aanval gespot: " . $_FILES['afb_url']['name'];
	    }

	}
?>
<script language="javascript">
if (window.opener && !window.opener.closed)
{
	<?
	if ($bestype=="swf") {
		?>
		window.opener.document.getElementById("<?=$OpenerFieldToUpdate?>").value = "<?=$afbid?>";
		window.opener.document.getElementById("<?=$veldguid?>").value = "<?=$afbguid?>";
		window.opener.document.getElementById("<?=$veldext?>").value = "<?=$bestype?>";
		window.opener.document.getElementById("<?=$veldmaand?>").value = "<?=$afbmaand?>";
		window.opener.document.getElementById("<?=$veldjaar?>").value = "<?=$afbjaar?>";
		window.opener.<?=$toonfunctie?>();
		window.opener.<?=$toonflashfunctie?>();
		<?
	}
	else{
	?>
	window.opener.document.getElementById("<?=$OpenerFieldToUpdate?>").value = "<?=$afbid?>";
	window.opener.document.getElementById("<?=$veldguid?>").value = "<?=$afbguid?>";
	window.opener.document.getElementById("<?=$veldext?>").value = "<?=$bestype?>";
	window.opener.document.getElementById("<?=$veldmaand?>").value = "<?=$afbmaand?>";
	window.opener.document.getElementById("<?=$veldjaar?>").value = "<?=$afbjaar?>";
	window.opener.<?=$toonfunctie?>();
	<?
	}
	?>
	window.close();
}
</script>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "LINKS")) { die; }
if ($GLOBALS['linkid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM links WHERE linkid=".$GLOBALS['linkid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['linkid']>0)
    {
    	$link_rubriekid = $row_rs['link_rubriekid'];
        $linktitel_nl = $row_rs['linktitel_nl'];
        $linkbeschrijving_nl = $row_rs['linkbeschrijving_nl'];
    	$linkurl = $row_rs['linkurl'];
        $linkpubliceren = Int2Bool($row_rs['linkpubliceren']);
    }
    mysql_free_result($rs);
}
else
{
    $linkpubliceren = false;
    $link_rubriekid = $GLOBALS['linkrubid'];
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Link " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmslinks.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&linkrubid=".$GLOBALS['linkrubid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmslinks-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" . $GLOBALS['linkrubid'] . "&linkid=".$GLOBALS['linkid'], "link", 700)?>
    <?=FrmText("Naam link".Icoon("NL"), "linktitel_nl", $linktitel_nl, 60, 100) ?>
	<?=FrmSelectByQuery("Rubriek", "link_rubriekid", $link_rubriekid, "SELECT rubriekid, rubnaam_nl FROM links_rubrieken ORDER BY rubvolgorde", "rubriekid", "rubnaam_nl", true)?>
    <?=FrmText("Website / URL", "linkurl", $linkurl, 60, 255) ?>

    <?=FrmKopregel("Beschrijving") ?>
    <?=FrmFCKText("Beschrijving".Icoon("NL"), "linkbeschrijving_nl", $linkbeschrijving_nl, 200) ?>

    <?=FrmCheckbox("Publiceren", "linkpubliceren", $linkpubliceren) ?>
    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("linktitel_nl") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("link");
frmvalidator.addValidation("linktitel_nl","req","Naam (NL) is verplicht");
frmvalidator.addValidation("link_rubriekid","req","Rubriek is verplicht");
frmvalidator.addValidation("link_rubriekid","gt=-1","Rubriek is verplicht");
frmvalidator.addValidation("linkurl","req","URL is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Productgroepen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe productgroep", "cmsproductgroepen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="150"><b>Naam in menu</b></td>
    <td><b>Naam productgroep</b></td>
    <td width="100"><b>Subcategorie&euml;n</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="70"><center><b>Volgorde</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM productgroepen ORDER BY prodgrp_volgorde, prodgrp_naam_nl;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
    	$ProdTxt = "subcategorieŽn";
	    $WijzigURL = "cmsproductgroepen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$row_rs["prodgrpid"];
	    $VerwijderURL = "cmsproductgroepen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$row_rs["prodgrpid"];
	    $VerwijderMelding = "Weet u zeker dat u deze productgroep wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantProducten = toInt(TelRecords("SELECT * FROM producten WHERE pr_prodgrpid=" . $row_rs['prodgrpid'].""));
	    $ProdURL = "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$row_rs["prodgrpid"];
	    if ($AantProducten<1) {
	    	$MagVerwijderen = true;
	    	$AantProducten = 0;
	    }
	    if ($AantProducten==1) {
	    	$ProdTxt = "subcategorie";
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Productgroep wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["prodgrp_menunaam_nl"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["prodgrp_naam_nl"]?></a></td>
			    <td><a href="<?=$ProdURL?>"><?=$AantProducten?>&nbsp;<?=$ProdTxt?></a></td>
			    <td><center><?=ToonGereed($row_rs["prodgrp_publiceren"])?></td>
			    <td><center><?=$row_rs["prodgrp_volgorde"]?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
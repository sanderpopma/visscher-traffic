<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}

if ($GLOBALS['tariefid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM tarieven WHERE tariefid=".$GLOBALS['tariefid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['tariefid']>0)
    {
        $tariefdatum = MaakDatum($row_rs["tariefdatum"]);
        $tarievenactief = Int2Bool($row_rs["tarievenactief"]);
        $taremailadres = $row_rs["taremailadres"];
        $tarcallemailadres = $row_rs["tarcallemailadres"];
        $btwpercentage = MaakDecimal($row_rs["btwpercentage"]);
        $bedrijfsnaam = $row_rs['bedrijfsnaam'];
        $bedrijfstelnr = $row_rs['bedrijfstelnr'];
        $bedrijfsfaxnr = $row_rs['bedrijfsfaxnr'];
        $bedrijfsmobnr = $row_rs['bedrijfsmobnr'];
        $bedrijfsemail = $row_rs['bedrijfsemail'];
        $bedrijfsadres = $row_rs['bedrijfsadres'];
        $bedrijfspostcode = $row_rs['bedrijfspostcode'];
        $bedrijfsplaats = $row_rs['bedrijfsplaats'];
        $bedrijfspostadres = $row_rs['bedrijfspostadres'];
        $bedrijfspostpostcode = $row_rs['bedrijfspostpostcode'];
        $bedrijfspostplaats = $row_rs['bedrijfspostplaats'];
    	$kvknummer = $row_rs['kvknummer'];
    	$bedrijfswebsite = $row_rs['bedrijfswebsite'];
		$bedrijfslinkedin = $row_rs['bedrijfslinkedin'];
		$bedrijfstwitter = $row_rs['bedrijfstwitter'];
    	$metroutelink = Int2Bool($row_rs["metroutelink"]);
    	$tar_lat = $row_rs['tar_lat'];
    	$tar_lng = $row_rs['tar_lng'];
    	$googleanalyticscode = $row_rs['googleanalyticscode'];
    	$geenverzendkostenboven = $row_rs['geenverzendkostenboven'];
    }
    mysql_free_result($rs);
}
else
{
    $tariefdatum = MaakDatum(Now);
    $tarievenactief = false;
    $metroutelink = true;
    $query_rs = "SELECT * FROM tarieven WHERE tarievenactief<>0;";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['tariefid']>0)
    {
        $taremailadres = $row_rs["taremailadres"];
        $tarcallemailadres = $row_rs["tarcallemailadres"];
        $btwpercentage = MaakDecimal($row_rs["btwpercentage"]);
        $bedrijfsnaam = $row_rs['bedrijfsnaam'];
        $bedrijfstelnr = $row_rs['bedrijfstelnr'];
        $bedrijfsfaxnr = $row_rs['bedrijfsfaxnr'];
        $bedrijfsmobnr = $row_rs['bedrijfsmobnr'];
        $bedrijfsemail = $row_rs['bedrijfsemail'];
        $bedrijfsadres = $row_rs['bedrijfsadres'];
        $bedrijfspostcode = $row_rs['bedrijfspostcode'];
        $bedrijfsplaats = $row_rs['bedrijfsplaats'];
        $bedrijfspostadres = $row_rs['bedrijfspostadres'];
        $bedrijfspostpostcode = $row_rs['bedrijfspostpostcode'];
        $bedrijfspostplaats = $row_rs['bedrijfspostplaats'];
		$kvknummer = $row_rs['kvknummer'];
		$bedrijfswebsite = $row_rs['bedrijfswebsite'];
		$bedrijfslinkedin = $row_rs['bedrijfslinkedin'];
		$bedrijfstwitter = $row_rs['bedrijfstwitter'];
    	$metroutelink = Int2Bool($row_rs["metroutelink"]);
    	$tar_lat = $row_rs['tar_lat'];
    	$tar_lng = $row_rs['tar_lng'];
    	$googleanalyticscode = $row_rs['googleanalyticscode'];
    	$geenverzendkostenboven = $row_rs['geenverzendkostenboven'];
	}
    mysql_free_result($rs);
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Systeeminstellingen " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmstarieven.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmstarieven-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&tariefid=" . $GLOBALS['tariefid'], "tarieven", 700)?>
<?
if ($PgMode=="TOEVOEG")
{
?>
	<?=FrmKoptekst("<span style='color: red';'>Onderstaande gegevens zijn een kopie van de op dit moment actieve tarieven");?>
<?
}
?>
    <?=FrmDatum("Datum", "tariefdatum", $tariefdatum) ?>
    <?=FrmText("E-mailadres (informatieaanvragen)", "taremailadres", $taremailadres, 60, 250) ?>
    <?=FrmHidden("tarcallemailadres", '') ?>
    <?=FrmText(MaakPercentageLabel("BTW Percentage"), "btwpercentage", $btwpercentage, 10, 5) ?>
    <?=FrmHidden("geenverzendkostenboven", '0') ?>
    <?=FrmCheckbox("Instellingen Actief", "tarievenactief", $tarievenactief) ?>

	<?=FrmKoptekst("Bedrijfsgegevens");?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Bedrijfsnaam", "bedrijfsnaam", $bedrijfsnaam, 40, 100) ?>
    <?=FrmText("Telefoon", "bedrijfstelnr", $bedrijfstelnr, 30, 50) ?>
    <?=FrmText("Fax", "bedrijfsfaxnr", $bedrijfsfaxnr, 30, 50) ?>
    <?=FrmText("Mobiel", "bedrijfsmobnr", $bedrijfsmobnr, 30, 50) ?>
    <?=FrmText("E-mail", "bedrijfsemail", $bedrijfsemail, 30, 50) ?>
    <?=FrmText("Website", "bedrijfswebsite", $bedrijfswebsite, 40, 250) ?>
    <?=FrmText("KVK-nummer", "kvknummer", $kvknummer, 30, 50) ?>
    <?=FrmCheckbox("Met link Route", "metroutelink", $metroutelink) ?>
	<?=FrmKoptekst("Vestigingsadres");?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Adres", "bedrijfsadres", $bedrijfsadres, 30, 50) ?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Postcode", "bedrijfspostcode", $bedrijfspostcode, 30, 50) ?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Plaats", "bedrijfsplaats", $bedrijfsplaats, 30, 50) ?>

	<?=FrmKoptekst("Social Media");?>
    <?=FrmText("LinkedIn", "bedrijfslinkedin", $bedrijfslinkedin, 60, 250) ?>
    <?=FrmText("Twitter", "bedrijfstwitter", $bedrijfstwitter, 60, 250) ?>

	<? if ($GLOBALS['MetGoogleMaps']==true) {
		?>
		<?=FrmKopregel("Google Maps")?>
	    <?=FrmText("Lengtegraad", "tar_lng", $tar_lng, 20, 20) ?>
	    <?=FrmText("Breedtegraad", "tar_lat", $tar_lat, 20, 20) ?>
		<?
	}
	?>

	<?=FrmKopregel("Google Analytics")?>
    <?=FrmTextArea("Trackingcode", "googleanalyticscode", $googleanalyticscode, 5, 60) ?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("tariefdatum") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("tarieven");
frmvalidator.addValidation("tariefdatum","req","Datum is verplicht");
frmvalidator.addValidation("tariefdatum","date","Datum heeft een ongeldige waarde");
frmvalidator.addValidation("taremailadres","req","E-mailadres afzender is verplicht");
frmvalidator.addValidation("btwpercentage","req","BTW Percentage is verplicht");
frmvalidator.addValidation("btwpercentage","dec","BTW Percentage heeft een ongeldige waarde");
frmvalidator.addValidation("geenverzendkostenboven","req","Geef het bedrag op waarboven geen verzendkosten worden gerekend");
frmvalidator.addValidation("geenverzendkostenboven","dec","Bedrag heeft een ongeldige waarde");
frmvalidator.addValidation("bedrijfsemail","req","E-mail is verplicht");
//frmvalidator.addValidation("bedrijfsadres","req","Adres is verplicht");
//frmvalidator.addValidation("bedrijfspostcode","req","Postcode is verplicht");
//frmvalidator.addValidation("bedrijfsplaats","req","Plaats is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<? if ($GLOBALS['MetGoogleMaps']==true) {
?>

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=<?=$GLOBALS['GoogleMapsKey']?>" type="text/javascript"></script>

<div id="map" style="width: <?=$GLOBALS['BreedteWebsiteTekstdeel']-50?>px; height: 400px"></div>

    <script type="text/javascript">
    //<![CDATA[
	function UpdateGMap(){

	    if (GBrowserIsCompatible()) {

	      // A function to create the marker and set up the event window
	      // Dont try to unroll this function. It has to be here for the function closure
	      // Each instance of the function preserves the contends of a different instance
	      // of the "marker" and "html" variables which will be needed later when the event triggers.
	      function createMarker(point,html) {
	        var marker = new GMarker(point);
	        GEvent.addListener(marker, "click", function() {
	          marker.openInfoWindowHtml(html);
	        });
	        return marker;
	      }

	   var geocoder;

	   var restaurant = document.getElementById("bedrijfsnaam").value;
	   var address = document.getElementById("bedrijfsadres").value + ', ' + document.getElementById("bedrijfspostcode").value + ', ' + document.getElementById("bedrijfsplaats").value + ', Nederland';

		// Create new geocoding object
	    geocoder = new GClientGeocoder();
		// Retrieve location information, pass it to addToMap()
	    geocoder.getLocations(address, addToMap);

		function addToMap(response)
		   {
		      // Retrieve the object
		      place = response.Placemark[0];

		      // Retrieve the latitude and longitude
		      point = new GLatLng(place.Point.coordinates[1],
		                          place.Point.coordinates[0]);

		      // Center the map on this point
		      map.setCenter(point, 7);

		      // Create a marker
		      //marker = new GMarker(point);
			  marker = createMarker(point,'<div style="width:240px">' + restaurant + '<br />' + address + '' + '<br /><\/div>');
				document.getElementById("tar_lat").value = place.Point.coordinates[1];
				document.getElementById("tar_lng").value = place.Point.coordinates[0];

		      // Add the marker to map
		      map.addOverlay(marker);

		      // Add address information to marker
		      //marker.openInfoWindowHtml(place.address);
		   }

	      // Display the map, with some controls and set the initial location
	      var map = new GMap2(document.getElementById("map"));
	      map.addControl(new GLargeMapControl());
	      map.addControl(new GMapTypeControl());


	    }

	    // display a warning if the browser was not compatible
	    else {
	      alert("Sorry, the Google Maps API is not compatible with this browser");
	    }


	}

    //]]>
    UpdateGMap();
    </script>

<?
}
else
{
?>
    <script type="text/javascript">
	function UpdateGMap(){
	}
    </script>
<?
}
?>
<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
// Rechten PAGINAS

if ($GLOBALS['pagid']<1) {
	redirect("cmspaginas.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);
	die;
}

$MagVerder = true;
$naartaal = $_GET['naartaal'];
$query_rs = "SELECT * FROM paginas WHERE hoortbijnlpaginaid=".$GLOBALS['pagid']." AND paginataalcode='" . $naartaal . "'";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if ($row_rs['paginaid']>0)
{
	$MagVerder = false;
	$GLOBALS['pagid'] = $row_rs['paginaid'];
}
mysql_free_result($rs);

if ($MagVerder==false) {
	//Pagina in deze taal bestaat al
	redirect("cmspaginas-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&pagid=".$GLOBALS['pagid']);
	die;
}

$MagVerder = true;
$query_rs = "SELECT * FROM paginas WHERE paginaid=".$GLOBALS['pagid']."";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if ($row_rs['paginaid']>0)
{
	if (toInt($row_rs['valtonderpaginaid'])<1) {
		$MagVerder = false;
	}
}
else
{
    $MagVerder = false;
}
mysql_free_result($rs);

if ($MagVerder==false) {
	// Dit type pagina kan niet worden aangemaakt in een andere taal
	redirect("cmspaginas-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&pagid=".$GLOBALS['pagid']);
	die;
}

$nieuwepagid = -1;
//Pagina Toevoegen
$qry1="INSERT INTO paginas (paginataalcode, publiceren) VALUES( ";
$qry2 = "".SQLStr($naartaal).", ".
	"".SQLBool(false)."";
$qry3=")";
$query_rs = $qry1.$qry2.$qry3;
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
// Ophalen nieuwe ID
$nieuwepagid = mysql_insert_id();

$query_rs = "SELECT * FROM paginas WHERE paginaid=".$GLOBALS['pagid']."";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if ($row_rs['paginaid']>0)
{
	$qry1="UPDATE paginas SET ";
	$qry2 = "paginatitel=".SQLStr($row_rs['paginatitel']).", ".
		"paginacode=".SQLStr($row_rs['paginacode']).", ".
		"paginamenunaam=".SQLStr($row_rs['paginamenunaam']).", ".
		"hoofdmenuvolgorde=".SQLStr($row_rs['hoofdmenuvolgorde']).", ".
		"submenuvolgorde=".SQLStr($row_rs['submenuvolgorde']).", ".
		"opnemeninsubmenu=".SQLStr($row_rs['opnemeninsubmenu']).", ".
		"valtonderpaginaid=".SQLStr(GeefDBWaarde("paginaid", "paginas", "paginataalcode='".$naartaal."' AND hoofdmenuvolgorde>0 AND hoortbijnlpaginaid=".$row_rs['valtonderpaginaid']."")).", ".
		"paginaurl=".SQLStr('').", ".
		"paginagoogletitel=".SQLStr($row_rs['paginagoogletitel']).", ".
		"paginaqrystring=".SQLStr($row_rs['paginaqrystring']).", ".
		"paginanieuwvenster=".SQLStr($row_rs['paginanieuwvenster']).", ".
		"pagina_afbeeldingid=".SQLStr($row_rs['pagina_afbeeldingid']).", ".
		"ledenloginnodig=".SQLStr($row_rs['ledenloginnodig']).", ".
		"hoortbijnlpaginaid=".SQLStr($GLOBALS['pagid']).", ".
		"moduletype=".SQLStr($row_rs['moduletype'])."";
	$qry3=" WHERE paginaid=".$nieuwepagid;
	$query_rs2 = $qry1.$qry2.$qry3;
	$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
}
mysql_free_result($rs);

// Pagina is aangemaakt. Toevoegen / kopieren paragrafen

$query_rs = "SELECT * FROM paragrafen WHERE pa_paginaid=".$GLOBALS['pagid']." ORDER BY paragraafid;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
		$qry1="INSERT INTO paragrafen (pa_paginaid, pa_publiceren) VALUES( ";
		$qry2 = "".SQLStr($nieuwepagid).", ".
			"".SQLStr($row_rs['pa_publiceren'])."";
		$qry3=")";
		$query_rs2 = $qry1.$qry2.$qry3;
		$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
		$nieuweparid = mysql_insert_id();

		$qry1="UPDATE paragrafen SET ";
		$qry2 = "pa_nieuwsid=".SQLStr($row_rs['pa_nieuwsid']).", ".
			"pa_eventid=".SQLStr($row_rs['pa_eventid']).", ".
			"pa_naam=".SQLStr($row_rs['pa_naam']).", ".
			"pa_volgorde=".SQLStr($row_rs['pa_volgorde']).", ".
			"paragraaftype=".SQLStr($row_rs['paragraaftype']).", ".
			"pa_tekst=".SQLStr($row_rs['pa_tekst']).", ".
			"pa_afbeeldinglr=".SQLStr($row_rs['pa_afbeeldinglr']).", ".
			"pa_ilinkid=".SQLStr($row_rs['pa_ilinkid']).", ".
			"pa_linktekst=".SQLStr($row_rs['pa_linktekst']).", ".
			"pa_elinkurl=".SQLStr($row_rs['pa_elinkurl']).", ".
			"pa_emailadres=".SQLStr($row_rs['pa_emailadres']).", ".
			"pa_fotoboekid=".SQLStr($row_rs['pa_fotoboekid']).", ".
			"pa_afbeeldingid=".SQLStr($row_rs['pa_afbeeldingid']).", ".
			"pa_formulierid=".SQLStr($row_rs['pa_formulierid']).", ".
			"pa_bestandid=".SQLStr($row_rs['pa_bestandid']).", ".
			"pa_eventlinkid=".SQLStr($row_rs['pa_eventlinkid']).", ".
			"pa_afbeeldingonderschrift=".SQLStr($row_rs['pa_afbeeldingonderschrift'])."";
		$qry3=" WHERE paragraafid=".$nieuweparid;
		$query_rs2 = $qry1.$qry2.$qry3;
		$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);

redirect("cmspaginas-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&pagid=".$nieuwepagid);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
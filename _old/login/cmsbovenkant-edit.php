<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM afbeeldingen_boven WHERE abid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['abid']>0)
    {
		$ab_afbeeldingid = $row_rs["ab_afbeeldingid"];
		$ab_publiceren = Int2Bool($row_rs["ab_publiceren"]);
		$ab_hmid = $row_rs["ab_hmid"];
    }
    mysql_free_result($rs);
}
else
{
	$ab_afbeeldingid = -1;
	$ab_publiceren = true;
	$ab_hmid = $GLOBALS['parid'];
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Foto " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsbovenkant.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&parid=".$GLOBALS['parid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsbovenkant-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid']."&parid=".$GLOBALS['parid'], "grassoorten", 700)?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotogeen.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbheel").src = "<?=$GLOBALS['ApplicatieRoot']?>/images/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbheel").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbheel").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbheel").style.border = "0px";
            document.getElementById("ab_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("ab_afbeeldingid", $ab_afbeeldingid)?>
	<?=FrmHidden("ab_hmid", $ab_hmid)?>

    <tr class='regel' valign='top'>
        <td>Afbeelding</td>
        <td>:</td>
        <td>
        <span style="color: red; font-weight: bold;">Let op: de afbeelding vooraf op maat snijden op <?=$GLOBALS['BreedteWebsiteBovenFoto']?>x<?=$GLOBALS['HoogteWebsiteBovenFoto']?> pixels.</span><br /><br />
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $ab_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('ab_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbheel" id="afbheel" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('ab_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbheel" id="afbheel" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>
    <?=FrmCheckbox("Publiceren", "ab_publiceren", $ab_publiceren) ?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("grassoorten");
frmvalidator.addValidation("ab_afbeeldingid","req","Afbeelding is verplicht");
frmvalidator.addValidation("ab_afbeeldingid","gt=0","Afbeelding is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
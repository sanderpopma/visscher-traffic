<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$afbid = toInt($_GET["afbid"]);
?>
<?
$afbtype = $_FILES['afb_url']['type'];
$afbnaam = $_FILES['afb_url']['name'];

$afbformaat = "PAGINA";
switch($OpenerFieldToUpdate)
{
	case "pa_afbeeldingid":
		$afbformaat = "PAGINA";
		$afbpxsize = $GLOBALS['AfbeeldingMaxPagina'];
		$resizetype="wh";
		$toonfunctie = "toonFoto";
		$veldguid = "afbguid";
		$veldext = "afbext";
		$veldmaand = "afbmaand";
		$veldjaar = "afbjaar";
		break;
	case "wijn_fles1_afbeeldingid":
		$afbformaat = "FLES1";
		$afbpxsize = $GLOBALS['AfbeeldingMaxArtikel'];
		$resizetype="hw";
		$toonfunctie = "toonFoto";
		$veldguid = "afbguid";
		$veldext = "afbext";
		$veldmaand = "afbmaand";
		$veldjaar = "afbjaar";
		break;
	case "wijn_fles2_afbeeldingid":
		$afbformaat = "FLES2";
		$afbpxsize = $GLOBALS['AfbeeldingMaxArtikel'];
		$resizetype="hw";
		$toonfunctie = "toonFoto2";
		$veldguid = "afbguid2";
		$veldext = "afbext2";
		$veldmaand = "afbmaand2";
		$veldjaar = "afbjaar2";
		break;
	case "wijn_etiket_afbeeldingid":
		$afbformaat = "ETIKET";
		$afbpxsize = $GLOBALS['AfbeeldingMaxArtikelEtiket'];
		$resizetype="wh";
		$toonfunctie = "toonFoto3";
		$veldguid = "afbguid3";
		$veldext = "afbext3";
		$veldmaand = "afbmaand3";
		$veldjaar = "afbjaar3";
		break;
}
if (toInt($_FILES['afb_url']['size'])<1)
{
echo "Bestand is te groot (maximaal ".($GLOBALS['MaxBestandsGrootte']/1000)."Kb)";
die;
}

$magverder=false;
if ($magverder==false && toInt(strpos($afbtype, "jpeg"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbtype, "jpg"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbtype, "gif"))>0) $magverder=true;
if ($magverder==false && toInt(strpos($afbnaam, "jpg"))>0) $magverder=true;

if($magverder==false)
{
echo "Geen JPG of GIF gebruikt<br>";
echo $afbtype."<br>";
echo $afbnaam."<br>";
echo $_FILES['afb_url']['size']."<br>";
die;
}

//echo $_FILES['afb_url']['name']."<br>";
//echo $_FILES['afb_url']['type']."<br>";
//echo $_FILES['afb_url']['size']."<br>";
//echo $_FILES['afb_url']['tmp_name']."<br>";

switch($afbtype)
{
	case "image/gif":
		$bestype="gif";
		break;
	case "image/jpeg":
		$bestype="jpg";
		break;
	case "image/jpg":
		$bestype="jpg";
		break;
	case "image/pjpeg":
		$bestype="jpg";
		break;
}

    if (is_uploaded_file($_FILES['afb_url']['tmp_name']))
	{
	$afbguid = MaakGUID(GeefGUID());
	$query_rs = "INSERT INTO afbeeldingen(afbeeldingguid, afbdatum) VALUES('" . $afbguid . "', NOW())";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

	$query_rs = "SELECT * FROM afbeeldingen WHERE afbeeldingguid='" . $afbguid . "' ORDER BY afbeeldingid DESC";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	$totalRows_rs = mysql_num_rows($rs);
	$afbid = $row_rs['afbeeldingid'];
	$afbmaand = Maak2Dig(datMonth($row_rs['afbdatum']));
	$afbjaar = datYear($row_rs['afbdatum']);
	mysql_free_result($rs);

	$query_rs = "UPDATE afbeeldingen SET afbeeldingnaam=".SQLStr($afbnaam).", uploadgereed=1, afbeeldinggereed=1, afbeeldingextentie=" . SQLStr($bestype) . ", afbeeldingformaat='$afbformaat' WHERE afbeeldingid=".$afbid."";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

		MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar");
		MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand");
        copy($_FILES['afb_url']['tmp_name'], "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);

	include_once("resizeimage.inc.php");
	$rimg=new RESIZEIMAGE("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
	echo $rimg->error();
	//$rimg->resize_percentage(50);
	if ($resizetype=="hw")
	{
		$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
	}
	else
	{
		$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
	}
	$rimg->close();

    } else {
        echo "Mogelijke aanval gespot: " . $_FILES['afb_url']['name'];
    }
?>
<script language="javascript">
if (window.opener && !window.opener.closed)
{
	window.opener.document.getElementById("<?=$OpenerFieldToUpdate?>").value = "<?=$afbid?>";
	window.opener.document.getElementById("<?=$veldguid?>").value = "<?=$afbguid?>";
	window.opener.document.getElementById("<?=$veldext?>").value = "<?=$bestype?>";
	window.opener.document.getElementById("<?=$veldmaand?>").value = "<?=$afbmaand?>";
	window.opener.document.getElementById("<?=$veldjaar?>").value = "<?=$afbjaar?>";
	window.opener.<?=$toonfunctie?>();
	window.close();
}
</script>
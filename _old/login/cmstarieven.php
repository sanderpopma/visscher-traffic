<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Systeeminstellingen beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe instellingen", "cmstarieven-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar Stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Datum instellingen</b></td>
    <td><b>Bedrijfsnaam</b></td>
    <td><b>BTW Percentage</b></td>
    <td><b>Tel.nr.</b></td>
    <td width="100" align="center"><b>Actief</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>
<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM tarieven ORDER BY tariefdatum DESC";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
do
{
    $WijzigURL = "cmstarieven-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&tariefid=" . $row_rs["tariefid"];
    $VerwijderURL = "cmstarieven-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&tariefid=" . $row_rs["tariefid"];
    $VerwijderMelding = "Weet u zeker dat u de tarieven van [" . MaakDatum($row_rs["tariefdatum"]) . "] wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
?>
<tr class="regel">
    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Tarieven wijzigen")?></td>
    <td><a href="<?=$WijzigURL?>"><?=MaakDatum($row_rs["tariefdatum"])?></a></td>
	<td><?=$row_rs['bedrijfsnaam']?></td>
    <td><?=MaakDecimal($row_rs["btwpercentage"])?></td>
	<td><?=$row_rs['bedrijfstelnr']?></td>
    <td align="center"><?=ToonGereed($row_rs["tarievenactief"])?></td>
    <td>
    <?
    if ($row_rs["tarievenactief"]==0)
    {
        ?>
        <?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
        <?
    }
    ?>
    </td>
</tr>
<?
}
while ($row_rs = mysql_fetch_assoc($rs));
mysql_free_result($rs);
?>

<?=SluitCMSTabel()?>

<?=SluitPagina()?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
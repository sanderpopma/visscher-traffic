<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$partype = $_GET['partype'];
$itemid = $_GET['itemid'];

switch(strtoupper($partype)){
	case "NIEUWS":
        $AnnuleerLink = "cmsnieuws-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&nwsid=" . $itemid;
        $WhereDeel = "pa_nieuwsid=" . $itemid;
        $Onderdeel = "Nieuwsbericht";
        $OnderdeelTitel = GeefDBWaarde("nwstitel", "nieuws", "nieuwsid=" . $itemid);
		break;
	case "VACATURE":
        $AnnuleerLink = "cmsvacatures-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&vacid=" . $itemid;
        $WhereDeel = "pa_vacatureid=" . $itemid;
        $Onderdeel = "Vacature";
        $OnderdeelTitel = GeefDBWaarde("vactitel", "vacatures", "vacatureid=" . $itemid);
		break;
	case "EVENT":
        $AnnuleerLink = "cmsevents-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&evid=" . $itemid;
        $WhereDeel = "pa_eventid=" . $itemid;
        $Onderdeel = "Evenement / activiteit";
        $OnderdeelTitel = GeefDBWaarde("evtitel", "evenementen", "eventid=" . $itemid);
		break;
	case "PROJECT":
		$AnnuleerLink = "cmsprojecten-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&evid=" . $itemid;
		$WhereDeel = "pa_projectid=" . $itemid;
		$Onderdeel = "Project/Portfolio";
		$OnderdeelTitel = GeefDBWaarde("proj_naam", "projecten", "projid=" . $itemid);
		break;
	default:
        $partype = "PAGINA";
        $AnnuleerLink = "cmspaginas-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&pagid=" . $itemid;
        $WhereDeel = "pa_paginaid=" . $itemid;
        $Onderdeel = "Pagina";
        $OnderdeelTitel = GeefDBWaarde("paginatitel", "paginas", "paginaID=" . $itemid);
} // switch

if ($parid<1){
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG"){
}
else
{
    $MaxNr = 0;
	$query_rs = "SELECT MAX(pa_volgorde) as maxnr FROM paragrafen WHERE (" . $WhereDeel . ");";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		if ($row_rs['maxnr']!="") {
			$MaxNr = $row_rs['maxnr'];
		}
	}
	mysql_free_result($rs);
    $PA_Volgorde = $MaxNr + 1;
    $PA_Publiceren = true;
}
?>
<?
OpenPagina("CMS", "");
?>
<? GebruikDatumObjecten(); ?>
<?=OpenCMSTabel("Alinea " . $PgMode . "en")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();")?>
    <?=ToonCMSNavKnop("stop", "Annuleren", $AnnuleerLink)?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsparagraaf-add2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&parid=" . $GLOBALS['parid'] . "&partype=" . $GLOBALS['partype'] . "&itemid=" . $GLOBALS['itemid'], "paragraaf", 700)?>
	<tr class="regel">
	        <td>Onderdeel</td>
	        <td>:</td>
	        <td><?=$Onderdeel?></td>
	</tr>
    <tr class="regel">
        <td>Titel v/h onderdeel</td>
        <td>:</td>
        <td><?=$OnderdeelTitel?></td>
    </tr>
    <?=FrmKopregel("Kies het soort alinea dat u wilt toevoegen")?>
<?=SluitForm()?>

<?
$x = 0;
$query_rs = "SELECT * FROM paragraaftypen WHERE typegebruiken<>0 and ismodule=0 ORDER BY afbeeldvolgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
$AantTypen = mysql_num_rows($rs);
if (mysql_num_rows($rs)>0) {
?>
<table width="900">
    <? if ($x % 3 == 0) {  ?>
    <tr valign="top">
    <? } ?>
<?
	do
	{
?>
    <td style="font-size: 10px;" align="center"><a style="text-decoration: none; font-size: 10px; color: Black; font-weight: normal;" href="cmsparagraaf-add2.php?hmid=<?=$GLOBALS['hmid']?>&smid=<?=$GLOBALS['smid']?>&parid=<?=$GLOBALS['parid']?>&partype=<?=$GLOBALS['partype']?>&itemid=<?=$GLOBALS['itemid']?>&seltype=<?=$row_rs['paragraaftype']?>"><img border="0" src="<?=$row_rs['pt_afbeeldingurl']?>" alt="<?=$row_rs['omschrijvingbijtoevoegen']?>" /><br /><?=$row_rs['omschrijvingbijtoevoegen']?></a></td>
    <? if ($x % 3 == 2) {  ?>
    </tr>
    <? } ?>
<?
	$x = $x + 1;
	}
	while ($row_rs = mysql_fetch_assoc($rs));
?>
<? if ($AantTypen % 3 != 0) { ?>
	<?
	for ($y = 1; $y <= (3-($AantTypen % 3)); $y++) {
    echo "<td></td>";
	}
	?>
    </tr>
<? } ?>
</table>
<?
}
mysql_free_result($rs);
?>

<?
$x = 0;
$query_rs = "SELECT * FROM paragraaftypen WHERE typegebruiken<>0 and ismodule<>0 ORDER BY afbeeldvolgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
$AantTypen = mysql_num_rows($rs);
if (mysql_num_rows($rs)>0) {
?>
<hr class="streep" />
<table width="900">
	<?=FrmKopregel("Of kies een specifieke alinea")?>
    <? if ($x % 3 == 0) {  ?>
    <tr valign="top">
    <? } ?>
<?
do
{
?>
    <td style="font-size: 10px;" align="center"><a style="text-decoration: none; font-size: 10px; color: Black; font-weight: normal;" href="cmsparagraaf-add2.php?hmid=<?=$GLOBALS['hmid']?>&smid=<?=$GLOBALS['smid']?>&parid=<?=$GLOBALS['parid']?>&partype=<?=$GLOBALS['partype']?>&itemid=<?=$GLOBALS['itemid']?>&seltype=<?=$row_rs['paragraaftype']?>"><img border="0" src="<?=$row_rs['pt_afbeeldingurl']?>" alt="<?=$row_rs['omschrijvingbijtoevoegen']?>" /><br /><?=$row_rs['omschrijvingbijtoevoegen']?></a></td>
    <? if ($x % 3 == 2) {  ?>
    </tr>
    <? } ?>
<?
$x = $x + 1;
}
while ($row_rs = mysql_fetch_assoc($rs));
?>
<? if ($AantTypen % 3 != 0) { ?>
	<?
for ($y = 1; $y <= (3-($AantTypen % 3)); $y++) {
	echo "<td></td>";
}
?>
    </tr>
<? } ?>
</table>
<?
}
mysql_free_result($rs);
?>
<script language="JavaScript">
var frmvalidator  = new Validator("paragraaf");
</script>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
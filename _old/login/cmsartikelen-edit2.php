<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['prodgrpid']<1) {
	redirect("cmsproductgroepen.php");
	die;
}
if ($GLOBALS['prodid']<1) {
	redirect("cmsproducten.php?prodgrpid=".$GLOBALS['prodgrpid']);
	die;
}

$art_productid = $_POST['art_productid'];
$art_naam = $_POST['art_naam'];
$art_prijs = $_POST['art_prijs'];
$art_omschrijving = $_POST['art_omschrijving'];
$art_publiceren = $_POST['art_publiceren'];
$art_min_bestelhoeveelheid = $_POST['art_min_bestelhoeveelheid'];

$PgMode = "TOEVOEG";
$OrgPgMode = "TOEVOEG";
if ($GLOBALS['artid']>0)
{
	$PgMode = "WIJZIG";
	$OrgPgMode = "WIJZIG";
}

if ($PgMode=="TOEVOEG") {
	$qry1="INSERT INTO artikelen (art_productid) VALUES( ";
	$qry2 = "".SQLStr($art_productid)."";
	$qry3=")";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$GLOBALS['artid'] = mysql_insert_id();
}

if ($GLOBALS['artid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE artikelen SET ";
	$qry2 = "art_productid=".SQLStr($art_productid).", ".
		"art_naam=".SQLStr($art_naam).", ".
		"art_prijs=".SQLDec($art_prijs).", ".
		"art_min_bestelhoeveelheid=".SQLStr($art_min_bestelhoeveelheid).", ".
		"art_omschrijving=".SQLStr($art_omschrijving).", ".
		"art_afbeeldingid=".SQLStr($_POST['art_afbeeldingid']).", ".
		"art_publiceren=".SQLBool($art_publiceren)."";

	$qry3=" WHERE artikelid=".$GLOBALS['artid'];

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

}

redirect("cmsartikelen.php?prodgrpid=".$GLOBALS['prodgrpid']."&prodid=".$GLOBALS['prodid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
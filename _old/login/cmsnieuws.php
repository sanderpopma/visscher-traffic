<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Nieuws")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw bericht", "cmsnieuws-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="50" align="center"><b>Taal</b></td>
    <td width="100"><b>Datum</b></td>
    <td><b>Titel</b></td>
    <td width="100" align="center"><b>Gelezen</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM nieuws ORDER BY nwspubliceren, nwsdatum DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsnieuws-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&nwsid=" .$row_rs["nieuwsid"];
	    $VerwijderURL = "cmsnieuws-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&nwsid=" .$row_rs["nieuwsid"];
	    $VerwijderMelding = "Weet u zeker dat u dit bericht, inclusief de bijbehorende alinea's, wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Bericht wijzigen")?></td>
			    <td align="center"><?=Icoon($row_rs["nwstaalcode"])?></td>
			    <td><?=MaakDatum($row_rs["nwsdatum"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["nwstitel"]?></a></td>
			    <td align="center"><?=$row_rs["nwsgelezen"]?></td>
			    <td align="center"><?=ToonGereed($row_rs["nwspubliceren"])?></td>
			    <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
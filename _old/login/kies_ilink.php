<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$GekozenPaginaTaal = $_GET["GekozenPaginaTaal"];
$CurVal = toInt($_GET["CurVal"]);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Interne link kiezen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close()") ?>
<?=SluitCMSNavBalk()?>

<script language="javascript">
function kiesilink(pag_id, pag_titel)
{
    window.opener.document.getElementById("<?=$OpenerFieldToUpdate?>").value = pag_id;
    if (window.opener.document.getElementById("pa_ilinktekst")) {
	    window.opener.document.getElementById("pa_ilinktekst").value = pag_titel;
    }
    if (window.opener.document.getElementById("ilinktekst")) {
	    window.opener.document.getElementById("ilinktekst").innerHTML = pag_titel;
    }
    if (window.opener.toonILinkVoorbeeld) {
	    window.opener.toonILinkVoorbeeld();
    }
    window.close();
}
</script>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Kies een pagina om een verwijzing naar te maken</b></td>
</tr>
<?
function ToonILinkSubmenuPaginas($onderpag_id, $menu_niveau)
{
	mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
	$query_rs2 = "SELECT * FROM paginas WHERE valtonderpaginaid=" . $onderpag_id . " AND (ledenloginnodig=0) ORDER BY submenuvolgorde;";
	$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
	$row_rs2 = mysql_fetch_assoc($rs2);
	if (mysql_num_rows($rs2)>0) {
		do
		{
        $classtxt = "regel";
	    if (toInt($row_rs2['paginaid'])==toInt($CurVal)) {
	     	$classtxt = "regelinv";
	    }
        $tmpTab = "";
        for ($x=1; $x<$menu_niveau; $x++)
        {
            $tmpTab = $tmpTab . "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $tmpTab = $tmpTab . "-&nbsp;";
	    $tmpPagTitel = $row_rs2["paginatitel"];
	    $tmpPagTitel = str_replace("'", "\'", $tmpPagTitel);
    ?>
        <tr class="<?=$classtxt?>">
            <td></td>
            <td><?=$tmpTab?><a onclick="kiesilink('<?=$row_rs2['paginaid']?>', '<?=$tmpPagTitel?>'); return false;" href=""><?=$row_rs2["paginatitel"]?></a></td>
        </tr>
    <?
        ToonILinkSubmenuPaginas($row_rs2['paginaid'], $menu_niveau+1);
	}
	while ($row_rs2 = mysql_fetch_assoc($rs2));
}
mysql_free_result($rs2);
}
?>
<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM paginas WHERE paginataalcode='" . $GekozenPaginaTaal . "' AND (valtonderpaginaid IS NULL OR valtonderpaginaid=0) AND (ledenloginnodig=0) ORDER BY hoofdmenuvolgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $classtxt = "regel";
	    if (toInt($row_rs['paginaid'])==toInt($CurVal)) {
	     	$classtxt = "regelinv";
	     }
	    $tmpPagTitel = $row_rs["paginatitel"];
	    $tmpPagTitel = str_replace("'", "\'", $tmpPagTitel);
		?>
		<tr class="<?=$classtxt?>">
		    <td></td>
		    <td><a onclick="kiesilink('<?=$row_rs['paginaid']?>', '<?=$tmpPagTitel?>'); return false;" href=""><?=$row_rs["paginatitel"]?></a></td>
		</tr>
		<?
		    ToonILinkSubmenuPaginas($row_rs['paginaid'], 1);
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<br /><br />

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Banners")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe banner", "cmsbanners-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsbanners.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="30"><b>Taal</b></td>
    <td><b>Banner omschrijving</b></td>
    <td width="100"><center><b>Actief</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM banners ORDER BY bannernaam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsbanners-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&banid=" .$row_rs["bannerid"];
	    $VerwijderURL = "cmsbanners-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&banid=" .$row_rs["bannerid"];
	    $VerwijderMelding = "Weet u zeker dat u deze banner wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    if ($row_rs['banneractief_nl']!=0) {
	    	$MagVerwijderen = false;
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Product wijzigen")?></td>
			    <td><?=Icoon(strtoupper($row_rs['bannertaalcode']))?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bannernaam"]?></a></td>
			    <td><center><?=ToonGereed($row_rs["banneractief_nl"])?></td>
			    <td>
			    <? if ($MagVerwijderen) {
			    	?>
				<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
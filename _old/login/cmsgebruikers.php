<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Gebruikers beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe gebruiker", "cmsgebruikers-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Naam Gebruiker</b></td>
    <td><b>Loginnaam</b></td>
    <td><b>E-mail</b></td>
    <td width="80" align="center"><b>Logins</b></td>
    <td width="100" align="center"><b>Administrator</b></td>
    <td width="100" align="center"><b>Actief</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>


<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM cmsbeheerders ORDER BY beheerafbeeldnaam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{

    	$MagVerwijderen = true;
    	$ProdTxt = "producten";
	    $WijzigURL = "cmsgebruikers-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&gebruikerid=" .$row_rs["beheerderid"];
	    $VerwijderURL = "cmsgebruikers-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&gebruikerid=" .$row_rs["beheerderid"];
	    $VerwijderMelding = "Weet u zeker dat u de gebruiker [" . $row_rs['beheerafbeeldnaam'] . "] wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    if (toInt($row_rs["beheerderid"])==toInt(GeefHuidigeUserId())) {
	    	$MagVerwijderen = false;
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Gebruiker wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["beheerafbeeldnaam"]?></a></td>
				<td><?=$row_rs['beheerlogin']?></td>
				<td><?=$row_rs['beheeremailadres']?></td>
				<td><center><?=$row_rs["aantallogins"]?></td>
			    <td><center><?=ToonGereed($row_rs["beheerisadministrator"])?></td>
			    <td><center><?=ToonGereed($row_rs["beheerderactief"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
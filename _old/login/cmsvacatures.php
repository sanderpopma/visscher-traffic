<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Vacatures")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe vacature", "cmsvacatures-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="50" align="center"><b>Taal</b></td>
    <td width="100"><b>Datum</b></td>
    <td><b>Titel</b></td>
    <td width="100" align="center"><b>Gelezen</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM vacatures ORDER BY vacpubliceren, vacdatumgeplaatst DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsvacatures-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vacid=" .$row_rs["vacatureid"];
	    $VerwijderURL = "cmsvacatures-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vacid=" .$row_rs["vacatureid"];
	    $VerwijderMelding = "Weet u zeker dat u deze vacature, inclusief de bijbehorende alinea's, wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td align="center"><?=Icoon($row_rs["vactaalcode"])?></td>
			    <td><?=MaakDatum($row_rs["vacdatumgeplaatst"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["vactitel"]?></a></td>
			    <td align="center"><?=$row_rs["vacgelezen"]?></td>
			    <td align="center"><?=ToonGereed($row_rs["vacpubliceren"])?></td>
			    <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
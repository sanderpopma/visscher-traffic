<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) { die; }
if ($GLOBALS['vertaalid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM vertalingen WHERE vertaalid=".$GLOBALS['vertaalid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['vertaalid']>0)
    {
        $zoekcode = $row_rs['zoekcode'];
        $tekst_nl = $row_rs['tekst_nl'];
        $tekst_en = $row_rs['tekst_en'];
        $tekst_de = $row_rs['tekst_de'];
        $tekst_fr = $row_rs['tekst_fr'];
    }
    mysql_free_result($rs);
}
else
{
	// Geen default values
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Vertaling " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsvertalingen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsvertalingen-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vertaalid=" . $GLOBALS['vertaalid'], "vertaal", 700)?>
    <?=FrmText("Zoekcode", "zoekcode", $zoekcode, 40, 50) ?>
		<?
		mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
		$query_rs = "SELECT * FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde;";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		if (mysql_num_rows($rs)>0) {
			do
			{
			$myval ="";
			switch($row_rs['taalcode']){
				case "NL":
					$myval = $tekst_nl;
					break;
				case "EN":
					$myval = $tekst_en;
					break;
				case "DE":
					$myval = $tekst_de;
					break;
				case "FR":
					$myval = $tekst_fr;
					break;
				default:
					//
			} // switch
			?>
		    <?=FrmText($row_rs['taalnaam'], "tekst_" . strtolower($row_rs['taalcode']), $myval, 60, 255) ?>
			<?
			}
			while ($row_rs = mysql_fetch_assoc($rs));
		}
		mysql_free_result($rs);
	    ?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("zoekcode") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("vertaal");
frmvalidator.addValidation("zoekcode","req","Zoekcode is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
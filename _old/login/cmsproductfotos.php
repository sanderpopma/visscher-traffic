<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters

if ($GLOBALS['prodgrpid']<1 || $GLOBALS['prodid']<1 ) {
	redirect("cmsproductgroepen.php");
}
$ProdNaam = GeefDBWaarde("productnaam_nl", "producten", "productid=".$GLOBALS['prodid']);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Foto's bij het product ".$ProdNaam)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe foto", "cmsproductfotos-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=".$GLOBALS['prodgrpid']. "&prodid=".$GLOBALS['prodid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar product", "cmsproducten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=".$GLOBALS['prodgrpid']. "&prodid=".$GLOBALS['prodid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Afbeelding</b></td>
    <td><b>Hoort bij product</b></td>
    <td><b>Hoort bij marktsegment</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM productfotos WHERE foto_productid=".$GLOBALS['prodid'].";";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsproductfotos-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$GLOBALS["prodgrpid"]."&prodid=".$row_rs['foto_productid']."&prodfotoid=".$row_rs['prodfotoid'];
	    $VerwijderURL = "cmsproductfotos-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$GLOBALS["prodgrpid"]."&prodid=".$row_rs['foto_productid']."&prodfotoid=".$row_rs['prodfotoid'];
	    $VerwijderMelding = "Weet u zeker dat u dit deze foto bij dit product wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Foto wijzigen")?></td>
			    <td><img src="<?=GeefAfbeeldingSrc($row_rs['foto_afbeeldingid'])?>" class="border" width="<?=$GLOBALS['AfbeeldingMiniWidth']?>" /></td>
			    <td><a href="<?=$WijzigURL?>"><?=$ProdNaam?></a></td>
			    <td>
			    <? if ($row_rs['foto_marktsegmentid']>0) {
			    	?>
					<a href="<?=$WijzigURL?>"><?=GeefDBWaarde("marktsegment_naam_nl", "marktsegmenten", "marktsegmentid=".$row_rs['foto_marktsegmentid']."")?></a>
			    	<?
			    }
			    ?>
				</td>
			    <td><center><?=ToonGereed($row_rs["foto_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
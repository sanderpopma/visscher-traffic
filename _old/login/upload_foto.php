<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$afbSize = $_GET["afbSize"];
$PgMode = "TOEVOEG";
$FrmTitel = "Afbeelding toevoegen";
if ($afbSize!="KLEIN" && $afbSize!="ART" && $afbSize!="GEEN") {$afbSize="HEEL";}
switch($afbSize){
	case "KLEIN":
		$afbFormaat = $GLOBALS['AfbeeldingMaxHeel'];
		$afbFormaatTonen = $GLOBALS['AfbeeldingMaxKlein'];
		$kiesURL = "kies_fotoklein2.php";
		break;
	case "ART":
		$afbFormaat = $GLOBALS['AfbeeldingMaxArtikel'];
		$afbFormaatTonen = $GLOBALS['AfbeeldingMaxArtikel'];
		$kiesURL = "kies_fotoart2.php";
		break;
	case "GEEN":
		$kiesURL = "kies_fotogeen2.php";
		break;
	default:
        $afbFormaat = $GLOBALS['AfbeeldingMaxHeel'];
        $afbFormaatTonen = $GLOBALS['AfbeeldingMaxHeelTonen'];
        $kiesURL = "kies_foto2.php";
} // switch
?>
<?=OpenPagina("CMS", "")?>

<form method="post" action="upload_foto2.php?OpenerFieldToUpdate=<?=$OpenerFieldToUpdate?>&afbSize=<?=$afbSize?>" name="afbeelding" enctype="multipart/form-data">
<?=OpenCMSTabel($FrmTitel)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("afbeeldingen", "Bestaande foto kiezen", $kiesURL . "?OpenerFieldToUpdate=" . $OpenerFieldToUpdate) ?>
    <?=ToonCMSNavKnop("folder_importeren", "Uploaden", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close();") ?>
<?=SluitCMSNavBalk()?>

<?=FrmHidden("MAX_FILE_SIZE", $GLOBALS['MaxBestandsGrootte'])?>
<? if ($afbSize=="GEEN") {
	?>
	<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding wordt niet automatisch verkleind");?>
	<?
}
else
{
	?>
	<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding wordt automatisch verkleind naar ". $afbFormaat . "px en afgebeeld op ". $afbFormaatTonen . "px");?>
	<?
}
?>
<tr>
	<td>Kies afbeelding</td>
	<td>:</td>
	<td><input class="Veld" type="file" name="afb_url" size="40"></td>
</tr>
<tr>
	<td colspan="2"></td><td>Let op: alleen JPG of GIF bestanden kunnen worden gebruikt</td>
</tr>

    <?=FrmSubmit("Uploaden")?>

<?=SluitCMSTabel()?>
</form>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("afbeelding");
frmvalidator.addValidation("afb_url","req","Kies een bestand");
</script>

<script language="javascript">
window.focus();
</script>
<script language="javascript">
    window.resizeTo(800, 220);
</script>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Vragen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe vraag", "cmsfaq-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="30"><b>Taal</b></td>
	<td width="30"><b>Nr.</b></td>
    <td><b>Vraag</b></td>
    <td><b>Categorie</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM faq ORDER BY faqtaalcode, volgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
	    $WijzigURL = "cmsfaq-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&faqid=" .$row_rs["faqid"];
	    $VerwijderURL = "cmsfaq-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&faqid=" .$row_rs["faqid"];
	    $VerwijderMelding = "Weet u zeker dat u deze vraag wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Vraag wijzigen")?></td>
			    <td><?=Icoon($row_rs["faqtaalcode"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["volgorde"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["vraag"]?></a></td>
			    <td><?=GeefDbWaarde("faqcat_naam", "faq_categorieen", "faqcatid=".$row_rs["faq_faqcatid"])?></td>
			    <td><center><?=ToonGereed($row_rs["publiceren"])?></td>
				<td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
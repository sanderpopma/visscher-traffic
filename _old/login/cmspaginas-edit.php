<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PAGINAS") then Response.End
if ($GLOBALS['pagid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
	$tmpPagId = $GLOBALS['pagid'];
	$tmpHmid = GeefPaginaHMID($tmpPagId);
	$tmpNLPagId = GeefDBWaarde("hoortbijnlpaginaid", "paginas", "paginaid=".$tmpHmid);
	if ($tmpNLPagId>0) { $tmpHmid = $tmpNLPagId; }
	if ($tmpPagId==$tmpHmid) { $IsHMNL=true; }
}
$HeeftSubPaginas=false;
$SubmenuVolgorde=0;

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM paginas WHERE paginaid=".$GLOBALS['pagid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['paginaid']>0)
    {
        $PaginaTitel = $row_rs['paginatitel'];
        $PaginaMenunaam = $row_rs['paginamenunaam'];
        $SubmenuVolgorde = $row_rs['submenuvolgorde'];
        $OpnemenInSubmenu = Int2Bool($row_rs['opnemeninsubmenu']);
        $Publiceren = Int2Bool($row_rs['publiceren']);
        $ValtOnderPaginaId = toInt($row_rs['valtonderpaginaid']);
        $HoortBijNLPaginaID = toInt($row_rs['hoortbijnlpaginaid']);
        $PaginaTaalcode = $row_rs['paginataalcode'];
        $Paginacode = $row_rs['paginacode'];
        $PaginaURL = $row_rs['paginaurl'];
		$kleur_basis = $row_rs['kleur_basis'];
		$kleur_tekst = $row_rs['kleur_tekst'];
        $paginanieuwvenster = Int2Bool($row_rs['paginanieuwvenster']);
        $hoofdmenuvaltop = Int2Bool($row_rs['hoofdmenuvaltop']);
        $PaginaGoogleTitel = $row_rs['paginagoogletitel'];
        $paginagooglebeschrijving = $row_rs['paginagooglebeschrijving'];
        $pagina_afbeeldingid = $row_rs['pagina_afbeeldingid'];
    	$HeeftSubPaginas = (TelRecords("SELECT * FROM paginas WHERE valtonderpaginaid=" . $GLOBALS['pagid'] . ";")>0);
    }
    mysql_free_result($rs);
}
else
{
    $query_rs = "SELECT MAX(submenuvolgorde) as maxnr FROM paginas WHERE valtonderpaginaid=".$GLOBALS['smid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $SubmenuVolgorde = toInt($row_rs['maxnr']);
        if ($SubmenuVolgorde<0) { $SubmenuVolgorde=0;}
	}
	mysql_free_result($rs);
    $query_rs = "SELECT * FROM paginas WHERE paginaid=".$GLOBALS['smid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['paginaid']>0)
    {
        $PaginaTaalcode = $row_rs['paginataalcode'];
	}
	mysql_free_result($rs);
	$paginanieuwvenster = false;
	$hoofdmenuvaltop = false;
    $SubmenuVolgorde=$SubmenuVolgorde+1;
    $OpnemenInSubmenu = true;
    $Publiceren = false;
    $ValtOnderPaginaId = 0;
    $pagina_afbeeldingid = -1;
    $kleur_basis = "#000000";
    $kleur_tekst = "#ffffff";
}
if (toInt($ValtOnderPaginaId)>0){
    $smid = $ValtOnderPaginaId;
}
else
{
    if ($GLOBALS['pagid']>0) {
        $smid = $GLOBALS['pagid'];
    }
}
if ($kleur_tekst=="") { $kleur_tekst="#ffffff"; }
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Pagina " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
	    <? if (1==2) { ?>
        <?=ToonCMSNavKnop("fotos", "Afbeeldingen (rechts)", "cmspagina-afbeeldingen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['pagid']."&pagid=".$GLOBALS['pagid']) ?>
    	<? } ?>
	    <? if ($IsHMNL==true) {
	    	?>
	    	<?=ToonCMSNavKnop("fotos", "Afbeeldingen bovenkant", "cmsbovenkant.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['pagid']."&parid=".$tmpHmid) ?>
    		<?
    	}
    	?>
    	<? if ($GLOBALS['MetCMSSubpaginas']==true) {
    		?>
	    	<?=ToonCMSNavKnop("toevoegen", "Nieuwe subpag.", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['pagid']) ?>
		    <? if ($HeeftSubPaginas==true) { ?>
		        <?=ToonCMSNavKnop("documenten", "Subpaginas", "cmspaginas.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['pagid']) ?>
		    <? } ?>
	    <?
    	}
    	?>
	    <?=ToonCMSNavKnop("zoeken", "Voorb.", "javascript:window.open('cmspaginas-voorbeeld.php?pagid=" . $GLOBALS['pagid'] . "', 'paginavoorbeeld');return false;") ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug", "cmspaginas.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmspaginas-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $GLOBALS['pagid'], "pagina", 700)?>
    <?=FrmTekstregel("Taal pagina", Icoon($PaginaTaalcode)) ?>
    <? if ($GLOBALS['smid']>0) { ?>
        <?=FrmTekstregel("Hoort bij", GeefDBWaarde("paginamenunaam", "paginas", "paginaid=" . $GLOBALS['smid'])) ?>
    <? } ?>
    <? if ($GLOBALS['MetPaginaCode']) { ?>
        <?=FrmText("Paginacode", "paginacode", $Paginacode, 40, 50) ?>
    <? } else { ?>
        <?=FrmHidden("paginacode", $Paginacode) ?>
    <? } ?>
    <?=FrmText("Titel pagina", "paginatitel", $PaginaTitel, 50, 100) ?>
    <?=FrmText("Naam in menu", "paginamenunaam", $PaginaMenunaam, 30, 50) ?>
    <? if ($ValtOnderPaginaId<1 && $PgMode=="WIJZIG") { ?>
    <? } else { ?>
	    <?=FrmCheckbox("Zichtbaar in menu", "opnemeninsubmenu", $OpnemenInSubmenu) ?>
	    <?=FrmText("Afbeeldvolgorde", "submenuvolgorde", $SubmenuVolgorde, 4, 4) ?>
    <? } ?>
    <?=FrmCheckbox("Publiceren", "publiceren", $Publiceren) ?>

   <? if ($IsHMNL==true && 1==2) { ?>
	    <?php echo FrmKopregel("Lay-out") ?>
	    <?php echo FrmTextKleur("Kleur Menu-knop", "kleur_basis", $kleur_basis, 10, 25) ?>
	    <?php echo FrmHidden("kleur_tekst", $kleur_tekst) ?>
	    <tr>
			<td>Voorbeeld</td>
			<td>:</td>
			<td><div style="width: 150px; height: 14px; padding: 2px; background-color: <?=$kleur_basis?>; color: <?=$kleur_tekst?>;">Voorbeeldtekst</div></td>
		</tr>
		<?=FrmHidden("hoofdmenuvaltop", false) ?>
    <? } ?>

    <? if ($IsHMNL==true && 1==2) {
    ?>

    <?=FrmKopregel("Lichtgroene achtergrondafbeelding achter tekst") ?>
	<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding moet vooraf exact op 700x600px worden gemaakt");?>
    <script type="text/javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotogeen.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("pagina_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("pagina_afbeeldingid", $pagina_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
$query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $pagina_afbeeldingid .";";
$rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
$row_rsafb = mysql_fetch_assoc($rsafb);
if ($row_rsafb['afbeeldingid']>0)
{
$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('pagina_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
$defVerwVisible = "visible";
}
else
{
$defVerwVisible = "hidden";
?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('pagina_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
}
mysql_free_result($rsafb);
?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script type="text/javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>

	<?
    }
    ?>
    <?=FrmKopregel("Extra instellingen") ?>
    <? if ($GLOBALS['MetPaginaURL']) { ?>
        <?=FrmText("Pagina URL", "paginaurl", $PaginaURL, 50, 150) ?>
    <? } else { ?>
    	<?=FrmHidden("paginaurl", $PaginaURL) ?>
    <? } ?>
    <?=FrmHidden("paginanieuwvenster", $paginanieuwvenster) ?>
    <?=FrmText("Titel t.b.v. zoekmachines", "paginagoogletitel", $PaginaGoogleTitel, 70, 255) ?>
    <?=FrmTextArea("Beschrijving t.b.v. zoekmachines", "paginagooglebeschrijving", $paginagooglebeschrijving, 5, 70) ?>
    <?
    	$txtHuidige = $GLOBALS['TitelWebsite'] . " - " . $PaginaTitel . " - " . $GLOBALS['SubTitelWebsite'];
    	if ($PaginaGoogleTitel!="") {
    		$txtHuidige = $PaginaGoogleTitel . " - " . $GLOBALS['TitelWebsite']  . " - " . $GLOBALS['SubTitelWebsite'];
    	}
    	$txtHuidige = substr($txtHuidige, 0, 65);
    	$txtHuidigeDescr = "";
    	if ($paginagooglebeschrijving!="") { $txtHuidigeDescr = substr($paginagooglebeschrijving, 0, 150); }
    ?>
	<tr valign="top"><td></td><td></td><td><i>Huidige:</i><br /><div style="border: solid 1px #cccccc; padding: 4px;"><span style="color: #0000ff; font-family: Arial; font-weight: normal; font-size: 16px; text-decoration: underline;"><?=$txtHuidige?></span><br /><span style="color: #000000; font-family: Arial; font-weight: normal; font-size: 12px;"><?=$txtHuidigeDescr?></span><br /><span style="color: #0e774a; font-family: Arial; font-weight: normal; font-size: 11px;"><?=$GLOBALS['ServerRoot']?>/<?=$GLOBALS['HMURL']?>?pagid=<?=$pagid?>&taalcode=<?=$PaginaTaalcode?></span></div></td></tr>


    <?=FrmSubmit("Opslaan")?>

    <tr>
    <td></td>
    <td></td>
    <td><br /><br />
        <?
        if ($PgMode=="WIJZIG"){
            if ($HoortBijNLPaginaID>0) {
                $WhereDeel = $HoortBijNLPaginaID;
            }
            else
			{
                $WhereDeel = $GLOBALS['pagid'];
            }
            ?>
            <?
            if ($HoortBijNLPaginaID>0 && $HoortBijNLPaginaID<>$GLOBALS['pagid']) {
            ?>
                <b>Pagina bewerken in het Nederlands:</b><br />
                <li><?=PlaatsIcoonLink("NL", "Bekijk/bewerk de pagina in het Nederlands", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $HoortBijNLPaginaID, "")?><br /></li>
            <?
			}
            else
            {
            ?>
                <?
			    $query_rs = "SELECT * FROM paginas WHERE hoortbijnlpaginaid=" . $WhereDeel . " AND paginataalcode<>'" . $PaginaTaalcode . "';";
			    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
			    $row_rs = mysql_fetch_assoc($rs);
			    if ($row_rs['paginaid']>0)
			    {
                    ?>
                    <br /><b>Deze pagina is ook beschikbaar in de volgende talen:</b><br />
                    <?
					do
					{
                    ?>
                        <li><?=PlaatsIcoonLink($row_rs['paginataalcode'], "Bekijk/bewerk de pagina in het " . GeefDBWaarde("taalnaam", "talen", "taalcode='" . $row_rs['paginataalcode'] . "'"), "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $row_rs['paginaid'], "")?></li><br />
                    <?
					}
					while ($row_rs = mysql_fetch_assoc($rs));
                    ?>
                    <?
                }
				mysql_free_result($rs);

			    $query_rs = "SELECT * FROM talen WHERE taalcode<>'NL' AND taalcode<>'" . $PaginaTaalcode . "' AND taalpubliceren<>0 AND taalcode NOT IN(SELECT paginataalcode FROM paginas WHERE hoortbijnlpaginaid=" . $WhereDeel . ") ORDER BY taalvolgorde;";
			    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
			    $row_rs = mysql_fetch_assoc($rs);
			    if ($row_rs['taalcode']!="")
			    {
                    ?>
                    <br /><b>Deze pagina aanmaken in een andere taal:</b><br />
                    <?
					do
					{
                    ?>
                        <li><?=Icoon($row_rs['taalcode'])?><a onclick="return confirm('Deze pagina aanmaken in het <?=$row_rs['taalnaam']?>?');" class="icoon" href="cmspaginas-vertaal.php?hmid=<?=$GLOBALS['hmid']?>&smid=<?=$GLOBALS['smid']?>&pagid=<?=$WhereDeel?>&naartaal=<?=$row_rs['taalcode']?>">Aanmaken in het <?=$row_rs['taalnaam']?></a></li><br />
                    <?
					}
					while ($row_rs = mysql_fetch_assoc($rs));
                    ?>
                    <?
                }
				mysql_free_result($rs);
            }
        }
        ?>
    </td>
    </tr>
<?=SluitForm()?>

<?=ZetFocus("paginatitel") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("pagina");
frmvalidator.addValidation("paginatitel","req","Titel is verplicht");
<? if ($GLOBALS['MetPaginaCode']==true) {
	?>
	frmvalidator.addValidation("paginacode","req","Paginacode is verplicht");
	<?
}
?>
<? if ($ValtOnderPaginaId<1 && $PgMode=="WIJZIG") {
	//
	}
	else
	{
	?>
	frmvalidator.addValidation("submenuvolgorde","numeric","Volgorde is ongeldig");
	<?
	}
?>
</script>

<?=SluitCMSTabel(); ?>
<br /><br />
<?
if ($PgMode=="WIJZIG"){
	ToonModuleBeheerLink($GLOBALS['pagid']);
}
?>

<script type="text/javascript">
function openSorter(page_type, sub_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&subtype=' + sub_type + '&paginaid=' + page_id , 'sorteren', 'width=820, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>
<?
if ($PgMode=="WIJZIG"){
	ToonParagrafenLijst("PAGINA", $GLOBALS['pagid']);
}
?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Afbeeldingen beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("delete", "Alle ongebruikte verwijderen", "cmsafbeeldingen-delalle.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Omschrijving</b></td>
    <td width="100" align="center"><b>Aantal</b></td>
</tr>

<?
$query_rs = "SELECT * FROM afbeelding_soorten ORDER BY afbsoort;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsafbeeldingenlijst.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&afbtype=" . $row_rs['afbsoort'];
	    $ItemNaam = $row_rs['afbsoortomschrijving'];
	    $AantAfb = TelRecords("SELECT * FROM afbeeldingen WHERE afbeeldingformaat='" . $row_rs['afbsoort'] . "'");
		if ($AantAfb<1) {
			$AantAfb=0;
		}
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, $ItemNaam)?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$ItemNaam?></a></td>
			    <td align="center"><?=$AantAfb?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
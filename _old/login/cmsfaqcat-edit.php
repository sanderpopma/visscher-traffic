<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "FAQ") then Response.End
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM faq_categorieen WHERE faqcatid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['faqcatid']>0)
    {
    	$faqcat_taalcode = $row_rs['faqcat_taalcode'];
        $faqcat_naam = $row_rs['faqcat_naam'];
        $faqcat_volgorde = toInt($row_rs['faqcat_volgorde']);
        $faqcat_publiceren = Int2Bool($row_rs['faqcat_publiceren']);
    }
    mysql_free_result($rs);
}
else
{
    $query_rs = "SELECT MAX(faqcat_volgorde) as maxnr FROM faq_categorieen";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $faqcat_volgorde = toInt($row_rs['maxnr']);
        if ($faqcat_volgorde<0) { $faqcat_volgorde=0;}
	}
	mysql_free_result($rs);
    $faqcat_volgorde=$faqcat_volgorde+1;
    $faqcat_publiceren = false;
    $faqcat_taalcode = $GLOBALS['StdTaalcode'];
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Categorie " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsfaqcat.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsfaqcat-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid'], "faq", 700)?>
    <?=FrmText("Volgorde", "faqcat_volgorde", $faqcat_volgorde, 4, 4) ?>
	<?=FrmSelectByQuery("Taal", "faqcat_taalcode", $faqcat_taalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>
    <?=FrmText("Categorienaam", "faqcat_naam", $faqcat_naam, 80, 100) ?>
    <?=FrmCheckbox("Publiceren", "faqcat_publiceren", $faqcat_publiceren) ?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("vraag") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("faq");
frmvalidator.addValidation("faqcat_naam","req","Naam is verplicht");
frmvalidator.addValidation("faqcat_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("faqcat_volgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
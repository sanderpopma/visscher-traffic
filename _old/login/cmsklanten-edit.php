<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM klanten WHERE klantid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['klantid']>0)
    {
        $heefttoegang = Int2Bool($row_rs["heefttoegang"]);
        $bedrijfsnaam = $row_rs["bedrijfsnaam"];
        $bedrijfsadres = $row_rs["bedrijfsadres"];
        $bedrijfspostcode = $row_rs['bedrijfspostcode'];
        $bedrijfsplaats = $row_rs['bedrijfsplaats'];
        $klantloginnaam = $row_rs['klantloginnaam'];
        $klantwachtwoord = $row_rs['klantwachtwoord'];
    }
    mysql_free_result($rs);
}
else
{
    $heefttoegang = false;
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Klant " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsklanten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsklanten-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid'], "klant", 700)?>
    <?=FrmText("Bedrijfsnaam", "bedrijfsnaam", $bedrijfsnaam, 60, 100) ?>
    <?=FrmText("Adres", "bedrijfsadres", $bedrijfsadres, 60, 100) ?>
    <?=FrmText("Postcode", "bedrijfspostcode", $bedrijfspostcode, 20, 50) ?>
    <?=FrmText("Plaats", "bedrijfsplaats", $bedrijfsplaats, 60, 100) ?>

    <?=FrmKopregel("Toegang webshop") ?>
    <?=FrmText("Loginnaam", "klantloginnaam", $klantloginnaam, 40, 50) ?>
    <?=FrmText("Wachtwoord", "klantwachtwoord", $klantwachtwoord, 40, 50) ?>
    <?=FrmCheckbox("Heeft toegang", "heefttoegang", $heefttoegang) ?>


    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("bedrijfsnaam") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("klant");
frmvalidator.addValidation("bedrijfsnaam","req","Bedrijfsnaam is verplicht");
frmvalidator.addValidation("bedrijfsplaats","req","Plaats is verplicht");
frmvalidator.addValidation("klantloginnaam","req","Loginnaam is verplicht");
frmvalidator.addValidation("klantwachtwoord","req","Wachtwoord is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
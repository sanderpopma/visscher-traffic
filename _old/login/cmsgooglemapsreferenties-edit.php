<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM googlemapsreferenties WHERE referentieid=".$GLOBALS['itemid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['referentieid']>0)
    {
        $ref_naam = $row_rs['ref_naam'];
        $ref_adres = $row_rs['ref_adres'];
        $ref_postcode = $row_rs['ref_postcode'];
        $ref_plaats = $row_rs['ref_plaats'];
        $ref_land = $row_rs['ref_land'];
        $ref_beschrijving = $row_rs['ref_beschrijving'];
        $ref_lng = $row_rs['ref_lng'];
        $ref_lat = $row_rs['ref_lat'];
        $ref_publiceren = Int2Bool($row_rs['ref_publiceren']);
        $ref_afbeeldingid = $row_rs['ref_afbeeldingid'];
    }
    mysql_free_result($rs);
}
else
{
    $ref_land = GeefDbWaarde("landnaam", "_landen", "land_taalcode='" . $GLOBALS['StdTaalcode']."' AND isstandaardland<>0 AND land_publiceren<>0");
    $ref_publiceren = true;
    $ref_afbeeldingid = -1;
}
$ref_afbeeldingid = toInt($ref_afbeeldingid);
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Referentie " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsgooglemapsreferenties.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsgooglemapsreferenties-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" . $GLOBALS['itemid'], "referenties", 700)?>
	<?=FrmKopregel("Adresgegevens")?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Naam referentie", "ref_naam", $ref_naam, 40, 100) ?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Adres", "ref_adres", $ref_adres, 40, 100) ?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Postcode", "ref_postcode", $ref_postcode, 20, 50) ?>
    <?=FrmOnEventText("onchange", "UpdateGMap()", "Plaats", "ref_plaats", $ref_plaats, 40, 100) ?>
    <?=FrmSelectByQuery("Land", "ref_land", $ref_land, "SELECT landnaam FROM _landen WHERE land_publiceren<>0", "landnaam", "landnaam", false)?>

	<?=FrmKopregel("Beschrijving")?>
    <?=FrmTextArea("Beschrijving", "ref_beschrijving", $ref_beschrijving, 4, 60) ?>

	<? if (1==1) {
		?>
	<?=FrmKopregel("Kleine afbeelding")?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoklein.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("ref_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("ref_afbeeldingid", $ref_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $ref_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('ref_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('ref_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>
	<?
	}
	?>
	<?=FrmKopregel("Google Maps")?>
    <?=FrmText("Lengtegraad", "ref_lng", $ref_lng, 20, 20) ?>
    <?=FrmText("Breedtegraad", "ref_lat", $ref_lat, 20, 20) ?>

    <?=FrmCheckbox("Zichtbaar", "ref_publiceren", $ref_publiceren) ?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("ref_naam") ?>

<SCRIPT type="text/javascript">
var frmvalidator  = new Validator("referenties");
frmvalidator.addValidation("ref_naam","req","Naam is verplicht");
frmvalidator.addValidation("ref_adres","req","Adres is verplicht");
frmvalidator.addValidation("ref_plaats","req","Plaats is verplicht");
frmvalidator.addValidation("ref_land","req","Land is verplicht");
</script>



<?=SluitCMSTabel(); ?>

<br /><br />

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=<?=$GLOBALS['GoogleMapsKey']?>" type="text/javascript"></script>

<div id="map" style="width: <?=$GLOBALS['BreedteWebsiteTekstdeel']-50?>px; height: 400px"></div>

    <script type="text/javascript">
    //<![CDATA[
	function UpdateGMap(){

	    if (GBrowserIsCompatible()) {

	      // A function to create the marker and set up the event window
	      // Dont try to unroll this function. It has to be here for the function closure
	      // Each instance of the function preserves the contends of a different instance
	      // of the "marker" and "html" variables which will be needed later when the event triggers.
	      function createMarker(point,html) {
	        var marker = new GMarker(point);
	        GEvent.addListener(marker, "click", function() {
	          marker.openInfoWindowHtml(html);
	        });
	        return marker;
	      }

	   var geocoder;

	   var restaurant = document.getElementById("ref_naam").value;
	   var address = document.getElementById("ref_adres").value + ', ' + document.getElementById("ref_postcode").value + ', ' + document.getElementById("ref_plaats").value + ', ' + document.getElementById("ref_land").value;

		// Create new geocoding object
	    geocoder = new GClientGeocoder();
		// Retrieve location information, pass it to addToMap()
	    geocoder.getLocations(address, addToMap);

		function addToMap(response)
		   {
		      // Retrieve the object
		      place = response.Placemark[0];

		      // Retrieve the latitude and longitude
		      point = new GLatLng(place.Point.coordinates[1],
		                          place.Point.coordinates[0]);

		      // Center the map on this point
		      map.setCenter(point, 7);

		      // Create a marker
		      //marker = new GMarker(point);
			  marker = createMarker(point,'<div style="width:240px">' + restaurant + '<br />' + address + '' + '<br /><\/div>');
				document.getElementById("ref_lat").value = place.Point.coordinates[1];
				document.getElementById("ref_lng").value = place.Point.coordinates[0];

		      // Add the marker to map
		      map.addOverlay(marker);

		      // Add address information to marker
		      //marker.openInfoWindowHtml(place.address);
		   }

	      // Display the map, with some controls and set the initial location
	      var map = new GMap2(document.getElementById("map"));
	      map.addControl(new GLargeMapControl());
	      map.addControl(new GMapTypeControl());
	      //map.setCenter(new GLatLng(43.907787,-79.359741),8);

	      // Set up three markers with info windows

	      //var point = new GLatLng(43.65654,-79.90138);
	      //var marker = createMarker(point,'<div style="width:240px">Some stuff to display in the First Info Window. With a <a href="http://www.econym.demon.co.uk">Link<\/a> to my home page<\/div>')
	      //map.addOverlay(marker);

	      //var point = new GLatLng(43.91892,-78.89231);
	      //var marker = createMarker(point,'Some stuff to display in the<br>Second Info Window')
	      //map.addOverlay(marker);

	      //var point = new GLatLng(43.82589,-79.10040);
	      //var marker = createMarker(point,'Some stuff to display in the<br>Third Info Window')
	      //map.addOverlay(marker);


	    }

	    // display a warning if the browser was not compatible
	    else {
	      alert("Sorry, the Google Maps API is not compatible with this browser");
	    }


	}

    //]]>
    UpdateGMap();
    </script>
<br /><br />
<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Foto kiezen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("folder_importeren", "Nieuwe foto uploaden", "upload_foto.php?afbSize=KLEIN&OpenerFieldToUpdate=" . $OpenerFieldToUpdate) ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close()") ?>
<?=SluitCMSNavBalk()?>

<script language="javascript">
    window.resizeTo(650, 500);

function kiesFoto(afb_ID, afb_Maand, afb_Jaar, afb_GUID, afb_Ext)
{
    window.opener.document.getElementById("<?=$OpenerFieldToUpdate?>").value = afb_ID;
    window.opener.document.getElementById("AfbMaand").value = afb_Maand;
    window.opener.document.getElementById("AfbJaar").value = afb_Jaar;
    window.opener.document.getElementById("AfbGUID").value = afb_GUID;
    window.opener.document.getElementById("AfbExt").value = afb_Ext;
    window.opener.toonFoto();
    window.close();
}
</script>

<tr class="kadervoet">
    <td><b>Kies een eerder gebruikte foto</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT *, YEAR(afbdatum) as afbjaar, MONTH(afbdatum) as afbmaand FROM afbeeldingen WHERE afbeeldinggereed<>0 AND afbeeldingformaat IN('KLEIN', 'HEEL') ORDER BY afbeeldingid;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
		$tmpdat = getdate($row_rs['afbdatum']);
	    $afbMaand = Maak2Dig($row_rs['afbmaand']);
	    $afbJaar = $row_rs['afbjaar'];
	    $afbGUID = $row_rs['afbeeldingguid'];
	    $afbExt = $row_rs['afbeeldingextentie'];
	    $afbID = $row_rs['afbeeldingid'];
	    $tmpWidth = "";
		if ($row_rs['afbeeldingformaat']!="KLEIN") {
			$tmpWidth = " width=".$GLOBALS['AfbeeldingMaxKlein']." ";
		}
		?>

		<tr class="regel"><td>
		    <a onclick="kiesFoto('<?=$afbID?>', '<?=$afbMaand?>', '<?=$afbJaar?>', '<?=$afbGUID?>', '<?=$afbExt?>');return false;" href=""><img <?=$tmpWidth?> src="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=$afbJaar?>/<?=$afbMaand?>/<?=$afbGUID?>.<?=$afbExt?>" class="border" /></a><br />
		    <hr class="streep" />
		</td></tr>


		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<br /><br />

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
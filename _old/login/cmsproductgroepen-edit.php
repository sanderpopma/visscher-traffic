<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['prodgrpid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
$prodgrp_volgorde=0;
$HeeftProducten = false;
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM productgroepen WHERE prodgrpid=".$GLOBALS['prodgrpid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['prodgrpid']>0)
    {
        $prodgrp_naam_nl = $row_rs['prodgrp_naam_nl'];
        $prodgrp_menunaam_nl = $row_rs['prodgrp_menunaam_nl'];
        $prodgrp_volgorde = $row_rs['prodgrp_volgorde'];
        $prodgrp_beschrijving_nl = $row_rs['prodgrp_beschrijving_nl'];
        $prodgrp_url_nl = $row_rs['prodgrp_url_nl'];
        $prodgrp_googletitel_nl = $row_rs['prodgrp_googletitel_nl'];
        $prodgrp_publiceren = Int2Bool($row_rs['prodgrp_publiceren']);
        $prodgrp_metwinkelwagen = Int2Bool($row_rs['prodgrp_metwinkelwagen']);
    }
    mysql_free_result($rs);
    $HeeftProducten = (toInt(TelRecords("SELECT * FROM producten WHERE pr_prodgrpid=".$GLOBALS['prodgrpid'].""))>0);
}
else
{
    $query_rs = "SELECT MAX(prodgrp_volgorde) as maxnr FROM productgroepen";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $prodgrp_volgorde = toInt($row_rs['maxnr']);
        if ($prodgrp_volgorde<0) { $prodgrp_volgorde=0;}
	}
	mysql_free_result($rs);
    $prodgrp_volgorde=$prodgrp_volgorde+1;
    $prodgrp_publiceren = false;
    $prodgrp_metwinkelwagen = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Productgroep " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
    	<?=ToonCMSNavKnop("toevoegen", "Nieuwe subcategorie", "cmsproducten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=".$GLOBALS['prodgrpid']) ?>
	    <? if ($HeeftProducten==true) { ?>
	        <?=ToonCMSNavKnop("documenten", "Toon subcategorieŽn", "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=".$GLOBALS['prodgrpid']) ?>
	    <? } ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsproductgroepen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsproductgroepen-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" . $GLOBALS['prodgrpid'], "productgroep", 700)?>
    <?=FrmText("Naam productgroep" , "prodgrp_naam_nl", $prodgrp_naam_nl, 60, 100) ?>
    <?=FrmText("Naam in menu" , "prodgrp_menunaam_nl", $prodgrp_menunaam_nl, 30, 50) ?>
    <?=FrmText("Afbeeldvolgorde", "prodgrp_volgorde", $prodgrp_volgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "prodgrp_publiceren", $prodgrp_publiceren) ?>

    <?=FrmHidden("prodgrp_url_nl", $prodgrp_url_nl) ?>
    <?=FrmHidden("prodgrp_googletitel_nl", $prodgrp_googletitel_nl) ?>

    <?=FrmKopregel("Instellingen webshop") ?>
    <?=FrmCheckbox("Met winkelwagen", "prodgrp_metwinkelwagen", $prodgrp_metwinkelwagen) ?>

    <?=FrmKopregel("Beschrijving") ?>
    <?=FrmFCKText("Beschrijving", "prodgrp_beschrijving_nl", $prodgrp_beschrijving_nl, 200) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("prodgrp_naam_nl") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("productgroep");
frmvalidator.addValidation("prodgrp_naam_nl","req","Naam is verplicht");
frmvalidator.addValidation("prodgrp_menunaam_nl","req","Naam in menu is verplicht");
frmvalidator.addValidation("prodgrp_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("prodgrp_volgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['avid']<1)
{
    redirect("cmsinfoaanvraag.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid'] . "&page=" . $GLOBALS['page']);
}
else
{
    $PgMode = "BEHANDEL";
}
if ($PgMode=="BEHANDEL")
{
    $query_rs = "SELECT * FROM informatieaanvragen WHERE aanvraagid=".$GLOBALS['avid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['aanvraagid']>0)
    {
        $datumontvangen = MaakDatum($row_rs["datumontvangen"]);
        $bedrijfsnaam = $row_rs["bedrijfsnaam"];
        $contactpersoon = $row_rs["contactpersoon"];
        $telefoonnummer = $row_rs['telefoonnummer'];
		$emailadres = $row_rs['emailadres'];
		$adres = $row_rs['adres'];
        $postcode = $row_rs['postcode'];
        $plaats = $row_rs['plaats'];
		$opmerkingen = $row_rs['opmerkingen'];
        $isafgehandeld = Int2Bool($row_rs["isafgehandeld"]);
    	$iscontactopgenomen = Int2Bool($row_rs["iscontactopgenomen"]);
    	$isbezoekgepland = Int2Bool($row_rs["isbezoekgepland"]);
    	$isofferteuitgebracht = Int2Bool($row_rs["isofferteuitgebracht"]);
    	$isopdrachtontvangen = Int2Bool($row_rs["isopdrachtontvangen"]);
	}
    mysql_free_result($rs);
}

?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Informatieaanvraag " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsinfoaanvraag.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&page=" . $GLOBALS['page']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsinfoaanvraag-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&avid=" . $GLOBALS['avid'] . "&page=" . $GLOBALS['page'], "informatieaanvragen", 700)?>
	<tr><td>Datum</td><td>:</td><td><?=$datumontvangen?></td></tr>
	<tr><td>Bedrijfsnaam</td><td>:</td><td><?=$bedrijfsnaam?></td></tr>
	<tr><td>Contactpersoon</td><td>:</td><td><?=$contactpersoon?></td></tr>
	<tr><td>Telefoonnummer</td><td>:</td><td><?=$telefoonnummer?></td></tr>
	<tr><td>E-mailadres</td><td>:</td><td><a href="mailto:<?=$emailadres?>"><?=$emailadres?></a></td></tr>
	<tr><td>Adres</td><td>:</td><td><?=$adres?></td></tr>
	<tr><td>Postcode</td><td>:</td><td><?=$postcode?></td></tr>
	<tr><td>Plaats</td><td>:</td><td><?=$plaats?></td></tr>
	<tr><td style="vertical-align: top;">Opmerkingen</td><td style="vertical-align: top;">:</td><td><?=$opmerkingen?></td></tr>
	<?=FrmKopregel("Status van deze aanvraag")?>
	<?=FrmCheckbox("Afgehandeld", "isafgehandeld", $isafgehandeld) ?>
	<?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=SluitCMSTabel(); ?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
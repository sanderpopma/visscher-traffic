<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) { die; }

$zoekcode = $_POST['zoekcode'];
$tekst_nl = $_POST['tekst_nl'];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO vertalingen (zoekcode, tekst_nl) VALUES( ";
$qry2 = "".SQLStr($zoekcode).", ".
	"".SQLStr($tekst_nl)."";
$qry3=")";
if ($GLOBALS['vertaalid']<1)
{
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

    $query_rs = "SELECT * FROM vertalingen ORDER BY vertaalid DESC";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['vertaalid']>0)
    {
		$GLOBALS['vertaalid'] = $row_rs['vertaalid'];
    }
    mysql_free_result($rs);
}

if ($GLOBALS['vertaalid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE vertalingen SET ";
	$qry2 = "zoekcode=".SQLStr($zoekcode).", ";

	$query_rs = "SELECT * FROM talen WHERE taalpubliceren<>0 AND taalcode<>'NL' ORDER BY taalvolgorde;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		do
		{
			$ltc = strtolower($row_rs['taalcode']);
			$vnm = "tekst_".$ltc;
			if ($_POST[$vnm]) {
				$qry2 .= "tekst_" . $ltc . "=".SQLStr($_POST[$vnm]).", ";
			}
		}
		while ($row_rs = mysql_fetch_assoc($rs));
	}
	mysql_free_result($rs);
	$qry2 .= "tekst_nl=".SQLStr($tekst_nl)."";
	$qry3=" WHERE vertaalid=".$GLOBALS['vertaalid']." LIMIT 1";

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
}

redirect("cmsvertalingen.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
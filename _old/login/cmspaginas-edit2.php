<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
// if Not HeeftRechten(GeefHuidigeUserId, "PAGINAS") then Response.End

$paginatitel = $_POST["paginatitel"];
$PaginaMenunaam = $_POST['paginamenunaam'];
$SubmenuVolgorde = $_POST['submenuvolgorde'];
$OpnemenInSubmenu = $_POST['opnemeninsubmenu'];
$hoofdmenuvaltop = $_POST['hoofdmenuvaltop'];
$Publiceren = $_POST['publiceren'];
$PaginaTaalcode = $_POST['paginataalcode'];
$Paginacode = $_POST['paginacode'];
$PaginaURL = Post2URL($_POST['paginaurl']);
$paginanieuwvenster = $_POST['paginanieuwvenster'];
//$PaginaQryString = $_POST['paginaqrystring'];
$PaginaGoogleTitel = $_POST['paginagoogletitel'];
$paginagooglebeschrijving = $_POST['paginagooglebeschrijving'];
$pagina_afbeeldingid = $_POST['pagina_afbeeldingid'];
$kleur_basis = $_POST['kleur_basis'];
$kleur_tekst = $_POST['kleur_tekst'];

$ledenloginnodig = false;

$ValtOnder = $GLOBALS['hmid'];
$query_rs = "SELECT * FROM adminmenu WHERE admmenuid=".$GLOBALS['hmid']."";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if ($row_rs['admmenuid']>0)
{
	if (strtoupper($row_rs['admmoduletype'])=="PAGINA") {
		$ValtOnder = $GLOBALS['smid'];
	}
}
mysql_free_result($rs);

if ($ValtOnder>0) {
	$query_rs = "SELECT * FROM paginas WHERE paginaid=".$ValtOnder."";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if ($row_rs['paginaid']>0)
	{
		$ledenloginnodig = Int2Bool($row_rs['ledenloginnodig']);
	}
	mysql_free_result($rs);
}
$PgMode = "TOEVOEG";
$qry1="INSERT INTO paginas (paginatitel, paginacode, kleur_basis, kleur_tekst, paginamenunaam, valtonderpaginaid, moduletype, paginataalcode, hoofdmenuvaltop, opnemeninsubmenu, submenuvolgorde, paginaurl, paginagoogletitel, paginagooglebeschrijving, paginanieuwvenster, pagina_afbeeldingid, ledenloginnodig, publiceren) VALUES( ";
$qry2 = "".SQLStr($paginatitel).", ".
	"".SQLStr($Paginacode).", ".
	"".SQLStr($kleur_basis).", ".
	"".SQLStr($kleur_tekst).", ".
	"".SQLStr($PaginaMenunaam).", ".
	"".SQLStr($ValtOnder).", ".
	"".SQLStr("pagina").", ".
	"".SQLStr($GLOBALS['StdTaalcode']).", ".
	"".SQLBool($hoofdmenuvaltop).", ".
	"".SQLBool($OpnemenInSubmenu).", ".
	"".SQLStr($SubmenuVolgorde).", ".
	"".SQLStr($PaginaURL).", ".
	"".SQLStr($PaginaGoogleTitel).", ".
	"".SQLStr($paginagooglebeschrijving).", ".
	"".SQLBool($paginanieuwvenster).", ".
	"".SQLStr($pagina_afbeeldingid).", ".
	"".SQLBool($ledenloginnodig).", ".
	"".SQLBool($Publiceren)."";
$qry3=")";

if ($GLOBALS['pagid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE paginas SET ";
	$qry2 = "paginatitel=".SQLStr($paginatitel).", ".
		"paginacode=".SQLStr($Paginacode).", ".
		"kleur_basis=".SQLStr($kleur_basis).", ".
		"kleur_tekst=".SQLStr($kleur_tekst).", ".
		"paginamenunaam=".SQLStr($PaginaMenunaam).", ".
		"hoofdmenuvaltop=".SQLBool($hoofdmenuvaltop).", ".
		"opnemeninsubmenu=".SQLBool($OpnemenInSubmenu).", ".
		"submenuvolgorde=".SQLStr($SubmenuVolgorde).", ".
		"paginaurl=".SQLStr($PaginaURL).", ".
		"paginagoogletitel=".SQLStr($PaginaGoogleTitel).", ".
		"paginagooglebeschrijving=".SQLStr($paginagooglebeschrijving).", ".
		"paginanieuwvenster=".SQLBool($paginanieuwvenster).", ".
		"pagina_afbeeldingid=".SQLStr($pagina_afbeeldingid).", ".
		"publiceren=".SQLBool($Publiceren)."";

	$qry3=" WHERE paginaid=".$GLOBALS['pagid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

if ($PgMode=="TOEVOEG")
{
    $query_rs = "SELECT * FROM paginas ORDER BY paginaid DESC";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['paginaid']>0)
    {
        $GLOBALS['pagid'] = toInt($row_rs["paginaid"]);
    }
    mysql_free_result($rs);
}
redirect("cmspaginas-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&pagid=".$GLOBALS['pagid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$openerfieldtoupdate = $_GET["openerfieldtoupdate"];
$opslaankiezen =  $_GET["opslaankiezen"];
?>
<?
OpenPagina("CMS", "");
?>
<script language="javascript">
function selecteerFotoboek(fb_id, fb_titel)
{
	parent.frames[0].selecteerFotoboek(fb_id, fb_titel);
	return false;

    if(parent.frames[0])
    {
    parent.window.opener.document.getElementById("<?=$openerfieldtoupdate?>").value = fb_id;
	if (parent.window.opener.document.getElementById("fotoboektitel")) {
		parent.window.opener.document.getElementById("fotoboektitel").innerHTML = fb_titel;
	}
    //window.opener.toonFotoboekVoorbeeld();
    parent.window.close();
    }
    else
    {
        return false;
    }
}
</script>
<?=OpenCMSTabel("Fotoboek kiezen of aanmaken")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("folder_nieuw", "Nieuw fotoboek", "kies_fotoboek-edit.php?openerfieldtoupdate=" . $openerfieldtoupdate) ?>
    <?=ToonCMSNavKnop("stop", "Sluit", "javascript:parent.window.close()") ?>
<?=SluitCMSNavBalk()?>
<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td style="font-size: 10px;" colspan="20">
    Kies een fotoboek uit de lijst of voeg een nieuwe toe
    </td>
</tr>
<tr class="kadernav">
    <td width="30" align="center">&nbsp;<?=Icoon("folder_open")?></td>
    <td width="100"><b>Datum</b></td>
    <td><b>Titel / omschrijving</b></td>
    <td width="80" align="center"><b>Foto's</b></td>
    <td width="100"><b>Wijzigen</b></td>
</tr>

<?
$GeenRes = true;
$query_rs = "SELECT * FROM fotoboeken ORDER BY fbdatum, fbtitel;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
$GeenRes = false;
do
{
	$trclass = "regel";
	$MagVerwijderen = true;
	if ($GLOBALS['fotoboekid']==$row_rs['fotoboekid']) {
		$trclass = "regelinv";
		$MagVerwijderen = false;
	}
	$AantGebruikt = TelRecords("SELECT * FROM paragrafen WHERE pa_fotoboekid=" . $row_rs['fotoboekid'] . ";");
	$VerwijderMelding = "Weet u zeker dat u dit fotoboek wilt verwijderen?";
		if ($AantGebruikt>0) {
			$VerwijderMelding = "Als u dit fotoboek verwijdert, worden ook alle verwijzingen (alineas) naar dit fotoboek verwijderd. Weet u zeker dat u dit fotoboek wilt verwijderen?";
		}
		else
		{
			$AantGebruikt = 0;
		}
	$AantFotos = TelRecords("SELECT * FROM fotoboeken_fotos WHERE fbf_fotoboekid=" . $row_rs['fotoboekid'] . ";");
	if ($AantFotos<1) { $AantFotos=0; }
	?>
<tr class="<?=$trclass?>">
    <td align="center"><a href="javascript:selecteerFotoboek(<?=$row_rs['fotoboekid']?>, '<?=$row_rs['fbtitel']?>')"><?=Icoon("pijl_links")?></a></td>
    <td><a href="javascript:selecteerFotoboek(<?=$row_rs['fotoboekid']?>, '<?=$row_rs['fbtitel']?>')"><?=MaakDatum($row_rs['fbdatum'])?></a></td>
    <td><a href="javascript:selecteerFotoboek(<?=$row_rs['fotoboekid']?>, '<?=$row_rs['fbtitel']?>')"><?=$row_rs['fbtitel']?></a></td>
    <td align="center"><?=$AantFotos?></td>
	<td><a href="kies_fotoboek-edit.php?fotoboekid=<?=$row_rs['fotoboekid']?>&openerfieldtoupdate=<?=$GLOBALS['openerfieldtoupdate']?>">Wijzigen</a></td>
    <!--
    <td>
    <? if ($MagVerwijderen==true) { ?>
        <a class="icoon" onclick="return confirm('<?=$VerwijderMelding?>');" href="bestanden-del.php?bestid=<?=$row_rs['bestandid']?>&bestguid=<?=$row_rs['bestandsguid']?>&folderid=<?=$mijnFolderId?>&openerfieldtoupdate=<?=$openerfieldtoupdate?>&openerfieldno=<?=$openerfieldno?>"><?=Icoon("delete")?></a>
    <? } ?>
    </td>
    -->
    <?
    if ($opslaankiezen==1 && $GLOBALS['fotoboekid']==$row_rs['fotoboekid']) {
    	?>
    	<script type="text/javascript">
    		selecteerFotoboek(<?=$row_rs['fotoboekid']?>, '<?=$row_rs['fbtitel']?>');
		</script>
    	<?
    }
    ?>
</tr>
<?
}
while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>
<?
if ($GeenRes==true) {
?>
<tr class="regel">
    <td></td>
    <td colspan="20">Er zijn nog geen fotoboeken</td>
</tr>
<?
}
?>

<?
SluitCMSTabel();
?>

<br /><br />

<script language="javascript">
window.focus();
</script>
<script language="javascript">
    window.resizeTo(800, 600);
</script>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}

$tariefdatum = $_POST["tariefdatum"];
$btwpercentage = $_POST["btwpercentage"];
$geenverzendkostenboven = $_POST["geenverzendkostenboven"];
$taremailadres = $_POST["taremailadres"];
$tarcallemailadres = $_POST["tarcallemailadres"];
$tarievenactief = $_POST["tarievenactief"];
$bedrijfsnaam = $_POST["bedrijfsnaam"];
$bedrijfstelnr = $_POST["bedrijfstelnr"];
$bedrijfsfaxnr = $_POST["bedrijfsfaxnr"];
$bedrijfsmobnr = $_POST["bedrijfsmobnr"];
$bedrijfsemail = $_POST["bedrijfsemail"];
$bedrijfsadres = $_POST["bedrijfsadres"];
$bedrijfspostcode = strtoupper($_POST["bedrijfspostcode"]);
$bedrijfsplaats = $_POST["bedrijfsplaats"];
$kvknummer = $_POST["kvknummer"];
$bedrijfswebsite = Post2URL($_POST["bedrijfswebsite"]);
$bedrijfslinkedin = Post2URL($_POST["bedrijfslinkedin"]);
$bedrijfstwitter = Post2URL($_POST["bedrijfstwitter"]);

$metroutelink = $_POST["metroutelink"];
$tar_lat = $_POST['tar_lat'];
$tar_lng = $_POST['tar_lng'];
$googleanalyticscode = $_POST['googleanalyticscode'];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO tarieven (taremailadres, tarcallemailadres, tariefdatum, btwpercentage, geenverzendkostenboven, bedrijfsnaam, bedrijfstelnr, bedrijfsfaxnr, bedrijfsmobnr, bedrijfsemail, bedrijfsadres, bedrijfspostcode, bedrijfsplaats, kvknummer, bedrijfswebsite, bedrijfslinkedin, bedrijfstwitter, metroutelink, tar_lat, tar_lng, googleanalyticscode, tarievenactief) VALUES( ";
$qry2 = "".SQLStr($taremailadres).", ".
	"".SQLStr($tarcallemailadres).", ".
	"".SQLDat($tariefdatum).", ".
	"".SQLDec($btwpercentage).", ".
	"".SQLDec($geenverzendkostenboven).", ".
	"".SQLStr($bedrijfsnaam).", ".
	"".SQLStr($bedrijfstelnr).", ".
	"".SQLStr($bedrijfsfaxnr).", ".
	"".SQLStr($bedrijfsmobnr).", ".
	"".SQLStr($bedrijfsemail).", ".
	"".SQLStr($bedrijfsadres).", ".
	"".SQLStr($bedrijfspostcode).", ".
	"".SQLStr($bedrijfsplaats).", ".
	"".SQLStr($kvknummer).", ".
	"".SQLStr($bedrijfswebsite).", ".
	"".SQLStr($bedrijfslinkedin).", ".
	"".SQLStr($bedrijfstwitter).", ".
	"".SQLBool($metroutelink).", ".
	"".SQLStr($tar_lat).", ".
	"".SQLStr($tar_lng).", ".
	"".SQLStr($googleanalyticscode).", ".
	"".SQLBool($tarievenactief)."";

$qry3=")";

if ($GLOBALS['tariefid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE tarieven SET ";
	$qry2 = "taremailadres=".SQLStr($taremailadres).", ".
		"tarcallemailadres=".SQLStr($tarcallemailadres).", ".
		"tariefdatum=".SQLDat($tariefdatum).", ".
		"btwpercentage=".SQLDec($btwpercentage).", ".
		"geenverzendkostenboven=".SQLDec($geenverzendkostenboven).", ".
		"bedrijfsnaam=".SQLStr($bedrijfsnaam).", ".
		"bedrijfstelnr=".SQLStr($bedrijfstelnr).", ".
		"bedrijfsfaxnr=".SQLStr($bedrijfsfaxnr).", ".
		"bedrijfsmobnr=".SQLStr($bedrijfsmobnr).", ".
		"bedrijfsemail=".SQLStr($bedrijfsemail).", ".
		"bedrijfsadres=".SQLStr($bedrijfsadres).", ".
		"bedrijfspostcode=".SQLStr($bedrijfspostcode).", ".
		"bedrijfsplaats=".SQLStr($bedrijfsplaats).", ".
		"kvknummer=".SQLStr($kvknummer).", ".
		"bedrijfswebsite=".SQLStr($bedrijfswebsite).", ".
		"bedrijfslinkedin=".SQLStr($bedrijfslinkedin).", ".
		"bedrijfstwitter=".SQLStr($bedrijfstwitter).", ".
		"metroutelink=".SQLBool($metroutelink).", ".
		"tar_lat=".SQLStr($tar_lat).", ".
		"tar_lng=".SQLStr($tar_lng).", ".
		"googleanalyticscode=".SQLStr($googleanalyticscode).", ".
		"tarievenactief=".SQLBool($tarievenactief)."";

	$qry3=" WHERE tariefid=".$GLOBALS['tariefid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

if ($PgMode=="TOEVOEG")
{
    $query_rs = "SELECT * FROM tarieven ORDER BY tariefid DESC";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['tariefid']>0)
    {
        $GLOBALS['tariefid'] = toInt($row_rs["tariefid"]);
    }
    mysql_free_result($rs);
}

if (Bool2Int($_POST['tarievenactief'])==1)
{
	$query_rs = "UPDATE tarieven SET tarievenactief=0 WHERE tariefid<>".$GLOBALS['tariefid']."";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
}
else
{
    $AantActief = TelRecords("SELECT * FROM tarieven WHERE tariefid<>" . $GLOBALS['tariefid'] . " AND tarievenactief=1");
    if ($AantActief<1)
    {
	    $query_rs = "UPDATE tarieven SET tarievenactief=1 WHERE tariefid=".$GLOBALS['tariefid']."";
	    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    }

}
redirect("cmstarieven.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
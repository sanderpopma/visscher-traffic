<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters

$MagVerder = true;

if (!strpos(@$_SERVER["HTTP_REFERER"], "cmsafbeeldingen.php")) {
	$MagVerder = false;
}

if ($MagVerder==true) {
	$query_rs = "SELECT * FROM afbeeldingen ORDER BY afbeeldingid;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		do
		{
			$MagVerwijderen = true;
			if (TelRecords("SELECT * FROM paragrafen WHERE pa_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM paginas WHERE pagina_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM googlemapsreferenties WHERE ref_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM artikelen WHERE art_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM afbeeldingen_boven WHERE ab_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM pagina_afbeeldingen WHERE paa_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}
			if (TelRecords("SELECT * FROM fotoboeken_fotos WHERE fbf_afbeeldingid=" . $row_rs['afbeeldingid'] . ";")>0) {
				$MagVerwijderen = false;
			}

			if ($MagVerwijderen==true) {
		        $AfbGUID = MaakGUIDString($row_rs['afbeeldingguid']);
		        $AfbMaand = Maak2Dig(datMonth($row_rs['afbdatum']));
		        $AfbJaar = datYear($row_rs['afbdatum']);
		        $AfbExt = $row_rs['afbeeldingextentie'];
		        $AfbPath = "../" . $GLOBALS['UploadImageFolder'] . "/" . $AfbJaar . "/" . $AfbMaand . "/" . $AfbGUID . "." . $AfbExt;
		        $AfbPathFull = "../" . $GLOBALS['UploadImageFolder'] . "/" . $AfbJaar . "/" . $AfbMaand . "/full_" . $AfbGUID . "." . $AfbExt;
				if (file_exists($AfbPath)) {
					echo $AfbPath . " verwijderd <br />";
					unlink($AfbPath);
				}
				if (file_exists($AfbPathFull)) {
					echo $AfbPathFull . " verwijderd <br />";
					unlink($AfbPathFull);
				}

				$query_rsa = "DELETE FROM afbeeldingen WHERE afbeeldingid=".$row_rs['afbeeldingid']."";
				$rsa = mysql_query($query_rsa, $GLOBALS['conn']) or die(mysql_error());

			}
		}
		while ($row_rs = mysql_fetch_assoc($rs));
	}
	mysql_free_result($rs);
}

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
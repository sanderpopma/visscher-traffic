<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$OpenerFieldToUpdate = $_GET["OpenerFieldToUpdate"];
$UploadURL = "upload_foto.php?afbSize=KLEIN&OpenerFieldToUpdate=" . $OpenerFieldToUpdate;
$KiesURL = "kies_fotoklein2.php?OpenerFieldToUpdate=" . $OpenerFieldToUpdate;
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Foto kiezen of uploaden")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close()") ?>
<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Wat wilt u doen?</b></td>
</tr>
<tr class="regel">
    <td><?=PlaatsIcoonLink("afbeeldingen", "", $KiesURL, "Eerder gebruikte foto kiezen")?></td>
    <td><a href="<?=$KiesURL?>">Een eerder gebruikte foto kiezen</a></td>
</tr>
<tr class="regel">
    <td><?=PlaatsIcoonLink("folder_importeren", "", $UploadURL, "Nieuwe foto uploaden")?></td>
    <td><a href="<?=$UploadURL?>">Een nieuwe foto uploaden</a></td>
</tr>

<?
SluitCMSTabel();
?>

<br /><br />

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
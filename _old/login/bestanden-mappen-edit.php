<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$onderfolderid = toInt($_GET["onderfolderid"]);
$openerfieldtoupdate = $_GET['openerfieldtoupdate'];
$openerfieldno = $_GET['openerfieldno'];

if ($GLOBALS['folderid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM bestanden_mappen WHERE mapid=".$GLOBALS['folderid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['mapid']>0)
    {
    	$mapnaam = $row_rs['mapnaam'];
    	$bm_valtondermapid = $row_rs['bm_valtondermapid'];
    }
	else
	{
		die;
	}
    mysql_free_result($rs);
}
else
{
	$bm_valtondermapid = $onderfolderid;
}
if ($bm_valtondermapid<1){ $bm_valtondermapid = 0; }
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Map " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
	<?=ToonCMSNavKnop("stop", "Annuleren", "bestanden.php?folderid=" . $onderfolderid . "&openerfieldtoupdate=" . $openerfieldtoupdate . "&openerfieldno=" . $openerfieldno) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("bestanden-mappen-edit2.php?folderid=" . $GLOBALS['folderid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate . "&openerfieldno=" . $openerfieldno . "&onderfolderid=" . $onderfolderid, "map", 500)?>
	<?=FrmHidden("bm_valtondermapid", $bm_valtondermapid)?>
    <?=FrmText("Naam Map", "mapnaam", $mapnaam, 50, 99) ?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("mapnaam") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("map");
frmvalidator.addValidation("mapnaam","req","Naam Map is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
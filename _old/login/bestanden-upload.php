<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$openerfieldtoupdate = $_GET["openerfieldtoupdate"];
$openerfieldno = $_GET["openerfieldno"];
$onderfolderid = toInt($_GET['onderfolderid']);
if ($onderfolderid<1){ $onderfolderid = 0; }

$FrmTitel = "Nieuw bestand uploaden";
?>
<?=OpenPagina("CMS Upload", "")?>

<form method="post" action="bestanden-upload2.php?openerfieldtoupdate=<?=$openerfieldtoupdate?>&openerfieldno=<?=$openerfieldno?>&onderfolderid=<?=$onderfolderid?>" name="bestand" enctype="multipart/form-data">
<?=OpenCMSTabel($FrmTitel)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("folder_importeren", "Uploaden", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "bestanden.php?folderid=" . $onderfolderid . "&openerfieldtoupdate=" . $openerfieldtoupdate."&openerfieldno=".$openerfieldno) ?>

<?=SluitCMSNavBalk()?>

<?=FrmHidden("MAX_FILE_SIZE", $GLOBALS['MaxBestandsGrootte'])?>
<?=FrmKoptekst("<span style='color: red';'>Let op: maximaal ". ($GLOBALS['MaxBestandsGrootte']/1000000) . "Mb");?>
<tr>
	<td>Kies bestand</td>
	<td>:</td>
	<td><input class="Veld" type="file" name="file_url" size="40"></td>
</tr>

    <?=FrmSubmit("Uploaden")?>

<?=SluitCMSTabel()?>
</form>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("bestand");
frmvalidator.addValidation("file_url","req","Kies een bestand");
</script>

<script language="javascript">
window.focus();
</script>
<script language="javascript">
    window.resizeTo(800, 220);
</script>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
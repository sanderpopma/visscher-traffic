<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}

$verzendwijzecode = $_POST['verzendwijzecode'];
$verzendwijzenaam_nl = $_POST['verzendwijzenaam_nl'];
$verzendwijzevolgorde = $_POST['verzendwijzevolgorde'];
$verzendwijzekosten = $_POST['verzendwijzekosten'];
$verzendwijzeactief = $_POST['verzendwijzeactief'];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO winkel_verzendwijzen (verzendwijzecode, verzendwijzenaam_nl, verzendwijzevolgorde, verzendwijzekosten, verzendwijzeactief) VALUES( ";
$qry2 = "".SQLStr($verzendwijzecode).", ".
	"".SQLStr($verzendwijzenaam_nl).", ".
	"".SQLStr($verzendwijzevolgorde).", ".
	"".SQLDec($verzendwijzekosten).", ".
	"".SQLBool($verzendwijzeactief)."";
$qry3=")";

if ($GLOBALS['itemid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE winkel_verzendwijzen SET ";
	$qry2 = "verzendwijzecode=".SQLStr($verzendwijzecode).", ".
		"verzendwijzenaam_nl=".SQLStr($verzendwijzenaam_nl).", ".
		"verzendwijzevolgorde=".SQLStr($verzendwijzevolgorde).", ".
		"verzendwijzekosten=".SQLDec($verzendwijzekosten).", ".
		"verzendwijzeactief=".SQLBool($verzendwijzeactief)."";

	$qry3=" WHERE verzendwijzeid=".$GLOBALS['itemid']." LIMIT 1";
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

redirect("cmsverzendwijzen.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['prodid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM producten WHERE productid=".$GLOBALS['prodid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['productid']>0)
    {
    	$pr_prodgrpid = $row_rs['pr_prodgrpid'];
        $productnaam_nl = $row_rs['productnaam_nl'];
        $productbeschrijving_nl = $row_rs['productbeschrijving_nl'];
        $pr_publiceren = Int2Bool($row_rs['pr_publiceren']);
        $pr_volgorde = $row_rs['pr_volgorde'];
    }
    mysql_free_result($rs);
    $HeeftArtikelen = (toInt(TelRecords("SELECT * FROM artikelen WHERE art_productid=".$GLOBALS['prodid'].""))>0);
}
else
{
    $pr_publiceren = false;
    $pr_prodgrpid = $GLOBALS['prodgrpid'];
    $query_rs = "SELECT MAX(pr_volgorde) as maxnr FROM producten WHERE pr_prodgrpid=".$GLOBALS['prodgrpid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $pr_volgorde = toInt($row_rs['maxnr']);
        if ($pr_volgorde<0) { $pr_volgorde=0;}
    }
    mysql_free_result($rs);
    $pr_volgorde=$pr_volgorde+1;
    $HeeftArtikelen = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Subcategorie " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
    	<?=ToonCMSNavKnop("toevoegen", "Nieuw artikel", "cmsartikelen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=".$GLOBALS['prodgrpid']. "&prodid=".$GLOBALS['prodid']) ?>
	    <? if ($HeeftArtikelen==true) { ?>
	        <?=ToonCMSNavKnop("documenten", "Toon artikelen", "cmsartikelen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=".$GLOBALS['prodgrpid']. "&prodid=".$GLOBALS['prodid']) ?>
	    <? } ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&prodgrpid=".$GLOBALS['prodgrpid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsproducten-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=".$GLOBALS['prodid'], "product", 700)?>
    <?=FrmText("Naam subcategorie", "productnaam_nl", $productnaam_nl, 60, 100) ?>
	<?=FrmSelectByQuery("Productgroep", "pr_prodgrpid", $pr_prodgrpid, "SELECT prodgrpid, prodgrp_naam_nl FROM productgroepen ORDER BY prodgrp_volgorde", "prodgrpid", "prodgrp_naam_nl", true)?>
    <?=FrmText("Afbeeldvolgorde", "pr_volgorde", $pr_volgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "pr_publiceren", $pr_publiceren) ?>

    <?=FrmKopregel("Beschrijving subcategorie") ?>
    <?=FrmFCKText("Beschrijving", "productbeschrijving_nl", $productbeschrijving_nl, 200) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("productnaam_nl") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("product");
frmvalidator.addValidation("productnaam_nl","req","Naam is verplicht");
frmvalidator.addValidation("pr_prodgrpid","req","Productgroep is verplicht");
frmvalidator.addValidation("pr_prodgrpid","gt=-1","Productgroep is verplicht");
frmvalidator.addValidation("pr_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("pr_volgorde","num","Volgorde heeft een ongeldige waarde");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
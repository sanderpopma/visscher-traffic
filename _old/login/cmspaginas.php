<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PAGINAS") then Response.End
if ($GLOBALS['hmid'] < 1) { $hmid = GeefEersteAdmHoofdmenuID(); }
if ($GLOBALS['smid'] < 1)
{
	if ($GLOBALS['MetCMSHoofdpaginas']==true) {
		$smid = 0;
	}
	else{
		$smid = GeefEersteHoofdmenuID($GLOBALS['StdTaalcode']);
	}
}
?>
<?
OpenPagina("CMS", "");
?>

<?
$IsSubPag = false;
$OnderPagId = -1;
$query_rs = "SELECT * FROM paginas WHERE paginaid=".$GLOBALS['smid'].";";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
    $PagBalkTitel = $row_rs["paginamenunaam"];
    if (toInt($row_rs["valtonderpaginaid"])>0)
    {
        $IsSubPag = true;
        $OnderPagId = $row_rs["valtonderpaginaid"];
    }
}
mysql_free_result($rs);
OpenCMSTabel("Pagina's beheren - " . $PagBalkTitel);
?>

<script language="javascript">
function openSorter(page_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&paginaid=' + page_id , 'sorteren', 'width=820, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>

<?=OpenCMSNavBalk()?>
	<?=ToonCMSNavKnop("sorteren", "Pagina's Sorteren", "javascript:openSorter('paginas', '" . $GLOBALS['smid'] . "');") ?>
	<? if ($IsSubPag==false) {
		if ($GLOBALS['smid']==0) {
		?>
	    	<?=ToonCMSNavKnop("toevoegen", "Nieuwe hoofdpagina", "cmspaginashm-add.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
		<?
		}
		else
		{
		?>
	    <?=ToonCMSNavKnop("toevoegen", "Nieuwe pagina", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
		<?
		}
	}
	else
	{
		?>
		<? if ($GLOBALS['MetCMSSubpaginas']==true) {
			?>
		    <?=ToonCMSNavKnop("toevoegen", "Nieuwe subpagina", "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'])?>
			<?
		}
		?>
	    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmspaginas.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $OnderPagId)?>
		<?
	}
	?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Titel</b></td>
    <? if ($MetPaginaCode==true) { ?>
        <td><b>Paginacode</b></td>
    <? } ?>
    <td width="100" align="center"><b>In Menu</b></td>
    <td><b>Naam in menu</b></td>
    <td width="70" align="center"><b>SEOttl</b></td>
    <td width="70" align="center"><b>SEOdesc</b></td>
	<?
	if ($GLOBALS['MetCMSSubpaginas']==true || ($GLOBALS['smid']==0 && $GLOBALS['MetCMSHoofdpaginas']==true)) {
	?>
	    <td><b>Sub's</b></td>
    <?
	}
	?>
    <td width="100"><b>Voorb</b></td>
    <td width="70" align="center"><b>Sort.</b></td>
    <td width="100" align="center"><b>Actief</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
ToonCMSPaginaRegels("SELECT * FROM paginas WHERE (paginaid=" . $GLOBALS['smid'] . ") ORDER BY submenuvolgorde, opnemeninsubmenu, paginatitel;", "sub");
if ($GLOBALS['smid']==0 && $GLOBALS['MetCMSHoofdpaginas']==true) {
	ToonCMSPaginaRegels("SELECT * FROM paginas WHERE hoofdmenuvolgorde>0 ORDER BY hoofdmenuvolgorde, opnemeninsubmenu, paginatitel;", "main");
}
else
{
	ToonCMSPaginaRegels("SELECT * FROM paginas WHERE (valtonderpaginaid=" . $GLOBALS['smid'] . ") ORDER BY submenuvolgorde, opnemeninsubmenu, paginatitel;", "sub");
}


function ToonCMSPaginaRegels($regels_sql, $main_type)
{
    //$sqlst = "SELECT * FROM paginas WHERE ((valtonderpaginaid=" . $GLOBALS['smid'] . ") OR (paginaid=" . $GLOBALS['smid'] . ")) ORDER BY submenuvolgorde, opnemeninsubmenu, paginatitel;";
	$rs = mysql_query($regels_sql, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		do
		{
        $WijzigURL = "cmspaginas-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $row_rs['paginaid'];
        $VerwijderURL = "cmspaginas-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&pagid=" . $row_rs['paginaid'];
        $VerwijderMelding = "Weet u zeker dat u deze pagina, inclusief de bijbehorende alinea's, wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
        $AantSub = TelRecords("SELECT * FROM paginas WHERE valtonderpaginaid=" . $row_rs['paginaid'] . ";");
        if ($AantSub<0) {
        	$AantSub=0;
        }
			$volgwrd = $row_rs['submenuvolgorde'];
			if ($main_type=="main") {
				$volgwrd = $row_rs['hoofdmenuvolgorde'];
			}
    ?>
    <tr class="regel">
        <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Pagina wijzigen")?></td>
        <td><a href="<?=$WijzigURL?>"><?=$row_rs['paginatitel']?></a></td>

        <? if ($MetPaginaCode==true) { ?>
            <td><a href="<?=$WijzigURL?>"><?=$row_rs['paginacode']?></a></td>
        <? } ?>
        <td align="center">
        <? if (toInt($row_rs['valtonderpaginaid'])<1) { ?>
            <?=Icoon("slotdicht")?>
        <? } else { ?>
            <?=ToonGereed($row_rs['opnemeninsubmenu'])?>
        <? } ?>
        </td>
        <td><?=$row_rs['paginamenunaam']?></td>
        <td align="center"><?=ToonGereed($row_rs['paginagoogletitel']!="")?></td>
        <td align="center"><?=ToonGereed($row_rs['paginagooglebeschrijving']!="")?></td>

		<?
		if ($GLOBALS['MetCMSSubpaginas']==true || $main_type=="main")
		{
		?>

	        <? if ($GLOBALS['smid']<>$row_rs['paginaid']) { ?>
	            <td><a href="cmspaginas.php?hmid=<?=$GLOBALS['hmid']?>&smid=<?=$row_rs['paginaid']?>"><?=$AantSub?>&nbsp;sub's</a></td>
	        <? } else { ?>
	            <td></td>
	        <? } ?>
        <? } ?>
        <td><?=Icoon("zoeken")?><a class="zwart" href="" onclick="window.open('cmspaginas-voorbeeld.php?pagid=<?=$row_rs['paginaid']?>', 'paginavoorbeeld');return false;">Vrb</a></td>
        <td align="center"><?=$volgwrd?></td>
        <td align="center"><?=ToonGereed($row_rs['publiceren'])?></td>
        <? if (strtoupper($row_rs['moduletype'])=="PAGINA" && ($row_rs['valtonderpaginaid']>0)) { ?>
            <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
        <? } else {?>
			<? if (strtoupper($row_rs['moduletype'])=="PAGINA" && ($AantSub==0)) { ?>
				<td><?=PlaatsVerwijderLink("Verw.", $VerwijderURL, $VerwijderMelding)?></td>
			<? } else {?>
            <td></td>
			<? } ?>
        <? } ?>
    </tr>
    <?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
}
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
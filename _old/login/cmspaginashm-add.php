<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PAGINAS") then Response.End
if ($GLOBALS['pagid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
	die;
}
$query_rs = "SELECT MAX(hoofdmenuvolgorde) as maxnr FROM paginas WHERE hoofdmenuvolgorde>0";
   $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
   $row_rs = mysql_fetch_assoc($rs);
   if ($row_rs['maxnr']>0)
   {
       $HoofdmenuVolgorde = toInt($row_rs['maxnr']);
       if ($HoofdmenuVolgorde<0) { $HoofdmenuVolgorde=0;}
}
mysql_free_result($rs);
   $PaginaTaalcode = $GLOBALS['StdTaalcode'];

$paginanieuwvenster = false;
$HoofdmenuVolgorde=$HoofdmenuVolgorde+1;
$Publiceren = false;
$ValtOnderPaginaId = 0;
$pagina_afbeeldingid = -1;
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Hoofdpagina " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmspaginas.php") ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmspaginashm-add2.php", "pagina", 700)?>
    <?=FrmTekstregel("Taal pagina", Icoon($PaginaTaalcode)) ?>
    <? if ($GLOBALS['MetPaginaCode']) { ?>
        <?=FrmText("Paginacode", "paginacode", $Paginacode, 40, 50) ?>
    <? } ?>
    <?=FrmText("Titel pagina", "paginatitel", $PaginaTitel, 60, 100) ?>
    <?=FrmText("Naam in menu", "paginamenunaam", $PaginaMenunaam, 30, 50) ?>
    <?=FrmText("Afbeeldvolgorde", "hoofdmenuvolgorde", $HoofdmenuVolgorde, 4, 4) ?>

    <?=FrmCheckbox("Publiceren", "publiceren", $Publiceren) ?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("paginatitel") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("pagina");
frmvalidator.addValidation("paginatitel","req","Titel is verplicht");
<? if ($GLOBALS['MetPaginaCode']==true) {
	?>
	frmvalidator.addValidation("paginacode","req","Paginacode is verplicht");
	<?
}
?>
frmvalidator.addValidation("hoofdmenuvolgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>
<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
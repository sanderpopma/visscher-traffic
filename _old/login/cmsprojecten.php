<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "PROJECTEN")) { die; }

if ($GLOBALS['projcatid']<1) {
	redirect("cmsprojectcat.php");
}
$ProjCatNaam = GeefDBWaarde("projcat_naam", "project_categorieen", "projcatid=".$GLOBALS['projcatid']);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Projecten in de categorie ".$ProjCatNaam)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw project", "cmsprojecten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&projcatid=".$GLOBALS['projcatid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsprojectcat.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="30"><b>Taal</b></td>
    <td><b>Categorie</b></td>
    <td><b>Naam project</b></td>
    <td><b>Subtitel</b></td>
    <td width="80" align="center"><b>Bekeken</b></td>
    <td width="80" align="center"><b>Verkocht</b></td>
    <td width="80" align="center"><b>Opgeleverd</b></td>
    <td width="80"><center><b>Zichtbaar</b></td>
    <td width="80"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM projecten WHERE proj_categorieid=".$GLOBALS['projcatid']." ORDER BY proj_naam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsprojecten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" .$row_rs["proj_categorieid"]."&projid=".$row_rs['projid'];
	    $VerwijderURL = "cmsprojecten-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" .$row_rs["proj_categorieid"]."&projid=".$row_rs['projid'];
	    $VerwijderMelding = "Weet u zeker dat u dit project wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Link wijzigen")?></td>
				<td><?=Icoon($row_rs['proj_taalcode'])?></td>
			    <td><?=$ProjCatNaam?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["proj_naam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["proj_subtitel"]?></a></td>
			    <td align="center"><?=$row_rs["proj_gelezen"]?></td>
			    <td><center><?=ToonGereed($row_rs["proj_verkocht"])?></td>
			    <td><center><?=ToonGereed($row_rs["proj_opgeleverd"])?></td>
			    <td><center><?=ToonGereed($row_rs["proj_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
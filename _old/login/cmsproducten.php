<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters

if ($GLOBALS['prodgrpid']<1) {
	redirect("cmsproductgroepen.php");
}
$ProdGrpNaam = GeefDBWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=".$GLOBALS['prodgrpid']);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Subcategorie�n in de groep ".$ProdGrpNaam)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe subcategorie", "cmsproducten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=".$GLOBALS['prodgrpid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsproductgroepen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Productgroep</b></td>
    <td><b>Naam subcategorie</b></td>
    <td><b>Artikelen</b></td>
    <td width="100"><center><b>Volorde</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM producten WHERE pr_prodgrpid=".$GLOBALS['prodgrpid']." ORDER BY pr_volgorde, productnaam_nl;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
    	$ArtTxt = "artikelen";
	    $WijzigURL = "cmsproducten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$row_rs["pr_prodgrpid"]."&prodid=".$row_rs['productid'];
	    $VerwijderURL = "cmsproducten-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$row_rs["pr_prodgrpid"]."&prodid=".$row_rs['productid'];
	    $VerwijderMelding = "Weet u zeker dat u deze subcategorie wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantArtikelen = toInt(TelRecords("SELECT * FROM artikelen WHERE art_productid=" . $row_rs['productid'].""));
	    $ArtURL = "cmsartikelen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodid=" .$row_rs["productid"]. "&prodgrpid=" .$row_rs["pr_prodgrpid"];
	    if ($AantArtikelen<1) {
	    	$MagVerwijderen = true;
	    	$AantArtikelen = 0;
	    }
	    if ($AantArtikelen==1) {
	    	$ArtTxt = "artikel";
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><?=$ProdGrpNaam?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["productnaam_nl"]?></a></td>
			    <td><a href="<?=$ArtURL?>"><?=$AantArtikelen?>&nbsp;<?=$ArtTxt?></a></td>
			    <td><center><?=$row_rs["pr_volgorde"]?></td>
			    <td><center><?=ToonGereed($row_rs["pr_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Inschrijvingen beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Profiel toevoegen", "cmsleden-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
	<td><b>Nickname</b></td>
    <td><b>Voorletters</b></td>
    <td><b>Achternaam</b></td>
    <td><b>Plaats</b></td>
    <td width="50"><center><b>Logins</b></td>
    <td width="50"><center><b>Al lid</b></td>
    <td width="50"><center><b>Goedgek.</b></td>
    <td width="50"><center><b>Actief</b></td>
    <td width="100"><center><b>Toegang t/m</b></td>
    <td width="90"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM inschrijvingen ORDER BY isgoedgekeurd, isactief DESC, achternaam, voornaam, nickname;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsleden-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&lidid=" .$row_rs["inschrijvingid"];
	    $VerwijderURL = "cmsleden-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&lidid=" .$row_rs["inschrijvingid"];
	    $VerwijderMelding = "Weet u zeker dat u dit profiel wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Profiel wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["nickname"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["voorletters"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["achternaam"]?></a></td>
			    <td><?=$row_rs['woonplaats']?></td>
			    <td><center><?=$row_rs["aantallogins"]?></td>
			    <td><center><?=ToonGereed($row_rs["isalingeschreven"])?></td>
			    <td><center><?=ToonGereed($row_rs["isgoedgekeurd"])?></td>
			    <td><center><?=ToonGereed($row_rs["isactief"])?></td>
			    <td><center><?=MaakDatum($row_rs["datumtoegangtot"])?></td>
				<td>
				<? if ($MagVerwijderen) {
					?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
					<?
				}
				else
				{
					?>
					<center><?=Icoon("stop")?></center>
					<?
				}
				?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
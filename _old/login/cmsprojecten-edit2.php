<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");

// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "PROJECTEN")) { die; }

$proj_categorieid = $_POST['proj_categorieid'];
$proj_naam = $_POST['proj_naam'];
$proj_subtitel = $_POST['proj_subtitel'];
$proj_kortebeschrijving = $_POST['proj_kortebeschrijving'];
$proj_publiceren = $_POST['proj_publiceren'];
$proj_taalcode = $_POST['proj_taalcode'];
$proj_datum = $_POST["proj_datum"];
$proj_prijs = $_POST["proj_prijs"];
$proj_verkocht = $_POST['proj_verkocht'];
$proj_datumverkocht = $_POST["proj_datumverkocht"];
$proj_afbeeldingid = $_POST["proj_afbeeldingid"];
$proj_opgeleverd = $_POST['proj_opgeleverd'];

$PgMode = "TOEVOEG";
$qry1="INSERT INTO projecten (proj_naam, proj_subtitel, proj_categorieid, proj_kortebeschrijving, proj_datum, proj_taalcode, proj_prijs, proj_verkocht, proj_datumverkocht, proj_opgeleverd, proj_afbeeldingid, proj_publiceren) VALUES( ";
$qry2 = "".SQLStr($proj_naam).", ".
	"".SQLStr($proj_subtitel).", ".
	"".SQLStr($proj_categorieid).", ".
	"".SQLStr($proj_kortebeschrijving).", ".
	"".SQLDat($proj_datum).", ".
	"".SQLStr($proj_taalcode).", ".
	"".SQLDec($proj_prijs).", ".
	"".SQLBool($proj_verkocht).", ".
	"".SQLDat($proj_datumverkocht).", ".
	"".SQLBool($proj_opgeleverd).", ".
	"".SQLStr($proj_afbeeldingid).", ".
	"".SQLBool($proj_publiceren)."";
$qry3=")";

if ($GLOBALS['projid']>0)
{
	$PgMode = "WIJZIG";
	$qry1="UPDATE projecten SET ";
	$qry2 = "proj_naam=".SQLStr($proj_naam).", ".
		"proj_subtitel=".SQLStr($proj_subtitel).", ".
		"proj_categorieid=".SQLStr($proj_categorieid).", ".
		"proj_kortebeschrijving=".SQLStr($proj_kortebeschrijving).", ".
		"proj_datum=".SQLDat($proj_datum).", ".
		"proj_taalcode=".SQLStr($proj_taalcode).", ".
		"proj_prijs=".SQLDec($proj_prijs).", ".
		"proj_verkocht=".SQLBool($proj_verkocht).", ".
		"proj_datumverkocht=".SQLDat($proj_datumverkocht).", ".
		"proj_opgeleverd=".SQLBool($proj_opgeleverd).", ".
		"proj_afbeeldingid=".SQLStr($proj_afbeeldingid).", ".
		"proj_publiceren=".SQLBool($proj_publiceren)."";

	$qry3=" WHERE projid=".$GLOBALS['projid'];
}

	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

$projcatid = $proj_categorieid;

if ($PgMode=="TOEVOEG")
{
	$query_rs = "SELECT * FROM projecten ORDER BY projid DESC";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if ($row_rs['projid']>0)
	{
		$GLOBALS['projid'] = toInt($row_rs["projid"]);
	}
	mysql_free_result($rs);
}
redirect("cmsprojecten-edit.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&projcatid=".$GLOBALS['projcatid']."&projid=".$GLOBALS['projid']);

//redirect("cmsprojecten.php?hmid=".$GLOBALS['hmid']."&smid=".$GLOBALS['smid']."&projcatid=".$GLOBALS['projcatid']);

include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
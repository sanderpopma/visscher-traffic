<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "PROJECTEN")) { die; }
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Project CategorieŽn")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe categorie", "cmsprojectcat-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Categorie</b></td>
    <td width="100"><b>Projecten</b></td>
    <td width="60" align="center"><b>Nieuw</b></td>
    <td width="60"><center><b>Volgorde</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM project_categorieen ORDER BY projcat_volgorde, projcat_naam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
    	$ProjTxt = "projecten";
	    $WijzigURL = "cmsprojectcat-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" .$row_rs["projcatid"];
	    $VerwijderURL = "cmsprojectcat-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" .$row_rs["projcatid"];
	    $VerwijderMelding = "Weet u zeker dat u deze categorie wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    $AantProjecten = toInt(TelRecords("SELECT * FROM projecten WHERE proj_categorieid=" . $row_rs['projcatid'].""));
	    $ProjectURL = "cmsprojecten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" .$row_rs["projcatid"];
	    if ($AantProjecten<1) {
	    	$MagVerwijderen = true;
	    	$AantProjecten = 0;
	    }
	    if ($AantProjecten==1) {
	    	$ProjTxt = "project";
	    }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Categorie wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["projcat_naam"]?></a></td>
			    <td><a href="<?=$ProjectURL?>"><?=$AantProjecten?>&nbsp;<?=$ProjTxt?></a></td>
				<td><?=PlaatsIcoonLink("toevoegen", "Nieuw project", "cmsprojecten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['=smid'] . "&projcatid=" .$row_rs["projcatid"]. "", "Nieuw project")?></td>
				<td><center><?=$row_rs["projcat_volgorde"]?></td>
			    <td><center><?=ToonGereed($row_rs["projcat_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
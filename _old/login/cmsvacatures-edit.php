<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['vacid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM vacatures WHERE vacatureid=".$GLOBALS['vacid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['vacatureid']>0)
    {
        $vacdatumgeplaatst = MaakDatum($row_rs["vacdatumgeplaatst"]);
        $vacpubliceren = Int2Bool($row_rs["vacpubliceren"]);
        $vactitel = $row_rs["vactitel"];
        $vacinleiding = $row_rs["vacinleiding"];
        $vactaalcode = $row_rs['vactaalcode'];
        $vacaantaluur = $row_rs['vacaantaluur'];
        $vacstandplaats = $row_rs['vacstandplaats'];
        $vacprovincie = $row_rs['vacprovincie'];
    }
    mysql_free_result($rs);
}
else
{
    $vacdatumgeplaatst = MaakDatum(Now);
    $vacpubliceren = false;
    $vactitel = "";
    $vacinleiding = "";
    $vactaalcode = $GLOBALS['StdTaalcode'];
    $vacaantaluur = "";
    $vacstandplaats = "";
    $vacprovincie = "";
}
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Vacature " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsvacatures.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsvacatures-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vacid=" . $GLOBALS['vacid'], "vacature", 700)?>
    <?=FrmText("Titel / Functienaam", "vactitel", $vactitel, 60, 255) ?>
    <?=FrmTextArea("Inleiding vacature", "vacinleiding", $vacinleiding, 5, 60) ?>
	<?=FrmSelectByQuery("Taal", "vactaalcode", $vactaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>
	<? if ($GLOBALS['MetVacaturesUitgebreid']==true) {
		?>
	    <?=FrmText("Aantal uur", "vacaantaluur", $vacaantaluur, 40, 50) ?>
	    <?=FrmText("Standplaats", "vacstandplaats", $vacstandplaats, 60, 100) ?>
	    <?=FrmText("Provincie", "vacprovincie", $vacprovincie, 40, 100) ?>
		<?
	}
	?>
    <?=FrmDatum("Datum geplaatst", "vacdatumgeplaatst", $vacdatumgeplaatst) ?>
    <?=FrmCheckbox("Publiceren", "vacpubliceren", $vacpubliceren) ?>


    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("vactitel") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("vacature");
frmvalidator.addValidation("vactitel","req","Titel is verplicht");
frmvalidator.addValidation("vacinleiding","req","Inleiding is verplicht");
frmvalidator.addValidation("vactaalcode","req","Taal is verplicht");
frmvalidator.addValidation("vacdatumgeplaatst","req","Datum is verplicht");
frmvalidator.addValidation("vacdatumgeplaatst","date","Datum heeft een ongeldige waarde");
</script>


<?=SluitCMSTabel(); ?>

<br /><br />

<script language="javascript">
function openSorter(page_type, sub_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&subtype=' + sub_type + '&paginaid=' + page_id , 'sorteren', 'width=820, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>
<?
if ($PgMode=="WIJZIG"){
    ToonParagrafenLijst("VACATURE", $GLOBALS['vacid']);
}
?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
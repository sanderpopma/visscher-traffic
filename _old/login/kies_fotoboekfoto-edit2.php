<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$openerfieltoupdate = $_GET["openerfieltoupdate"];

if ($GLOBALS['fotoid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="TOEVOEG") {
	$afbSize="HEEL";

	$afbformaat = $afbSize;
	$resizetype="wh";
	$veldguid = "AfbGUID";
	$veldext = "AfbExt";
	$veldmaand = "AfbMaand";
	$veldjaar = "AfbJaar";

	$afbtype = $_FILES['afb_url']['type'];
	$afbnaam = $_FILES['afb_url']['name'];

	$afbresizemode = "";

	$afbpxsize = $GLOBALS['AfbeeldingMaxHeel'];

	if (toInt($_FILES['afb_url']['size'])<1)
	{
	echo "Bestand is te groot (maximaal ".($GLOBALS['MaxBestandsGrootte']/1000)."Kb)";
	echo "<br /><a href='javascript:history.back();'>Terug</a>";
	die;
	}

	$magverder=false;
	if ($magverder==false && toInt(strpos($afbtype, "jpeg"))>0) $magverder=true;
	if ($magverder==false && toInt(strpos($afbtype, "jpg"))>0) $magverder=true;
	if ($magverder==false && toInt(strpos($afbtype, "gif"))>0) $magverder=true;
	if ($magverder==false && toInt(strpos($afbnaam, "jpg"))>0) $magverder=true;

	if($magverder==false)
	{
	echo "Geen JPG of GIF gebruikt<br>";
	echo $afbtype."<br>";
	echo $afbnaam."<br>";
	echo $_FILES['afb_url']['size']."<br />";
	echo "<br /><a href='javascript:history.back();'>Terug</a>";
	die;
	}
	switch($afbtype)
	{
		case "image/gif":
			$bestype="gif";
			break;
		case "image/jpeg":
			$bestype="jpg";
			break;
		case "image/jpg":
			$bestype="jpg";
			break;
		case "image/pjpeg":
			$bestype="jpg";
			break;
	}

    if (is_uploaded_file($_FILES['afb_url']['tmp_name']))
	{
		$afbguid = MaakGUID(GeefGUID());
		$query_rs = "INSERT INTO afbeeldingen(afbeeldingguid, afbdatum) VALUES('" . $afbguid . "', NOW())";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

		$query_rs = "SELECT * FROM afbeeldingen WHERE afbeeldingguid='" . $afbguid . "' ORDER BY afbeeldingid DESC";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
		$row_rs = mysql_fetch_assoc($rs);
		$totalRows_rs = mysql_num_rows($rs);
		$afbid = $row_rs['afbeeldingid'];
		$afbmaand = Maak2Dig(datMonth($row_rs['afbdatum']));
		$afbjaar = datYear($row_rs['afbdatum']);
		mysql_free_result($rs);

		$query_rs = "UPDATE afbeeldingen SET afbeeldingnaam=".SQLStr($afbnaam).", uploadgereed=1, afbeeldinggereed=1, afbeeldingextentie=" . SQLStr($bestype) . ", afbeeldingformaat='$afbformaat' WHERE afbeeldingid=".$afbid."";
		$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

			MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar");
			MaakDir("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand");
	        copy($_FILES['afb_url']['tmp_name'], "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);

		list($width, $height, $type, $attr) = getimagesize("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
		$MoetUitvoeren = false;
		if ($width>$afbpxsize || $height>$afbpxsize) {
			$MoetUitvoeren = true;
		}
		if ($afbSize!="GEEN" && $MoetUitvoeren==true)
		{
			include_once("resizeimage.inc.php");
			$rimg=new RESIZEIMAGE("../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
			echo $rimg->error();
			//$rimg->resize_percentage(50);
			if ($afbresizemode=="w")
			{
				$rimg->resize_limitwh($GLOBALS['AfbeeldingMaxHeelTonen'],$GLOBALS['AfbeeldingMaxHeelTonen'], "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/full_".$afbguid.".".$bestype);
				$rimg->resize_fixedw($afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
			}
			else
			{
				if ($resizetype=="hw")
				{
					$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
				}
				else
				{
					$rimg->resize_limitwh($afbpxsize,$afbpxsize, "../" . $GLOBALS['UploadImageFolder'] . "/$afbjaar/$afbmaand/".$afbguid.".".$bestype);
				}
			}
			$rimg->close();

	    }
	}

	$qry1="INSERT INTO fotoboeken_fotos (fbf_fotoboekid, fbf_afbeeldingid, fbf_onderschrift) VALUES( ";
	$qry2 = "".SQLStr($GLOBALS['fotoboekid']).", ".
	"".SQLStr($afbid).", ".
	"".SQLStr($_POST['fbf_onderschrift'])."";
	$qry3=")";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

	redirect("kies_fotoboekfoto-edit.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate."&melding=ja");
}
else
{
	// wijzig
	$qry1="UPDATE fotoboeken_fotos SET ";
	$qry2 = "fbf_onderschrift=".SQLStr($_POST['fbf_onderschrift'])."";

	$qry3=" WHERE fotoboekfotoid=".$GLOBALS['fotoid']." LIMIT 1";
	$query_rs = $qry1.$qry2.$qry3;
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());

	redirect("kies_fotoboek-edit.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate);
?>
<?
}
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
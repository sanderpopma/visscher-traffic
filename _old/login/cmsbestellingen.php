<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Bestellingen")?>
<?=OpenCMSNavBalk()?>

<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="60"><b>Type</b></td>
    <td width="100"><center><b>Status</b></td>
    <td width="96"><b>Datum</b></td>
    <td><b>Bedrijfsnaam</b></td>
    <td><b>T.a.v.</b></td>
    <td><b>Plaats</b></td>
</tr>

<?
$query_rs = "SELECT * FROM winkel_bestellingen INNER JOIN winkel_bestelstatus ON winkel_bestelstatus.bestelstatuscode=winkel_bestellingen.bestelstatus ORDER BY bestelstatusvolgorde, be_datumtoegevoegd DESC, bestellingid DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = false;
	    $WijzigURL = "cmsbestellingen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["bestellingid"];
	    $VerwijderURL = "cmsbestellingen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["bestellingid"];
	    $VerwijderMelding = "Weet u zeker dat u deze bestelling wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		$mystatus = $row_rs['bestelstatusvolgorde'];
		if ($mystatus>1) {
			$MagVerwijderen = false;
		}
		if ($row_rs['bestelstatus']=="beh" || $row_rs['bestelstatus']=="ger") {
			$MagVerwijderen = false;
		}
		switch($mystatus){
			case 1:
				$mycol = $GLOBALS['KleurStatusGeel'];
				break;
			case 2:
				$mycol = $GLOBALS['KleurStatusOranje'];
				break;
			case 4:
				$mycol = $GLOBALS['KleurStatusGroen'];
				break;
			case 9:
				$mycol = $GLOBALS['KleurStatusRood'];
				break;
			default:
				$mycol = $GLOBALS['KleurRegelInv'];;
		} // switch
		?>
			<tr class="regel">
			    <td style="background-color: <?=$mycol?>;"><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><?=$row_rs["be_type"]?></td>
			    <td><center><?=$row_rs["bestelstatusnaam_nl"]?></td>
			    <td><a href="<?=$WijzigURL?>"><?=MaakDatum($row_rs["be_datumtoegevoegd"])?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsnaam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["tav"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["afleverplaats"]?></a></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
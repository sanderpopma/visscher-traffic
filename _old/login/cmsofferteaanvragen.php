<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Offerteaanvragen")?>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="100"><b>Datum</b></td>
    <td><b>Bedrijf</b></td>
    <td><b>Contactpersoon</b></td>
    <td><b>Telefoonnummer</b></td>
    <td width="100" align="center"><b>Behandeld</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM offerteaanvragen ORDER BY datumontvangen DESC;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsofferteaanvragen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&oaid=" .$row_rs["offerteaanvraagid"];
	    $VerwijderURL = "cmsofferteaanvragen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&oaid=" .$row_rs["offerteaanvraagid"];
	    $VerwijderMelding = "Weet u zeker dat u deze aanvraag wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Bekijken")?></td>
			    <td><?=MaakDatum($row_rs["datumontvangen"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsnaam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["contactpersoon"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["telefoon"]?></a></td>
			    <td align="center"><?=ToonGereed($row_rs["isafgehandeld"])?></td>
			    <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
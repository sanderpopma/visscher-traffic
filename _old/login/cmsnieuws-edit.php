<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if ($GLOBALS['nwsid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM nieuws WHERE nieuwsid=".$GLOBALS['nwsid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['nieuwsid']>0)
    {
        $nwsdatum = MaakDatum($row_rs["nwsdatum"]);
        $nwspubliceren = Int2Bool($row_rs["nwspubliceren"]);
        $nwstitel = $row_rs["nwstitel"];
        $nwsinleiding = $row_rs["nwsinleiding"];
        $nwstaalcode = $row_rs['nwstaalcode'];
        $nwsafbeeldingid = $row_rs['nwsafbeeldingid'];
    }
    mysql_free_result($rs);
}
else
{
    $nwsdatum = MaakDatum(Now);
    $nwspubliceren = false;
    $nwstitel = "";
    $nwsinleiding = "";
    $nwstaalcode = $GLOBALS['StdTaalcode'];
    $nwsafbeeldingid = -1;
}
$nwsafbeeldingid = toInt($nwsafbeeldingid);
?>
<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Nieuws " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsnieuws.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsnieuws-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&nwsid=" . $GLOBALS['nwsid'], "nieuws", 700)?>
    <?=FrmText("Titel bericht", "nwstitel", $nwstitel, 60, 100) ?>
	<tr class='regel' valign='top'><td>Inleiding (max. 250)</td><td>:</td><td><textarea class='Veld' id='nwsinleiding' name='nwsinleiding' rows='5' cols='60' onKeyDown='limitText(this,250);' onKeyUp='limitText(this,250);'><?=$nwsinleiding?></textarea></td></tr>
	<tr><td colspan="2"></td><td style="font-size: 10px;">Schrijf een korte inleiding (max. 250 karakters) en voeg na Opslaan een Alinea toe van het type Tekst (zie onder, gebruik de knop Nieuwe Alinea)</td></tr>

	<?=FrmSelectByQuery("Taal", "nwstaalcode", $nwstaalcode, "SELECT taalcode, taalnaam FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde", "taalcode", "taalnaam", false)?>

    <?=FrmDatum("Datum", "nwsdatum", $nwsdatum) ?>
    <?=FrmCheckbox("Publiceren", "nwspubliceren", $nwspubliceren) ?>

	<? if (1==2) {
		?>
	<?=FrmKopregel("Kleine afbeelding (nieuwslijst)")?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoklein.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("nwsafbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("nwsafbeeldingid", $nwsafbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $nwsafbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('nwsafbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('nwsafbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>
	<?
	}
	?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("nwstitel") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("nieuws");
frmvalidator.addValidation("nwstitel","req","Titel is verplicht");
frmvalidator.addValidation("nwsinleiding","req","Inleiding is verplicht");
frmvalidator.addValidation("nwstaalcode","req","Taal is verplicht");
frmvalidator.addValidation("nwsdatum","req","Datum is verplicht");
frmvalidator.addValidation("nwsdatum","date","Datum heeft een ongeldige waarde");
</script>


<?=SluitCMSTabel(); ?>

<br /><br />

<script language="javascript">
function openSorter(page_type, sub_type, page_id)
{
    result = window.open('sorteren.php?paginatype=' + page_type + '&subtype=' + sub_type + '&paginaid=' + page_id , 'sorteren', 'width=820, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
}
</script>
<?
if ($PgMode=="WIJZIG"){
    ToonParagrafenLijst("NIEUWS", $GLOBALS['nwsid']);
}
?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
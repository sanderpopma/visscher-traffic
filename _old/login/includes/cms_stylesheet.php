<?
$CMSImgRoot = $GLOBALS['ApplicatieRoot'] . "/login/cmsimg";

//Basis kleurenpalet
$KleurNavBalk = "#DDDDDD";
$KleurTitelBalk = "#EBF7FA";
$KleurNavKnop = "#EEEEEE";
$KleurNavKnopOver = "#FFFFCC";
$KleurOnderBalk = $GLOBALS['KleurNavKnop'];
$KleurSybitBlauw = "#026495";
$KleurSybitLichtBlauw = "#7dafc9";
$KleurSybitWit = "#FFFFFF";

$KleurBGWebsite = "#ffffff";

//CMS
$HoogteCMSHMKnoppen = 24;
$BreedteCMSSubmenu = 170;
$HoogteCMSBoven = 106;
$KleurCMSSubmenu = "#DDDDDD";
$KleurCMSSubmenuOver = "#EEEEEE";
$KleurCMSSubmenuSel = "#FFEFCF";
$KleurCMSSubmenuBorder = "#CCCCCC";
$KleurKnoppen = $GLOBALS['KleurSybitBlauw'];
$TekstKleurKnoppen = $GLOBALS['KleurSybitWit'];

//Bovenframe
$KleurCMSBoven1 = $GLOBALS['KleurSybitBlauw'];
$KleurCMSBoven2 = $GLOBALS['KleurSybitBlauw'];

//Kalender
$KalenderRandKleur = "#666666";
$KalenderLetterType = "Arial";
$KalenderLetterGrootte = "11px";
$KalenderBovenKleur = "#D4D0C8";
$KalenderTekstBovenMaandJaar = "#000000";
$KalenderAchtergrondKleur = $GLOBALS['KleurWit'];
$KalenderOnderKleur = "#666666";
?>
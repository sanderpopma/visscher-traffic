<?
function ToonModuleBeheerLink($pag_id){
    switch(strtoupper(GeefPaginaModuleType($pag_id)))
    {
        case "NIEUWS":
            $linkurl = "cmsnieuws";
            $linktxt = "nieuwsberichten";
            break;
        case "VACATURES":
            $linkurl = "cmsvacatures";
            $linktxt = "vacatures";
            break;
        case "CONTACT":
            $linkurl = "cmstarieven";
            $linktxt = "de contactgegevens";
            break;
        case "GOOGLEMAPSREFERENTIES":
            $linkurl = "cmsgooglemapsreferenties";
            $linktxt = "referenties";
            break;
        case "PROJECTEN":
            $linkurl = "cmsprojecten";
			$linktxt = "projecten";
            break;
        default:
        	//echo GeefPaginaModuleType($pag_id);
            $linkurl = "";
    }
    if ($linkurl!="") {
    	echo "<center><div style='width: 350px; border: solid 1px black; text-align: center; padding: 2px; background-color: #ffffda;'>".Icoon("wijzigen");
    	echo "<a href='" . $linkurl . ".php'>Klik hier om " . $linktxt . " te beheren</a></div></center>";
    	echo "<br /><br />";
    }
}
function ToonParagrafenLijst($Type_Lijst, $Item_Id)
{

    switch(strtoupper($Type_Lijst))
    {
        case "NIEUWS":
            $tmpTabelTitel = "bij dit nieuwsbericht";
            $tmpQryDeel = "pa_nieuwsid";
            break;
        case "VACATURE":
            $tmpTabelTitel = "bij deze vacature";
            $tmpQryDeel = "pa_vacatureid";
            break;
        case "EVENT":
            $tmpTabelTitel = "bij dit evenement";
            $tmpQryDeel = "pa_eventid";
            break;
        case "PROJECT":
            $tmpTabelTitel = "bij dit project";
            $tmpQryDeel = "pa_projectid";
            break;
        default:
            $Type_Lijst = "PAGINA";
            $tmpTabelTitel = "bij deze pagina";
            $tmpQryDeel = "pa_paginaid";
    }
?>
    <table width="500" align="center" cellpadding="0" cellspacing="0"><tr><td>
    <?=OpenCMSTabelVrij("Alinea's " . $tmpTabelTitel) ?>
    <?=OpenCMSNavBalk() ?>
    <?=ToonCMSNavKnop("sorteren", "Alinea's Sorteren", "javascript:openSorter('paragrafen', '" . $Type_Lijst . "', '" . $Item_Id . "');")?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe alinea", "cmsparagraaf-add.php?partype=" . $Type_Lijst . "&itemid=" . $Item_Id . "&hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=SluitCMSNavBalk()?>

    <tr class="kadervoet">
        <td width="30"><b></b></td>
        <td><b>Omschrijving</b></td>
        <td width="100"><b>Soort</b></td>
        <td width="70" align="center"><b>Volgorde</b></td>
        <td width="70" align="center"><b>Zichtbaar</b></td>
        <td width="100"><b>Verwijder</b></td>
    </tr>

    <?
	$query_rs = "SELECT * FROM paragrafen WHERE " . $tmpQryDeel . "=" . $Item_Id . " ORDER BY pa_volgorde, pa_naam;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		do
		{
	        $WijzigURL = "cmsparagraaf-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&parid=" . $row_rs['paragraafid'] . "&partype=" . $Type_Lijst . "&itemid=" . $Item_Id;
	        $VerwijderURL = "cmsparagraaf-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&parid=" . $row_rs['paragraafid'] . "&partype=" . $Type_Lijst . "&itemid=" . $Item_Id;
	        $MijnType = GeefDBWaarde("typeomschrijving", "paragraaftypen", "paragraaftype='" . $row_rs['paragraaftype'] . "'");
	        $VerwijderTekst = "Weet u zeker dat u deze alinea wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
			?>
		    <tr class="regel">
		        <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Alinea wijzigen")?></td>
		        <td><a href="<?=$WijzigURL?>"><?=$row_rs['pa_naam']?></a></td>
		        <td><?=$MijnType?></td>
		        <td align="center"><?=$row_rs['pa_volgorde']?></td>
		        <td align="center"><?=ToonGereed($row_rs['pa_publiceren'])?></td>
		        <td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderTekst)?></td>
		    </tr>
			<?
		}
		while ($row_rs = mysql_fetch_assoc($rs));
	}
	mysql_free_result($rs);
    ?>

    <?=SluitCMSTabelVrij()?>
    </td></tr></table>
    <br /><br />
<?
}
?>
<?
function ToonErrorPagina($Err_melding)
{
	?>
        <?
        OpenPagina("CMS", "");
        ?>
        <? OpenCMSTabel("Kan de gevraagde actie niet uitvoeren"); ?>
        <? OpenCMSNavBalk(); ?>
            <? ToonCMSNavKnop("stop", "Terug", "javascript:history.back()"); ?>
        <? SluitCMSNavBalk(); ?>
        <tr><td width="50"></td><td><?=$Err_melding?><br />
        <br />
        <a href="javascript:history.back()">&lt;-Terug</a><br />
        <br />
        </td></tr>
        <? SluitCMSTabel(); ?>
        <br /><br />
        <?
        SluitPagina();
        die;
        ?>
	<?
}

function GeefEersteAdmHoofdmenuID()
{
	mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
	$query_rs = "SELECT * FROM adminmenu WHERE admniveau=0 and adminrechtennodig=0 AND admpubliceren<>0 ORDER BY admvolgorde";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	$totalRows_rs = mysql_num_rows($rs);
	return $row_rs['admmenuid'];
}

function AantalAdmHMItems()
{
	mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
	$query_rs = "SELECT * FROM adminmenu WHERE admniveau=0 AND admpubliceren<>0";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	$totalRows_rs = mysql_num_rows($rs);
	return $totalRows_rs;
	    //$tmpRechten = IsAdministrator(GeefHuidigeUserId);
	    //if ($tmpRechten==true)
	    //{
	    //    $tmpSqlSt = "SELECT COUNT(*) as totitems FROM qryadmhoofdmenu;";
	    //}
	    //else
	    //{
	    //    $tmpSqlSt = "SELECT COUNT(*) as totitems FROM qryadmhoofdmenu WHERE adminrechtennodig=0;";
	    //}
}

function OpenCMSTabel($tbl_titel)
{
	?>
	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
	<tr height="<?=$GLOBALS['HoogteCMSHMKnoppen']?>">
	<td colspan="100" align="center" height="<?=$GLOBALS['HoogteCMSHMKnoppen']?>" class="kaderkop" style="border-bottom: solid black 1px; text-decoration: none;"><?=strtoupper($tbl_titel)?></td>
	</tr>
	<?
}

function OpenCMSTabelVrij($tbl_titel)
{
	?>
	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border: solid 1px black;">
	<tr height="<?=$GLOBALS['HoogteCMSHMKnoppen']?>">
	<td colspan="100" align="center" height="<?=$GLOBALS['HoogteCMSHMKnoppen']?>" class="kaderkop" style="border-bottom: solid black 1px; text-decoration: none;"><?=strtoupper($tbl_titel)?></td>
	</tr>
	<?
}

function OpenCMSNavBalk()
{
	?>
	<tr class="kadernav"><td colspan="100" align="right">
	<table cellpadding="0" cellspacing="0"><tr>
	<?
}

function OpenCMSNavBalkWebsite()
{
	?>
	<tr class="kadernavweb"><td colspan="100" align="right">
	<table cellpadding="0" cellspacing="0" align="right" bgcolor="#ffffff"><tr>
	<?
}

function ToonCMSNavKnop($Icon_Name, $Knop_tekst, $Knop_url)
{
    if (substr($Knop_url,0,11)=="javascript:")
    {
		$tmpUrl = str_replace("javascript:", "", $Knop_url);
		if (substr($tmpUrl,0,12)=="VerstuurKnop")
		{
		    $tmpUrl = "document.getElementById('VerstuurKnop').click();";
		}
		$onClickUrl = $tmpUrl;
    }
	else
	{
		$onClickUrl = "location.href='" . $Knop_url . "';";
	}
	?>
	<td bgcolor="#e3e3e3">&nbsp;|&nbsp;</td>
	<td height="20" class="navknop" onClick="<?=$onClickUrl?>" onMouseOver="this.style.cursor='pointer';this.className='navknopover';" onMouseOut="this.className='navknop';" valign="middle"><?=Icoon($Icon_Name)?><?=$Knop_tekst?>&nbsp;</td>
	<?
}

function SluitCMSNavBalk()
{
	?>
	<td bgcolor="#e3e3e3">&nbsp;|&nbsp;</td></tr></table>
	</td></tr>
	<?
}

function SluitCMSTabel()
{
	?>
	<tr class="kadervoet"><td colspan="100" style="border-bottom: solid 1px black;">&nbsp;</td></tr>
	</table>
	<?
}

function SluitCMSTabelVrij()
{
	?>
	<tr class="kadervoet"><td colspan="100">&nbsp;</td></tr>
	</table>
	<?
}
?>
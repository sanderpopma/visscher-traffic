<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "PROJECTEN")) { die; }
if ($GLOBALS['projcatid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
$projcat_volgorde=0;
$HeeftProjecten = false;
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM project_categorieen WHERE projcatid=".$GLOBALS['projcatid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['projcatid']>0)
    {
        $projcat_naam = $row_rs['projcat_naam'];
        $projcat_volgorde = $row_rs['projcat_volgorde'];
        $projcat_publiceren = Int2Bool($row_rs['projcat_publiceren']);
    }
    mysql_free_result($rs);
    $HeeftProjecten = (toInt(TelRecords("SELECT * FROM projecten WHERE proj_categorieid=".$GLOBALS['projcatid'].""))>0);
}
else
{
    $query_rs = "SELECT MAX(projcat_volgorde) as maxnr FROM project_categorieen";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $projcat_volgorde = toInt($row_rs['maxnr']);
        if ($projcat_volgorde<0) { $projcat_volgorde=0;}
	}
	mysql_free_result($rs);
    $projcat_volgorde=$projcat_volgorde+1;
    $projcat_publiceren = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Project Categorie " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
    	<?=ToonCMSNavKnop("toevoegen", "Nieuw project", "cmsprojecten-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=".$GLOBALS['projcatid']) ?>
	    <? if ($HeeftProjecten==true) { ?>
	        <?=ToonCMSNavKnop("documenten", "Toon projecten", "cmsprojecten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=".$GLOBALS['projcatid']) ?>
	    <? } ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsprojectcat.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsprojectcat-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&projcatid=" . $GLOBALS['projcatid'], "projectcategorie", 700)?>
    <?=FrmText("Categorie naam" . Icoon("NL"), "projcat_naam", $projcat_naam, 60, 100) ?>
    <?=FrmText("Afbeeldvolgorde", "projcat_volgorde", $projcat_volgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "projcat_publiceren", $projcat_publiceren) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("projcat_naam") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("projectcategorie");
frmvalidator.addValidation("projcat_naam","req","Naam is verplicht");
frmvalidator.addValidation("projcat_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("projcat_volgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
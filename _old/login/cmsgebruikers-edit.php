<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['gebruikerid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM cmsbeheerders WHERE beheerderid=".$GLOBALS['gebruikerid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['beheerderid']>0)
    {

        $beheerlogin = $row_rs['beheerlogin'];
        $beheerwachtwoord = $row_rs['beheerwachtwoord'];
        $beheerafbeeldnaam = $row_rs['beheerafbeeldnaam'];
        $beheeremailadres = $row_rs['beheeremailadres'];
        $beheerisadministrator = Int2Bool($row_rs['beheerisadministrator']);
        $beheerderactief = Int2Bool($row_rs['beheerderactief']);
     }
    mysql_free_result($rs);
}
else
{
        $beheerisadministrator = false;
        $beheerderactief = true;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Gebruiker " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsgebruikers.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsgebruikers-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&gebruikerid=" . $GLOBALS['gebruikerid'], "gebruiker", 700)?>
    <?=FrmText("Naam gebruiker", "beheerafbeeldnaam", $beheerafbeeldnaam, 40, 50) ?>
    <?=FrmText("Loginnaam", "beheerlogin", $beheerlogin, 30, 50) ?>
    <?=FrmText("Wachtwoord", "beheerwachtwoord", $beheerwachtwoord, 30, 50) ?>
    <?=FrmText("E-mailadres", "beheeremailadres", $beheeremailadres, 40, 150) ?>
    <?=FrmCheckbox("Administrator", "beheerisadministrator", $beheerisadministrator) ?>
    <?=FrmCheckbox("Actief", "beheerderactief", $beheerderactief) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("beheerafbeeldnaam") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("gebruiker");
frmvalidator.addValidation("beheerafbeeldnaam","req","Naam is verplicht");
frmvalidator.addValidation("beheerlogin","req","Login is verplicht");
frmvalidator.addValidation("beheerwachtwoord","req","Wachtwoord is verplicht");
frmvalidator.addValidation("beheeremailadres","req","E-mailadres is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$paginatype = $_GET['paginatype'];
$paginaid = toInt($_GET['paginaid']);
$subtype = $_GET['subtype'];
switch(strtoupper($subtype)){
	case "NIEUWS":
		$rsZoekIdVeld = "pa_nieuwsid";
		break;
	case "EVENT":
		$rsZoekIdVeld = "pa_eventid";
		break;
	case "PROJECT":
		$rsZoekIdVeld = "pa_projectid";
		break;
	case "VACATURE":
		$rsZoekIdVeld = "pa_vacatureid";
		break;
	default:
		$subtype="PAGINA";
		$rsZoekIdVeld = "pa_paginaid";
} // switch
$tmpTekst = "Alinea's";
$sqlst = "SELECT * FROM paragrafen WHERE " . $rsZoekIdVeld . "=" . $paginaid . " ORDER BY pa_volgorde, pa_naam;";
$rsTitel = "pa_naam";
$rsVerbergveld = "paragraafid";
$rsIdVeld = "paragraafid";
$rsSorteerVeld = "pa_volgorde";

if ($paginatype!="paragrafen"){
    $paginatype = "paginas";
    $tmpTekst = "Pagina's";
    $sqlst = "SELECT * FROM paginas WHERE (valtonderpaginaid=" . $paginaid . ") ORDER BY submenuvolgorde, opnemeninsubmenu, paginatitel;";
    $rsTitel = "paginatitel";
    if (toInt($paginaid)<1) {
	    $sqlst = "SELECT * FROM paginas WHERE (valtonderpaginaid=" . $paginaid . ") ORDER BY hoofdmenuvolgorde, submenuvolgorde, opnemeninsubmenu, paginatitel;";
	    $rsTitel = "paginamenunaam";
    }
    $rsVerbergveld = "valtonderpaginaid";
    $rsIdVeld = "paginaid";
    $rsSorteerVeld = "submenuvolgorde";
}
?>
<?
OpenPagina("CMS", "");
?>
<script type="text/javascript">
function Verplaats(vanx, naarx)
{
	//alert(document.getElementById("txt_" + naarx).innerHTML);
    tmpid = document.getElementById("newid_" + naarx).value;
    tmptxt = document.getElementById("txt_" + naarx).innerHTML;
    document.getElementById("newid_" + naarx).value = document.getElementById("newid_" + vanx).value;
    document.getElementById("newid_" + vanx).value = tmpid;
    document.getElementById("txt_" + naarx).innerHTML = document.getElementById("txt_" + vanx).innerHTML;
    document.getElementById("txt_" + vanx).innerHTML = tmptxt;
}
function Herlaad()
{
    document.location.href = document.location.href;
}
</script>

<?
OpenCMSTabel($tmpTekst . " sorteren");
?>
<?=OpenCMSNavBalk()?>
	<?=ToonCMSNavKnop("sorteren", "Reset", "javascript:Herlaad();") ?>
	<?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
	<?=ToonCMSNavKnop("stop", "Annuleren", "javascript:window.close();") ?>
<?=SluitCMSNavBalk()?>

<tr class="kadervoet">
    <td colspan="10"><b>&nbsp;Sorteer de <?=$tmpTekst?> en kies Opslaan als u de gewenste volgorde heeft bepaald</b></td>
</tr>
<tr><td>

<?
$IsEersteRecord = true;
$IsLaatsteRecord = false;
$rs = mysql_query($sqlst, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if ($row_rs[$rsIdVeld]>0)
{
	$AantRec = mysql_num_rows($rs);
	$x=1;
	?>
    <?=OpenForm("sorteren2.php", "sorteren", $GLOBALS['AfbeeldingMaxHeel'])?>
    <?=FrmHidden("paginatype", $paginatype)?>
    <?=FrmHidden("subtype", $subtype)?>
    <?=FrmHidden("paginaid", $paginaid)?>
	<?
	if ($paginatype=="paginas") {
		?>
        <tr class="regel" valign="bottom">
        <td></td>
        <td></td>
        <td><b>0.</b></td>
        <td><b><?=GeefDBWaarde("paginatitel", "paginas", "paginaid=" . $paginaid)?></b></td>
        </tr>
		<?
	}
	?>
	<?
	do
	{
        $ToonOp = true;
        $ToonNeer = true;
        if ($IsEersteRecord==true){
            $ToonOp = false;
            $IsEersteRecord = false;
        }
        if ($AantRec==$x){
            $ToonNeer = false;
            $IsLaatsteRecord = true;
        }
	?>
	    <tr class="regel" valign="bottom">
	        <td width="20">
	        <?=FrmHidden("oldid_" . $x, $row_rs[$rsIdVeld])?>
	        <?=FrmHidden("oldval_" . $x, $row_rs[$rsSorteerVeld]) ?>
	        <?=FrmHidden("newid_" . $x, $row_rs[$rsIdVeld]) ?>
	        <?=FrmHidden("newval_" . $x, $x) ?>
	        <? if ($ToonOp==true){ ?>
	        	<?=PlaatsIcoonLink("pijl_op", "", "javascript:Verplaats(" . $x . ", " . ($x-1) . ");", "")?>
	        <? } ?>
	        </td>
	        <td width="20">
	        <? if ($ToonNeer==true){ ?>
	        	<?=PlaatsIcoonLink("pijl_neer", "", "javascript:Verplaats(" . $x . ", " . ($x+1) . ");", "")?>
	        <? } ?>
	        </td>
	        <td width="20">
	        <? echo $x.". "; ?>
	        </td>
	        <td>
	        <div id="txt_<?=$x?>">
	        <?=$row_rs[$rsTitel]?>
	        </div>
	        </td>
	    </tr>
	<?
		$x = $x + 1;
	}
	while ($row_rs = mysql_fetch_assoc($rs));
	?>
    <tr><td colspan='4' align='right'><input class='Knop' id='VerstuurKnop' name='VerstuurKnop' type='SUBMIT' value='Opslaan'></TD></TR>
    <?=SluitForm()?>
    <?
}
mysql_free_result($rs);
?>
</td></tr>

<?
SluitCMSTabel();
?>

<br /><br />

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
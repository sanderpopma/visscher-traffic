<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!IsAdministrator(GeefHuidigeUserId())) {die;}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("E-mailberichten beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw bericht", "cmsemails-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"></td>
    <td><b>Zoekcode</b></td>
    <td><b>Omschrijving</b></td>
    <td><b>Titel bericht</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>


<?
$query_rs = "SELECT * FROM winkel_emailberichten ORDER BY em_zoekcode;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsemails-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["emailid"];
	    $VerwijderURL = "cmsemails-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["emailid"];
	    $VerwijderMelding = "Weet u zeker dat u dit bericht wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
    	//$MagVerwijderen = false;
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["em_zoekcode"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["em_omschrijving"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["em_titel"]?></a></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
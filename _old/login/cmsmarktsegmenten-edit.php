<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['msmid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
$marktsegment_volgorde=0;
$HeeftProducten = false;
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM marktsegmenten WHERE marktsegmentid=".$GLOBALS['msmid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['marktsegmentid']>0)
    {
        $marktsegment_naam_nl = $row_rs['marktsegment_naam_nl'];
        $marktsegment_naam_en = $row_rs['marktsegment_naam_en'];
        $marktsegment_menunaam_nl = $row_rs['marktsegment_menunaam_nl'];
        $marktsegment_menunaam_en = $row_rs['marktsegment_menunaam_en'];
        $marktsegment_volgorde = $row_rs['marktsegment_volgorde'];
        $marktsegment_afbeeldingid = $row_rs['marktsegment_afbeeldingid'];
        $marktsegment_beschrijving_nl = $row_rs['marktsegment_beschrijving_nl'];
        $marktsegment_beschrijving_en = $row_rs['marktsegment_beschrijving_en'];
        $marktsegment_url_nl = $row_rs['marktsegment_url_nl'];
        $marktsegment_url_en = $row_rs['marktsegment_url_en'];
        $marktsegment_googletitel_nl = $row_rs['marktsegment_googletitel_nl'];
        $marktsegment_googletitel_en = $row_rs['marktsegment_googletitel_en'];
        $marktsegment_publiceren = Int2Bool($row_rs['marktsegment_publiceren']);
    }
    mysql_free_result($rs);
    //$HeeftProducten = (toInt(TelRecords("SELECT * FROM producten WHERE pr_marktsegmentid=".$GLOBALS['msmid'].""))>0);
}
else
{
    $query_rs = "SELECT MAX(marktsegment_volgorde) as maxnr FROM marktsegmenten";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $marktsegment_volgorde = toInt($row_rs['maxnr']);
        if ($marktsegment_volgorde<0) { $marktsegment_volgorde=0;}
	}
	mysql_free_result($rs);
    $marktsegment_volgorde=$marktsegment_volgorde+1;
    $marktsegment_publiceren = false;
    $marktsegment_afbeeldingid = -1;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Marktsegment " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
	    <? if ($HeeftProducten==true) { ?>
	        <?=ToonCMSNavKnop("documenten", "Toon producten", "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&msmid=".$GLOBALS['msmid']) ?>
	    <? } ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmsmarktsegmenten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsmarktsegmenten-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&msmid=" . $GLOBALS['msmid'], "marktsegment", 700)?>
    <?=FrmText("Pagina titel / naam" . Icoon("NL"), "marktsegment_naam_nl", $marktsegment_naam_nl, 60, 100) ?>
    <?=FrmText("Naam in menu" . Icoon("NL"), "marktsegment_menunaam_nl", $marktsegment_menunaam_nl, 30, 50) ?>
    <?=FrmText("Pagina titel / naam" . Icoon("EN"), "marktsegment_naam_en", $marktsegment_naam_en, 60, 100) ?>
    <?=FrmText("Naam in menu" . Icoon("EN"), "marktsegment_menunaam_en", $marktsegment_menunaam_en, 30, 50) ?>
    <?=FrmText("Afbeeldvolgorde", "marktsegment_volgorde", $marktsegment_volgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "marktsegment_publiceren", $marktsegment_publiceren) ?>

    <?=FrmKopregel("Extra instellingen") ?>
    <?=FrmText("Pagina URL" . Icoon("NL"), "marktsegment_url_nl", $marktsegment_url_nl, 60, 255) ?>
    <?=FrmText("Pagina titel t.b.v. zoekmachines" . Icoon("NL"), "marktsegment_googletitel_nl", $marktsegment_googletitel_nl, 80, 255) ?>
    <?=FrmText("Pagina URL" . Icoon("EN"), "marktsegment_url_en", $marktsegment_url_en, 60, 255) ?>
    <?=FrmText("Pagina titel t.b.v. zoekmachines" . Icoon("EN"), "marktsegment_googletitel_en", $marktsegment_googletitel_en, 80, 255) ?>

    <?=FrmKopregel("Beschrijving") ?>
    <?=FrmFCKText("Beschrijving" . Icoon("NL"), "marktsegment_beschrijving_nl", $marktsegment_beschrijving_nl, 200) ?>
    <?=FrmFCKText("Beschrijving" . Icoon("EN"), "marktsegment_beschrijving_en", $marktsegment_beschrijving_en, 200) ?>

    <?=FrmKopregel("Afbeelding (bovenkant website)") ?>
	<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding moet vooraf exact op ". ($GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteRechts']). "x".($GLOBALS['HoogteWebsiteBoven']+$GLOBALS['HoogteKnoppenbalk'])."px worden gemaakt");?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotogeen.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("marktsegment_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("marktsegment_afbeeldingid", $marktsegment_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $marktsegment_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('marktsegment_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('marktsegment_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>
    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("marktsegment_naam_nl") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("marktsegment");
frmvalidator.addValidation("marktsegment_naam_nl","req","Naam (Nederlands) is verplicht");
frmvalidator.addValidation("marktsegment_menunaam_nl","req","Naam in menu (Nederlands) is verplicht");
frmvalidator.addValidation("marktsegment_naam_en","req","Naam (Engels) is verplicht");
frmvalidator.addValidation("marktsegment_menunaam_en","req","Naam in menu (Engels) is verplicht");
frmvalidator.addValidation("marktsegment_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("marktsegment_volgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
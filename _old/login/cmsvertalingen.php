<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) { die; }
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Vertalingen")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe vertaling", "cmsvertalingen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar stamgegevens", "cmsstamgegevens.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
	<td><b>Zoekcode</b></td>
	<?
	mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
	$query_rs = "SELECT * FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	if (mysql_num_rows($rs)>0) {
		do
		{
		?>
    	<td><b><?=$row_rs['taalnaam']?></b></td>
		<?
		}
		while ($row_rs = mysql_fetch_assoc($rs));
	}
	mysql_free_result($rs);
    ?>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
mysql_select_db($GLOBALS['database_dbc'], $GLOBALS['conn']);
$query_rs = "SELECT * FROM vertalingen ORDER BY zoekcode;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsvertalingen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vertaalid=" .$row_rs["vertaalid"];
	    $VerwijderURL = "cmsvertalingen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&vertaalid=" .$row_rs["vertaalid"];
	    $VerwijderMelding = "Weet u zeker dat u deze regel wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Vertaling wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["zoekcode"]?></a></td>
				<?
				$query_rs2 = "SELECT * FROM talen WHERE taalpubliceren<>0 ORDER BY taalvolgorde;";
				$rs2 = mysql_query($query_rs2, $GLOBALS['conn']) or die(mysql_error());
				$row_rs2 = mysql_fetch_assoc($rs2);
				if (mysql_num_rows($rs2)>0) {
					do
					{
					$veldnaam = "tekst_" . strtolower($row_rs2['taalcode']);
					?>
				    <td><a href="<?=$WijzigURL?>"><?=$row_rs[$veldnaam]?></a></td>
					<?
					}
					while ($row_rs2 = mysql_fetch_assoc($rs2));
				}
				mysql_free_result($rs2);
			    ?>
				<td><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
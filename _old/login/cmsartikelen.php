<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters

if ($GLOBALS['prodgrpid']<1 || $GLOBALS['prodid']<1) {
	redirect("cmsproductgroepen.php");
}
$ProdGrpNaam = GeefDBWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=".$GLOBALS['prodgrpid']);
$ProdNaam = GeefDBWaarde("productnaam_nl", "producten", "productid=".$GLOBALS['prodid']);
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Artikelen in de subcategorie ".$ProdGrpNaam . " - " . $ProdNaam)?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuw artikel", "cmsartikelen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=".$GLOBALS['prodgrpid'] . "&prodid=" . $GLOBALS['prodid']) ?>
    <?=ToonCMSNavKnop("stop", "Terug naar subcategorie", "cmsproducten.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']. "&prodgrpid=".$GLOBALS['prodgrpid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td><b>Productgroep</b></td>
    <td><b>Subcategorie</b></td>
    <td><b>Naam artikel</b></td>
    <td><b>Prijs</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM artikelen WHERE art_productid=".$GLOBALS['prodid']." ORDER BY art_naam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsartikelen-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$GLOBALS["prodgrpid"]."&prodid=".$row_rs['art_productid']."&artid=" . $row_rs['artikelid'];
	    $VerwijderURL = "cmsartikelen-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" .$GLOBALS["prodgrpid"]."&prodid=".$row_rs['art_productid']."&artid=" . $row_rs['artikelid'];
	    $VerwijderMelding = "Weet u zeker dat u dit artikel wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    //$AantBesteld = toInt(TelRecords("SELECT * FROM bestellingen WHERE be_artikelid=" . $row_rs['artikelid'].""));
	    $AantBesteld = 0;
	    if ($AantBesteld<1) {
	    	$MagVerwijderen = true;
	    	$AantBesteld = 0;
	    }
	    else
	    {
	    	$MagVerwijderen = false;
		}
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><?=$ProdNaam?></td>
			    <td><?=$ProdGrpNaam?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["art_naam"]?></a></td>
			    <td><?=MaakBedrag($row_rs["art_prijs"])?></td>
			    <td><center><?=ToonGereed($row_rs["art_publiceren"])?></td>
			    <td>
			    <? if ($MagVerwijderen==true) {
			    	?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
			    	<?
			    }
			    ?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
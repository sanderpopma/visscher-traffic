<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$partype = $_GET['partype'];
$itemid = $_GET['itemid'];
switch(strtoupper($partype)){
	case "NIEUWS":
        $AnnuleerLink = "cmsnieuws-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&nwsid=" . $itemid;
        $WhereDeelMax = "pa_nieuwsid=" . $itemid;
		break;
	case "VACATURE":
        $AnnuleerLink = "cmsvacatures-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&vacid=" . $itemid;
        $WhereDeelMax = "pa_vacatureid=" . $itemid;
		break;
	case "EVENT":
        $AnnuleerLink = "cmsevents-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&evid=" . $itemid;
        $WhereDeelMax = "pa_eventid=" . $itemid;
		break;
	case "PROJECT":
		$AnnuleerLink = "cmsprojecten-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&projid=" . $itemid;
		$WhereDeelMax = "pa_projectid=" . $itemid;
		break;
	default:
        $partype = "PAGINA";
        $AnnuleerLink = "cmspaginas-edit.php?hmid=" .$GLOBALS['hmid'] . "&smid=" .$GLOBALS['smid'] . "&pagid=" . $itemid;
        $WhereDeelMax = "pa_paginaid=" . $itemid;
} // switch

if ($GLOBALS['parid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM paragrafen WHERE paragraafid=".$GLOBALS['parid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['paragraafid']>0)
    {
        $PA_Naam = $row_rs["pa_naam"];
        $PA_Volgorde = $row_rs["pa_volgorde"];
        $PA_Publiceren = Int2Bool($row_rs["pa_publiceren"]);
        $Paragraaftype = $row_rs["paragraaftype"];

        $PA_Tekst = $row_rs["pa_tekst"];

        $PA_AfbeeldingID = $row_rs["pa_afbeeldingid"];
        $PA_AfbeeldingLR = $row_rs["pa_afbeeldinglr"];
        $PA_AfbeeldingOnderschrift = $row_rs["pa_afbeeldingonderschrift"];
		$PA_AfbeeldingNoBorder = Int2Bool($row_rs["pa_afbeeldingnoborder"]);
		$pa_afbeeldingformaat = toInt($row_rs["pa_afbeeldingformaat"]);

        $PA_LinkTekst = $row_rs["pa_linktekst"];
        $PA_ELinkURL = $row_rs["pa_elinkurl"];
        $PA_Emailadres = $row_rs["pa_emailadres"];
        $PA_ILinkID = $row_rs["pa_ilinkid"];
        $PA_EventLinkID = $row_rs["pa_eventlinkid"];

	    $PA_FormulierID = $row_rs["pa_formulierid"];
	    $PA_BestandID = $row_rs["pa_bestandid"];
	    $PA_FotoboekID = $row_rs["pa_fotoboekid"];

	    $pa_aantalkeer = $row_rs["pa_aantalkeer"];
		$pa_nwsgeeninleiding = Int2Bool($row_rs["pa_nwsgeeninleiding"]);

		$pa_alleenleden = Int2Bool($row_rs["pa_alleenleden"]);
    }
    mysql_free_result($rs);
}
else
{
	die;
}
if (toInt($PA_AfbeeldingID)<0) {$PA_AfbeeldingID=-1;}
if (toInt($PA_BestandID)<0) {$PA_BestandID=-1;}
if (toInt($PA_ILinkID)<0) {$PA_ILinkID=-1;}
if (toInt($PA_EventLinkID)<0) {$PA_EventLinkID=-1;}
if (toInt($PA_FormulierID)<0) {$PA_FormulierID=-1;}
if (toInt($PA_FotoboekID)<0) {$PA_FotoboekID=-1;}
if (toInt($pa_aantalkeer)<1) {$pa_aantalkeer=1;}
if (toInt($pa_afbeeldingformaat)<1) {$pa_afbeeldingformaat=1;}
if ($PA_AfbeeldingLR=="") { $PA_AfbeeldingLR="C"; }

if ($PA_Naam=="") { $PA_Naam=GeefDBWaarde("typeomschrijving", "paragraaftypen", "paragraaftype='" . $Paragraaftype . "'");}
?>


<?=OpenPagina("CMS", "")?>
<? GebruikDatumObjecten(); ?>

<?=OpenCMSTabel("Alinea " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", $AnnuleerLink) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsparagraaf-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&parid=" . $GLOBALS['parid'] . "&partype=" . $partype . "&itemid=" . $itemid, "paragraaf", 700)?>
    <?=FrmText("Omschrijving", "pa_naam", $PA_Naam, 60, 50) ?>
    <?=FrmText("Afbeeldvolgorde", "pa_volgorde", $PA_Volgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "pa_publiceren", $PA_Publiceren) ?>
    <tr class="regel">
        <td>Soort alinea</td>
        <td>:</td>
        <td><?=GeefDBWaarde("typeomschrijving", "paragraaftypen", "paragraaftype='" . $Paragraaftype . "'")?></td>
    </tr>
    <tr class="kadervoet"><td colspan="20"><b>Inhoud van de alinea</b></td></tr>

	<?
	switch(strtoupper($Paragraaftype))
	{
		case "TEKST":
			?>
			<?=FrmFCKText("Tekst", "pa_tekst", $PA_Tekst, 300) ?>
            <script language="javascript">
            function kiesFoto(fld_toupdate)
            {
                result = window.open('kies_fotoklein.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
            }
            function toonFoto()
            {
                document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
                document.getElementById("afbklein").style.border = "solid 1px black";
                document.getElementById("AfbVerwijderLink").style.visibility = "visible";
            }
            function wisFoto()
            {
                if(confirm('Deze afbeelding niet langer gebruiken?'))
                {
                    document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
                    document.getElementById("afbklein").style.border = "0px";
                    document.getElementById("pa_afbeeldingid").value = "-1";
                    document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
                }
            }
            </script>
			<?=FrmHidden("pa_afbeeldingid", $PA_AfbeeldingID)?>
            <tr class='regel' valign='top'>
                <td>Afbeelding</td>
                <td>:</td>
                <td>
                <?
			    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $PA_AfbeeldingID .";";
			    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
			    $row_rsafb = mysql_fetch_assoc($rsafb);
			    if ($row_rsafb['afbeeldingid']>0)
			    {
			    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
			    	?>
	                    <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
	                    <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
	                    <?=FrmHidden("AfbGUID", $AfbGUID)?>
	                    <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
	                    <a onclick="kiesFoto('pa_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
	                    <br />
	                    <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif" width="150" />
			    	<?
			    	$defVerwVisible = "visible";
                }
                else
                {
                	$defVerwVisible = "hidden";
                	?>
	                    <?=FrmHidden("AfbMaand", "0")?>
	                    <?=FrmHidden("AfbJaar", "0")?>
	                    <?=FrmHidden("AfbGUID", "0")?>
	                    <?=FrmHidden("AfbExt", "0")?>
	                    <a onclick="kiesFoto('pa_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
	                    <br />
	                    <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif" width="150" />
                	<?
				}
			    mysql_free_result($rsafb);
                ?>
                    <br /><br />
                    <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
                    <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
                    </div>

                <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
                    <script language="javascript">
                        toonFoto();
                    </script>
                <? } ?>
                </td>

            </tr>
            <?=FrmSelectValueOptionList("Positie Afbeelding", "pa_afbeeldinglr", $PA_AfbeeldingLR, ";L;R", "Geen;Links;Rechts", false) ?>
			<?
			break;
		case "FOTO":
			?>
            <script language="javascript">
            var orgwidth;
            function kiesFoto(fld_toupdate)
            {
                result = window.open('kies_foto.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
            }
            function toonFoto()
            {
                document.getElementById("afbheel").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
                document.getElementById("afbheel").style.border = "solid 1px black";
				orgwidth = <?=$GLOBALS['AfbeeldingMaxHeel']?>;
				afbResize();
            }
            </script>
            <?=FrmHidden("pa_afbeeldingid", $PA_AfbeeldingID)?>
            <tr class='regel' valign='top'>
                <td>Afbeelding</td>
                <td>:</td>
                <td>
                <?
		    	$HeeftAfb = false;
			    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $PA_AfbeeldingID .";";
			    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
			    $row_rsafb = mysql_fetch_assoc($rsafb);
			    if ($row_rsafb['afbeeldingid']>0)
			    {
			    	$HeeftAfb = true;
			    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
			    	?>
	                    <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
	                    <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
	                    <?=FrmHidden("AfbGUID", $AfbGUID)?>
	                    <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
	                    <a onclick="kiesFoto('pa_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
	                    <br />
	                    <img name="afbheel" id="afbheel" src="<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/<?=datYear($row_rsafb["afbdatum"])?>/<?=Maak2Dig(datMonth($row_rsafb["afbdatum"]))?>/<?=$AfbGUID?>.<?=$row_rsafb["afbeeldingextentie"]?>"/>
			    	<?
                }
                else
                {
                	?>
	                    <?=FrmHidden("AfbMaand", "0")?>
	                    <?=FrmHidden("AfbJaar", "0")?>
	                    <?=FrmHidden("AfbGUID", "0")?>
	                    <?=FrmHidden("AfbExt", "0")?>
	                    <a onclick="kiesFoto('pa_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
	                    <br />
	                    <img name="afbheel" id="afbheel" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
                	<?
				}
			    mysql_free_result($rsafb);

                ?>
				</td>
			</tr>
			<script type="text/javascript">
				function afbNoBorder(){
					if(document.getElementById("pa_afbeeldingnoborder").checked==true)
					{
		                document.getElementById("afbheel").style.border = "none"
	                }
	                else
	                {
		                document.getElementById("afbheel").style.border = "solid 1px black"
	                }
				}
			</script>
            <?=FrmOnEventCheckbox("onclick", "afbNoBorder()", "Zonder rand", "pa_afbeeldingnoborder", $PA_AfbeeldingNoBorder) ?>
            <?=FrmSelectValueOptionList("Positie Afbeelding", "pa_afbeeldinglr", $PA_AfbeeldingLR, ";L;C;R", "Geen;Links;Midden;Rechts", false) ?>
			<script type="text/javascript">
				function afbResize(){
					if(document.getElementById("pa_afbeeldingformaat").value!="")
					{
		                document.getElementById("afbheel").width = (orgwidth / document.getElementById("pa_afbeeldingformaat").value);
	                }
	                else
	                {
	                	//
	                }
				}
			</script>
            <?=FrmOnEventSelectValueOptionList("onchange", "afbResize()", "Afbeeldformaat", "pa_afbeeldingformaat", $pa_afbeeldingformaat, "1;2;4", "Normaal(1);Half(1/2);Kwart(1/4)", false) ?>
            <?=FrmFCKText("Foto onderschrift", "pa_tekst", $PA_Tekst, 150) ?>
            <?=FrmKoptekst("Afbeelding aanklikbaar maken (link opnemen)")?>
            <?=FrmText("Internetadres (URL)", "pa_elinkurl", $PA_ELinkURL, 80,255)?>
			<script type="text/javascript">
				<? if ($PA_AfbeeldingID<1) {
					?>
					orgwidth = 1;
					<?
				}
				else
				{
				?>
					orgwidth = document.getElementById("afbheel").width;
					if (document.getElementById("afbheel").width==0 || document.getElementById("afbheel").width==1) {
						orgwidth = <?=$GLOBALS['AfbeeldingMaxHeel']?>;
					}
				<?
				}
				?>
				afbNoBorder();
				afbResize();
			</script>
			<?
			break;
		case "FILM":
			?>
				<?=FrmTextArea("Code filmbestand", "pa_tekst", $PA_Tekst, 5,60)?>
            	<?=FrmFCKText("Film onderschrift", "pa_afbeeldingonderschrift", $PA_AfbeeldingOnderschrift, 150) ?>
			<?
			break;
		case "ELINK":
			?>
            <tr class='regel' valign='top'>
                <td>Internetadres (URL)</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonVoorbeeld();' onChange='javascript:toonVoorbeeld();' class='Veld' type='text' id='pa_elinkurl' name='pa_elinkurl' value='<?=VervangFrmWaarde($PA_ELinkURL)?>' size='80' maxlength='255'></td>
            </tr>
            <tr class='regel' valign='top'>
                <td>Tekst (zoals de link wordt getoond)</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonVoorbeeld();' onChange='javascript:toonVoorbeeld();' class='Veld' type='text' id='pa_linktekst' name='pa_linktekst' value='<?=VervangFrmWaarde($PA_LinkTekst)?>' size='80' maxlength='255'></td>
            </tr>
            <tr>
            <td>Voorbeeld</td>
            <td>:</td>
            <td><span id="voorbeeld"></span></td>
            </tr>
			<?
			break;
		case "EMAIL":
			?>
            <tr class='regel' valign='top'>
                <td>E-mailadres</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonVoorbeeld();' onChange='javascript:toonVoorbeeld();' class='Veld' type='text' id='pa_emailadres' name='pa_emailadres' value='<?=VervangFrmWaarde($PA_Emailadres)?>' size='80' maxlength='255'></td>
            </tr>
            <tr class='regel' valign='top'>
                <td>Tekst (zoals de link wordt getoond)</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonVoorbeeld();' onChange='javascript:toonVoorbeeld();' class='Veld' type='text' id='pa_linktekst' name='pa_linktekst' value='<?=VervangFrmWaarde($PA_LinkTekst)?>' size='80' maxlength='255'></td>
            </tr>
            <tr>
            <td>Voorbeeld</td>
            <td>:</td>
            <td><span id="voorbeeld"></span></td>
            </tr>

			<?
			break;
		case "ILINK":
			?>
            <script language="javascript">
            function kiesILink(fld_toupdate, par_taal)
            {
                result = window.open('kies_ilink.php?CurVal=' + document.getElementById("pa_ilinkid").value + '&GekozenPaginaTaal=' + par_taal + '&OpenerFieldToUpdate=' + fld_toupdate, 'kiesilink', 'width=500, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
            }
            </script>
            <?
            $PA_ILinkTekst = "";
            if ($PA_ILinkID>0) {
			    $query_rsi = "SELECT * FROM paginas WHERE paginaid=". $PA_ILinkID .";";
			    $rsi = mysql_query($query_rsi, $GLOBALS['conn']) or die(mysql_error());
			    $row_rsi = mysql_fetch_assoc($rsi);
			    if ($row_rsi['paginaid']>0)
			    {
            		$PA_ILinkTekst = $row_rsi['paginatitel'];
            	}
			    mysql_free_result($rsi);
            }
            switch(strtoupper($partype)){
            	case "NIEUWS":
            		$ParagraafTaal = GeefDBWaarde("nwstaalcode", "nieuws", "nieuwsid=(SELECT pa_nieuwsid FROM paragrafen WHERE paragraafid=" . $parid . ")");
            		break;
            	case "VACATURE":
            		$ParagraafTaal = GeefDBWaarde("vactaalcode", "vacatures", "vacatureid=(SELECT pa_vacatureid FROM paragrafen WHERE paragraafid=" . $parid . ")");
            		break;
            	case "EVENT":
            		$ParagraafTaal = GeefDBWaarde("evtaalcode", "evenementen", "eventid=(SELECT pa_eventid FROM paragrafen WHERE paragraafid=" . $parid . ")");
            		break;
				case "PROJECT":
					$ParagraafTaal = GeefDBWaarde("proj_taalcode", "projecten", "projid=(SELECT pa_projectid FROM paragrafen WHERE paragraafid=" . $parid . ")");
					break;
				default:
            		$ParagraafTaal = GeefDBWaarde("paginataalcode", "paginas", "paginaid=(SELECT pa_paginaid FROM paragrafen WHERE paragraafid=" . $parid . ")");
            } // switch
            if ($ParagraafTaal=="") {$ParagraafTaal=$GLOBALS['StdTaalcode'];}
            ?>
            <?=FrmHidden("pa_ilinkid", $PA_ILinkID)?>
            <?=FrmHidden("pa_ilinktekst", $PA_ILinkTekst)?>
            <tr class='regel' valign='top'>
                <td>Link naar pagina</td>
                <td>:</td>
                <td>
                <span id="ilinktekst"><?=$PA_ILinkTekst?></span><br />
                <a onclick="kiesILink('pa_ilinkid', '<?=$ParagraafTaal?>'); return false;" href="">Klik hier om de interne link te kiezen of te wijzigen</a></td>
            </tr>
            <tr class='regel' valign='top'>
                <td>Tekst (zoals de link wordt getoond)</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonILinkVoorbeeld();' onChange='javascript:toonILinkVoorbeeld();' class='Veld' type='text' id='pa_linktekst' name='pa_linktekst' value='<?=VervangFrmWaarde($PA_LinkTekst)?>' size='80' maxlength='255'></td>
            </tr>
            <tr>
            <td>Voorbeeld</td>
            <td>:</td>
            <td><span id="ilinkvoorbeeld"></span></td>
            </tr>
			<?
			break;
		case "FILE":
			?>
            <script language="javascript">
            function kiesBestand(fld_toupdate, fldtxt_toupdate, fld_no)
            {
                result = window.open('bestanden.php?openerfieldtoupdate=' + fld_toupdate + '&openerfieldno=' + fld_no + '&fileid=' + document.getElementById("pa_bestandid").value, 'kiesbestand', 'width=800, height=400, top=200, left=200, scrollbars=yes, toolbars=no');
            }
            </script>
			<?=FrmHidden("pa_bestandid", $PA_BestandID)?>
            <tr class='regel' valign='top'>
                <td>Bestand</td>
                <td>:</td>
                <td>
				<?
				$query_rsafb = "SELECT * FROM bestanden WHERE bestandid=". $PA_BestandID .";";
				$rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
				$row_rsafb = mysql_fetch_assoc($rsafb);
				if ($row_rsafb['bestandid']>0)
				{
					$tmpbestandsnaam = $row_rsafb['bestandsnaam'];
					$tmpbestandsicoon = GeefApplicatieIcoonViaExtentie( $row_rsafb['bestandsextentie']);
					$tmpbestandsguid = MaakGUIDString($row_rsafb["bestandsguid"]);
				?>
				<a onclick="kiesBestand('pa_bestandid', 'pa_linktekst', '0');return false;" href="">Klik hier om een ander bestand te kiezen of te uploaden</a><br />
		    	<?
				}
				else
				{
				?>
				<a onclick="kiesBestand('pa_bestandid', 'pa_linktekst', '0');return false;" href="">Klik hier om een bestand te kiezen of te uploaden</a><br />
               	<?
				}
				mysql_free_result($rsafb);
				?>
                </td>
            </tr>
			<?=FrmHidden("tmpbestandsnaam", $tmpbestandsnaam)?>
			<?=FrmHidden("tmpbestandsicoon", $tmpbestandsicoon)?>
			<?=FrmHidden("tmpbestandsguid", $tmpbestandsguid)?>
            <tr class='regel' valign='top'>
                <td>Tekst (zoals de link wordt getoond)</td>
                <td>:</td>
                <td><input onKeyDown='javascript:toonFileVoorbeeld();' onChange='javascript:toonFileVoorbeeld();' class='Veld' type='text' id='pa_linktekst' name='pa_linktekst' value='<?=VervangFrmWaarde($PA_LinkTekst)?>' size='80' maxlength='255'></td>
            </tr>
            <tr>
            <td>Voorbeeld</td>
            <td>:</td>
            <td>&nbsp;<img name='fileicon' id='fileicon' src='<?=$GLOBALS['ApplicatieRoot']?>/sybit/icons/help.gif' width='16' height='16' class='absmiddle' border='0' alt=''>&nbsp;<span id="filevoorbeeld"></span></td>
            </tr>
			<?
			break;
		case "FOTOBOEK":
			?>
            <script language="javascript">
            function kiesFotoboek(fld_toupdate)
            {
                result = window.open('kies_fotoboek.php?openerfieldtoupdate=' + fld_toupdate + '&fotoboekid=' + document.getElementById("pa_fotoboekid").value, 'kiesfotoboek', 'width=800, height=600, top=200, left=200, scrollbars=yes, toolbars=no');
            }
            </script>
			<?=FrmHidden("pa_fotoboekid", $PA_FotoboekID)?>
            <tr class='regel' valign='top'>
                <td>Fotoboek</td>
                <td>:</td>
                <td>
                <div id="fotoboektitel"></div><br />
				<?
				$query_rsafb = "SELECT * FROM fotoboeken WHERE fotoboekid=". $PA_FotoboekID .";";
				$rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
				$row_rsafb = mysql_fetch_assoc($rsafb);
				if ($row_rsafb['fotoboekid']>0)
				{
				?>
				<script type="text/javascript">
					document.getElementById("fotoboektitel").innerHTML = "<?=$row_rsafb['fbtitel']?> (<?=MaakDatum($row_rsafb['fbdatum'])?>)";
				</script>
				<a onclick="kiesFotoboek('pa_fotoboekid');return false;" href="">Klik hier om een ander fotoboek te kiezen of aan te maken</a><br />
		    	<?
				}
				else
				{
				?>
				<a onclick="kiesFotoboek('pa_fotoboekid');return false;" href="">Klik hier om een fotoboek te kiezen of aan te maken</a><br />
               	<?
				}
				mysql_free_result($rsafb);
				?>
                </td>
            </tr>
            <tr>
            <td>Voorbeeld</td>
            <td>:</td>
            <td></td>
            </tr>
			<?
			break;
		case "BLANK":
			?>
			<?=FrmText("Aantal witregels", "pa_aantalkeer", $pa_aantalkeer, 5, 3)?>
			<?
			break;
		case "NIEUWSBHZ":
		case "NIEUWSBHZFB":
		case "NIEUWSHOME":
			?>
			<?=FrmText("Aantal berichten", "pa_aantalkeer", $pa_aantalkeer, 5, 3)?>
            <?=FrmCheckbox("Zonder inleiding", "pa_nwsgeeninleiding", $pa_nwsgeeninleiding) ?>
		    <?=FrmCheckbox("Alleen voor leden", "pa_alleenleden", $pa_alleenleden) ?>
			<?
			break;

		case "CONTACT":
			?>
			<tr><td colspan="100">
			<?
			ToonContactgegevens($GLOBALS['StdTaalcode']);
			?>
			<hr class="streep" /><i>Contactgegevens zijn te beheren via Stamgegevens, Bedrijfsgegevens</i>
			</td></tr>
			<?
			break;

	}
	?>

    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("pa_naam") ?>

<script language="JavaScript">
function toonVoorbeeld()
{
    <? switch($Paragraaftype)
	{
		case "ELINK":
		?>
            var tmpTekst;
            if (document.getElementById("pa_linktekst").value!="")
            {
                tmpTekst = document.getElementById("pa_linktekst").value;
            }
            else
            {
                tmpTekst = document.getElementById("pa_elinkurl").value;
            }
            if (document.getElementById("pa_elinkurl").value!="")
            {
                document.getElementById("voorbeeld").innerHTML = "<a href='" + document.getElementById("pa_elinkurl").value + "' target='_blank'>" + tmpTekst + "</a>";
            }
            else
            {
                document.getElementById("voorbeeld").innerHTML = "<font color='<?=$GLOBALS['KleurRood']?>'>Voorbeeld niet beschikbaar</font>";
            }
        <?
        	break;
		case "EMAIL":
		?>
            var tmpTekst;
            if (document.getElementById("pa_linktekst").value!="")
            {
                tmpTekst = document.getElementById("pa_linktekst").value;
            }
            else
            {
                tmpTekst = document.getElementById("pa_emailadres").value;
            }
            if (document.getElementById("pa_emailadres").value!="")
            {
                document.getElementById("voorbeeld").innerHTML = "<a href='mailto:" + document.getElementById("pa_emailadres").value + "'>" + tmpTekst + "</a>";
            }
            else
            {
                document.getElementById("voorbeeld").innerHTML = "<font color='<?=$GLOBALS['KleurRood']?>'>Voorbeeld niet beschikbaar</font>";
            }
        <?
        	break;
		case "FOTOBOEK":
		?>
            var tmpTekst;
            if (document.getElementById("tmpfotoboektitel").value!="")
            {
                tmpTekst = document.getElementById("tmpfotoboektitel").value;
                document.getElementById("fotoboektitel").innerHTML = tmpTekst + "<br><br>";
            }
            else
            {
                document.getElementById("fotoboektitel").innerHTML = "";
            }
        <?
        	break;
        default:
        	//
        }
		?>
}
function toonILinkVoorbeeld()
{
    <? switch($Paragraaftype)
	{
		case "ILINK":
		?>
            var tmpTekst;
            if (document.getElementById("pa_ilinktekst").value!="")
            {
                document.getElementById("ilinktekst").innerHTML = "" + document.getElementById("pa_ilinktekst").value + "";
            }
            if (document.getElementById("pa_linktekst").value!="")
            {
                tmpTekst = document.getElementById("pa_linktekst").value;
            }
            else
            {
                tmpTekst = document.getElementById("pa_ilinktekst").value;
            }
            if (document.getElementById("pa_ilinkid").value!="" && document.getElementById("pa_ilinkid").value!="-1")
            {
                document.getElementById("ilinkvoorbeeld").innerHTML = "<a href='<?=$GLOBALS['ApplicatieRoot']?>/?pagid=" + document.getElementById("pa_ilinkid").value + "' target='_blank'>" + tmpTekst + "</a>";
            }
            else
            {
                document.getElementById("ilinkvoorbeeld").innerHTML = "<font color='<?=$GLOBALS['KleurRood']?>'>Voorbeeld niet beschikbaar</font>";
            }
        <?
        	break;
		case "EVENTLINK":
		?>
            var tmpTekst;
            if (document.getElementById("pa_ilinktekst").value!="")
            {
                document.getElementById("ilinktekst").innerHTML = "" + document.getElementById("pa_ilinktekst").value + "";
            }
            if (document.getElementById("pa_linktekst").value!="")
            {
                tmpTekst = document.getElementById("pa_linktekst").value;
            }
            else
            {
                tmpTekst = document.getElementById("pa_ilinktekst").value;
            }
            if (document.getElementById("pa_eventlinkid").value!="" && document.getElementById("pa_eventlinkid").value!="-1")
            {
                document.getElementById("ilinkvoorbeeld").innerHTML = "<a href='<?=$GLOBALS['ApplicatieRoot']?>/?evid=" + document.getElementById("pa_eventlinkid").value + "' target='_blank'>" + tmpTekst + "</a>";
            }
            else
            {
                document.getElementById("ilinkvoorbeeld").innerHTML = "<font color='<?=$GLOBALS['KleurRood']?>'>Voorbeeld niet beschikbaar</font>";
            }
        <?
        	break;
        default:
        	//
        }
		?>

}
function toonFileVoorbeeld()
{
            var tmpTekst;
            var tmpImg;
            if (document.getElementById("pa_linktekst").value!="")
            {
                tmpTekst = document.getElementById("pa_linktekst").value;
            }
            else
            {
                tmpTekst = document.getElementById("tmpbestandsnaam").value;
            }
            if (document.getElementById("tmpbestandsicoon").value!="")
            {
                tmpImg = "<?=$GLOBALS['ApplicatieRoot']?>/sybit/icons/" + document.getElementById("tmpbestandsicoon").value + ".gif";
            }
            else
            {
                tmpImg = "<?=$GLOBALS['ApplicatieRoot']?>/sybit/icons/help.gif";
            }
            if (document.getElementById("pa_bestandid").value!="" && document.getElementById("pa_bestandid").value!="-1")
            {
                document.getElementById("fileicon").src = tmpImg;
                document.getElementById("filevoorbeeld").innerHTML = "<a href='<?=$GLOBALS['ApplicatieRoot']?>/sybit/bestand.php?bestid=" + document.getElementById("pa_bestandid").value + "&bestguid=" + document.getElementById("tmpbestandsguid").value + "' target='_blank'>" + tmpTekst + "</a>";
            }
            else
            {
                document.getElementById("filevoorbeeld").innerHTML = "<font color='<?=$GLOBALS['KleurRood']?>'>Voorbeeld niet beschikbaar</font>";
            }
}
    <? switch($Paragraaftype)
	{
		case "ELINK":
		?>
toonVoorbeeld();
        <?
        	break;
		case "EMAIL":
		?>
toonVoorbeeld();
        <?
        	break;
		case "ILINK":
		?>
toonILinkVoorbeeld();
        <?
        	break;
		case "EVENTLINK":
		?>
toonILinkVoorbeeld();
        <?
        	break;
		case "FILE":
		?>
toonFileVoorbeeld();
        <?
        	break;
        default:
        	//
        }
		?>

 </script>
<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("paragraaf");
frmvalidator.addValidation("pa_naam","req","Omschrijving is verplicht");
frmvalidator.addValidation("pa_volgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("pa_volgorde","num","Volgorde is ongeldig");

    <? switch($Paragraaftype)
	{
		case "ILINK":
		?>
        frmvalidator.addValidation("pa_ilinkid","req","Interne link is verplicht");
        frmvalidator.addValidation("pa_ilinkid","gt=0","Interne link is verplicht");
        <?
        	break;
		case "ELINK":
		?>
        frmvalidator.addValidation("pa_elinkurl","req","Internetadres (URL) is verplicht");
        <?
        	break;
		case "EMAIL":
		?>
        frmvalidator.addValidation("pa_emailadres","req","E-mailadres is verplicht");
        <?
        	break;
		case "TEKST":
		?>
        frmvalidator.addValidation("pa_tekst","reqCK","Tekst is verplicht");
        <?
        	break;
		case "FILM":
		?>
        frmvalidator.addValidation("pa_tekst","req","Code is verplicht");
        <?
			break;
		case "FOTO":
		?>
        frmvalidator.addValidation("pa_afbeeldingid","req","Afbeelding is verplicht");
        frmvalidator.addValidation("pa_afbeeldingid","gt=0","Afbeelding is verplicht");
        <?
        	break;
		case "FORM":
		?>
        frmvalidator.addValidation("pa_formulierid","req","Formulier kiezen is verplicht");
        frmvalidator.addValidation("pa_formulierid","gt=0","Formulier kiezen is verplicht");
        <?
        	break;
		case "FOTOBOEK":
		?>
        frmvalidator.addValidation("pa_fotoboekid","req","Kies een fotoboek");
        frmvalidator.addValidation("pa_fotoboekid","gt=0","Kies een fotoboek");
        <?
        	break;
		case "FILE":
		?>
        frmvalidator.addValidation("pa_bestandid","req","Bestand is verplicht");
        frmvalidator.addValidation("pa_bestandid","gt=0","Bestand is verplicht");
        <?
        	break;
		case "BLANK":
		?>
        frmvalidator.addValidation("pa_aantalkeer","req","Aantal witregels is verplicht");
        frmvalidator.addValidation("pa_aantalkeer","num","Aantal witregels heeft een ongeldige waarde");
        frmvalidator.addValidation("pa_aantalkeer","gt=0","Aantal witregels heeft een ongeldige waarde");
        <?
        	break;
		case "NIEUWSHOME":
		?>
        frmvalidator.addValidation("pa_aantalkeer","req","Aantal berichten is verplicht");
        frmvalidator.addValidation("pa_aantalkeer","num","Aantal berichten heeft een ongeldige waarde");
        frmvalidator.addValidation("pa_aantalkeer","gt=0","Aantal berichten heeft een ongeldige waarde");
        <?
        	break;
        default:
        	//
        }
		?>
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
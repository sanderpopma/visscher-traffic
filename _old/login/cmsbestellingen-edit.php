<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId())) {die;}
if ($GLOBALS['itemid']<1) {
	redirect("cmsbestellingen.php");
	die;
}
if ($GLOBALS['itemid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Details bestelling"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("printer", "Printen", "javascript:window.print();") ?>
    <?=ToonCMSNavKnop("stop", "Terug", "cmsbestellingen.php") ?>
<?=SluitCMSNavBalk();?>

<tr><td><table width="<?=$GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteLinks']-140?>" align="center"><tr><td>

<?
	$query_afb = "SELECT * FROM winkel_bestellingen WHERE bestellingid=" . $GLOBALS['itemid']." LIMIT 1";
	$rsafb = mysql_query($query_afb, $GLOBALS['conn']) or die(mysql_error());
	$row_rsafb = mysql_fetch_assoc($rsafb);
	if (mysql_num_rows($rsafb)>0) {
	$mytaal = $row_rsafb['be_taalcode'];
	$allesopvoorraad = true;
	$voorraadicon = "isopvoorraad";
	$txtvoorraad = Vertaal("zijnopvoorraad");
	$query_art = "SELECT * FROM winkel_bestelregels INNER JOIN artikelen ON artikelen.artikelid=winkel_bestelregels.br_artikelid WHERE br_bestellingid=" . $row_rsafb['bestellingid'];
	$rsart = mysql_query($query_art, $GLOBALS['conn']) or die(mysql_error());
	$row_rsart = mysql_fetch_assoc($rsart);
	if (mysql_num_rows($rsart)>0) {
		$totprijs = 0;
		$totaantal = 0;
		?>
		<table width="100%" cellpadding="2" cellspacing="0" class="arttabel">
		<tr>
			<td class="artkop" width="50">Omschrijving</td>
			<td class="artkop" width="70"><?=Vertaal("artprijs")?></td>
			<td class="artkop" width="50"><?=Vertaal("aantal")?></td>
			<td class="artkop" width="70" align="right"><?=Vertaal("arteenheid")?></td>
			<td class="artkop" width="40" align="right"><?=Vertaal("totaal")?></td>
			<td class="artkop" width="60" align="right"><?=Vertaal("totaalprijs")?></td>
			<td class="artkop" width="20"></td>
		</tr>
		<?
		do
		{
			$prijsper = toInt($row_rsart["art_prijs_per"]);
			if ($prijsper<1) { $prijsper = 1; }

			$regelprijs = ($row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"])*($row_rsart["art_prijs"]/$prijsper);
			$regelaantal = $row_rsart["br_aantal"]*$row_rsart["br_bestelhoeveelheid"];
			$totprijs = $totprijs + $regelprijs;
			$totaantal = $totaantal + $regelaantal;

			$tdclass="artregel";
			?>
			<tr><td class="artvoet" colspan="20" style="color: black;">
				<a target="_blank" class="klein" href="<?=$GLOBALS['ServerRoot']?>/<?=$GLOBALS['WebshopHomeURL']?>&prodgrpid=<?=GeefDbWaarde("pr_prodgrpid", "producten", "productid=".$row_rsart['art_productid'])?>&artid=<?=$row_rsart['artikelid']?>">
				<?
				echo GeefDbWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=(SELECT pr_prodgrpid FROM producten WHERE productid=".$row_rsart['art_productid'] . ")") . " - " . GeefDbWaarde("productnaam_nl", "producten", "productid=".$row_rsart['art_productid']);
				?>
				</a>
			</td></tr>
			<tr>
				<td class="<?=$tdclass?>">
				<?=$row_rsart['art_naam']?>
				</td>
				<td class="<?=$tdclass?>"><?=$GLOBALS['StdValutaTeken']?>&nbsp;<?=MaakDecimal($row_rsart["art_prijs"])?></td>
				<td class="<?=$tdclass?>" align="right"><?=$row_rsart['br_aantal']?></td>
				<td class="<?=$tdclass?>" align="right"><?=$row_rsart["br_bestelhoeveelheid"]?></td>
				<td class="<?=$tdclass?>" align="right"><?=$regelaantal?></td>
				<td class="<?=$tdclass?>" align="right"><?=MaakBedrag($regelprijs)?>&nbsp;</td>
				<td class="<?=$tdclass?>"></td>
			</tr>
			<?
		}
		while ($row_rsart = mysql_fetch_assoc($rsart));


		$btwpercentage = $row_rsafb['btwpercentage'];
		$indicatieverzendkosten = $row_rsafb['indicatieverzendkosten'];
		$btwbedrag = ($totprijs+$indicatieverzendkosten)*($btwpercentage/100);
		$totinclbtw = $btwbedrag + $totprijs + $indicatieverzendkosten;

		?>
		<tr>
			<td class="artvoet" colspan="100">&nbsp;</td>
		</tr>
		</table>
		<?
	}
	mysql_free_result($rsart);

	?>
	<table width="100%" cellpadding="2" cellspacing="0" class="arttabel">
	<tr>
		<td class="artkop" colspan="4">Totaal van deze bestelling</td>
	</tr>
	<tr>
		<td class="artvoet" width="150"><b><?=Vertaal("totaal")?></b></td>
		<td class="artvoet" align="right" width="80"><b><?=MaakBedrag($totprijs)?></b></td>
		<td class="artvoet" width="20"></td>
		<td class="artvoet" width="<?=($GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteLinks']-40-250)?>"></td>
	</tr>
	<tr>
		<td class="artvoet" width="150"><b>Verzendkosten</b></td>
		<td class="artvoet" align="right" width="80"><b><?=MaakBedrag($indicatieverzendkosten)?></b></td>
		<td class="artvoet" width="20"></td>
		<td class="artvoet" width="<?=($GLOBALS['BreedteWebsite']-$GLOBALS['BreedteWebsiteLinks']-40-250)?>"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("totaal")?> (<?=Vertaal("exclbtw")?>)</b></td>
		<td class="artvoet" align="right"><b><?=MaakBedrag($totprijs+$indicatieverzendkosten)?></b></td>
		<td class="artvoet"></td>
		<td class="artvoet"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("btw")?> (<?=MaakPercentage($btwpercentage)?>)</b></td>
		<td class="artregel" align="right"><b><?=MaakBedrag($btwbedrag)?></b></td>
		<td class="artvoet"><b>+</b></td>
		<td class="artvoet"></td>
	</tr>
	<tr>
		<td class="artvoet"><b><?=Vertaal("totaal")?> (<?=Vertaal("inclbtw")?>)</b></td>
		<td class="artvoet" align="right"><b><?=MaakBedrag($totinclbtw)?></b></td>
		<td class="artvoet"></td>
		<td class="artvoet"></td>
	</tr>

	<tr>
		<td class="artvoet" colspan="100">&nbsp;</td>
	</tr>

	<tr>
		<td class="artkop" colspan="10">Gegevens van de aanvrager</td>
	</tr>
	<tr><td><?=Vertaal("bedrijfsnaam")?></td><td colspan="4"><?=$row_rsafb['bedrijfsnaam']?></td></tr>
	<tr><td><?=Vertaal("tav")?></td><td colspan="4"><?=$row_rsafb['tav']?></td></tr>
	<tr><td><?=Vertaal("afleveradres")?></td><td colspan="4"><?=$row_rsafb['afleveradres']?></td></tr>
	<tr><td><?=Vertaal("postcode")?> / <?=Vertaal("plaats")?></td><td colspan="4"><?=$row_rsafb['afleverpostcode']?>&nbsp;&nbsp;<?=$row_rsafb['afleverplaats']?>&nbsp;<? if ($row_rsafb['afleverland']!="") { echo "(".$row_rsafb['afleverland'].")";} ?></td></tr>
	<tr><td><?=Vertaal("factuuradres")?></td><td colspan="4"><?=$row_rsafb['factuuradres']?></td></tr>
	<tr><td><?=Vertaal("postcode")?> / <?=Vertaal("plaats")?></td><td colspan="4"><?=$row_rsafb['factuurpostcode']?>&nbsp;&nbsp;<?=$row_rsafb['factuurplaats']?>&nbsp;<? if ($row_rsafb['factuurland']!="") { echo "(".$row_rsafb['factuurland'].")";} ?></td></tr>
	<tr><td><?=Vertaal("emailadres")?></td><td colspan="4"><a href="mailto:<?=$row_rsafb['emailadres']?>"><?=$row_rsafb['emailadres']?></a></td></tr>
	<tr><td><?=Vertaal("telnr")?></td><td colspan="4"><?=$row_rsafb['telefoonnummer']?></td></tr>
	<tr><td><?=Vertaal("faxnr")?></td><td colspan="4"><?=$row_rsafb['faxnummer']?></td></tr>
	<tr valign="top"><td><?=Vertaal("gewensteleverdatum")?></td><td colspan="4"><?=MaakDatum($row_rsafb['gewensteleverdatum'])?><br /></td></tr>
	<tr valign="top"><td>Verzendwijze</td><td colspan="4"><?=GeefDbWaarde("verzendwijzenaam_".strtolower($GLOBALS['GekozenTaal']), "winkel_verzendwijzen", "verzendwijzecode='".$row_rsafb['be_verzendwijzecode']."'")?><br /></td></tr>
	<tr valign="top"><td>Betaalwijze</td><td colspan="4"><?=GeefDbWaarde("betaalwijzenaam_".strtolower($GLOBALS['GekozenTaal']), "winkel_betaalwijzen", "betaalwijzecode='".$row_rsafb['be_betaalwijzecode']."'")?><br /></td></tr>


	<tr valign="top"><td><?=Vertaal("opmerkingen")?></td><td colspan="4"><?=VervangBRMemo($row_rsafb['be_opmerkingen'])?></td></tr>

	<tr>
		<td class="artvoet" colspan="100">&nbsp;</td>
	</tr>

	<tr>
		<td class="artkop" colspan="10">Gegevens van de aanvraag</td>
	</tr>
	<tr><td>Ontvangen op:</td><td colspan="4"><?=MaakDatum($row_rsafb['be_datumtoegevoegd'])?></td></tr>

	<tr>
		<td class="artkop" colspan="10">Status van de order</td>
	</tr>
	<tr><td>Bestelstatus:</td><td colspan="4"><?=GeefDbWaarde("bestelstatusnaam_nl", "winkel_bestelstatus", "bestelstatuscode='" . $row_rsafb['bestelstatus'] . "'")?></td></tr>
	<script type="text/javascript">
	function GereedMelden(){
		if (confirm("Deze bestelling gereedmelden? (de besteller krijgt hiervan geen bericht)"))
		{
			location.href = "cmsbestellingen-gereed.php?itemid=<?=$GLOBALS['itemid']?>";
		}
	}
	function LatenVervallen(){
		if (confirm("Deze bestelling laten vervallen? (de besteller krijgt hiervan geen bericht)"))
		{
			location.href = "cmsbestellingen-vervallen.php?itemid=<?=$GLOBALS['itemid']?>";
		}
	}
	function VerWijderen(){
		if (confirm("Deze bestelling verwijderen? (de besteller krijgt hiervan geen bericht)"))
		{
			location.href = "cmsbestellingen-del.php?itemid=<?=$GLOBALS['itemid']?>";
		}
	}

	</script>
	<?
	switch($row_rsafb['bestelstatus']){
		default:
			?>
			<tr><td>Gereedmelden:</td><td colspan="4"><?=Icoon("gereed")?><a href="#" onclick="GereedMelden(); return false;">Deze bestelling gereedmelden</a></td></tr>
			<tr><td>Datum gereed:</td><td colspan="4"><?=MaakDatum($row_rsafb['datum_gereed'])?>&nbsp;<?=GeefDbWaarde("beheerafbeeldnaam", "cmsbeheerders", "beheerderid=" . $row_rsafb['door_gereed_mdwid'] . "")?></td></tr>
			<tr><td>Laten vervallen:</td><td colspan="4"><?=Icoon("nietgereed")?><a href="#" onclick="LatenVervallen(); return false;">Deze bestelling laten vervallen</a></td></tr>
			<tr><td>Datum vervallen:</td><td colspan="4"><?=MaakDatum($row_rsafb['datum_vervallen'])?>&nbsp;<?=GeefDbWaarde("beheerafbeeldnaam", "cmsbeheerders", "beheerderid=" . $row_rsafb['door_vervallen_mdwid'] . "")?></td></tr>
			<tr><td>Verwijderen:</td><td colspan="4"><?=Icoon("verwijderen")?><a href="#" onclick="VerWijderen(); return false;">Deze bestelling verwijderen</a></td></tr>
			<?
			;
	} // switch
	?>

	<tr>
		<td class="artvoet" colspan="100">&nbsp;</td>
	</tr>

	</table>

	<br />
	<?
	}
	mysql_free_result($rsafb);
?>

</td></tr></table>
</td></tr>
<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
if (!IsAdministrator(GeefHuidigeUserId()))
{
    die;
}
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Referenties beheren")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Referentie toevoegen", "cmsgooglemapsreferenties-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
	<td><b>Naam referentie</b></td>
    <td><b>Adres</b></td>
    <td><b>Postcode</b></td>
    <td><b>Plaats</b></td>
    <td><b>Land</b></td>
    <td align="center"><b>Op kaart</b></td>
    <td width="100" align="center"><b>Zichtbaar</b></td>
    <td width="90"><b>Verwijder</b></td>
</tr>

<?
function ToonOpMap($met_lng, $met_lat){
	if ($met_lng!="" && $met_lat!="") {
		echo Icoon("googlereferenties");
	}
	else
	{
		echo Icoon("stop");
	}
}
$query_rs = "SELECT * FROM googlemapsreferenties ORDER BY ref_publiceren DESC, ref_naam;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsgooglemapsreferenties-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["referentieid"];
	    $VerwijderURL = "cmsgooglemapsreferenties-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["referentieid"];
	    $VerwijderMelding = "Weet u zeker dat u deze referentie wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["ref_naam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["ref_adres"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["ref_postcode"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["ref_plaats"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["ref_land"]?></a></td>
			    <td align="center"><?=ToonOpMap($row_rs["ref_lng"], $row_rs["ref_lat"])?></td>
			    <td align="center"><?=ToonGereed($row_rs["ref_publiceren"])?></td>
				<td>
				<? if ($MagVerwijderen) {
					?>
					<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
					<?
				}
				else
				{
					?>
					<center><?=Icoon("stop")?></center>
					<?
				}
				?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if Not HeeftRechten(GeefHuidigeUserId, "PRODUCTEN") then Response.End
if ($GLOBALS['artid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
if ($GLOBALS['prodgrpid']<1 || $GLOBALS['prodid']<1) {
	redirect("cmsproductgroepen.php");
}
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM artikelen WHERE artikelid=".$GLOBALS['artid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['artikelid']>0)
    {
    	$art_productid = $row_rs['art_productid'];
        $art_naam = $row_rs['art_naam'];
        $art_omschrijving = $row_rs['art_omschrijving'];
        $art_publiceren = Int2Bool($row_rs['art_publiceren']);
        $art_prijs = MaakDecimal($row_rs['art_prijs']);
        $art_afbeeldingid = toInt($row_rs['art_afbeeldingid']);
        $art_min_bestelhoeveelheid = $row_rs['art_min_bestelhoeveelheid'];
    }
    mysql_free_result($rs);
}
else
{
    $art_publiceren = false;
    $art_productid = $GLOBALS['prodid'];
	$art_prijs = 0;
	$art_afbeeldingid = -1;
	$art_min_bestelhoeveelheid = 1;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Artikel " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Annuleren", "cmsartikelen.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']."&prodgrpid=".$GLOBALS['prodgrpid']."&prodid=".$GLOBALS['prodid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmsartikelen-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&prodgrpid=" . $GLOBALS['prodgrpid'] . "&prodid=".$GLOBALS['prodid'] . "&artid=".$GLOBALS['artid'], "artikel", 700)?>
	<?=FrmTekstRegel("Productgroep", GeefDBWaarde("prodgrp_naam_nl", "productgroepen", "prodgrpid=".$GLOBALS['prodgrpid']))?>
	<?=FrmSelectByQuery("Subcategorie", "art_productid", $art_productid, "SELECT productid, productnaam_nl FROM producten WHERE pr_prodgrpid=" . $GLOBALS['prodgrpid'] ." ORDER BY pr_volgorde", "productid", "productnaam_nl", true)?>
    <?=FrmText("Naam artikel", "art_naam", $art_naam, 60, 250) ?>
    <?=FrmText(MaakBedragLabel("Prijs"), "art_prijs", $art_prijs, 4, 4) ?>
    <?=FrmText("Min. bestelhoeveelheid", "art_min_bestelhoeveelheid", $art_min_bestelhoeveelheid, 6, 8) ?>

    <?=FrmCheckbox("Publiceren", "art_publiceren", $art_publiceren) ?>

    <?=FrmKopregel("Afbeelding") ?>
	<?=FrmKoptekst("<span style='color: red';'>Let op: deze wordt op " . $GLOBALS['AfbeeldingMaxArtikel'] . "x" . $GLOBALS['AfbeeldingMaxArtikel'] . "px (1:1) afgebeeld");?>
    <script language="javascript">
    function kiesFoto(fld_toupdate)
    {
		result = window.open('kies_fotoart.php?OpenerFieldToUpdate=' + fld_toupdate, 'kiesfoto', 'width=800, height=200, top=200, left=200, scrollbars=yes, toolbars=no');
    }
    function toonFoto()
    {
        document.getElementById("afbklein").src = "<?=$GLOBALS['ApplicatieRoot']?>/<?=$GLOBALS['UploadImageFolder']?>/"+document.getElementById("AfbJaar").value+"/"+document.getElementById("AfbMaand").value+"/"+document.getElementById("AfbGUID").value+"."+document.getElementById("AfbExt").value+"";
        document.getElementById("afbklein").style.border = "solid 1px black"
        document.getElementById("AfbVerwijderLink").style.visibility = "visible";
    }
    function wisFoto()
    {
        if(confirm('Deze afbeelding niet langer gebruiken?'))
        {
            document.getElementById("afbklein").src = "<?=$GLOBALS['AppImgRoot']?>/leeg.gif";
            document.getElementById("afbklein").style.border = "0px";
            document.getElementById("art_afbeeldingid").value = "-1";
            document.getElementById("AfbVerwijderLink").style.visibility = "hidden";
        }
    }
    </script>
	<?=FrmHidden("art_afbeeldingid", $art_afbeeldingid)?>

    <tr class='regel' valign='top'>
        <td colspan="10">
        <?
	    $query_rsafb = "SELECT * FROM afbeeldingen WHERE afbeeldingid=". $art_afbeeldingid .";";
	    $rsafb = mysql_query($query_rsafb, $GLOBALS['conn']) or die(mysql_error());
	    $row_rsafb = mysql_fetch_assoc($rsafb);
	    if ($row_rsafb['afbeeldingid']>0)
	    {
	    	$AfbGUID = MaakGUIDString($row_rsafb["afbeeldingguid"]);
	    	?>
                <?=FrmHidden("AfbMaand", Maak2Dig(datMonth($row_rsafb["afbdatum"])))?>
                <?=FrmHidden("AfbJaar", datYear($row_rsafb["afbdatum"]))?>
                <?=FrmHidden("AfbGUID", $AfbGUID)?>
                <?=FrmHidden("AfbExt", $row_rsafb["afbeeldingextentie"])?>
                <a onclick="kiesFoto('art_afbeeldingid');return false;" href="">Klik hier om een andere foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
	    	<?
	    	$defVerwVisible = "visible";
        }
        else
        {
        	$defVerwVisible = "hidden";
        	?>
                <?=FrmHidden("AfbMaand", "0")?>
                <?=FrmHidden("AfbJaar", "0")?>
                <?=FrmHidden("AfbGUID", "0")?>
                <?=FrmHidden("AfbExt", "0")?>
                <a onclick="kiesFoto('art_afbeeldingid');return false;" href="">Klik hier om een foto te kiezen of te uploaden</a><br />
                <br />
                <img name="afbklein" id="afbklein" src="<?=$GLOBALS['AppImgRoot']?>/leeg.gif"/>
        	<?
		}
	    mysql_free_result($rsafb);
        ?>
            <br /><br />
            <div id="AfbVerwijderLink" style="visibility: <?=$defVerwVisible?>;">
            <?=PlaatsICoonLink("verwijderen", "Deze afbeelding verwijderen", "javascript:wisFoto();", "a") ?>
            </div>

        <? if ($AfbGUID!="" && $AfbGUID!="0") { ?>
            <script language="javascript">
                toonFoto();
            </script>
        <? } ?>
        </td>

    </tr>

    <?=FrmKopregel("Omschrijving artikel") ?>
    <?=FrmFCKText("Omschrijving", "art_omschrijving", $art_omschrijving, 100) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("art_naam") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("artikel");
frmvalidator.addValidation("art_naam","req","Naam is verplicht");
frmvalidator.addValidation("art_productid","req","Subcategorie is verplicht");
frmvalidator.addValidation("art_productid","gt=-1","Subcategorie is verplicht");
frmvalidator.addValidation("art_min_bestelhoeveelheid","req","Minimale bestelhoeveelheid is verplicht");
frmvalidator.addValidation("art_min_bestelhoeveelheid","gt=-1","Minimale bestelhoeveelheid is verplicht");
frmvalidator.addValidation("art_min_bestelhoeveelheid","num","Minimale bestelhoeveelheid heeft een ongeldige waarde");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
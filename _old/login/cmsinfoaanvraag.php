<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$rowsPerPage = 50;

// by default we show first page
$pageNum = 1;

// if $_GET['page'] defined, use it as page number
if(isset($_GET['page']))
{
    $pageNum = $_GET['page'];
}
// counting the offset
$offset = ($pageNum - 1) * $rowsPerPage;
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Informatieaanvragen")?>
<tr>
<?
// how many rows we have in database
$query   = "SELECT COUNT(aanvraagid) AS numrows FROM informatieaanvragen";
$rs = mysql_query($query, $GLOBALS['conn']) or die(mysql_error());
$row = mysql_fetch_assoc($rs);
$numrows = $row['numrows'];
mysql_free_result($rs);

// how many pages we have when using paging?
$maxPage = ceil($numrows/$rowsPerPage);

// print the link to access each page
$self = $_SERVER['PHP_SELF'];
$nav  = '';
?>
<tr class="kadervoet"><td colspan="100" align="center" class="paging">
<?
for($page = 1; $page <= $maxPage; $page++)
{
   if ($page == $pageNum)
   {
      $nav .= "&nbsp;<b class='paging'>" . (($page*$rowsPerPage)-($rowsPerPage)+1) . " - " . ($page*$rowsPerPage) . "</b>&nbsp;"; // no need to create a link to current page
   }
   else
   {
      $nav .= " <a class='paging' href=\"$self?page=$page\">" . (($page*$rowsPerPage)-($rowsPerPage)+1) . " - " . ($page*$rowsPerPage) . "</a> ";
   }
}
if ($pageNum > 1)
{
   $page  = $pageNum - 1;
   $prev  = " <a  class='paging'href=\"$self?page=$page\">[&lt;]</a> ";

   $first = " <a  class='paging'href=\"$self?page=1\">[&lt;&lt;]</a> ";
}
else
{
   $prev  = '&nbsp;'; // we're on page one, don't print previous link
   $first = '&nbsp;'; // nor the first page link
}

if ($pageNum < $maxPage)
{
   $page = $pageNum + 1;
   $next = " <a  class='paging'href=\"$self?page=$page\">[&gt;]</a> ";

   $last = " <a  class='paging'href=\"$self?page=$maxPage\">[&gt;&gt;]</a> ";
}
else
{
   $next = '&nbsp;'; // we're on the last page, don't print next link
   $last = '&nbsp;'; // nor the last page link
}

// print the navigation link
echo $first . $prev . $nav . $next . $last;
?>
</td></tr>
</tr>

<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="100"><b>Datum</b></td>
    <td><b>Bedrijfsnaam</b></td>
    <td><b>Contact</b></td>
    <td><b>E-mail</b></td>
    <td width="80" align="center"><b>Afgehandeld</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM informatieaanvragen ORDER BY datumontvangen DESC LIMIT $offset, $rowsPerPage;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
	    $WijzigURL = "cmsinfoaanvraag-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&avid=" .$row_rs["aanvraagid"] . "&page=$pageNum";
	    $VerwijderURL = "cmsinfoaanvraag-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&avid=" .$row_rs["aanvraagid"] . "&page=$pageNum";
	    $VerwijderMelding = "Weet u zeker dat u dit bericht wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
		$mystatus = 1;
		if ($row_rs["iscontactopgenomen"]) { $mystatus = 2; }
		if ($row_rs["isbezoekgepland"]) { $mystatus = 3; }
		if ($row_rs["isofferteuitgebracht"]) { $mystatus = 4; }
		if ($row_rs["isopdrachtontvangen"]) { $mystatus = 5; }
		if ($row_rs["isafgehandeld"]) {
			if ($mystatus!=5) {
				$mystatus = 9;
			}
			$mystatus = 5;		// toevoegevoegd flevoboulevard
		}
		switch($mystatus){
			case 1:
				$mycol = $GLOBALS['KleurNietWijzigbaar'];
				break;
			case 2:
				$mycol = $GLOBALS['KleurStatusWit'];
				break;
			case 3:
				$mycol = $GLOBALS['KleurStatusGeel'];
				break;
			case 4:
				$mycol = $GLOBALS['KleurStatusOranje'];
				break;
			case 5:
				$mycol = $GLOBALS['KleurStatusGroen'];
				break;
			case 9:
				$mycol = $GLOBALS['KleurStatusRood'];
				break;
			default:
				$mycol = $GLOBALS['KleurRegelInv'];;
		} // switch
?>
			<tr class="regel" valign="top">
			    <td style="background-color: <?=$mycol?>;"><?=PlaatsIcoonLink("telefoon", "", $WijzigURL, "Informatieaanvraag bekijken")?></td>
			    <td><?=MaakDatum($row_rs["datumontvangen"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["bedrijfsnaam"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["contactpersoon"]?></a></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["emailadres"]?></a></td>
			    <td align="center"><?=ToonGereed($row_rs["isafgehandeld"])?></td>
			    <td style="color: <?=$mytxtcol?>;"><?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?></td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
$openerfieldtoupdate = $_GET['openerfieldtoupdate'];

if ($GLOBALS['fotoboekid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM fotoboeken WHERE fotoboekid=".$GLOBALS['fotoboekid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['fotoboekid']>0)
    {
    	$fbtitel = $row_rs['fbtitel'];
    	$fbdatum = MaakDatum($row_rs['fbdatum']);
    }
	else
	{
		die;
	}
    mysql_free_result($rs);
}
else
{
	$fbdatum = MaakDatum(date('d-m-Y'));
}
?>
<?=OpenPagina("CMS", "")?>

<script type="text/javascript">
	function OpslaanEnKiezen(){
		document.getElementById("opslaankiezen").value = 1;
		document.getElementById("VerstuurKnop").click();
	}
</script>
<?=OpenCMSTabel("Fotoboek " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
	<?
	if ($PgMode=="WIJZIG")
	{
	?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan & terug naar lijst", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan & Selecteren", "javascript:OpslaanEnKiezen();") ?>
    <?
    }
    else
	{
	?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
	<?
	}
	?>
	<?=ToonCMSNavKnop("stop", "Annuleren", "kies_fotoboeklijst.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("kies_fotoboek-edit2.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate, "fotoboek", 500)?>
	<?=FrmHidden("opslaankiezen", 0)?>
    <?=FrmText("Titel/omschrijving", "fbtitel", $fbtitel, 50, 99) ?>
    <?=FrmDatum("Datum fotoboek", "fbdatum", $fbdatum) ?>
    <?=FrmSubmit("Opslaan")?>
<?=SluitForm()?>

<?=ZetFocus("fbtitel") ?>

<script type="text/javascript">
var frmvalidator  = new Validator("fotoboek");
frmvalidator.addValidation("fbtitel","req","Titel is verplicht");
</script>

<?=SluitCMSTabel(); ?>

<?
if ($PgMode=="WIJZIG")
{
?>

<?=OpenCMSTabel("Foto's in dit fotoboek"); ?>
<?=OpenCMSNavBalk(); ?>
	<?=ToonCMSNavKnop("ster_plus", "Foto toevoegen", "kies_fotoboekfoto-edit.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate) ?>
<?=SluitCMSNavBalk();?>
<tr valign="top"><td colspan="100">

<?
	$query_rs = "SELECT * FROM afbeeldingen INNER JOIN fotoboeken_fotos ON fotoboeken_fotos.fbf_afbeeldingid=afbeeldingen.afbeeldingid WHERE fotoboeken_fotos.fbf_fotoboekid=". $GLOBALS['fotoboekid'] . " ORDER BY fotoboeken_fotos.fbf_volgorde, fotoboekfotoid;";
	$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
	$row_rs = mysql_fetch_assoc($rs);
	$AantFotos = mysql_num_rows($rs);
	if (mysql_num_rows($rs)>0) {
		$ftel = 0;
		?>
		<br /><br />
		<table width="96%" cellpadding="0" cellspacing="0" align="center">
		<?
		do
		{
			if ($ftel % 3==0) {
				echo "<tr valign=\"top\">";
			}
			?>
			<td width="33%" align="center" style="font-size: 9px; color: #444444;"><a href="kies_fotoboekfoto-edit.php?fotoboekid=<?=$GLOBALS['fotoboekid']?>&openerfieldtoupdate=<?=$openerfieldtoupdate?>&fotoid=<?=$row_rs['fotoboekfotoid']?>"><img border="0" style="border: solid 1px black;" id="tn<?=$row_rs['fotoboekfotoid']?>" src="<?=GeefAfbeeldingSrc($row_rs['afbeeldingid'])?>" width="160" alt="Wijzigen" /></a><br /><?=$row_rs['fbf_onderschrift']?>&nbsp;<?=PlaatsPrullenbakLink("", "kies_fotoboekfoto-del.php?fotoboekid=" . $GLOBALS['fotoboekid'] . "&openerfieldtoupdate=" . $openerfieldtoupdate . "&fotoid=" . $row_rs['fotoboekfotoid'], "")?><br /><br /></td>
			<?
			$ftel = $ftel + 1;
			if ($ftel % 3==0) {
				echo "</tr>";
			}
		}
		while ($row_rs = mysql_fetch_assoc($rs));
		?>
		</table>
		<?
	}
	mysql_free_result($rs);

?>

</td></tr>
<?=SluitCMSTabel(); ?>

<?
}
?>

<br /><br />


<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
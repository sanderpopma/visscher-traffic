<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
?>
<?
OpenPagina("CMS", "");
?>

<?=OpenCMSTabel("Vragen - categorieŽn")?>
<?=OpenCMSNavBalk()?>
    <?=ToonCMSNavKnop("toevoegen", "Nieuwe categorie", "cmsfaqcat-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk()?>


<tr class="kadervoet">
    <td width="30"><b></b></td>
    <td width="30"><b>Taal</b></td>
    <td><b>Categorie</b></td>
    <td width="100"><center><b>Volgorde</b></td>
    <td width="100"><center><b>Zichtbaar</b></td>
    <td width="100"><b>Verwijder</b></td>
</tr>

<?
$query_rs = "SELECT * FROM faq_categorieen ORDER BY faqcat_taalcode, faqcat_volgorde;";
$rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
$row_rs = mysql_fetch_assoc($rs);
if (mysql_num_rows($rs)>0) {
	do
	{
    	$MagVerwijderen = true;
	    $WijzigURL = "cmsfaqcat-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["faqcatid"];
	    $VerwijderURL = "cmsfaqcat-del.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&itemid=" .$row_rs["faqcatid"];
	    $VerwijderMelding = "Weet u zeker dat u deze categorie wilt verwijderen? (Dit kan niet ongedaan worden gemaakt!)";
	    if (TelRecords("SELECT * FROM faq WHERE faq_faqcatid=" . $row_rs['faqcatid'])>0) { $MagVerwijderen = false; }
		?>
			<tr class="regel">
			    <td><?=PlaatsIcoonLink("wijzigen", "", $WijzigURL, "Wijzigen")?></td>
			    <td><?=Icoon($row_rs["faqcat_taalcode"])?></td>
			    <td><a href="<?=$WijzigURL?>"><?=$row_rs["faqcat_naam"]?></a></td>
			    <td><center><a href="<?=$WijzigURL?>"><?=$row_rs["faqcat_volgorde"]?></a></td>
			    <td><center><?=ToonGereed($row_rs["publiceren"])?></td>
				<td>
					<? if ($MagVerwijderen==true) {
						?>
						<?=PlaatsVerwijderLink("Verwijder", $VerwijderURL, $VerwijderMelding)?>
						<?
					}
					?>
				</td>
			</tr>
		<?
	}
	while ($row_rs = mysql_fetch_assoc($rs));
}
mysql_free_result($rs);
?>

<?
SluitCMSTabel();
?>

<?
SluitPagina();
?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
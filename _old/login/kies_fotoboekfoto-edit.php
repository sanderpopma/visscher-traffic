<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
$openerfieldtoupdate = $_GET["openerfieldtoupdate"];

$afbSize = "HEEL";
$FrmTitel = "Afbeelding toevoegen aan fotoboek";
$BtnText = "Uploaden";

$afbFormaat = $GLOBALS['AfbeeldingMaxHeel'];
$afbFormaatTonen = $GLOBALS['AfbeeldingMaxHeelTonen'];
$kiesURL = "kies_fotoboekfoto2.php";

if ($GLOBALS['fotoid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}

if ($PgMode=="WIJZIG")
{
	$FrmTitel = "Afbeelding wijzigen";
	$BtnText = "Opslaan";

    $query_rs = "SELECT * FROM fotoboeken_fotos WHERE fbf_fotoboekid=".$GLOBALS['fotoboekid']." AND fotoboekfotoid=" . $GLOBALS['fotoid'];
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['fotoboekfotoid']>0)
    {
    	$fbf_onderschrift = $row_rs['fbf_onderschrift'];
    	$fbf_afbeeldingid = $row_rs['fbf_afbeeldingid'];
    }
	else
	{
		die;
	}
    mysql_free_result($rs);
}
else
{
	$fbf_onderschrift = "";
	$fbf_afbeeldingid = -1;
}
?>
<?=OpenPagina("CMS", "")?>

<form method="post" action="kies_fotoboekfoto-edit2.php?openerfieldtoupdate=<?=$openerfieldtoupdate?>&fotoboekid=<?=$GLOBALS['fotoboekid']?>&fotoid=<?=$GLOBALS['fotoid']?>" name="afbeelding" enctype="multipart/form-data">
<?=OpenCMSTabel($FrmTitel)?>
<?=OpenCMSNavBalk()?>
	<? if ($PgMode=="TOEVOEG") {
		?>
	    <?=ToonCMSNavKnop("folder_importeren", "Uploaden", "javascript:VerstuurKnop.click();") ?>
	    <?=ToonCMSNavKnop("stop", "Terug naar fotoboek", "kies_fotoboek-edit.php" . "?openerfieldtoupdate=" . $openerfieldtoupdate . "&fotoboekid=".$GLOBALS['fotoboekid']) ?>
		<?
	}
	else
	{
		?>
	    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
	    <?=ToonCMSNavKnop("stop", "Terug naar fotoboek", "kies_fotoboek-edit.php" . "?openerfieldtoupdate=" . $openerfieldtoupdate . "&fotoboekid=".$GLOBALS['fotoboekid']) ?>
		<?
	}
	?>
<?=SluitCMSNavBalk()?>

<? if ($PgMode=="TOEVOEG") {
	?>
<?=FrmHidden("MAX_FILE_SIZE", $GLOBALS['MaxBestandsGrootte'])?>
<? if ($_GET['melding']=="ja") {
	?>
	<tr><td colspan="10"><br /></td></tr>
	<?=FrmKoptekst("<span style='color: black';'>Afbeelding is toegevoegd. U kunt nu een nieuwe afbeelding toevoegen");?>
	<tr><td colspan="10"><br /><br />
	</td></tr>
	<?
}
?>
<?=FrmKoptekst("<span style='color: red';'>Let op: de afbeelding wordt automatisch verkleind naar ". $afbFormaat . "px en afgebeeld op ". $afbFormaatTonen . "px");?>
<tr>
	<td>Kies afbeelding</td>
	<td>:</td>
	<td><input class="Veld" type="file" name="afb_url" size="40"></td>
</tr>
<tr>
	<td colspan="2"></td><td>Let op: alleen JPG of GIF bestanden kunnen worden gebruikt<br /><br /></td>
</tr>
<?
}
else
{
	//wijzig
?>
	<tr><td></td><td></td><td><br /><img src="<?=GeefAfbeeldingSrc($fbf_afbeeldingid)?>" border="1" width="160" /></td></tr>
<?
}
?>
    <?=FrmTextArea("Onderschrift", "fbf_onderschrift", $fbf_onderschrift, 5, 60) ?>

    <?=FrmSubmit($BtnText)?>

<?=SluitCMSTabel()?>
</form>

<? if ($PgMode=="TOEVOEG") {
	?>

<script type="text/javascript">
var frmvalidator  = new Validator("afbeelding");
frmvalidator.addValidation("afb_url","req","Kies een bestand");
</script>
<?
}
?>

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>
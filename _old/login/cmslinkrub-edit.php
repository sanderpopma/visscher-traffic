<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludes.php");
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/openincludesadmin.php");
?>
<?
// Inlezen en verwerken paginaparameters
//if (!HeeftRechten(GeefHuidigeUserId(), "LINKS")) { die; }
if ($GLOBALS['linkrubid']<1)
{
    $PgMode = "TOEVOEG";
}
else
{
    $PgMode = "WIJZIG";
}
$rubvolgorde=0;
$HeeftLinks = false;
if ($PgMode=="WIJZIG")
{
    $query_rs = "SELECT * FROM links_rubrieken WHERE rubriekid=".$GLOBALS['linkrubid']."";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['rubriekid']>0)
    {
        $rubnaam_nl = $row_rs['rubnaam_nl'];
        $rubvolgorde = $row_rs['rubvolgorde'];
        $rubbeschrijving_nl = $row_rs['rubbeschrijving_nl'];
        $rubpubliceren = Int2Bool($row_rs['rubpubliceren']);
    }
    mysql_free_result($rs);
    $HeeftLinks = (toInt(TelRecords("SELECT * FROM links WHERE link_rubriekid=".$GLOBALS['linkrubid'].""))>0);
}
else
{
    $query_rs = "SELECT MAX(rubvolgorde) as maxnr FROM links_rubrieken";
    $rs = mysql_query($query_rs, $GLOBALS['conn']) or die(mysql_error());
    $row_rs = mysql_fetch_assoc($rs);
    if ($row_rs['maxnr']>0)
    {
        $rubvolgorde = toInt($row_rs['maxnr']);
        if ($rubvolgorde<0) { $rubvolgorde=0;}
	}
	mysql_free_result($rs);
    $rubvolgorde=$rubvolgorde+1;
    $rubpubliceren = false;
}
?>
<?=OpenPagina("CMS", "")?>

<?=OpenCMSTabel("Link Rubriek " . $PgMode . "en"); ?>
<?=OpenCMSNavBalk(); ?>
    <? if ($PgMode=="WIJZIG") { ?>
    	<?=ToonCMSNavKnop("toevoegen", "Nieuwe link", "cmslinks-edit.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=".$GLOBALS['linkrubid']) ?>
	    <? if ($HeeftLinks==true) { ?>
	        <?=ToonCMSNavKnop("documenten", "Toon links", "cmslinks.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=".$GLOBALS['linkrubid']) ?>
	    <? } ?>
    <? } ?>
    <?=ToonCMSNavKnop("opslaan", "Opslaan", "javascript:VerstuurKnop.click();") ?>
    <?=ToonCMSNavKnop("stop", "Terug naar overzicht", "cmslinkrub.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid']) ?>
<?=SluitCMSNavBalk();?>

<?=OpenForm("cmslinkrub-edit2.php?hmid=" . $GLOBALS['hmid'] . "&smid=" . $GLOBALS['smid'] . "&linkrubid=" . $GLOBALS['linkrubid'], "linkrubriek", 700)?>
    <?=FrmText("Rubriek naam" . Icoon("NL"), "rubnaam_nl", $rubnaam_nl, 60, 100) ?>
    <?=FrmText("Afbeeldvolgorde", "rubvolgorde", $rubvolgorde, 4, 4) ?>
    <?=FrmCheckbox("Publiceren", "rubpubliceren", $rubpubliceren) ?>

    <?=FrmKopregel("Beschrijving") ?>
    <?=FrmFCKText("Beschrijving" . Icoon("NL"), "rubbeschrijving_nl", $rubbeschrijving_nl, 200) ?>

    <?=FrmSubmit("Opslaan")?>

<?=SluitForm()?>

<?=ZetFocus("rubnaam_nl") ?>

<SCRIPT language="JavaScript">
var frmvalidator  = new Validator("linkrubriek");
frmvalidator.addValidation("rubnaam_nl","req","Naam is verplicht");
frmvalidator.addValidation("rubvolgorde","req","Volgorde is verplicht");
frmvalidator.addValidation("rubvolgorde","numeric","Volgorde is ongeldig");
</script>

<?=SluitCMSTabel(); ?>

<br /><br />

<?=SluitPagina();?>

<?
include($_SERVER['DOCUMENT_ROOT']."/sybit/includes/closeincludes.php");
?>